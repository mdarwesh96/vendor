import React, { useEffect, useState } from 'react';

import {
  View,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import { persistor, store } from './src/common/Store';
// import store from './Store';
import { createStore, applyMiddleware } from 'redux';
import { PersistGate } from 'redux-persist/lib/integration/react';

import { Provider } from 'react-redux';

import reducers from './src/reducers/reducers';
import AppNavigation from './src/AppNavigation';
import AppNavigationDefault from './src/AppNavigationDefault';
import NetworkScreenProblem from './src/NetworkScreenProblem';
import NetInfo from '@react-native-community/netinfo';
import LoginScreen from './src/screens/Login/LoginScreen';
import { YellowBox } from 'react-native';
import NotifService from './NotifService';
import PushNotification from 'react-native-push-notification';
import NotificationHandler from './NotificationHandler';
import messaging from '@react-native-firebase/messaging';

import thunk from 'redux-thunk';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import FlashMessage from "react-native-flash-message";

// I18nManager.forceRTL(true);

import splash from './src/screens/Splash';
import LoadingScreen from './src/screens/LoadingScreen';
global.backgroundColor = '#c7a84c';
YellowBox.ignoreWarnings(['Warning: Each', 'Warning: Failed']);
console.disableYellowBox = true;
const InitialNavigator = createSwitchNavigator(
  {
    Splash: splash,
    AppNavigation: AppNavigation,
    AppNavigationDefault: AppNavigationDefault,
    LoginScreen: LoginScreen,
    LoadingScreen: LoadingScreen,
  },
  {
    initialRouteName: 'Splash',
  }
);

const NetworkNavigator = createSwitchNavigator(
  {
    NetworkScreenProblem: NetworkScreenProblem,
  },
  {
    initialRouteName: 'NetworkScreenProblem',
  }
);
const AppContainer = createAppContainer(InitialNavigator);
const NetworkContainer = createAppContainer(NetworkNavigator);

let showRealApp = false;

export default function App() {
  const [showRealApp, setshowRealApp] = useState(false);

  const [lastId, setlastId] = useState(0);
  const [fcmRegistered, setfcmRegistered] = useState('');
  const [networkState, setnetworkState] = useState(true);
  const [lastChannelCounter, setlastChannelCounter] = useState(0);
  const [registerToken, setregisterToken] = useState(false);

  const onRegister = async (token) => {
    setregisterToken(token.token);
  };

  const onNotif = (notif) => {
    if (Platform.OS === 'ios') {
      // alert(notif.title, notif.message);
    } else {
      if (notif.foreground === true && notif.userInteraction === false) {
        localNotif('sample.mp3', notif.title, notif.message);
      }
    }
  };
  const handlePerm = (perms) => {
    Alert.alert('Permissions', JSON.stringify(perms));
  };

  const createDefaultChannels = () => {
    PushNotification.createChannel(
      {
        channelId: 'default-channel-id', // (required)
        channelName: `Default channel`, // (required)
        channelDescription: 'A default channel', // (optional) default: undefined.
        soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
        importance: 4, // (optional) default: 4. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
      },
      (created) =>
        console.log(`createChannel 'default-channel-id' returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
    PushNotification.createChannel(
      {
        channelId: 'sound-channel-id', // (required)
        channelName: `Sound channel`, // (required)
        channelDescription: 'A sound channel', // (optional) default: undefined.
        soundName: 'sample.mp3', // (optional) See `soundName` parameter of `localNotification` function
        importance: 4, // (optional) default: 4. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
      },
      (created) =>
        console.log(`createChannel 'sound-channel-id' returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
  };

  const createOrUpdateChannel = () => {
    setlastChannelCounter(lastChannelCounter + 1);
    PushNotification.createChannel(
      {
        channelId: 'custom-channel-id', // (required)
        channelName: `Custom channel - Counter: ${lastChannelCounter}`, // (required)
        channelDescription: `A custom channel to categorise your custom notifications. Updated at: ${Date.now()}`, // (optional) default: undefined.
        soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
        importance: 4, // (optional) default: 4. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
      },
      (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
  };

  const popInitialNotification = () => {
    PushNotification.popInitialNotification((notification) =>
      console.log('InitialNotication:', notification)
    );
  };

  const checkPermission = (cbk) => {
    return PushNotification.checkPermissions(cbk);
  };

  const requestPermissions = () => {
    return PushNotification.requestPermissions();
  };

  const cancelNotif = () => {
    PushNotification.cancelLocalNotifications({ id: '' + this.lastId });
  };

  const cancelAll = () => {
    PushNotification.cancelAllLocalNotifications();
  };
  const loadApp = async (isInternetReachable) => {
    try {
      setnetworkState(isInternetReachable.isConnected);
      // alert( );
    } catch (e) {}
  };
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((isInternetReachable) => {
      loadApp(isInternetReachable);

      StatusBar.setBarStyle('light-content', true);

      createDefaultChannels();

      NotificationHandler.attachRegister(onRegister);
      NotificationHandler.attachNotification(onNotif);

      // Clear badge number at start
      PushNotification.getApplicationIconBadgeNumber(function (number) {
        if (number > 0) {
          PushNotification.setApplicationIconBadgeNumber(0);
        }
      });

      PushNotification.getChannels(function (channels) {
        console.log(channels);
      });

      messaging().onMessage(async (remoteMessage) => {
        const owner = JSON.parse(remoteMessage.data.owner);
        const user = JSON.parse(remoteMessage.data.user);
        const picture = JSON.parse(remoteMessage.data.picture);

        console.log(
          `The user "${user.name}" liked your picture "${picture.name}"`
        );
      });

      messaging().setBackgroundMessageHandler(async (remoteMessage) => {
        // Update a users messages list using AsyncStorage
        const currentMessages = await AsyncStorage.getItem('messages');
        const messageArray = JSON.parse(currentMessages);
        messageArray.push(remoteMessage.data);
        await AsyncStorage.setItem('messages', JSON.stringify(messageArray));
      });
      // Assume a message-notification contains a "type" property in the data payload of the screen to open

      messaging().onNotificationOpenedApp((remoteMessage) => {
        console.log(
          'Notification caused app to open from background state:',
          remoteMessage.notification
        );
        cancelNotif();
        navigation.navigate(remoteMessage.data.type);
      });

      // Check whether an initial notification is available
      messaging()
        .getInitialNotification()
        .then((remoteMessage) => {
          if (remoteMessage) {
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage.notification
            );
            setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
          }
          setLoading(false);
        });
    });
    return () => {
      unsubscribe;
    };
  }, []);



  const localNotif = (soundName, title, msg) => {
    setlastId(lastId + 1);
    PushNotification.localNotification({
      /* Android Only Properties */
      channelId: soundName ? 'sound-channel-id' : 'default-channel-id',
      ticker: 'My Notification Ticker', // (optional)
      autoCancel: true, // (optional) default: true
      largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
      smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
      bigText: msg, // (optional) default: "message" prop
      subText: '', // (optional) default: none
      color: 'red', // (optional) default: system default
      vibrate: true, // (optional) default: true
      vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
      tag: 'some_tag', // (optional) add tag to message
      group: 'group', // (optional) add group to message
      groupSummary: false, // (optional) set this notification to be the group summary for a group of notifications, default: false
      ongoing: false, // (optional) set whether this is an "ongoing" notification
      invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

      when: null, // (optionnal) Add a timestamp pertaining to the notification (usually the time the event occurred). For apps targeting Build.VERSION_CODES.N and above, this time is not shown anymore by default and must be opted into by using `showWhen`, default: null.
      usesChronometer: false, // (optional) Show the `when` field as a stopwatch. Instead of presenting `when` as a timestamp, the notification will show an automatically updating display of the minutes and seconds since when. Useful when showing an elapsed time (like an ongoing phone call), default: false.
      timeoutAfter: null, // (optional) Specifies a duration in milliseconds after which this notification should be canceled, if it is not already canceled, default: null

      /* iOS only properties */
      alertAction: 'view', // (optional) default: view
      category: '', // (optional) default: empty string

      /* iOS and Android properties */
      id: lastId, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
      title: title, // (optional)
      message: msg, // (required)
      userInfo: { screen: 'home' }, // (optional) default: {} (using null throws a JSON value '<null>' error)
      playSound: !!soundName, // (optional) default: true
      soundName: soundName ? soundName : 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
      number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
    });
  };

  _renderItem = ({ item }) => {
    return (
      <View style={styles.slide}>
        <Image
          resizeMode='cover'
          style={{ width: '100%', height: '100%' }}
          source={item.image}
        />
      </View>
    );
  };

 

  return (
    <Provider store={createStore(reducers, {}, applyMiddleware(thunk))}>
      <PersistGate loading={<View />} persistor={persistor}>
        {networkState !== false && <AppContainer></AppContainer>}
        {networkState === false && <NetworkContainer />}
        <FlashMessage position="bottom" /> 

      </PersistGate>
    </Provider>
  );
}

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue',
  },
  image: {
    width: 320,
    height: 320,
    marginVertical: 32,
  },
  text: {
    color: 'rgba(255, 255, 255, 0.8)',
    textAlign: 'center',
  },
  title: {
    fontSize: 22,
    color: 'white',
    textAlign: 'center',
  },
});
