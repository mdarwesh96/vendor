import {combineReducers} from 'redux';

import {
  GET_VENDOR_ORDERS_SUCCESS,GET_VENDOR_ORDERS_FAIL,UPDATE_ORDER_FAIL,UPDATE_ORDER_SUCCESS,GET_VENDOR_ORDERS_Attempt,FORGET_FIRST_SUCCESS,FORGET_FIRST_FAIL,FORGET_FIRST_ATTEMPT,FORGET_SECOND_ATTEMPT,FORGET_SECOND_SUCCESS,FORGET_SECOND_FAIL
  ,ACCEPT_ORDER_FAIL,ACCEPT_ORDER_SUCCESS,ACCEPT_ORDER_ATEMPT,COUNTRIES_FAIL,COUNTRIES_SUCCESS,ADD_PRODUCT_ATTEMPT
  ,UPDATE_VENDOR_ORDERS_SUCCESS,UPDATE_VENDOR_ORDERS_FAIL,UPDATE_VENDOR_ORDERS_ATTEMPT,UPADTE_CURRENCY,UPADTE_AVAILABILITY,
  GET_VERSION_FAIL,GET_VERSION_Attempt,GET_VERSION_SUCCESS,
  LOGOUT_USER_ATTEMPT,LOGOUT_USER_SUCCESS,LOGOUT_USER_FAILL,
  REGISTER_SUCCESS,REGISTER_FAILED,REGISTER_ATTEMPT,LOGIN_USER_ATTEMPT,EDIT_PROFILE_AATEMPT,EDIT_PROFILE_FAIL,EDIT_PROFILE_SUCCESS,
  UPDATE_VENDOR_STATE_SUCCESS,UPDATE_VENDOR_STATE_FAIL,UPDATE_VENDOR_STATE_ATTEMPT,GET_PAYMENT_FAIL,GET_PAYMENT_SUCCESS,
    GET_CATEGORIES_SUCCESS,GET_CATEGORIES_FAIL,LOGIN_USER_FAIL,LOGIN_USER_SUCCESS,GET_VENDOR_DETAILS_FAIL,GET_VENDOR_DETAILS_SUCCESS,
    GET_LABELS_SUCCESS,GET_LABELS_FAIL,GET_CURRENCIES_SUCCESS,GET_CURRENCIES_FAUL,DELETE_PRODUCT_SUCCESS,DELETE_PRODUCT_FAUL,GET_VENDOR_DETAILS_ATTEMPT,
    ADD_PRODUCT_FAIL,ADD_PRODUCT_SUCCESS,EDIT_PRODUCT_FAIL,EDIT_PRODUCT_SUCCESS} from '../actions/types';
  
  const INITIAL_STATE = {error: ''};
    const GetCategoriesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case GET_CATEGORIES_FAIL:
      {

        return {error: action.error};
      }
      case GET_CATEGORIES_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const GetLabelsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case GET_LABELS_FAIL:
      {
      
        return {error: action.error};
      }
      case GET_LABELS_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };
  const GetCountriesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case COUNTRIES_FAIL:
      {
      
        return {error: action.error};
      }
      case COUNTRIES_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  
  const LoginReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case LOGIN_USER_ATTEMPT:
      {
        return {error: ''};
      }

      case LOGIN_USER_FAIL:
      {

        return {error: action.error};
      }
      case LOGIN_USER_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const VendorDetailsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case GET_VENDOR_DETAILS_ATTEMPT:
      {
      
        return {error: ''};
      }
      case GET_VENDOR_DETAILS_FAIL:
      {
        return {error: action.error};
      }
      case GET_VENDOR_DETAILS_SUCCESS: {

  
        return {error: 'success',payload:action.payload,prices:action.payload.convert_price,rates:action.payload.rate_item,ratecount:action.payload.count_rate,ratevendor:action.payload.rate};
      }
      default:
        return state;
    }
  };

  const GetCurrenciesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case GET_CURRENCIES_FAUL:
      {

        return {error: action.error};
      }
      case GET_CURRENCIES_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };


  const DeleteItemReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case DELETE_PRODUCT_FAUL:
      {

 
        return {error: action.error};
      }
      case DELETE_PRODUCT_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };


  const UpdateItemReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case EDIT_PRODUCT_FAIL:
      {
 
        return {error: action.error};
      }
      case EDIT_PRODUCT_SUCCESS: {
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  
  const AddItemReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ADD_PRODUCT_ATTEMPT:
      {


        return {error: ''};
      }
    
      case ADD_PRODUCT_FAIL:
      {


        return {error: action.error};
      }
      case ADD_PRODUCT_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };
  const RegisterReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case REGISTER_ATTEMPT:
    {
      
      return {error: ''};

    }
      case REGISTER_FAILED:
      {

        return {error: action.error};
      }
      case REGISTER_SUCCESS: {
 
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };
  const EditProfileReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case EDIT_PROFILE_AATEMPT:
    {
      
      return {error: ''};

    }
      case EDIT_PROFILE_FAIL:
      {

        return {error: action.error};
      }
      case EDIT_PROFILE_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };


  const UpdateVendorReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case UPDATE_VENDOR_STATE_ATTEMPT:
    {
      
      return {error: ''};

    }
      case UPDATE_VENDOR_STATE_FAIL:
      {

        return {error: action.error};
      }
      case UPDATE_VENDOR_STATE_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const GetPaymentReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
 
      case GET_PAYMENT_FAIL:
      {

        return {error: action.error};
      }
      case GET_PAYMENT_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  
  const GetOrdersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
 case GET_VENDOR_ORDERS_Attempt:
 {
  return {error:''}
 }
      case GET_VENDOR_ORDERS_FAIL:
      {

        return {error: action.error};
      }
      case GET_VENDOR_ORDERS_SUCCESS: {
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  
  const UpdateVendorOrdersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
 
      case UPDATE_VENDOR_ORDERS_ATTEMPT:
      {

        return {error: ''};
      }
      case UPDATE_VENDOR_ORDERS_FAIL:
      {
        
        return {error: action.error};
      }
      case UPDATE_VENDOR_ORDERS_SUCCESS: {
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };


  const AcceptOrderReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

      case ACCEPT_ORDER_FAIL:
      {

        return {error: action.error};
      }
 
      case ACCEPT_ORDER_ATEMPT:
      {

        return {error: ''};
      }
      case ACCEPT_ORDER_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const UpdateCurrencyReducer=(state = INITIAL_STATE, action) => {
    switch (action.type) {

      case UPADTE_CURRENCY:
      {
        return {currency:action.currency,currencyid:action.currencyid};

      }
      default:
      return state;
    }
  };

  const UpdateAvailabilityReducer=(state = INITIAL_STATE, action) => {
    switch (action.type) {

      case UPADTE_AVAILABILITY:
      {
        
        return {availableindex:action.availableindex};

      }
      default:
      return state;
    }
  };
  
  const ForgetPasswordSecondReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case FORGET_SECOND_ATTEMPT:
      {
        return {error: ''};

      }
    
      case FORGET_SECOND_FAIL:
      {

        return {error: action.error};
      }
      case FORGET_SECOND_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const LogoutReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
       
      case LOGOUT_USER_ATTEMPT:
      {
     
        return {error:""};
      }
    
      case LOGOUT_USER_FAILL:
      {
     
        return {error: action.error};
      }
      case LOGOUT_USER_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
   
  
        return state;
    }
  };
  const ForgetPasswordFirstReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case FORGET_FIRST_ATTEMPT:
      {
        return {error: ''};

      }
    
      case FORGET_FIRST_FAIL:
      {

        return {error: action.error};
      }
      case FORGET_FIRST_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const GetVersionReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case GET_VERSION_Attempt:
      {
        return {error: ''};

      }
    
      case GET_VERSION_FAIL:
      {

        return {error: action.error};
      }
      case GET_VERSION_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };


  export default combineReducers({
    GetCategoriesReducer:GetCategoriesReducer,
    LoginReducer:LoginReducer,
    VendorDetailsReducer:VendorDetailsReducer,
    GetLabelsReducer:GetLabelsReducer,
    GetCurrenciesReducer:GetCurrenciesReducer,
    DeleteItemReducer:DeleteItemReducer,
    UpdateItemReducer:UpdateItemReducer,
    AddItemReducer:AddItemReducer,
    RegisterReducer:RegisterReducer,
    EditProfileReducer:EditProfileReducer,
    UpdateVendorReducer:UpdateVendorReducer,
    GetPaymentReducer:GetPaymentReducer,
    GetOrdersReducer:GetOrdersReducer,
    UpdateVendorOrdersReducer:UpdateVendorOrdersReducer,
    AcceptOrderReducer:AcceptOrderReducer,
    UpdateCurrencyReducer:UpdateCurrencyReducer,
    UpdateAvailabilityReducer:UpdateAvailabilityReducer,
    GetCountriesReducer:GetCountriesReducer,
    ForgetPasswordFirstReducer:ForgetPasswordFirstReducer,
    ForgetPasswordSecondReducer:ForgetPasswordSecondReducer,
    GetVersionReducer:GetVersionReducer,
    LogoutReducer:LogoutReducer
});
