import React, { useEffect, useState } from 'react';

import {
  View,
  StyleSheet,
  ImageBackground,
  Text,
  ScrollView,
  Linking,
  Dimensions,
  Platform,
  Image,
  TouchableHighlight,
  NativeModules,
} from 'react-native';
import Iconh from 'react-native-vector-icons/Ionicons';
import { Icon } from 'react-native-elements';

import { drawer } from '../../AppNavigation';
import { drawerD } from '../../AppNavigationDefault';

const DEVICE_width = Dimensions.get('window').width;
const DEVICE_hight = Dimensions.get('window').height;
let bgg = require('../../imgs/profilebg.png');

const MARGIN = 60;
let language = 2;
let deviceLocale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;

deviceLocale = 'ar';

export default function AboutScreen(props) {
  const [terms, setterms] = useState(false);
  const [faqshown, setfaqshown] = useState(false);
  useEffect(() => {
    // loadApp();
  }, []);

  return (
    <View style={{ height: '100%', width: '100%' }}>
      <View
        style={{
          flexDirection: 'column',
          height: DEVICE_hight,

          justifyContent: 'space-between',
        }}>
        <View
          style={{
            height: '100%',
            width: '100%',
            flexDirection: 'column',
          }}
          backgroundColor={'white'}>
          <ImageBackground
            style={{
              width: '100%',
              height: 150,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
            }}
            resizeMode='stretch'
            source={require('../../imgs/transheader.png')}>
            <View
              style={{
                width: '100%',
                marginTop: 70,
                paddingHorizontal: 30,
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
              }}>
                         <TouchableHighlight   underlayColor='transparent'
                     onPress={() => {  try {
                      drawer.current.open();
                    } catch (ff) {
                      drawerD.current.open();
                    }}}
                activeOpacity={0.2}>
                <Image
                 source={require('../../imgs/menueic.png')}
              style={{width:25,height:20,tintColor:'white'}}
              resizeMode='contain'
                />
                </TouchableHighlight>

     <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',alignItems:'flex-start',height:20}}
              resizeMode='stretch'
                /> 
              <Icon size={30} color='white' />
            </View>
          </ImageBackground>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ flexDirection: 'column' }}>
              <View
                style={{
                  width: '100%',

                  marginTop: 20,
                }}>
                <Text
                  style={{
                    fontSize: 18,
                    color: '#0A2C6B',
                    marginHorizontal: '10%',
                    width: '80%',
                    marginVertical: 10,
                    textAlign: 'center',
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                  }}>
           تقدم شركة وديلي بدولة الإمارات خدمات تطبيق "وديلي" الذي ينظم عمليات شراء الطعام والهدايا من البائعين المسجلين بالتطبيق وتوصيلها إلى الأشخاص المحددين داخل المدن الإماراتية
                </Text>
              </View>

              {(Platform.OS === 'ios' ||
                (Platform.OS !== 'ios' && deviceLocale === 'en')) && (
                <View
                  style={{
                    width: '100%',
                    marginTop: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableHighlight
                    underlayColor='transparent'
                    activeOpacity={0.2}
                    onPress={() => {
                      if (faqshown === false) {
                        setterms(false);
                      }
                      setfaqshown(!faqshown);
                    }}
                    style={{ width: '100%' }}>
                    <View
                      style={{
                        flexDirection:
                          deviceLocale !== 'ar' ? 'row' : 'row-reverse',

                        width: '100%',
                      }}>
                      <View
                        style={{
                          flex: 0.95,
                          flexDirection:
                            deviceLocale !== 'ar' ? 'row' : 'row-reverse',
                        }}>
                        <View
                          style={{
                            width: '100%',
                            marginLeft: 20,
                            flex: 1,
                            marginVertical: 5,

                            flexDirection:
                              deviceLocale !== 'ar' ? 'row' : 'row-reverse',
                            justifyContent: 'flex-start',
                            alignContent: 'flex-start',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: '#0A2C6B',
                              fontSize: 15,

                              fontWeight: '300',
                              marginRight: 10,
                            }}>
أسئلة و أجوبة
                          </Text>
                        </View>
                      </View>
                      <View
                        style={{
                          paddingBottom: 8,
                          paddingTop: 8,
                          flex: 0.05,
                          marginVertical: 5,
                          marginLeft: 10,
                        }}>
                        <Icon
                          name='chevron-left'
                          type='entypo'
                          color='#0A2C6B'
                          style={{
                            transform:
                              deviceLocale !== 'ar'
                                ? [{ rotateY: '180deg' }]
                                : [{ rotateY: '0deg' }],
                          }}
                          containerStyle={{
                            marginLeft: deviceLocale !== 'ar' ? -10 : 0,
                            marginRight: deviceLocale === 'ar' ? -10 : 0,
                            width: 20,
                          }}
                        />
                      </View>
                    </View>
                  </TouchableHighlight>
                </View>
              )}
              {Platform.OS !== 'ios' && deviceLocale === 'ar' && (
                <View
                  style={{
                    width: '100%',
                    marginTop: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <TouchableHighlight
                    underlayColor='transparent'
                    activeOpacity={0.2}
                    onPress={() => {
                      if (faqshown === false) {
                        setterms(false);
                      }
                      setfaqshown(!faqshown);
                    }}
                    style={{ width: '100%' }}>
                    <View
                      style={{
                        flexDirection: 'row',

                        width: '100%',
                      }}>
                      <View style={{ flex: 0.95, flexDirection: 'row' }}>
                        <View
                          style={{
                            width: '100%',
                            marginLeft: 20,
                            marginVertical: 5,
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignContent: 'flex-start',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              color: '#0A2C6B',
                              fontSize: 15,

                              fontWeight: '300',
                              marginRight: 10,
                            }}>
                            أسئلة و أجوبة

                          </Text>
                        </View>
                      </View>

                      <View
                        style={{
                          paddingBottom: 8,
                          paddingTop: 8,
                          flex: 0.05,
                          marginVertical: 5,
                          marginLeft: 10,
                          marginRight: 10,
                        }}>
                        <Icon
                          name='chevron-left'
                          type='entypo'
                          color='#0A2C6B'
                          style={{
                            transform:
                              deviceLocale !== 'ar'
                                ? [{ rotateY: '180deg' }]
                                : [{ rotateY: '0deg' }],
                          }}
                          containerStyle={{
                            marginLeft: 0,
                            marginRight: -10,
                            width: 20,
                          }}
                        />
                      </View>
                    </View>
                  </TouchableHighlight>
                </View>
              )}

              <View
                style={{
                  width: '100%',
                  backgroundColor: '#CCCCCC',
                  height: StyleSheet.hairlineWidth,
                }}
              />
              {faqshown === true && (
                <View style={{ width: '100%', flexDirection: 'column' }}>
                  <View
                    style={{
                      backgroundColor: 'grey',
                      width: '100%',
                      flexDirection: 'row-reverse',

                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        width: DEVICE_width - 80,
                        marginHorizontal: 40,
                        color: 'black',
                        fontSize: 16,
                        textAlign: 'right',
                        marginVertical: 8,
                      }}>
                      question number one
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '100%',

                      backgroundColor: 'white',
                      flexDirection: 'row-reverse',
                      justifyContent: 'flex-start',
                      alignContent: 'flex-start',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: 'grey',
                        fontSize: 12,
                        width: DEVICE_width - 100,
                        marginHorizontal: 50,
                        marginVertical: 5,
                        textAlign: 'right',
                      }}>
                      response for question one response for question one
                      response for question one response for question one
                      response for question one
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: 'grey',
                      flexDirection: 'row-reverse',
                      justifyContent: 'flex-start',
                      alignContent: 'flex-start',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        width: DEVICE_width - 80,
                        marginHorizontal: 40,
                        color: 'black',
                        textAlign: 'right',
                        fontSize: 16,
                        marginVertical: 8,
                      }}>
                      question number one
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '100%',

                      backgroundColor: 'white',
                      flexDirection: 'row-reverse',
                      justifyContent: 'flex-start',
                      alignContent: 'flex-start',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: 'grey',
                        fontSize: 12,
                        width: DEVICE_width - 100,
                        marginHorizontal: 50,
                        marginVertical: 5,
                        textAlign: 'right',
                      }}>
                      response for question one response for question one
                      response for question one response for question one
                      response for question one
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: 'grey',
                      flexDirection: 'row-reverse',
                      justifyContent: 'flex-start',
                      alignContent: 'flex-start',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        width: DEVICE_width - 80,
                        marginHorizontal: 40,
                        color: 'black',
                        fontSize: 16,
                        textAlign: 'right',
                        marginVertical: 8,
                      }}>
                      question number one
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '100%',

                      backgroundColor: 'white',
                      flexDirection: 'row-reverse',
                      justifyContent: 'flex-start',
                      alignContent: 'flex-start',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: 'grey',
                        fontSize: 12,
                        width: DEVICE_width - 100,
                        marginHorizontal: 50,
                        marginVertical: 5,
                        textAlign: 'right',
                      }}>
                      response for question one response for question one
                      response for question one response for question one
                      response for question one
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: 'grey',
                      flexDirection: 'row-reverse',
                      justifyContent: 'flex-start',
                      alignContent: 'flex-start',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        width: DEVICE_width - 80,
                        marginHorizontal: 40,
                        color: 'black',
                        fontSize: 16,
                        marginVertical: 8,
                        textAlign: 'right',
                      }}>
                      question number one
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '100%',

                      backgroundColor: 'white',
                      flexDirection: 'row-reverse',
                      justifyContent: 'flex-start',
                      alignContent: 'flex-start',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        color: 'grey',
                        fontSize: 12,
                        width: DEVICE_width - 100,
                        marginHorizontal: 50,
                        marginVertical: 5,
                        textAlign: 'right',
                      }}>
                      response for question one response for question one
                      response for question one response for question one
                      response for question one
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '100%',
                      backgroundColor: '#CCCCCC',
                      height: StyleSheet.hairlineWidth,
                    }}
                  />
                </View>
              )}

            
           
              <View
                style={{
                  width: '100%',
                  backgroundColor: '#CCCCCC',
                  height: StyleSheet.hairlineWidth,
                }}
              />
          
      
              <View
                style={{
                  flexDirection: 'row',
                  width: '90%',
                  marginLeft: '5%',
                  marginRight: '5%',
                  marginTop: 40,
                }}>
                <TouchableHighlight
                  underlayColor='transparent'
                  activeOpacity={0.2}
                  style={{
                    margin: 10,
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}
                  onPress={() => {
                    Linking.openURL('https://www.google.com');
                  }}>
                  <Image
                    style={{ width: 50, height: 50 }}
                    source={require('../../imgs/facebook.png')}></Image>
                </TouchableHighlight>

                <TouchableHighlight
                  underlayColor='transparent'
                  activeOpacity={0.2}
                  style={{
                    margin: 10,
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}
                  onPress={() => {
                    Linking.openURL('https://www.google.com');
                  }}>
                  <Image
                    style={{ width: 50, height: 50 }}
                    source={require('../../imgs/twittre.png')}></Image>
                </TouchableHighlight>
                <TouchableHighlight
                  underlayColor='transparent'
                  activeOpacity={0.2}
                  style={{
                    margin: 10,
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}
                  onPress={() => {
                    Linking.openURL('https://www.google.com');
                  }}>
                  <Image
                    style={{ width: 50, height: 50 }}
                    source={require('../../imgs/youtube.png')}></Image>
                </TouchableHighlight>
                <TouchableHighlight
                  underlayColor='transparent'
                  activeOpacity={0.2}
                  style={{
                    margin: 10,
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}
                  onPress={() => {
                    Linking.openURL('https://www.google.com');
                  }}>
                  <Image
                    style={{ width: 50, height: 50 }}
                    source={require('../../imgs/instagram.png')}></Image>
                </TouchableHighlight>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 80,
    marginRight: 10,
    marginLeft: 10,
  },
});
