import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Button,
} from 'react-native';

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class PaymentItem extends Component {
  constructor() {
    super();

    this.state = {
      currentPosition: 1,
    };
  }
  renderLabel = ({ position, stepStatus, label, currentPosition }) => {
    return (
      <Text
        style={
          position === currentPosition
            ? styles.stepLabelSelected
            : styles.stepLabel
        }>
        {label}
      </Text>
    );
  };

  handleCheck = (checkedId) => {
    this.setState({ checkedId });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.item.expanded) {
      this.setState(() => {
        return {
          layout_Height: null,
        };
      });
    } else {
      this.setState(() => {
        return {
          layout_Height: 0,
        };
      });
    }
  }

  render() {
    let currentPosition = 1;
    if (this.props.item.state === 1) {
      currentPosition = 2;
    } else if (this.props.item.state === 2) {
      currentPosition = 3;
    } else {
      currentPosition = 4;
    }

    return (
      <View
        style={{
          marginTop: 5,
          borderWidth: 0.5,
          backgroundColor: 'white',
          borderRadius: 10,
          borderColor: '#dddddd',
          flexDirection: 'column',
          width: DEVICE_WIDTH - 40,
          marginLeft: 10,
          marginRight: 10,
          marbackgroundColor: 'white',
          marginBottom: 10,
        }}>
        {this.props.data.map((item, key) => (
          <View style={{ flexDirection: 'column' }}>
            <View
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#0599ea',
                width: '100%',
                borderTopRightRadius: 10,
                borderTopLeftRadius: 10,
              }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: 'bold',
                  padding: 10,
                  color: 'white',
                }}>
                {item.month}
              </Text>
            </View>
            <View
              style={{
                width: '100%',
                flexDirection: 'row-reverse',
                marginBottom: 10,
                marginTop: 10,
              }}>
              <View
                style={{
                  flexDirection: 'row-reverse',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 13,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {' '}
                    المجموع{' '}
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {parseFloat(item.allprice).toFixed(3)}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row-reverse',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: 'red',
                      fontSize: 13,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {' '}
                    الباقي من الحساب
                  </Text>
                  <Text
                    style={{
                      color: 'red',
                      fontSize: 16,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {parseFloat(item.credit).toFixed(3)}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row-reverse',
                width: '100%',
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row-reverse',
                  width: '30%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: 'green',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {' '}
                  الدفعة
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row-reverse',
                  width: '30%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: 'green',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {' '}
                  date
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row-reverse',
                  width: '30%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: 'green',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {' '}
                  transfer id
                </Text>
              </View>
            </View>

            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                paddingHorizontal: 10,
                paddingVertical: 5,
              }}>
              {item.payment.map((subitem, key) => (
                <View
                  style={{
                    flexDirection: 'row-reverse',
                    width: '100%',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row-reverse',
                      width: '30%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{ color: 'green' }}>
                      {parseFloat(subitem.paymentamount).toFixed(3)}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row-reverse',
                      width: '30%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{ color: 'green' }}>
                      {subitem.paymentdate}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row-reverse',
                      width: '30%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{ color: 'green' }}>
                      {subitem.paymenttransferid}
                    </Text>
                  </View>
                </View>
              ))}
            </ScrollView>
          </View>
        ))}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#F5FCFF',
  },

  iconStyle: {
    width: 30,
    height: 30,
    justifyContent: 'flex-end',
    alignItems: 'center',
    tintColor: '#fff',
  },

  sub_Category_Text: {
    fontSize: 18,
    color: '#000',
    padding: 10,
  },

  category_Text: {
    textAlign: 'left',
    color: '#fff',
    fontSize: 21,
    padding: 10,
  },

  category_View: {
    marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#0091EA',
  },

  Btn: {
    padding: 10,
    backgroundColor: '#FF6F00',
  },
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC',
  },
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  stepLabel: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#999999',
  },
  stepLabelSelected: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#4aae4f',
  },
  seprator: {
    height: 0.5,
    marginTop: 4,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#CCCCCC',
  },
});
