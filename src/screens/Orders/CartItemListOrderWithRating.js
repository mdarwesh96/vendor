import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Alert,Dimensions } from 'react-native';
const { height, width } = Dimensions.get('window')
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class CartItemListOrderWithRating extends Component {
  componentDidMount() {
  }

  constructor(props) {
    super(props)
    this.state = {
      value: 0,
      amount: 0,
      v3: this.props.itemmm.quantity_wanted,
      activeRowKey: null ,
      seeFeature:false,
      delete:false,
      imageLoadingvendor:true
    }
    this.amount = 0
  
  }
  getitemimage=(id,imageslist)=>
  {
    let i='';
 for(let f=0;f<imageslist.length;f++)
 {
   if(""+imageslist[f][0].item_id === ''+id)
   {
    i=imageslist[f][0].item_photo;
   }
 }
 

    return i;
  }

    render() {  


        return (

          <View style={{ height: this.props.imageh+10, borderWidth: 0.0, borderColor: 'white',flexDirection:'column' ,width:'90%',marginLeft:'5%',marginRight:'5%'}}>
            <View style={styles.seprator}></View>
                    <View style={{ height: this.props.imageh+5,flexDirection:'row-reverse' }}>


         
        

                     <Image
                     style={{ width: this.props.imageh, height: this.props.imageh,marginTop:8,borderRadius:10}}


                     source = { this.state.imageLoadingvendor 
                      ? 
                      
                      { uri: "http://ecommerce.waddeely.com"+this.getitemimage(this.props.itemmm.item.id,this.props.images)}
                      : 
                      require('../../imgs/defaultt.png') }
                            
                      
                      onError={_=>this.setState({imageLoadingvendor:false})}


                   />
                     
                               <View style={{ marginTop:5,marginRight:15,marginLeft:15,marginTop:20 }}>

                     <View style={{flexDirection:'row-reverse',alignItems:'flex-start',justifyContent:'flex-start'}}>
                     <Text style={{fontWeight:'500',fontSize:17}}>
             {this.props.itemmm.item.name_ar}
               </Text>
               </View>
               <View style={{flexDirection:'row-reverse',alignItems:'flex-start',justifyContent:'flex-start',marginLeft:5,marginRight:5}}>

               <Text style={{fontWeight:'200',fontSize:14,textAlign:'right'}}>

               {this.props.itemmm.item.description_ar}

</Text>


</View>
{this.props.itemmm.item_count>1&&
<Text style={{fontWeight:'200',fontSize:14,textAlign:'right'}}>

{this.props.itemmm.item_count} قطع

</Text>
}
{(this.props.itemmm.item_count === '1'||this.props.itemmm.item_count === 1)&&
  <Text style={{fontWeight:'200',fontSize:14,textAlign:'right'}}>
  
  {this.props.itemmm.item_count} قطعه

  
  </Text>
  }
{(this.props.itemmm.item_extar!=='null'&&this.props.itemmm.item_extar!==null &&this.props.itemmm.item_extar!=='')&&
<Text   numberOfLines={1} ellipsizeMode="tail"  style={{fontWeight:'200',fontSize:14,textAlign:'right'}}>

{this.props.itemmm.item_extar}</Text>
}




                     </View>
                

          </View>
    
          
             
     
           
           
   

      
  
      </View>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        backgroundColor: "#ffffff",
        marginTop:10,
      },
      container: {
        paddingLeft: 5,
        paddingRight: 16,
        paddingVertical: 12,
        flexDirection: 'row-reverse',
        alignItems: 'flex-end'
      },
      content: {
        marginLeft: 16,
        flex: 1,
        flexDirection:'column'
      },
      contentHeader: {
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        marginBottom: 6,
        marginTop:10
      },
      separator: {
        height: 1,
        backgroundColor: "#CCCCCC"
      },
      image:{
        width:35,
        height:35,
        borderRadius:20,
        marginRight:5
      },
      indicator:{
        width:30,
        height:30,
        marginLeft:0
      },
      time:{
        fontSize:13,
        color:"#808080",
        marginLeft:10

      },
      name:{
        fontSize:16,
        fontWeight:"bold",
        marginRight:10
      },
      thirdNotificationItemText:{
        fontSize:11, 
        color:"#808080",
        flexDirection:'row-reverse'
 
      },
      seprator: {
        height: .5,
        marginTop:4,
        backgroundColor: "#CCCCCC"
      } , myStarStyle: {
        color: '#0599ea',
        backgroundColor: 'transparent',
        textShadowColor: '#0599ea',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 2,
      },
      myEmptyStarStyle: {
        color: 'grey',
        textShadowColor: 'grey',
    
      },
});

export default CartItemListOrderWithRating;
