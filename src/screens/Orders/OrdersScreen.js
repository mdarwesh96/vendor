import React, { useEffect, useState, useRef } from 'react';
import {
  FlatList,
  View,
  Text,
  Platform,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Dimensions,
  TouchableHighlight,
  StyleSheet,
  Keyboard,
  NativeModules,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { drawer } from '../../AppNavigation';
import { drawerD } from '../../AppNavigationDefault';
import LinearGradient from 'react-native-linear-gradient';
import PanelWithoutPadding from '../../common/actualComponents/PanelWithoutPadding';
import AppMetrics from '../../common/metrics'
import { ButtonGroup } from 'react-native-elements';
import OrdersItems from './OrdersItems';
import WaitingItems from './WaitingItems';
import DeliveredItems from './DeliveredItems';
import CanceledItems from './CanceledItems';
import EcutedItem from './EcutedItem';
import { useDispatch, useSelector } from 'react-redux';
import { GetOrdersAction } from '../../actions/Actions';
import { UpdateOrdersAction } from '../../actions/Actions';
import Spinner from 'react-native-loading-spinner-overlay';
const DEVICE_hight = Dimensions.get('window').height;
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import ActionSheet from 'react-native-actions-sheet';

const component4 = () => <Text>الطلبات</Text>;
const component3 = () => <Text>ضمن التنفيذ </Text>;
const component1 = () => <Text>تم التسليم </Text>;
const component0 = () => <Text>المرفوض</Text>;
let colorss = '#0599ea';
let selctedf = '';
let timetodeliver = '';
import BottomSheet from 'reanimated-bottom-sheet';

// import SwipeablePanell from '../../common/actualComponents/Panels';

const DEVICE_width = Dimensions.get('window').width;
let deviceLocale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;
if (
  typeof deviceLocale !== 'undefined' &&
  deviceLocale !== null &&
  deviceLocale.includes('ar')
) {
  deviceLocale = 'ar';
} else {
  deviceLocale = 'en';
}

export default function OrdersScreen(props) {
  const dispatch = useDispatch();
  const sheetRefAccept = useRef(null);
  const sheetRefRefuse = useRef(null);
  //1 - DECLARE VARIABLES
  const [selectedIndexOptions, setselectedIndexOptions] = useState(3);
  const [SelectExpectedTime, setSelectExpectedTime] = useState(false);
  const [SelectRefuse, setSelectRefuse] = useState(false);
  const [SelectAccept, setSelectAccept] = useState(false);


  
  const actionSheetRef = useRef();
  const showModalVisible = () => {
    actionSheetRef.current?.setModalVisible(true);
  };
  const hideModalVisible = () => {
    actionSheetRef.current?.setModalVisible(false);
  };

  actionSheetRef.current?.snapToOffset(300);

  const [spinner, setspinner] = useState(false);
  const [token, settoken] = useState('');
  const [totalpp, settotalpp] = useState(0);
  const [fullheightttAccept, setfullheightttAccept] = useState(0);
  const [fullheightttRefuse, setfullheightttRefuse] = useState(0);
  const [data, setdata] = useState([]);
  const [currency, setcurrency] = useState('درهم اماراتي');
  const [first, setfirst] = useState(true);
  const [currencyid, setcurrencyid] = useState('1');
  const [selcted, setselcted] = useState('');
  const [dataimages, setdataimages] = useState([]);
  const [pricess, setpricess] = useState([]);
  const [basket, setbasket] = useState([]);
  const [reason, setreason] = useState('');
  const [successimage, setsuccessimage] = useState('');
  const [srcimage, setsrcimage] = useState(require('../../imgs/defaultt.png'));
  const [selectedid, setselectedid] = useState(null);
  const [firstdata, setfirstdata] = useState([]);
  const [seconddata, setseconddata] = useState([]);
  const [thirddata, setthirddata] = useState([]);
  const [fourthdata, setfourthdata] = useState([]);
  const [fourthitems, setfourthitems] = useState([]);
  const [thirditems, setthirditems] = useState([]);
  const [seconditems, setseconditems] = useState([]);
  const [firstitems, setfirstitems] = useState([]);
  const [cancableid, setcancableid] = useState(10);


  const [updated, setupdated] = useState(false);
  const [getorders, setgetorders] = useState(false);

  const LoginReducer = useSelector((state) => state.LoginReducer);
  const GetOrdersReducer = useSelector((state) => state.GetOrdersReducer);
  const UpdateVendorOrdersReducer = useSelector(
    (state) => state.UpdateVendorOrdersReducer
  );

  const getmybaasket = (barray, id) => {
    let d = '';
  
    for (let s = 0; s < barray.length; s++) {
      if ('' + barray[s].order_id === '' + id) {
       
        d = barray[s].expected_delivary_date;
      }
    }

    return d;
  };

  closepanell = (index) => {
   
    setSelectExpectedTime(false)
    setSelectRefuse(false)
    setSelectAccept(false)
 
    opened = false;
  };
  function handleBackButtonClick() {

    setSelectExpectedTime(false)
    setSelectRefuse(false)
setSelectAccept(false)
    opened = false;
    return true;
  }



const notRepeatedBefore=(seconddatat,id)=>
{
  debugger;
  if(seconddatat.length===0)
  {
    debugger;
    return true;
  }else {
    debugger;
    for(let y=0;y<seconddatat.length;y++)
    {
      debugger;
      if(''+seconddatat[y].id===''+id)
      {
        debugger;
        return false
      }
    }
    debugger;
    return true;
  }

}

 

  const getunrepeated = (orders, basket) => {
    let s = [];
    let idd = [];

    for (let a = 0; a < orders.length; a++) {
      if (idd.length == 0) {
        idd.push(orders[a][0].id);
        s.push(orders[a]);
      } else {
        if (idd.includes(orders[a][0].id)) {
        } else {
          idd.push(orders[a][0].id);

          s.push(orders[a]);
        }
      }
    }

    let firstdata = [];
    let seconddata = [];
    let thirddata = [];
    let fourthdata = [];
    // let fifthdata=[];
    // let sixthdata=[];
    let firstitems = [];
    let seconditems = [];
    let thirditems = [];
    let fourthitems = [];
    // let fifthitems=[];
    // let sixthitems=[];

    for (let q = 0; q < s.length; q++) {
      if ((s[q][0].status === '' + 0&&''+s[q][0].type==='0')) {
        firstdata.push(s[q][0]);

        for (let j = 0; j < basket.length; j++) {
          if (basket[j].order_id === s[q][0].id) {
            firstitems.push(basket[j]);
          } else {
          }
        }
      } else if ((s[q][0].status === '' + 2&&''+s[q][0].type==='0')) {
      

        seconddata.push(s[q][0]);
        for (let j = 0; j < basket.length; j++) {
          if ((basket[j].order_id === s[q][0].id)) {
            seconditems.push(basket[j]);
          }
        }
      } else if (s[q][0].status === '' + 9) {
        thirddata.push(s[q][0]);
        for (let j = 0; j < basket.length; j++) {
          if (basket[j].order_id === s[q][0].id) {
            thirditems.push(basket[j]);
          }
        }
      } else if (s[q][0].status === '' + 10 || s[q][0].status === '' + 6) {
        fourthdata.push(s[q][0]);
        for (let j = 0; j < basket.length; j++) {
          if (basket[j].order_id === s[q][0].id) {
            fourthitems.push(basket[j]);
          }
        }
      }else if(s[q][0].status === '' + 1&&''+s[q][0].type==='1')
      {
     
        for (let j = 0; j < basket.length; j++) {
        
          if (basket[j].order_id === s[q][0].id&&(''+basket[j].status!=='2')) {
            if(notRepeatedBefore(seconddata,basket[j].order_id)){
            
            seconddata.push(s[q][0]);
            }

            seconditems.push(basket[j]);
           
          }
        }

      }
    }
    debugger;
    setfirstdata(firstdata);
    setseconddata(seconddata);
    setthirddata(thirddata);
    setfourthdata(fourthdata);

    setfirstitems(firstitems);
    setseconditems(seconditems);
    setthirditems(thirditems);
    setfourthitems(fourthitems);
debugger;
    settotalpp(GetOrdersReducer.payload.total_payment);

    if (Platform.OS === 'ios') {
      setInterval(() => {
        setspinner(false);
      }, 1000);
    } else {
      // setInterval(() => {

      setspinner(false);

      // }, 1000);
    }

    return s;
  };

 

  acceptorder = (id, timetodeliverr) => {
    setselcted('update');
    timetodeliver = timetodeliverr;
    selctedf = 'accept';
    opened = true;
    setreason('');
    setsuccessimage('');
    timetodeliverr = '';
    setselectedid(id);
    setSelectAccept(false)
    setSelectExpectedTime(true)
    setSelectRefuse(false)

  };

  sendtodelivery = (id) => {
    setselcted('update');

    selctedf = '';
    setspinner(true);

    dispatch(
      UpdateOrdersAction(
        timetodeliver,
        id,
        LoginReducer.payload.token.token,
        3,
        reason,
        successimage
      )
    );
    setSelectRefuse(false)
    setSelectAccept(false)
    setSelectExpectedTime(false)


  };



  updateVendorStateForSpecialOrder= (id,timetodeliver) => {
    setselcted('update');
    selctedf = '';
    setspinner(true);

    dispatch(
      UpdateOrdersAction(
        timetodeliver,
        id,
        LoginReducer.payload.token.token,
        2,
        reason,
        successimage
      )
    );
    setSelectRefuse(false)
    setSelectAccept(false)
    setSelectExpectedTime(false)


  };


  cancelorder = (id) => {
    selctedf = 'refuse';
    opened = true;
    setreason('');
    setsuccessimage('');
    setselectedid(id);
    setcancableid(6);
    setSelectAccept(false)
    setSelectRefuse(true)
    setSelectExpectedTime(false)
  };

  failOrder = (id) => {
    selctedf = 'refuse';
    opened = true;
    setreason('');
    setsuccessimage('');
    setselectedid(id);
    setcancableid(10);
    opened = true;
    setSelectAccept(false)
    setSelectExpectedTime(false)
    setSelectRefuse(true)

  };
  confirmorder = (id) => {
    selctedf = 'accept';
    setreason('');
    setsuccessimage('');
    setselectedid(id);
  };

  handleChoosePhoto = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.uri) {
        // this.setState({ photo: response ,avatarSource:response});
      }
    });
  };

  const getitems = (items, id, index) => {
    let arrayy = [];
    for (let s = 0; s < items.length; s++) {
      if ('' + items[s].order_id === '' + id[index].id) {
        arrayy.push(items[s]);
      }
    }

    return arrayy;
  };

  const selectPhotoTapped = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: 'itemfiles',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = { uri: response.uri };

        ImgToBase64.getBase64String(response.uri)
          .then((base64String) => {
            setsuccessimage(base64String);
            // this.setState({"imagebase":base64String});
          })
          .catch((err) => {});

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        // this.setState({
        //     srcimage: source,
        // });
        setsrcimage(source);
      }
    });
  };

  const openCamera = (cropit) => {
    ImagePicker.openCamera({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  const openGallary = (cropit) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  if (selcted === 'getorders') {
   
    if (
      typeof GetOrdersReducer !== 'undefined' &&
      GetOrdersReducer.error === 'success'
    ) {
      setdata(GetOrdersReducer.payload.order);

      getunrepeated(
        GetOrdersReducer.payload.order,
        GetOrdersReducer.payload.basket
      );

      let s = GetOrdersReducer.payload.order;

      setdataimages(GetOrdersReducer.payload.item_photo);

      setpricess(GetOrdersReducer.payload.convert_price);

      setbasket(GetOrdersReducer.payload.basket);

      setselcted('');
      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }
    } else if (
      typeof GetOrdersReducer !== 'undefined' &&
      GetOrdersReducer.error !== 'success' &&
      GetOrdersReducer.error !== ''
    ) {
   
      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }
      setdata([]);
      setdataimages([]);
      setselcted('');
      setpricess([]);
    }
  } else if (selcted === 'update') {

    if (
      typeof UpdateVendorOrdersReducer !== 'undefined' &&
      UpdateVendorOrdersReducer.error !== ''
    ) {
    
      setupdated(false);
      if (
        typeof UpdateVendorOrdersReducer !== 'undefined' &&
        UpdateVendorOrdersReducer.error === 'success'
      ) {
        
        dispatch(
          GetOrdersAction(
            currencyid,
            LoginReducer.payload.token.token,
            LoginReducer.payload.data.id
          )
        );
        setselcted('getorders');

        setspinner(true);
      } else {
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }
        Toast.showWithGravity(
          'حدث خطأ ، يرجى المحاولة لاحقًا  ',
          Toast.LONG,
          Toast.BOTTOM
        );
        setselcted('');

      }
    }
  }
  const getlistlength = (data, selectedIndexOptions) => {
    let d = [];
    return d;
  };
  const updateIndexOptions = (selectedIndex) => {
    timetodeliver = '';
    setSelectAccept(false)
    setSelectExpectedTime(false)
    setSelectRefuse(false)

    if (selectedIndex === 0) {
      colorss = 'red';
    } else if (selectedIndex === 1) {
      colorss = 'green';
    } else {
      colorss = '#0599ea';
    }
    setselectedIndexOptions(selectedIndex);
  };

  const rowItem = (item) => (
    <View
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 20,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}>
      <Text style={{ fontSize: 18 }}>{item.test}</Text>
    </View>
  );
  const loadApp = async () => {
    settoken(props.navigation.state.params.usertoken);
  };

  useEffect(() => {
    // sheetRef.current.snapTo(fullheighttt)

    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        if (opened) {
          if (selctedf === 'refuse') {
            setSelectRefuse(true)
            setSelectAccept(false)
            setSelectExpectedTime(false)
        
          } else if (selctedf === 'accept') {
            setSelectRefuse(false)
            setSelectAccept(true)
            setSelectExpectedTime(false)
           
          }
        }
      }
    );

    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        if (opened) {
          if (selctedf === 'refuse') {
            setSelectRefuse(true)
            setSelectAccept(false)
            setSelectExpectedTime(false)
        
          } else if (selctedf === 'accept') {
            setSelectRefuse(false)
          setsetSelectAccept(true)
          setSelectExpectedTime(false)
          }
        }
      }
    );

    loadApp();
   
    if (first) {
      if (
        typeof LoginReducer !== 'undefined' &&
        typeof LoginReducer.payload !== 'undefined' &&
        typeof LoginReducer.payload.data !== 'undefined'
      ) {
      
        setselcted('getorders');

        dispatch(
          GetOrdersAction(
            currencyid,
            LoginReducer.payload.token.token,
            LoginReducer.payload.data.id
          )
        );
        setfirst(false);

        setspinner(true);
      }
    }
    const timer = window.setInterval(() => {
      if (
        typeof LoginReducer !== 'undefined' &&
        typeof LoginReducer.payload !== 'undefined' &&
        typeof LoginReducer.payload.data !== 'undefined'
      ) {
        setselcted('getorders');

        dispatch(
          GetOrdersAction(
            currencyid,
            LoginReducer.payload.token.token,
            LoginReducer.payload.data.id
          )
        );
      }
    }, 50000);

    return () => {
      // Return callback to run on unmount.
      window.clearInterval(timer);
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, [GetOrdersReducer]);

  return (
    <View
      style={{
        height: AppMetrics.screenHeight,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          height: '100%',
          width: '100%',
          flexDirection: 'column',

          justifyContent: 'space-between',
        }}>
        <ImageBackground
          style={{
            width: '100%',
            height: 150,
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
          }}
          resizeMode='stretch'
          source={require('../../imgs/headerr.png')}>
          <View
            style={{
              width: '100%',
              marginTop: 70,
              paddingHorizontal: 30,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
            }}>
                   <TouchableHighlight   underlayColor='transparent'
                     onPress={() => {  try {
                      drawer.current.open();
                    } catch (ff) {
                      drawerD.current.open();
                    }}}
                activeOpacity={0.2}>
                <Image
                 source={require('../../imgs/menueic.png')}
              style={{width:25,height:20,tintColor:'white'}}
              resizeMode='contain'
                />
                </TouchableHighlight>

     <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',alignItems:'flex-start',height:20}}
              resizeMode='stretch'
                /> 
            <Icon size={30} />
          </View>
        </ImageBackground>

        <View
          style={{ height: '100%', width: '100%', backgroundColor: 'white' }}>
          <ButtonGroup
            onPress={(index) => {
              updateIndexOptions(index);
            }}
            selectedIndex={selectedIndexOptions}
            innerBorderStyle={{ color: '#f2f2f2' }}
            buttons={[
              { element: component0 },
              { element: component1 },
              { element: component3 },
              { element: component4 },
            ]}
            textStyle={{ color: 'white' }}
            selectedButtonStyle={{
              backgroundColor: colorss,
              borderRadius: 30,
            }}
            underlayColor='#0599ea'
            containerStyle={{
              height: 50,
              borderRadius: 30,
              borderBottomColor: '#f2f2f2',
              borderColor: '#f2f2f2',
              backgroundColor: '#f2f2f2',
              width: '94%',
              marginLeft: '3%',
              marginRight: '3%',
            }}
          />
<View style={{marginBottom:150}}>
          {typeof fourthdata !== 'undefined' &&
            fourthdata.length > 0 &&
            selectedIndexOptions === 0 && (
              <FlatList
                showsVerticalScrollIndicator={false}
                style={{ marginBottom: 100 }}
                data={fourthdata}
                renderItem={({ item, index }) => (
                  <CanceledItems
                    item={getitems(fourthitems, fourthdata, index)}
                    data={fourthdata[index]}
                    index={index}
                    images={dataimages}
                    pricestotal={totalpp}
                    imageh={(0.2 * DEVICE_hight) / 2}
                    iddddd={fourthdata[index].id}
                    currency={currency}
                    prices={pricess}
                    parentFlatList={this}
                  />
                )}
                keyExtractor={(item) => item.id}
              />
            )}
          {typeof thirddata !== 'undefined' &&
            thirddata.length > 0 &&
            selectedIndexOptions === 1 && (
              <FlatList
                showsVerticalScrollIndicator={false}
                style={{ marginBottom: 100 }}
                data={thirddata}
                renderItem={({ item, index }) => (
                  <DeliveredItems
                    item={getitems(thirditems, thirddata, index)}
                    images={dataimages}
                    pricestotal={totalpp}
                    imageh={(0.2 * DEVICE_hight) / 2}
                    iddddd={thirddata[index].id}
                    data={thirddata[index]}
                    index={index}
                    prices={pricess}
                    currency={currency}
                    parentFlatList={this}
                  />
                )}
                keyExtractor={(item) => item.id}
              />
            )}

          {typeof seconddata !== 'undefined' &&
            seconddata.length > 0 &&
            selectedIndexOptions === 2 && (
              <FlatList
                style={{ marginBottom: 100 }}
                showsVerticalScrollIndicator={false}
                data={seconddata}
                renderItem={({ item, index }) => (
                  <OrdersItems
                    pricestotal={totalpp}
                    imageh={(0.2 * DEVICE_hight) / 2}
                    basket={getmybaasket(basket, seconddata[index].id)}
                    index={index}
                    item={getitems(seconditems, seconddata, index)}
                    images={dataimages}
                    data={seconddata[index]}
                    iddddd={seconddata[index].id}
                    currency={currency}
                    prices={pricess}
                    parentFlatList={this}
                  />
                )}
                keyExtractor={(item) => item.id}
              />
            )}

          {typeof firstdata !== 'undefined' &&
            firstdata.length > 0 &&
            selectedIndexOptions === 3 && (
              <FlatList
                style={{ marginBottom: 100 }}
                showsVerticalScrollIndicator={false}
                data={firstdata}
                renderItem={({ item, index }) => (
                  <EcutedItem
                    item={getitems(firstitems, firstdata, index)}
                    images={dataimages}
                    pricestotal={totalpp}
                    imageh={(0.2 * DEVICE_hight) / 2}
                    iddddd={firstdata[index].id}
                    data={firstdata[index]}
                    index={index}
                    currency={currency}
                    prices={pricess}
                    parentFlatList={this}
                  />
                )}
                keyExtractor={(item) => item.id}
              />
            )}
          {firstdata.length === 0 && selectedIndexOptions === 3 && (
            <View
              style={{
                width: '100%',
                height: DEVICE_hight - 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  size: 25,
                  fontWeight: 'bold',
                  color: '#0599ea',
                }}>
                لا توجد طلبات سابقة
              </Text>
            </View>
          )}
          {seconddata.length === 0 && selectedIndexOptions === 2 && (
            <View
              style={{
                width: '100%',
                height: DEVICE_hight - 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  size: 25,
                  fontWeight: 'bold',
                  color: '#0599ea',
                }}>
                لا توجد طلبات سابقة
              </Text>
            </View>
          )}
          {thirddata.length === 0 && selectedIndexOptions === 1 && (
            <View
              style={{
                width: '100%',
                height: DEVICE_hight - 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  size: 25,
                  fontWeight: 'bold',
                  color: '#0599ea',
                }}>
                لا توجد طلبات سابقة
              </Text>
            </View>
          )}
          {fourthdata.length === 0 && selectedIndexOptions === 0 && (
            <View
              style={{
                width: '100%',
                height: DEVICE_hight - 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  size: 25,
                  fontWeight: 'bold',
                  color: '#0599ea',
                }}>
                لا توجد طلبات سابقة
              </Text>
            </View>
          )}
          </View>
        </View>

        <ActionSheet
          ref={actionSheetRef}
          containerStyle={{
            backgroundColor: 'transparent',
          }}
          elevation={0}>
          <View
            style={{
              backgroundColor: '#E5E5E5',
              margin: 20,
              borderRadius: 10,
            }}>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 50,
              }}
              onPress={() => {
                hideModalVisible();

                setTimeout(() => {
                  openCamera(true);
                }, 1000);
              }}>
              <Text>الكاميرا</Text>
            </TouchableOpacity>
            <View
              style={{
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderColor: '#707070',
              }}
            />
            <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 50,
              }}
              onPress={() => {
                hideModalVisible();

                setTimeout(() => {
                  openGallary(true);
                }, 1000);
              }}>
              <Text>المعرض</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              backgroundColor: '#E5E5E5',
              marginHorizontal: 20,
              borderRadius: 10,
              marginBottom: 20,
            }}>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                height: 50,
              }}
              onPress={() => {
                hideModalVisible();
              }}>
              <Text style={{ color: 'red' }}>إلغاء</Text>
            </TouchableOpacity>
          </View>
        </ActionSheet>

        <Spinner
          visible={spinner}
          textContent={''}
          textStyle={{ color: '#FFF' }}
        />
      </View>



<PanelWithoutPadding
          isActive={SelectAccept}
          closeOnTouchOutside={true}
          onClose={this}
          closeeee={false}
          openLarge={true}
          style={{
            width: AppMetrics.screenWidth,
            flexDirection: 'column',
            justifyContent:'center',
            alignItems:'center',
            height: AppMetrics.screenHeight * 0.25,
          }}>
              <LinearGradient
                  colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                  style={{
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    borderRadius: 22,
                    width:AppMetrics.screenWidth,height:AppMetrics.screenHeight * 0.25
                  }}>
        <View
      style={{
        flexDirection: 'column',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#cccccc',
      }}>
      <Text
        style={{
          fontSize: 15,
          padding: 5,
          fontWeight: '400',
          height: 30,
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
          textAlign: 'center',
        }}>
        تأكيد تسليم الطلب عن طريق التقاط صورة هوية العميل
      </Text>
      <TouchableOpacity
        activeOpacity={0.9}
        style={{ width: '100%', height: 280 }}
        onPress={() => {
          {
            showModalVisible();
          }
        }}>
        <ImageBackground
          resizeMode='cover'
          source={srcimage}
          style={{ width: '100%', height: 300 }}></ImageBackground>
      </TouchableOpacity>

      <TouchableOpacity
        activeOpacity={0.9}
        style={{
          alignItems: 'center',
          height: 100,
          width: '100%',
          flexDirection: 'column',
          backgroundColor: '#3888C6',
        }}
        onPress={() => {
          if (successimage.length > 0) {
            setselcted('update');

            opened = false;
            // setopened(false)

            dispatch(
              UpdateOrdersAction(
                timetodeliver,
                selectedid,
                LoginReducer.payload.token.token,
                9,
                reason,
                successimage
              )
            );

            selctedf = '';
            setSelectAccept(false)
            setSelectExpectedTime(false)
            setSelectRefuse(false)
            setspinner(true);
          }
        }}>
        <Text
          style={{
            color: 'white',
            paddingTop: 30,
            paddingBottom: 30,
            fontSize: 20,
            fontWeight: '400',
            textAlign: 'center',
            backgroundColor: '#3888C6',
            width: '100%',
          }}>
          {' '}
          إرسال{' '}
        </Text>
      </TouchableOpacity>
    </View>
 
    </LinearGradient>
            </PanelWithoutPadding>
  
    
      <PanelWithoutPadding
          isActive={SelectRefuse}
          closeOnTouchOutside={true}
          onClose={this}
          closeeee={false}
          openLarge={true}
          style={{
            width: AppMetrics.screenWidth,
            flexDirection: 'column',
            justifyContent:'center',
            alignItems:'center',
            height: AppMetrics.screenHeight * 0.25,
          }}>
            
             <View
      style={{
        flexDirection: 'column',
        height: AppMetrics.screenHeight * 0.25,
        backgroundColor: 'white',
        paddingVertical:20
        
      }}>
      <Text
        style={{
          height: 30,
          fontSize: 17,
          fontWeight: '300',
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        سبب إلغاء الطلب
      </Text>

      <View style={styles.inputWrapper}>
        <TextInput
          style={styles.inputaddress}
          placeholder={' السبب '}
          multiline={true}
          autoCapitalize={'none'}
          returnKeyType={'next'}
          autoCorrect={false}
          onChangeText={(reason) => {
            opened = true;
            setreason(reason);
          }}
          value={reason}
          secureTextEntry={false}
          placeholderTextColor='grey'
          underlineColorAndroid='transparent'
        />
      </View>

      <LinearGradient
        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
        style={{
          borderRadius: 30,
          width: '50%',
          marginHorizontal: '25%',
          textAlign: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          height: 40,
        }}>
        <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
          style={{
            textAlign: 'center',
            alignItems: 'center',
          }}
      
        onPress={() => {
          if (reason.length > 0) {
            setselcted('update');
            dispatch(
              UpdateOrdersAction(
                timetodeliver,
                selectedid,
                LoginReducer.payload.token.token,
                10,
                reason,
                successimage
              )
            );

            setspinner(true);
            selctedf = '';
        setSelectRefuse(false)
        
          
          } else {
            Toast.showWithGravity(
              'يرجى تحديد سبب الإلغاء  ',
              Toast.LONG,
              Toast.BOTTOM
            );
          }
        }}>
        <Text
          style={{
            color: 'white',
            fontSize: 20,
            fontWeight: '400',
            textAlign: 'center',
            width: '100%',
            height: '100%',
            justifyContent:'center',
            textAlign:'center'
          }}>
          {' '}
          إرسال{' '}
        </Text>
      </TouchableHighlight>
      </LinearGradient>
    </View>
            </PanelWithoutPadding>
  
       <PanelWithoutPadding
          isActive={SelectExpectedTime}
          closeOnTouchOutside={true}
          onClose={this}
          closeeee={false}
          openLarge={true}
          style={{
            width: AppMetrics.screenWidth,
            flexDirection: 'column',
            justifyContent:'center',
            alignItems:'center',
            height: AppMetrics.screenHeight * 0.25,
          }}>
              <LinearGradient
                  colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                  style={{
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    borderRadius: 22,
                    width:AppMetrics.screenWidth,height:AppMetrics.screenHeight * 0.25
                  }}>
           <View >
      {Platform.OS === 'ios' && (
        <View style={{ flexDirection: 'column'}}>
          <TouchableOpacity
            activeOpacity={0.9}
            style={{
              alignItems: 'center',
               flex:1,
              flexDirection: 'column-reverse',
              borderBottomColor: 'white',
              borderBottomWidth: StyleSheet.hairlineWidth,
            }}
            onPress={() => {
              setspinner(true);
              dispatch(
                UpdateOrdersAction(
                  ' وقت التسليم المقدر من ساعة إلى ساعتين',
                  selectedid,
                  LoginReducer.payload.token.token,
                  2,
                  '',
                  ''
                )
              );

              setselcted('update');

              selctedf = '';
              setfullheightttRefuse(0);
              setfullheightttAccept(0);
              setSelectExpectedTime(false)

            }}>
            <Text
              style={{
                color: 'white',
                paddingTop: 20,
                paddingBottom: 20,
                fontSize: 20,
                fontWeight: '400',
                textAlign: 'center',
                width: '100%',
                height: '100%',
                paddingBottom: 10,
              }}>
              {' '}
              وقت التسليم المقدر من ساعة إلى ساعتين
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.9}
            style={{
              alignItems: 'center',
             flex:1,
              flexDirection: 'column-reverse',
              borderBottomColor: 'white',
              borderBottomWidth: StyleSheet.hairlineWidth,
            }}
            onPress={() => {
              setselcted('update');

              setspinner(true);

              dispatch(
                UpdateOrdersAction(
                  'وقت التسليم المقدر من ساعة إلى ساعتين',
                  selectedid,
                  LoginReducer.payload.token.token,
                  2,
                  '',
                  ''
                )
              );

              selctedf = '';
              setfullheightttRefuse(0);
              setfullheightttAccept(0);
            
              setSelectExpectedTime(false)

            }}>
            <Text
              style={{
                color: 'white',
                paddingTop: 30,
                paddingBottom: 30,
                fontSize: 20,
                fontWeight: '400',
                textAlign: 'center',
                width: '100%',
                height: '100%',
                paddingBottom: 10,
              }}>
              {' '}
              يقدر وقت التسليم من ساعتين إلى ثلاث ساعات
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.9}
            style={{
              alignItems: 'center',
           flex:1,
              flexDirection: 'column-reverse',
              borderBottomColor: 'white',
              borderBottomWidth: StyleSheet.hairlineWidth,
            }}
            onPress={() => {
              setselcted('update');

              setspinner(true);

              dispatch(
                UpdateOrdersAction(
                  ' يقدر التسليم أكثر من  ثلاث ساعات',
                  selectedid,
                  LoginReducer.payload.token.token,
                  2,
                  '',
                  ''
                )
              );

              selctedf = '';
              setfullheightttRefuse(0);
              setfullheightttAccept(0);
             
              setSelectExpectedTime(false)

            }}>
            <Text
              style={{
                color: 'white',
                paddingTop: 20,
                paddingBottom: 20,
                fontSize: 20,
                fontWeight: '400',
                textAlign: 'center',
                width: '100%',
                height: '100%',
                paddingBottom: 10,
              }}>
              {' '}
              يقدر التسليم أكثر من ثلاث ساعات
            </Text>
          </TouchableOpacity>
        </View>
      )}
      {Platform.OS !== 'ios' && (
        <View style={{ flexDirection: 'column', height: '100%' }}>
          <TouchableOpacity
            activeOpacity={0.9}
            style={{
              alignItems: 'center',
          flex:1,
              flexDirection: 'column-reverse',
              backgroundColor: '#cccccc',
            }}
            onPress={() => {
              setselcted('update');

              setspinner(true);

              dispatch(
                UpdateOrdersAction(
                  ' وقت التسليم المقدر من ساعة إلى ساعتين',
                  selectedid,
                  LoginReducer.payload.token.token,
                  2,
                  '',
                  ''
                )
              );

              selctedf = '';
              // setfullheightttRefuse(0);
              // setfullheightttAccept(0);
              
              setSelectExpectedTime(false)

            }}>
            <Text
              style={{
                color: 'white',
                paddingTop: 20,
                paddingBottom: 20,
                fontSize: 20,
                fontWeight: '400',
                textAlign: 'center',
                width: '100%',
                height: '100%',
                paddingBottom: 10,
              }}>
              {' '}
              وقت التسليم المقدر من ساعة إلى ساعتين
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.9}
            style={{
              alignItems: 'center',
           flex:1,
              flexDirection: 'column-reverse',
              backgroundColor: '#cccccc',
            }}
            onPress={() => {
              setselcted('update');

              setspinner(true);

              dispatch(
                UpdateOrdersAction(
                  'وقت التسليم المقدر من ساعة إلى ساعتين',
                  selectedid,
                  LoginReducer.payload.token.token,
                  2,
                  '',
                  ''
                )
              );

              selctedf = '';
           
              setSelectExpectedTime(false)

            }}>
   
            <Text
              style={{
                color: 'white',
                paddingTop: 20,
                paddingBottom: 20,
                fontSize: 20,
                fontWeight: '400',
                textAlign: 'center',
                width: '100%',
                height: '100%',
                paddingBottom: 10,
              }}>
              {' '}
              يقدر وقت التسليم من ساعتين إلى ثلاث ساعات
            </Text>
          
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.9}
            style={{
              alignItems: 'center',
            flex:1,
              flexDirection: 'column-reverse',
              backgroundColor: '#cccccc',
            }}
            onPress={() => {
              setselcted('update');

              setspinner(true);

              dispatch(
                UpdateOrdersAction(
                  ' يقدر التسليم أكثر من  ثلاث ساعات',
                  selectedid,
                  LoginReducer.payload.token.token,
                  2,
                  '',
                  ''
                )
              );

              selctedf = '';
              setSelectExpectedTime(false)
            }}>  
        

            <Text
              style={{
                color: 'white',
                paddingTop: 20,
                paddingBottom: 20,
                fontSize: 20,
                fontWeight: '400',
                textAlign: 'center',
                width: '100%',
                height: '100%',
                paddingBottom: 10,
              }}>
              {' '}
              يقدر التسليم أكثر من ثلاث ساعات
            </Text>
       
          </TouchableOpacity>
        </View>
      )}
    </View>
    </LinearGradient>
            </PanelWithoutPadding>
  
    </View>
  );
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF',
  },
  inputaddress: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 80,
    marginTop: 5,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: 'grey',
    color: 'grey',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',
    marginBottom: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  inputWrapperr: {
    flexDirection: 'row-reverse',
    width: DEVICE_width - 140,
    marginBottom: 5,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputtt: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 50,
    marginTop: 10,
    flexDirection: 'row-reverse',
    marginLeft: 30,
    marginRight: 30,
    alignItems: 'center',

    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'grey',
    color: 'grey',
  },
  header: {
    height: 25,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  panelHandle: {
    width: 35,
    height: 6,
    borderRadius: 4,
    backgroundColor: 'rgba(255,255,255,0.7)',
    marginBottom: 0,
  },
});
