import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Button,
} from 'react-native';

import CartItemListOrderWithRating from './CartItemListOrderWithRating';
import Toast from 'react-native-simple-toast';

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class OrdersItems extends Component {
  gettotalp = (totals, currency, id) => {
    let t = 0;
    for (let f = 0; f < totals.length; f++) {
      if ('' + totals[f].order_id === '' + id) {
        t = parseInt(totals[f].total_price).toFixed(2) + ' ' + currency;
      }
    }
    return t;
  };
  sameDay(d1, d2) {
    return (
      d1.getFullYear() == d2.getFullYear() &&
      d1.getMonth() == d2.getMonth() &&
      d1.getDate() == d2.getDate()
    );
  }

  countof = (array, currency, pricesarray) => {
    let c = 0;
    try {
      for (let q = 0; q < array.length; q++) {
        for (let d = 0; d < pricesarray.length; d++) {
          if ('' + pricesarray[d].item_id === '' + array[q].item.id) {
            c =
              c +
              parseInt(array[q].item_count) * parseFloat(pricesarray[d].price);
          }
        }
      }
    } catch (e) {}
    return c.toFixed(2) + currency;
  };
  GETDATEE = (timee) => {
    let ds = '';

    let d = new Date(parseFloat(timee));

    ds =
      d.getDate() +
      1 +
      '-' +
      parseInt(d.getMonth() + 1) +
      '-' +
      d.getFullYear();

    return ds;
  };

  render() {

    return (
      <View
        style={{
          marginTop: 10,
          borderWidth: 0.5,
          backgroundColor: 'white',
          borderRadius: 10,
          borderColor: '#dddddd',
          flexDirection: 'column',
          width: DEVICE_WIDTH - 20,
          marginLeft: 10,
          marginRight: 10,
          marbackgroundColor: 'white',
        }}>
        <View style={{ flexDirection: 'column' }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
              margin: 10,
            }}>

            <View style={{ flexDirection: 'column' }}>
              <Text style={{ textAlign: 'center' }}>رقم الطلب</Text>
              <Text style={{ textAlign: 'center' }}>{this.props.data.id}</Text>
            </View>
            {''+this.props.item.type==='0'&&

            <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
              <Text style={{ marginRight: 10, textAlign: 'center' }}>
                التكلفة الإجمالية
              </Text>
              <Text style={{ marginRight: 10 }}>
                {this.gettotalp(
                  this.props.pricestotal,
                  this.props.currency,
                  this.props.iddddd
                )}
              </Text>
            </View>
  }
  {this.props.data.type==='1'&&
                              <Text style={{ marginRight: 10, textAlign: 'center' }}>{'طلب خاص'}</Text>
  }

            {'' + this.props.basket === '0' &&
              '' + this.props.basket === 'NAN' && (
                <View style={{ flexDirection: 'column' }}>
                  <Text style={{ textAlign: 'center' }}>تاريخ الطلب</Text>
                  <Text style={{ textAlign: 'center' }}>
                    {this.props.data.created_at.substring(0, 10)}
                  </Text>
                </View>
              )}

            {'' + this.props.basket !== '0' &&
              '' + this.props.basket !== 'NAN' && (
                <View style={{ flexDirection: 'column' }}>
                  <Text style={{ textAlign: 'center' }}>
                    تاريخ التسليم المتوقع
                  </Text>
                  <Text style={{ textAlign: 'center' }}>
                    {this.GETDATEE(this.props.basket)}
                  </Text>
                </View>
              )}
          </View>

          <View style={{ flexDirection: 'row-reverse' }}></View>

          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              paddingHorizontal: 10,
              paddingVertical: 5,
            }}>
            {this.props.item.map((item, key) => (
              <CartItemListOrderWithRating
                itemmm={item}
                images={this.props.images}
                pricestotal={this.props.pricestotal}
                imageh={this.props.imageh}
                parentFlatList={this.props.parentFlatList}
              />
            ))}
          </ScrollView>
          <View style={styles.seprator}></View>
          {this.props.data.status === '7' && (
            <View
              style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
              <View
                style={{
                  backgroundColor: '#0599ea',
                  marginLeft: 10,
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 5,
                  }}>
                  {' '}
                  في انتظار إجراءات التسليم
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'column',
                  width: '50%',
                  marginRight: 10,
                }}>
                <View
                  style={{
                    flexDirection: 'row-reverse',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <Text
                    style={{
                      fontWeight: '200',
                      fontSize: 13,
                      marginRight: 5,
                      textAlign: 'right',
                    }}>
                    العنوان
                  </Text>
                  <Text
                    style={{ fontWeight: '200', fontSize: 13, marginRight: 5 }}>
                    {' '}
                    :
                  </Text>
                  <Text
                    style={{
                      fontWeight: '200',
                      fontSize: 13,
                      marginLeft: 5,
                      width: DEVICE_WIDTH - 100,
                      textAlign: 'right',
                    }}
                    numberOfLines={2}>
                    {this.props.data.destination_address}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row-reverse',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <Text
                    style={{
                      fontWeight: '200',
                      fontSize: 13,
                      marginRight: 5,
                      textAlign: 'right',
                    }}>
                    الهاتف
                  </Text>
                  <Text
                    style={{ fontWeight: '200', fontSize: 13, marginRight: 5 }}>
                    {' '}
                    :
                  </Text>
                  <Text
                    style={{
                      fontWeight: '200',
                      fontSize: 13,
                      marginLeft: 5,
                      width: DEVICE_WIDTH - 100,
                      textAlign: 'right',
                    }}
                    numberOfLines={2}>
                    {this.props.data.destination_phone}
                  </Text>
                </View>
              </View>
            </View>
          )}
          {this.props.data.status !== '7' && (
            <View
              style={{
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
                width: '100%',
              }}>
              <View style={{ flexDirection: 'column', width: '80%' }}>
                <View
                  style={{
                    flexDirection: 'row-reverse',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <Text
                    style={{
                      fontWeight: '200',
                      fontSize: 13,
                      marginRight: 5,
                      textAlign: 'right',
                    }}>
                    العنوان
                  </Text>
                  <Text
                    style={{ fontWeight: '200', fontSize: 13, marginRight: 5 }}>
                    {' '}
                    :
                  </Text>
                  <Text
                    style={{
                      fontWeight: '200',
                      fontSize: 13,
                      marginLeft: 5,
                      width: DEVICE_WIDTH - 100,
                      textAlign: 'right',
                    }}
                    numberOfLines={2}>
                    {this.props.data.destination_address}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row-reverse',
                    paddingLeft: 10,
                    paddingRight: 10,
                  }}>
                  <Text
                    style={{
                      fontWeight: '200',
                      fontSize: 13,
                      marginRight: 5,
                      textAlign: 'right',
                    }}>
                    الهاتف
                  </Text>
                  <Text
                    style={{ fontWeight: '200', fontSize: 13, marginRight: 5 }}>
                    {' '}
                    :
                  </Text>
                  <Text
                    style={{
                      fontWeight: '200',
                      fontSize: 13,
                      marginLeft: 5,
                      width: DEVICE_WIDTH - 100,
                      textAlign: 'right',
                    }}
                    numberOfLines={2}>
                    {this.props.data.destination_phone}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row-reverse',
                  width: '20%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 10,
                }}>
                <Text style={{ fontSize: 7 }}>
                  {this.props.data.expected_delivary_time}
                </Text>
              </View>
            </View>
          )}

          {this.props.data.status !== '7' && (
            <View
              style={{
                flexDirection: 'row-reverse',
                paddingLeft: 10,
                paddingRight: 10,
                justifyContent: 'space-between',
                marginBottom: 5,
                marginTop: 10,
              }}>
              <TouchableOpacity
                activeOpacity={0.9}
                style={{
                  backgroundColor: '#0599ea',
                  borderRadius: 5,
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 40,
                }}
                onPress={() => {
                 
                  if (
                    '' + this.props.basket === '0' ||
                    '' + this.props.basket === 'NAN'
                  ) {
                 
if(this.props.data.type==='0'){
                    this.props.parentFlatList.sendtodelivery(
                      this.props.data.id
                    );
}else{
  this.props.parentFlatList.updateVendorStateForSpecialOrder(
    this.props.data.id,this.props.basket
    );
}
                  } else {
                   

                    // if (
                    //   this.sameDay(
                    //     new Date(),
                    //     new Date(parseFloat(this.props.basket))
                    //   )
                    // ) {
                      if(this.props.data.type==='0'){

                      this.props.parentFlatList.sendtodelivery(
                        this.props.data.id
                      );
                      }else{
                        this.props.parentFlatList.updateVendorStateForSpecialOrder(
                          this.props.data.id,this.props.basket
                        );
                      }
                    // } else {
                    //   debugger;

                    //   Toast.showWithGravity(
                    //     'لا يمكنك إجراء أي تحديث حتى تاريخ التسليم المتوقع ',
                    //     Toast.LONG,
                    //     Toast.BOTTOM
                    //   );
                    // }
                  }
                }}>
                <Text style={{ color: 'white', textAlign: 'center' }}>
                  أرسل للتسليم
                </Text>
              </TouchableOpacity>
              {/* 
<TouchableOpacity   activeOpacity={.9} style={{backgroundColor:'green',borderRadius:5,width:'30%',  justifyContent:'center',alignItems:'center',height: 40,}}
         onPress={() =>
          { 
            if((''+this.props.basket==='0'||''+this.props.basket==='NAN'))
            {
              this.props.parentFlatList.confirmorder(this.props.data.id);
              
            }else{
          if(this.sameDay( new Date,new Date(parseFloat(this.props.basket)))){

          this.props.parentFlatList.confirmorder(this.props.data.id)}else{
            Toast.showWithGravity('لا يمكنك إجراء أي تحديث حتى تاريخ التسليم المتوقع ', Toast.LONG, Toast.BOTTOM);

          }
        }
      }
        }
      >

<Text style={{color:'white',textAlign:'center'}}> تم التسليم </Text>
      </TouchableOpacity> */}
              {/* <TouchableOpacity   activeOpacity={.9}  style={{backgroundColor:'red',borderRadius:5,width:'30%',  justifyContent:'center',alignItems:'center',height: 40,}}
         onPress={() =>{ 


          if((''+this.props.basket==='0'||''+this.props.basket==='NAN'))
{
  this.props.parentFlatList.failOrder(this.props.data.id)

}else{
if(this.sameDay( new Date,new Date(parseFloat(this.props.basket)))){

          this.props.parentFlatList.failOrder(this.props.data.id)
          }else{
            Toast.showWithGravity('لا يمكنك إجراء أي تحديث حتى تاريخ التسليم المتوقع ', Toast.LONG, Toast.BOTTOM);

          }
        }
        }}
      >
    <Text style={{color:'white',textAlign:'center'}}>تعذر التسليم 
</Text>

      </TouchableOpacity>
      */}
            </View>
          )}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#F5FCFF',
  },

  iconStyle: {
    width: 30,
    height: 30,
    justifyContent: 'flex-end',
    alignItems: 'center',
    tintColor: '#fff',
  },

  sub_Category_Text: {
    fontSize: 18,
    color: '#000',
    padding: 10,
  },

  category_Text: {
    textAlign: 'left',
    color: '#fff',
    fontSize: 21,
    padding: 10,
  },

  category_View: {
    marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#0091EA',
  },

  Btn: {
    padding: 10,
    backgroundColor: '#FF6F00',
  },
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC',
  },
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  stepLabel: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#999999',
  },
  stepLabelSelected: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#4aae4f',
  },
  seprator: {
    height: 0.5,
    marginTop: 4,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#CCCCCC',
  },
});
