import React, { useEffect, useState, useRef } from 'react';
import { ImageBackground, View, Image, Text, Alert } from 'react-native';
import { ProgressBar } from 'react-native-paper';
import moment from 'moment';
let emaill, passwordd;
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
import {
  LoginUserAction,
  GetUserDetailsAction,
  UpdateAvailabiltyAction,
  UpdateVendorStateAction,
} from '../actions/Actions';
let country = 'الإمارات العربية المتحدة';
let currencyid = '1';
let currency = 'درهم اماراتي';
let email, password, devicetoken;
let firstlogin = true;
let getuserdata = false;
export default function LoadingScreen(props) {
  const dispatch = useDispatch();
  // const [clickable, setclickable] = useState(false);
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const [selected, setselected] = useState('');
  const [progress, setprogress] = useState(0.1);

  let bg = require('../imgs/bg.png');

  const LoginReducer = useSelector((state) => state.LoginReducer);
  const VendorDetailsReducer = useSelector(
    (state) => state.VendorDetailsReducer
  );
  const checkdayisopenday = (day) => {
    let weeksid = [];

    if (
      VendorDetailsReducer.payload.vendor.work_days === null ||
      VendorDetailsReducer.payload.vendor.work_days.length === 0 ||
      (VendorDetailsReducer.payload.vendor.work_days > 0 &&
        VendorDetailsReducer.payload.vendor.work_days[0] === 'undefined')
    ) {
      vendorisopen = true;
      setselected('updatevendorstate');

      dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 1));
      dispatch(UpdateAvailabiltyAction(1));
    } else {
      for (
        let s = 0;
        s < VendorDetailsReducer.payload.vendor.work_days.length;
        s++
      ) {
        if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الأحد'
        ) {
          weeksid.push(0);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الإثنين'
        ) {
          weeksid.push(1);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
          'الثلاثاء'
        ) {
          weeksid.push(2);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
          'الأربعاء'
        ) {
          weeksid.push(3);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الخميس'
        ) {
          weeksid.push(4);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الجمعة'
        ) {
          weeksid.push(5);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'السبت'
        ) {
          weeksid.push(6);
        }
      }

      if (weeksid.includes(day)) {
        vendorisopen = false;
        setselected('updatevendorstate');

        dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 3));
        dispatch(UpdateAvailabiltyAction(3));
      } else {
        vendorisopen = true;
        setselected('updatevendorstate');

        dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 1));
        dispatch(UpdateAvailabiltyAction(1));
      }
    }
  };
  if (selected === 'login') {
    if (
      typeof LoginReducer.payload !== 'undefined' &&
      typeof LoginReducer.payload.status !== 'undefined'
    ) {
      if (LoginReducer.payload.status === '2') {
        Toast.showWithGravity(
          'حسابك غير نشط يرجى الاتصال بالإدارة  ',
          Toast.LONG,
          Toast.BOTTOM
        );
        setselected('');
      } else if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error === 'success' &&
        getuserdata === false
      ) {
        if ('' + LoginReducer.payload.data.type === '2') {
          AsyncStorage.setItem('loggedin', 'true').then(() => {
            AsyncStorage.setItem('email', emaill).then(() => {
              AsyncStorage.setItem('password', '' + passwordd).then(() => {
                AsyncStorage.setItem(
                  'name',
                  LoginReducer.payload.data.name
                ).then(() => {
                  AsyncStorage.setItem(
                    'token',
                    LoginReducer.payload.token.token
                  ).then(() => {
                    AsyncStorage.setItem(
                      'id',
                      '' + LoginReducer.payload.data.id
                    ).then(() => {
                      let id = LoginReducer.payload.data.id;
                      setselected('getvendordetails');

                      if (getuserdata === false) {
                        dispatch(GetUserDetailsAction(id, currencyid));
                        getuserdata = true;
                      }
                    });
                  });
                });
              });
            });
          });
        } else {
          setemail('');
          setpassword('');
          setselected('');
          Toast.showWithGravity(
            'هذا المستخدم ليس بائعًا  ',
            Toast.LONG,
            Toast.BOTTOM
          );
        }
      }
    } else {
      if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error !== 'success'
      ) {
        if (
          typeof LoginReducer.error.response !== 'undefined' &&
          typeof LoginReducer.error.response.data !== 'undefined' &&
          typeof LoginReducer.error.response.data.message !== 'undefined' &&
          LoginReducer.error.response.data.message === 'password is wrong'
        ) {
          Toast.showWithGravity('كلمة السر خاطئة', Toast.SHORT, Toast.BOTTOM);

          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('name', 'null').then(() => {
                AsyncStorage.setItem('password', 'null').then(() => {
                  AsyncStorage.setItem('token', 'null').then(() => {
                    AsyncStorage.setItem('id', 'null').then(() => {
                      setselected('');
                      setemail('');
                      setpassword('');
                    });
                  });
                });
              });
            });
          });
        } else if (
          typeof LoginReducer.error.response !== 'undefined' &&
          typeof LoginReducer.error.response.data !== 'undefined' &&
          typeof LoginReducer.error.response.data.message !== 'undefined'
        ) {
          Toast.showWithGravity(
            'بريد إلكتروني خاطئ',
            Toast.SHORT,
            Toast.BOTTOM
          );

          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('password', 'null').then(() => {
                AsyncStorage.setItem('token', 'null').then(() => {
                  AsyncStorage.setItem('id', 'null').then(() => {
                    setemail('');
                    setpassword('');
                    setselected('');
                  });
                });
              });
            });
          });
        } else if (LoginReducer.error !== '') {
          setselected('');

          // Toast.showWithGravity(LoginReducer.error.message, Toast.SHORT, Toast.BOTTOM)
        } else {
          //   setselectedd('')
          //   setInterval(() => {
          //     setspinner(false)
          //   }, 1000);
        }
      }
    }
  } else if (selected === 'getvendordetails') {
 
    if (
      typeof VendorDetailsReducer !== 'undefined' &&
      typeof VendorDetailsReducer.payload !== 'undefined' &&
      VendorDetailsReducer.error === 'success'
    ) {
      if (firstlogin) {
        firstlogin = false;
        var current = new Date();
        var c = current.getTime();

        var day = current.getDay();
        let from = VendorDetailsReducer.payload.vendor.work_hours[0].start;
        let to = VendorDetailsReducer.payload.vendor.work_hours[0].end;

        // let durationfrom = moment.duration(from, 'milliseconds');
        // let durationto = moment.duration(to, 'milliseconds');

        var now = moment.utc();
        var hour = now.hour();
        var min = now.minutes();

        let curremntmillic = moment
          .utc([2010, 1, 14, hour, min, 0, 0])
          .valueOf();

        //  let  startt = setDateTime(new Date(current), from);
        //  let  endd = setDateTime(new Date(current), to);

        if (
          typeof LoginReducer !== 'undefined' &&
          typeof LoginReducer.payload !== 'undefined'
        ) {

          if(from.includes('صباحا'))
          {
        

           from= from.replace('صباحا','') 
        

          }
          if(to.includes('صباحا'))
          {
      

            to=to.replace('صباحا','') 
          }
          if(from.includes('مساء'))
          {
        

            from=from.replace('مساء','') 
          }
          if(to.includes('مساء'))
          {
           to= to.replace('مساء','') 
        
          }

        
          if (curremntmillic > from && curremntmillic < to) {
            //  vendorisopen=true;
            //     setselectedd('updatevendorstate')

            //     dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token,2));
            checkdayisopenday(day);
          } else {
            vendorisopen = false;
            setselected('updatevendorstate');
            dispatch(
              UpdateVendorStateAction(LoginReducer.payload.token.token, 3)
            );
            dispatch(UpdateAvailabiltyAction(3));
          }
        }
      }
    } else {
      // Toast.showWithGravity('errorr happen,please try loginn again  ', Toast.LONG, Toast.BOTTOM)
    }
  } else if (selected === 'updatevendorstate') {
 
    setselected('');
    props.navigation.navigate('HomeSeller', {
      token: LoginReducer.payload.token.token,
      stateavailable: vendorisopen,
      id: LoginReducer.payload.data.id,
      currencyid: currencyid,
      currency: currency,
    });
    // props.navigation.navigate('AppNavigationDefault', {
    //   token: LoginReducer.payload.token.token,
    //   stateavailable: vendorisopen,
    //   id: LoginReducer.payload.data.id,
    //   currencyid: currencyid,
    //   currency: currency,
    // });
  }

  const loadApp = async () => {
    try {
      emaill = await AsyncStorage.getItem('email');
      passwordd = await AsyncStorage.getItem('password');

      setselected('login');
      devicetoken = await AsyncStorage.getItem('devicetoken');

      if (devicetoken === null) {
        devicetoken = 'null';
      }

      if (emaill !== null && passwordd !== null) {
        dispatch(LoginUserAction(emaill.trim(), passwordd, devicetoken));
      } else {
        try {
          props.navigation.navigate('AppNavigation', {});
        } catch (ee) {}
      }
    } catch (error) {}
  };
  useEffect(() => {
    loadApp();
  }, []);
  useEffect(() => {
    timeoutHandle = setTimeout(() => {
      let newp = parseFloat(progress + progress);
      setprogress(newp);
    }, 100);
  }, [progress]);

  return (
    <ImageBackground
      style={{
        height: '100%',
        width: '100%',
      }}
      source={bg}>
      <View
        style={{
          flexDirection: 'column',
          marginVertical: '10%',
          width: '100%',
          justifyContent: 'center',

          height: '80%',
        }}>
        <Image
          style={{
            width: '100%',
            height: 150,
          }}
          resizeMode='contain'
          source={require('../imgs/logoooo.png')}></Image>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 22,
            color: '#00c4df',
            marginTop: -10,
          }}>
          Waddelly
        </Text>
        <ProgressBar progress={progress} style={{ margin: 40 }} />
      </View>
    </ImageBackground>
  );
}
