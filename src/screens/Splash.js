import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  View,
  Image,
  StyleSheet,
  Text,
  NativeModules,
  Platform,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import { Dialog } from 'react-native-simple-dialogs';
import FastImage from 'react-native-fast-image';
import AsyncStorage from '@react-native-async-storage/async-storage';
import logoImg from '../imgs/logoo.gif';
import { GetCategoriesSubCategoriesLabelsAction } from '../actions/Actions';
import { GetLabelsAction } from '../actions/Actions';
// import {UpdateCurrencyAction} from '../actions/Actions'
import { GetAppVersion } from '../actions/Actions';
import { GetCountriesCitiesCounranciesAction } from '../actions/Actions';
import VersionCheck from 'react-native-version-check';
let kk = true;
let loggedinh = null,
  FF = null;
export default function Splash(props) {
  const dispatch = useDispatch();
  const [isNeeded, setisNeeded] = useState(false);
  const [currentVersion, setcurrentVersion] = useState(null);

  const [updateversionneeded, setupdateversionneeded] = useState(null);
  const GetVersionReducer = useSelector((state) => state.GetVersionReducer);

  if (updateversionneeded === null) {
    if (
      typeof GetVersionReducer !== 'undefined' &&
      GetVersionReducer.error !== ''
    ) {
      let v = '1.0';

      if (Platform.OS === 'ios') {
        v = GetVersionReducer.payload.data.ios_vendor;
      } else {
        v = GetVersionReducer.payload.data.android_vendor;
      }

      VersionCheck.needUpdate({
        latestVersion: v,
      }).then((res) => {
        if (
          typeof res !== 'undefined' &&
          updateversionneeded === null &&
          kk === true
        ) {
          setcurrentVersion(res.currentVersion);
          setisNeeded(res.isNeeded);

          if (res.isNeeded) {
            kk = false;

            setupdateversionneeded(true);
            // open store if update is needed.
          } else {
            kk = false;

            setupdateversionneeded(false);
          }

          setcurrentVersion(res.currentVersion);
          setisNeeded(res.isNeeded);

          if (res.isNeeded) {
            setupdateversionneeded(true);
            // open store if update is needed.
          } else {
            setupdateversionneeded(false);
          }
        }
      });
    }
  } else if (updateversionneeded === false) {
  }
  const loadApp = async () => {
    try {
      loggedinh = await AsyncStorage.getItem('loggedin');

      let FFd = await AsyncStorage.getItem('first');
      if (FFd !== null) {
        FF = FFd;
      }

      // setloggedin(loggedinh);
    } catch (e) {}
  };
  useEffect(() => {
    loadApp();
    timeoutHandle = setTimeout(() => {
      dispatch(GetAppVersion());

      // dispatch(GetCurrenciesAction());
      dispatch(GetCountriesCitiesCounranciesAction());

      dispatch(GetLabelsAction());

      dispatch(GetCategoriesSubCategoriesLabelsAction());

      if (loggedinh === 'true') {
        props.navigation.navigate('AppNavigationDefault', {});
      } else if (loggedinh === 'false' || FF === null) {
        props.navigation.navigate('AppNavigation', {});
      }
    }, 1500);

    let c = '5';
    let d = 'درهم اماراتي';

    // dispatch(UpdateCurrencyAction(d,c));
  }, []);

  return (
    <View
      style={{
        backgroundColor: 'white',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <FastImage
        style={{
          alignSelf: 'center',
          height: '100%',
          width: '100%',
        }}
        source={logoImg}
        resizeMode={FastImage.resizeMode.cover}
        Priority={FastImage.priority.high}
      />

      <Dialog
        visible={updateversionneeded === true}
        title={'تحديث التطبيق'}
        titleStyle={{ color: 'red' }}
        onTouchOutside={() => {}}>
        <View style={{ flexDirection: 'column-reverse' }}>
          <TouchableOpacity
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              backgroundColor: 'red',
              padding: 20,
            }}
            onPress={() => {
              setupdateversionneeded(null);
              // Linking.openURL('https://www.google.com');
              // if(Platform.OS==='ios'){
              let appStoreId = '529379082';
              let appName = 'waddely';
              let appStoreLocale = 'us';
              let playStoreId = 'tobeapps.com.waddely';

              AppLink.openInStore({
                appName,
                appStoreId,
                appStoreLocale,
                playStoreId,
              })
                .then(() => {
                  // do stuff
                })
                .catch((err) => {
                  // handle error
                });
              // }else{

              // // Linking.openURL("market://details?id=<package_name>");

              // }
            }}>
            <Text>{'تحديث'}</Text>
          </TouchableOpacity>
          <Text style={{ textAlign: 'center', padding: 30 }}>
            {'تحتاج إلى تحديث التطبيق الخاص بك من المتجر'}
          </Text>
        </View>
      </Dialog>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
