import React, { useEffect, useState, useRef } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  TouchableHighlight,
  TextInput,
  Animated,
  Platform,
  NativeModules,
  Dimensions,
  ImageBackground,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import moment from 'moment';
let header = require('../../imgs/registerheader.png');
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import Icond from 'react-native-vector-icons/FontAwesome5';
import ActionSheet from 'react-native-actions-sheet';
let vendorisopen = false;
let loadd = true;
const DEVICE_width = Dimensions.get('window').width;
const DEVICE_hight = Dimensions.get('window').height;
const MARGIN = 60;
import SwipeablePanel from '../../common/actualComponents/Panel';
import SwipeablePanels from '../../common/actualComponents/Panels';

import SwipeablePanelm from '../../common/actualComponents/Panelm';
import BottomSheet from 'reanimated-bottom-sheet';

import SelectableFlatlist, { STATE } from 'react-native-selectable-flatlist';
import SelectableGrid from '../../common/selectablegrid/SelectableGrid';

let deviceLocale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;
if (
  typeof deviceLocale !== 'undefined' &&
  deviceLocale !== null &&
  deviceLocale.includes('ar')
) {
  deviceLocale = 'ar';
} else {
  deviceLocale = 'en';
}
let holidays = [
  { label_name_ar: 'السبت' },
  { label_name_ar: 'الأحد' },
  { label_name_ar: 'الإثنين' },
  { label_name_ar: 'الثلاثاء' },
  { label_name_ar: 'الأربعاء' },
  { label_name_ar: 'الخميس' },
  { label_name_ar: 'الجمعة' },
];
// let subcitylisttt1=[];
// let subcitylisttt2=[];
// let subcitylisttt3=[];
// let subcitylisttt4=[];
// let subcitylisttt5=[];
// let subcitylisttt6=[];
// let subcitylisttt7=[];
// let subcitylisttt8=[];
// let subcitylisttt9=[];
// let subcitylisttt10=[];
// let subcitylisttt11=[];
// let subcitylisttt12=[];
// let subcitylisttt13=[];
// let subcitylisttt14=[];
// let subcitylisttt15=[];
// let subcitylisttt16=[];
// let subcitylisttt17=[];
// let subcitylisttt18=[];
// let subcitylisttt19=[];
// let subcitylisttt20=[];
let subcitylist=[];
import TagsViewSelectionClickablelogin from '../../common/Tags/TagsViewSelectionClickablelogin';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import { UpdateVendorStateAction } from '../../actions/Actions';
import { UpdateAvailabiltyAction } from '../../actions/Actions';
import { showMessage, hideMessage } from "react-native-flash-message";

import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
import { LoginUserAction } from '../../actions/Actions';
import { GetUserDetailsAction } from '../../actions/Actions';
import { RegisterAction } from '../../actions/Actions';
import { ForgetPasswordFirstAction } from '../../actions/Actions';
import { ForgetPasswordSecondAction } from '../../actions/Actions';
import AppMetrics from '../../common/metrics';
let country = 'الإمارات العربية المتحدة';
let currencyid = '1';
let currency = 'درهم اماراتي';
// import DateTimePicker from '@react-native-community/datetimepicker';
let opened = false;
let d = 'nulll';
let enn = true;
export default function LoginScreen(props) {
  const sheetRef = useRef(null);
  const [sheetIsOpen, setSheetIsOpen] = useState(false);

  const dispatch = useDispatch();

  //1 - DECLARE VARIABLES
  const [selectedd, setselectedd] = useState('');
  const [animation, setanimation] = useState(new Animated.Value(0));
  const [clickable, setclickable] = useState(false);
  const [
    swipeablePanelActivecategory,
    setswipeablePanelActivecategory,
  ] = useState(false);
  const [spinner, setspinner] = useState(false);
  const [tags, settags] = useState([]);
  const [srcimage, setsrcimage] = useState(require('../../imgs/defaultt.png'));
  const [name, setname] = useState('');
  const [phone, setphone] = useState('');
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const [address, setaddress] = useState('');
  const [code, setcode] = useState('');
  const [, forceUpdate] = React.useState(0);

  const [devicetoken, setdevicetoken] = useState('null');
  const [deliverysubcity20, setdeliverysubcity20] = useState('الكل');
  const [deliverycity20, setdeliverycity20] = useState('المدينة');
  const [cityid20, setcityid20] = useState(null);
  const [subcityid20, setsubcityid20] = useState('الكل');
  const [deliverysubcity19, setdeliverysubcity19] = useState('الكل');
  const [deliverycity19, setdeliverycity19] = useState('المدينة');
  const [cityid19, setcityid19] = useState(null);
  const [subcityid19, setsubcityid19] = useState('الكل');
  const [deliverysubcity18, setdeliverysubcity18] = useState('الكل');
  const [deliverycity18, setdeliverycity18] = useState('المدينة');
  const [cityid18, setcityid18] = useState(null);
  const [subcityid18, setsubcityid18] = useState('الكل');
  const [deliverysubcity17, setdeliverysubcity17] = useState('الكل');
  const [deliverycity17, setdeliverycity17] = useState('المدينة');
  const [cityid17, setcityid17] = useState(null);
  const [subcityid17, setsubcityid17] = useState('الكل');

  const [deliverysubcity16, setdeliverysubcity16] = useState('الكل');
  const [deliverycity16, setdeliverycity16] = useState('المدينة');
  const [cityid16, setcityid16] = useState(null);
  const [subcityid16, setsubcityid16] = useState('الكل');

  const [deliverysubcity15, setdeliverysubcity15] = useState('الكل');
  const [deliverycity15, setdeliverycity15] = useState('المدينة');
  const [cityid15, setcityid15] = useState(null);
  const [subcityid15, setsubcityid15] = useState('الكل');
  const [deliverysubcity14, setdeliverysubcity14] = useState('الكل');
  const [deliverycity14, setdeliverycity14] = useState('المدينة');
  const [cityid14, setcityid14] = useState(null);
  const [subcityid14, setsubcityid14] = useState('الكل');
  const [deliverysubcity13, setdeliverysubcity13] = useState('الكل');
  const [deliverycity13, setdeliverycity13] = useState('المدينة');
  const [cityid13, setcityid13] = useState(null);
  const [subcityid13, setsubcityid13] = useState('الكل');
  const [deliverysubcity12, setdeliverysubcity12] = useState('الكل');
  const [deliverycity12, setdeliverycity12] = useState('المدينة');
  const [cityid12, setcityid12] = useState(null);
  const [subcityid12, setsubcityid12] = useState('الكل');
  const [deliverysubcity11, setdeliverysubcity11] = useState('الكل');
  const [deliverycity11, setdeliverycity11] = useState('المدينة');
  const [cityid11, setcityid11] = useState(null);
  const [subcityid11, setsubcityid11] = useState('الكل');
  const [deliverysubcity10, setdeliverysubcity10] = useState('الكل');
  const [deliverycity10, setdeliverycity10] = useState('المدينة');
  const [cityid10, setcityid10] = useState(null);
  const [subcityid10, setsubcityid10] = useState('الكل');
  const [deliverysubcity9, setdeliverysubcity9] = useState('الكل');
  const [deliverycity9, setdeliverycity9] = useState('المدينة');
  const [cityid9, setcityid9] = useState(null);
  const [subcityid9, setsubcityid9] = useState('الكل');
  const [deliverysubcity8, setdeliverysubcity8] = useState('الكل');
  const [deliverycity8, setdeliverycity8] = useState('المدينة');
  const [cityid8, setcityid8] = useState(null);
  const [subcityid8, setsubcityid8] = useState('الكل');
  const [deliverysubcity7, setdeliverysubcity7] = useState('الكل');
  const [deliverycity7, setdeliverycity7] = useState('المدينة');
  const [cityid7, setcityid7] = useState(null);
  const [subcityid7, setsubcityid7] = useState('الكل');
  const [deliverysubcity6, setdeliverysubcity6] = useState('الكل');
  const [deliverycity6, setdeliverycity6] = useState('المدينة');
  const [cityid6, setcityid6] = useState(null);
  const [subcityid6, setsubcityid6] = useState('الكل');
  const [deliverysubcity5, setdeliverysubcity5] = useState('الكل');
  const [deliverycity5, setdeliverycity5] = useState('المدينة');
  const [cityid5, setcityid5] = useState(null);
  const [subcityid5, setsubcityid5] = useState('الكل');
  const [deliverysubcity4, setdeliverysubcity4] = useState('الكل');
  const [deliverycity4, setdeliverycity4] = useState('المدينة');
  const [cityid4, setcityid4] = useState(null);
  const [subcityid4, setsubcityid4] = useState('الكل');
  const [deliverysubcity3, setdeliverysubcity3] = useState('الكل');
  const [deliverycity3, setdeliverycity3] = useState('المدينة');
  const [cityid3, setcityid3] = useState(null);
  const [subcityid3, setsubcityid3] = useState('الكل');
  const [deliverysubcity2, setdeliverysubcity2] = useState('الكل');
  const [deliverycity2, setdeliverycity2] = useState('المدينة');
  const [cityid2, setcityid2] = useState(null);
  const [subcityid2, setsubcityid2] = useState('الكل');
  const [deliverysubcity1, setdeliverysubcity1] = useState('الكل');
  const [deliverycity1, setdeliverycity1] = useState('المدينة');
  const [cityid1, setcityid1] = useState(null);
  const [subcityid1, setsubcityid1] = useState('الكل');
  const [selecteddd, setselecteddd] = useState('000');

  const [count, setcount] = useState(0);
  const [subcitylarge, setsubcitylarge] = useState(false);
  const [newpassword, setnewpassword] = useState('');
  const [forgetemail, setforgetemail] = useState('');
  const [labelfirst, setlabelfirst] = useState(true);
  const [holidayfirst, setholidayfirst] = useState(true);
  const [imagebaseprofile, setimagebaseprofile] = useState('');
  const [firstlogin, setfirstlogin] = useState(false);
  const [selectedlabels, setselectedlabels] = useState([]);
  const [selectedholidays, setselectedholidays] = useState([]);
  const [subcityswipable, setsubcityswipable] = useState(false);
  const [cityswipable, setcityswipable] = useState(false);

  const [fullheighttt, setfullheighttt] = useState(0);

  const [categoryname, setcategoryname] = useState('اختر فئة المحل ');

  const [subcityselected, setsubcityselected] = useState('المنطقة');
  const [cityselected, setcityselected] = useState('المدينة');
  const [categoryid, setcategoryid] = useState(1);

  const [start, setstart] = useState('صباحا');
  const [startclock, setstartclock] = useState('');
  const [startclocktext, setstartclocktext] = useState('');

  const [startclockid, setstartclockid] = useState(2);

  const [end, setend] = useState('مساء');
  const [endclock, setendclock] = useState('');
  const [endclocktext, setendclocktext] = useState('');

  const [endclockid, setendclockid] = useState(2);

  const [swipeablePanelendhour, setswipeablePanelendhour] = useState(false);
  const [swipeablePanelstarthour, setswipeablePanelstarthour] = useState(false);

  const [amcolors, setamcolors] = useState('white');
  const [amsizes, setamsizes] = useState(20);
  const [amweights, setamweights] = useState('bold');
  const [ambacks, setambacks] = useState('#0599ea');

  const [pmcolors, setpmcolors] = useState('grey');
  const [pmsizes, setpmsizes] = useState(17);
  const [pmweights, setpmweights] = useState('normal');
  const [pmbacks, setpmbacks] = useState('white');

  const [amcolor, setamcolor] = useState('grey');
  const [amsize, setamsize] = useState(17);
  const [amweight, setamweight] = useState('normal');
  const [amback, setamback] = useState('white');

  const [pmcolor, setpmcolor] = useState('white');
  const [pmsize, setpmsize] = useState(20);
  const [pmweight, setpmweight] = useState('bold');
  const [pmback, setpmback] = useState('#0599ea');

  const [citylist, setcitylist] = useState([]);
  const [cityid, setcityid] = useState(null);
  const [subcityid, setsubcityid] = useState(null);
  const [subcityidd, setsubcityidd] = useState(null);

  const [deliverycount, setdeliverycount] = useState(0);

  const GetCategoriesReducer = useSelector(
    (state) => state.GetCategoriesReducer
  );
  const LoginReducer = useSelector((state) => state.LoginReducer);
  const RegisterReducer = useSelector((state) => state.RegisterReducer);
  const VendorDetailsReducer = useSelector(
    (state) => state.VendorDetailsReducer
  );
  const GetCountriesReducer = useSelector((state) => state.GetCountriesReducer);
  const ForgetPasswordFirstReducer = useSelector(
    (state) => state.ForgetPasswordFirstReducer
  );
  const ForgetPasswordSecondReducer = useSelector(
    (state) => state.ForgetPasswordSecondReducer
  );
  const actionSheetRef = useRef();
  const showModalVisible = () => {
    actionSheetRef.current?.setModalVisible(true);
  };
  const hideModalVisible = () => {
    actionSheetRef.current?.setModalVisible(false);
  };

  actionSheetRef.current?.snapToOffset(300);

try{

  if (enn) {
    if (
      typeof GetCountriesReducer !== 'undefined' &&
      typeof GetCountriesReducer.error !== '' &&
      GetCountriesReducer.error == 'success'
    ) {
      enn=false;
   
      if (
        typeof GetCountriesReducer.payload !== 'undefined' &&
        typeof GetCountriesReducer.payload.data !== 'undefined'
&&enn ) {
        let citylist = [];

        for (let f = 0; f < GetCountriesReducer.payload.data.length; f++) {
          if (GetCountriesReducer.payload.data[f].country_name_ar === 'الإمارات العربية المتحدة') {
            if (GetCountriesReducer.payload.data[f].cities.length > 0) {
              for (
                let d = 0;
                d < GetCountriesReducer.payload.data[f].cities.length;
                d++
              ) {
                citylist.push({
                  id: d + 1,
                  name:
                    GetCountriesReducer.payload.data[f].cities[d].city_name_ar,
                  idd: GetCountriesReducer.payload.data[f].cities[d].id,
                });
              }
            }
          }
        }
        setcitylist(citylist);
        enn = false;
        return citylist;
      }
    }
  }
}catch(ee)
{
  
}

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <View style={styles.panelHandle} />
      </View>
    );
  };

  const renderContent = () => (
    <View
      style={{
        flexDirection: 'column',
        height: DEVICE_hight / 3,
        backgroundColor: 'white',
      }}>
      {(Platform.OS === 'ios' ||
        (Platform.OS === 'android' && deviceLocale === 'en')) && (
        <View
          style={{
            flexDirection: 'column',
            backgroundColor: 'white',
          }}>
          {count === 0 && (
            <Text style={{ textAlign: 'right', margin: 10, fontSize: 17 }}>
              اكتب بريدك الإلكتروني
            </Text>
          )}
          {count === 0 && (
            <View style={styles.inputWrapper}>
              <TextInput
                style={styles.input}
                placeholder={'البريد الإلكتروني'}
                autoCapitalize={'none'}
                // autoFocus={fff}
                returnKeyType={'next'}
                autoCorrect={false}
                onChangeText={(email) => {
                  setforgetemail(email);
                  // or some other action

                  // setKeyboardVisible(true);
                }}
                value={forgetemail}
                secureTextEntry={false}
                placeholderTextColor='grey'
                underlineColorAndroid='transparent'
              />
            </View>
          )}
          {count === 1 && (
            <View>
              <View style={{ height: 20 }} />
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.input}
                  placeholder={' رمز التأكيد'}
                  autoCapitalize={'none'}
                  returnKeyType={'next'}
                  autoCorrect={false}
                  onChangeText={(code) => {
                    setcode(code);
                  }}
                  value={code}
                  secureTextEntry={false}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
              <View style={styles.inputWrapper}>
                <TextInput
                  secureTextEntry={true}
                  placeholder={'كلمة المرور الجديدة '}
                  returnKeyType={'done'}
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  secureTextEntry
                  onChangeText={(newpassword) => {
                    setnewpassword(newpassword);
                  }}
                  value={newpassword}
                  style={styles.input}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
            </View>
          )}
        </View>
      )}

      {Platform.OS === 'android' && deviceLocale === 'ar' && (
        <View
          style={{
            flexDirection: 'column',
            backgroundColor: 'white',
          }}>
          {count === 0 && (
            <Text style={{ textAlign: 'left', margin: 10, fontSize: 17 }}>
              اكتب بريدك الإلكتروني
            </Text>
          )}
          {count === 0 && (
            <View style={styles.inputWrapper}>
              <TextInput
                style={styles.input}
                placeholder={'البريد الإلكتروني'}
                autoCapitalize={'none'}
                // autoFocus={fff}
                returnKeyType={'next'}
                autoCorrect={false}
                onChangeText={(email) => {
                  setforgetemail(email);
                  // or some other action

                  // setKeyboardVisible(true);
                }}
                value={forgetemail}
                secureTextEntry={false}
                placeholderTextColor='grey'
                underlineColorAndroid='transparent'
              />
            </View>
          )}
          {count === 1 && (
            <View>
              <View style={{ height: 20 }} />
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.input}
                  placeholder={' رمز التأكيد'}
                  autoCapitalize={'none'}
                  returnKeyType={'next'}
                  autoCorrect={false}
                  onChangeText={(code) => {
                    setcode(code);
                  }}
                  value={code}
                  secureTextEntry={false}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
              <View style={styles.inputWrapper}>
                <TextInput
                  secureTextEntry={true}
                  placeholder={'كلمة المرور الجديدة '}
                  returnKeyType={'done'}
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  secureTextEntry
                  onChangeText={(newpassword) => {
                    setnewpassword(newpassword);
                  }}
                  value={newpassword}
                  style={styles.input}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
            </View>
          )}
        </View>
      )}
      <LinearGradient
        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
        style={{
          borderRadius: 30,
          width: '50%',
          marginHorizontal: '25%',
          textAlign: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          height: 40,
        }}>
        <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
          style={{
            textAlign: 'center',
            alignItems: 'center',
          }}
          onPress={(_) => {
            if (count === 0) {
              if (forgetemail.trim().length > 0) {
                if (validateEmail(forgetemail.trim())) {
                  setselectedd('forgetpasswordfirst');

                  dispatch(ForgetPasswordFirstAction(forgetemail.trim()));

                  setspinner(true);
                } else {
                  Toast.showWithGravity(
                    'بريد إلكتروني خاطئ',
                    Toast.LONG,
                    Toast.BOTTOM
                  );
                }
              } else {
                Toast.showWithGravity(
                  'تحتاج إلى إضافة الحقول المفقودة',
                  Toast.LONG,
                  Toast.BOTTOM
                );
              }
            } else if (count === 1) {
              if (code.length > 0 && newpassword.length > 0) {
                if (newpassword.length >= 8) {
                  dispatch(
                    ForgetPasswordSecondAction(
                      forgetemail.trim(),
                      code,
                      newpassword
                    )
                  );

                  setspinner(true);

                  setselectedd('forgetpasswordsecond');
                } else {
                  Toast.showWithGravity(
                    'طول كلمة المرور غير صالحة',
                    Toast.LONG,
                    Toast.BOTTOM
                  );
                }
              } else {
                Toast.showWithGravity(
                  'تحتاج إلى إضافة الحقول المفقودة',
                  Toast.LONG,
                  Toast.BOTTOM
                );
              }
            }
          }}>
          <Text
            style={{
              color: 'white',
              paddingHorizontal: 20,
              fontSize: 20,

              textAlign: 'center',
            }}>
            {' '}
            إرسال
          </Text>
        </TouchableHighlight>
      </LinearGradient>
    </View>
  );
  const getsubcitylist = (country, cityid) => {
    let subcitylistinternal = [];

    if (
      typeof GetCountriesReducer.payload !== 'undefined' &&
      typeof GetCountriesReducer.payload.data !== 'undefined'
    ) {
      for (let f = 0; f < GetCountriesReducer.payload.data.length; f++) {
        if (GetCountriesReducer.payload.data[f].country_name_ar === country) {
          if (GetCountriesReducer.payload.data[f].subcities.length > 0) {
            for (
              let d = 0;
              d < GetCountriesReducer.payload.data[f].subcities.length;
              d++
            ) {
              if (
                GetCountriesReducer.payload.data[f].subcities[d].city_id ===
                cityid
              ) {

                subcitylistinternal.push({
                  id: d + 1,
                  name:
                    GetCountriesReducer.payload.data[f].subcities[d]
                      .subcity_name_ar,
                  idd: GetCountriesReducer.payload.data[f].subcities[d].id,
                });

              }
            }
          }
        }
      }
    }
    if (selecteddd === '1') {
      subcitylisttt1 = subcitylistinternal;
    } else if (selecteddd === '2') {
      subcitylisttt2 = subcitylistinternal;
    } else if (selecteddd === '3') {
      subcitylisttt3 = subcitylistinternal;
    } else if (selecteddd === '4') {
      subcitylisttt4 = subcitylist;
    } else if (selecteddd === '5') {
      subcitylisttt5 = subcitylist;
    } else if (selecteddd === '6') {
      subcitylisttt6 = subcitylistinternal;
    } else if (selecteddd === '7') {
      subcitylisttt7 = subcitylist;
    } else if (selecteddd === '8') {
      subcitylisttt8 = subcitylist;
    } else if (selecteddd === '9') {
      subcitylisttt9 = subcitylist;
    } else if (selecteddd === '10') {
      subcitylisttt10 = subcitylist;
    } else if (selecteddd === '11') {
      subcitylisttt11 = subcitylist;
    } else if (selecteddd === '12') {
      subcitylisttt12 = subcitylist;
    } else if (selecteddd === '13') {
      subcitylisttt13 = subcitylist;
    } else if (selecteddd === '14') {
      subcitylisttt14 = subcitylist;
    } else if (selecteddd === '15') {
      subcitylisttt15 = subcitylistinternal;
    } else if (selecteddd === '16') {
      subcitylisttt16 = subcitylistinternal;
    } else if (selecteddd === '17') {
      subcitylisttt17 = subcitylistinternal;
    } else if (selecteddd === '18') {
      subcitylisttt18 = subcitylistinternal;
    } else if (selecteddd === '19') {
      subcitylisttt19 = subcitylistinternal;
    } else if (selecteddd === '20') {
      subcitylisttt20 = subcitylistinternal;
    }
    subcitylist=subcitylistinternal;
    return subcitylistinternal;
  };

  selectlabelsss = (selectedd) => {
    setselectedlabels(selectedd);
    setswipeablePanelActivecategory(false);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setsubcityswipable(false);

    setcityswipable(false);
  };

  updtaefirstste = (s, kkkk) => {
    if (kkkk === 'categories') {
      setlabelfirst(false);
    } else {
      setholidayfirst(false);
    }
    setswipeablePanelActivecategory(false);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setsubcityswipable(false);
    setcityswipable(false);
  };

  selectholidaysss = (selecteddd) => {
    setselectedholidays(selecteddd);
    setswipeablePanelActivecategory(false);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setsubcityswipable(false);
    setcityswipable(false);
  };

  const handleClose = () => {
    setfullheighttt(0);
    opened = false;
    setdeliverycount(0);
    setforgetemail('');
    setcount(0);
    setcode('');
    setnewpassword('');

    Animated.timing(animation, {
      toValue: 0,
      duration: 200,
      useNativeDriver: true,
    }).start();
    setsrcimage(require('../../imgs/defaultt.png'));
    setemail('');
    setpassword('');
    setname('');
    setphone('');
    setselectedd('');
    setswipeablePanelActivecategory(false);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setsubcityswipable(false);
    setnewpassword('');
    setcode('');
    setforgetemail('');
    setcount(0);
    setcityswipable(false);
    setsubcityselected('المنطقة');
    setcityselected('المدينة');
    setcityid(null);
    setsubcityid(null);
  
    setsubcityidd(null);
    setimagebaseprofile('');
    setcategoryname('اختر فئة المحل ');
    setcategoryid(1);

    settags([]);
    holidays = [
      { label_name_ar: 'السبت' },
      { label_name_ar: 'الأحد' },
      { label_name_ar: 'الإثنين' },
      { label_name_ar: 'الثلاثاء' },
      { label_name_ar: 'الأربعاء' },
      { label_name_ar: 'الخميس' },
      { label_name_ar: 'الجمعة' },
    ];
    setselectedholidays([]);
    setselectedlabels([]);
    setstart('صباحا');
    setstartclock('');
    setstartclocktext('');
    setstartclockid(2);
    setend('مساء');
    setendclock('');
    setendclocktext('');
    setendclockid(2);
    setaddress('');
  };
  const validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };
  const setDateTime = (date, str) => {
    var sp = str.split(':');
    date.setHours(parseInt(sp[0], 10));
    date.setMinutes(parseInt(sp[1], 10));
    return date;
  };

  const getlabels = (catid) => {
    settags([]);
    if (catid !== null) {
      if (
        typeof GetCategoriesReducer !== 'undefined' &&
        typeof GetCategoriesReducer.payload !== 'undefined'
      ) {
        for (let i = 0; i < GetCategoriesReducer.payload.length; i++) {
          let t = [];

          if (GetCategoriesReducer.payload[i].id === catid) {
            for (
              let f = 0;
              f < GetCategoriesReducer.payload[i].label.length;
              f++
            ) {
              t.push(GetCategoriesReducer.payload[i].label[f]);
            }
          }

          if (t.length > 0) {
            settags(t);
          }
        }
      }
    }
  };

  const checkdayisopenday = (day) => {
    let weeksid = [];

    if (
      VendorDetailsReducer.payload.vendor.work_days === null ||
      VendorDetailsReducer.payload.vendor.work_days.length === 0 ||
      (VendorDetailsReducer.payload.vendor.work_days > 0 &&
        VendorDetailsReducer.payload.vendor.work_days[0] === 'undefined')
    ) {
      vendorisopen = true;
      setselectedd('updatevendorstate');

      dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 1));
      dispatch(UpdateAvailabiltyAction(1));
    } else {
      for (
        let s = 0;
        s < VendorDetailsReducer.payload.vendor.work_days.length;
        s++
      ) {
        if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الأحد'
        ) {
          weeksid.push(0);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الإثنين'
        ) {
          weeksid.push(1);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
          'الثلاثاء'
        ) {
          weeksid.push(2);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
          'الأربعاء'
        ) {
          weeksid.push(3);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الخميس'
        ) {
          weeksid.push(4);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الجمعة'
        ) {
          weeksid.push(5);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'السبت'
        ) {
          weeksid.push(6);
        }
      }

      if (weeksid.includes(day)) {
        vendorisopen = false;
        setselectedd('updatevendorstate');

        dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 3));
        dispatch(UpdateAvailabiltyAction(3));
      } else {
        vendorisopen = true;
        setselectedd('updatevendorstate');

        dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 1));
        dispatch(UpdateAvailabiltyAction(1));
      }
    }
  };

  closepanell = (index) => {
    setswipeablePanelActivecategory(false);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setsubcityswipable(false);
    setcityswipable(false);
    setselectedd('');
    setcityswipable(false);
  };

  if (selectedd === 'login') {
    if (
      typeof LoginReducer.payload !== 'undefined' &&
      typeof LoginReducer.payload.status !== 'undefined'
    ) {
      if (LoginReducer.payload.status === '2') {
        Toast.showWithGravity(
          'حسابك غير نشط يرجى الاتصال بالإدارة  ',
          Toast.LONG,
          Toast.BOTTOM
        );
        setselectedd('');
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }
      } else if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error === 'success'
      ) {
        if ('' + LoginReducer.payload.data.type === '2') {
          if (clickable) {
            AsyncStorage.setItem('loggedin', 'true').then(() => {
              AsyncStorage.setItem('email', email).then(() => {
                AsyncStorage.setItem('password', '' + password).then(() => {
                  AsyncStorage.setItem(
                    'name',
                    LoginReducer.payload.data.name
                  ).then(() => {
                    AsyncStorage.setItem(
                      'token',
                      LoginReducer.payload.token.token
                    ).then(() => {
                      AsyncStorage.setItem(
                        'id',
                        '' + LoginReducer.payload.data.id
                      ).then(() => {
                        let id = LoginReducer.payload.data.id;
                        setfirstlogin(true);

                        setselectedd('getvendordetails');
                        setclickable(false);

                        dispatch(GetUserDetailsAction(id, currencyid));
                      });
                    });
                  });
                });
              });
            });
          } else {
            if (Platform.OS === 'ios') {
              setInterval(() => {
                setspinner(false);
              }, 1000);
            } else {
              // setInterval(() => {

              setspinner(false);

              // }, 1000);
            }
            setemail('');
            setpassword('');
            setselectedd('');
          }
        } else {
          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }
          setemail('');
          setpassword('');
          setselectedd('');
          Toast.showWithGravity(
            'هذا المستخدم ليس بائعًا  ',
            Toast.LONG,
            Toast.BOTTOM
          );
        }
      }
    } else {
      if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error !== 'success'
      ) {
        if (
          typeof LoginReducer.error.response !== 'undefined' &&
          typeof LoginReducer.error.response.data !== 'undefined' &&
          typeof LoginReducer.error.response.data.message !== 'undefined' &&
          LoginReducer.error.response.data.message === 'password is wrong'
        ) {
          Toast.showWithGravity('كلمة السر خاطئة', Toast.SHORT, Toast.BOTTOM);

          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('name', 'null').then(() => {
                AsyncStorage.setItem('password', 'null').then(() => {
                  AsyncStorage.setItem('token', 'null').then(() => {
                    AsyncStorage.setItem('id', 'null').then(() => {
                      setselectedd('');
                      setemail('');
                      setpassword('');
                      if (Platform.OS === 'ios') {
                        setInterval(() => {
                          setspinner(false);
                        }, 1000);
                      } else {
                        // setInterval(() => {

                        setspinner(false);

                        // }, 1000);
                      }
                    });
                  });
                });
              });
            });
          });
        } else if (
          typeof LoginReducer.error.response !== 'undefined' &&
          typeof LoginReducer.error.response.data !== 'undefined' &&
          typeof LoginReducer.error.response.data.message !== 'undefined'
        ) {
          Toast.showWithGravity(
            'بريد إلكتروني خاطئ',
            Toast.SHORT,
            Toast.BOTTOM
          );

          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('password', 'null').then(() => {
                AsyncStorage.setItem('token', 'null').then(() => {
                  AsyncStorage.setItem('id', 'null').then(() => {
                    setemail('');
                    setpassword('');
                    setselectedd('');

                    if (Platform.OS === 'ios') {
                      setInterval(() => {
                        setspinner(false);
                      }, 1000);
                    } else {
                      // setInterval(() => {

                      setspinner(false);

                      // }, 1000);
                    }
                  });
                });
              });
            });
          });
        } else if (LoginReducer.error !== '') {
          setselectedd('');

          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          // Toast.showWithGravity(LoginReducer.error.message, Toast.SHORT, Toast.BOTTOM)
        } else {
          //   setselectedd('')
          //   setInterval(() => {
          //     setspinner(false)
          //   }, 1000);
        }
      }
    }
  } else if (selectedd === 'register') {
    if (
      typeof RegisterReducer.payload !== 'undefined' &&
      RegisterReducer.error === 'success'
    ) {
      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }
      setselectedd('');

      setselectedholidays([]);
      setselectedlabels([]);
      handleClose();
      showMessage({
        message: 'لقد قمت بالتسجيل بنجاح وسنوافق على حسابك قريبًا ',
        type: "info",
        duration:2000
      });
      // Toast.showWithGravity(
      //   'لقد قمت بالتسجيل بنجاح وسنوافق على حسابك قريبًا ',
      //   Toast.LONG,
      //   Toast.BOTTOM
      // );
    } else {
      if (
        typeof RegisterReducer.error !== 'undefined' &&
        RegisterReducer.error === 'The email has already been taken.'
      ) {
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }

        setselectedd('');

        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        setnewpassword('');
        setcode('');
        setforgetemail('');
        setcount(0);
        setcityswipable(false);
        Toast.showWithGravity(
          'تم بالفعل تسجيل البريد الإلكتروني من قبل ',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else {
        if (RegisterReducer.error !== '') {
          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          setselectedd('');
        }
      }
    }
  } else if (selectedd === 'getvendordetails') {
    if (
      typeof VendorDetailsReducer !== 'undefined' &&
      typeof VendorDetailsReducer.payload !== 'undefined' &&
      VendorDetailsReducer.error === 'success'
    ) {
      if (firstlogin) {
        var current = new Date();
        var c = current.getTime();

        var day = current.getDay();

        let from = VendorDetailsReducer.payload.vendor.work_hours[0].start;
        let to = VendorDetailsReducer.payload.vendor.work_hours[0].end;

        // let durationfrom = moment.duration(from, 'milliseconds');
        // let durationto = moment.duration(to, 'milliseconds');

        var now = moment.utc();
        var hour = now.hour();
        var min = now.minutes();

        let curremntmillic = moment
          .utc([2010, 1, 14, hour, min, 0, 0])
          .valueOf();

        //  let  startt = setDateTime(new Date(current), from);
        //  let  endd = setDateTime(new Date(current), to);

        setfirstlogin(false);

        if (
          typeof LoginReducer !== 'undefined' &&
          typeof LoginReducer.payload !== 'undefined'
        ) {
          if(from.includes('صباحا'))
          {
            from= from.replace('صباحا','') 
           }
          if(to.includes('صباحا'))
          {
             to=to.replace('صباحا','') 
          }
          if(from.includes('مساء'))
          {
             from=from.replace('مساء','') 
          }
          if(to.includes('مساء'))
          {
           to= to.replace('مساء','') 
          }
          if (curremntmillic > from && curremntmillic < to) {
            //  vendorisopen=true;
            //     setselectedd('updatevendorstate')

            //     dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token,2));

            checkdayisopenday(day);
          } else {
            vendorisopen = false;
            setselectedd('updatevendorstate');

            dispatch(
              UpdateVendorStateAction(LoginReducer.payload.token.token, 3)
            );
            dispatch(UpdateAvailabiltyAction(3));
          }
        }
      }
    } else {
      // Toast.showWithGravity('errorr happen,please try loginn again  ', Toast.LONG, Toast.BOTTOM)
    }
  } else if (selectedd === 'updatevendorstate') {
    setselectedd('');

    if (Platform.OS === 'ios') {
      setInterval(() => {
        setspinner(false);
      }, 1000);
    } else {
      // setInterval(() => {

      setspinner(false);

      // }, 1000);
    }
    AsyncStorage.setItem('first', 'true').then(() => {
      props.navigation.navigate('HomeSeller', {
        token: LoginReducer.payload.token.token,
        stateavailable: vendorisopen,
        id: LoginReducer.payload.data.id,
        currencyid: currencyid,
        currency: currency,
      });
    });
  } else if (selectedd === 'forgetpasswordfirst') {
    if (
      typeof ForgetPasswordFirstReducer !== 'undefined' &&
      typeof ForgetPasswordFirstReducer.error !== 'undefined' &&
      ForgetPasswordFirstReducer.error === 'success'
    ) {
      if (ForgetPasswordFirstReducer.error === 'success') {
        if (
          ForgetPasswordFirstReducer.payload.message !== 'this email not found'
        ) {
          setcount(1);
          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          setselectedd('');

          Toast.showWithGravity(
            'يرجى التحقق من بريدك ونسخ الرمز المرسل ',
            Toast.LONG,
            Toast.BOTTOM
          );
        } else {
          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          setselectedd('');
          Toast.showWithGravity(
            'لم يتم تسجيل هذا البريد الإلكتروني من قبل ',
            Toast.LONG,
            Toast.BOTTOM
          );
        }
      } else {
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }

        setselectedd('');
        Toast.showWithGravity(
          'لم يتم تسجيل هذا البريد الإلكتروني من قبل ',
          Toast.LONG,
          Toast.BOTTOM
        );
      }
    } else if (
      typeof ForgetPasswordFirstReducer !== 'undefined' &&
      typeof ForgetPasswordFirstReducer.error !== 'undefined' &&
      ForgetPasswordFirstReducer.error !== '' &&
      ForgetPasswordFirstReducer.error !== 'success'
    ) {
      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }
      setselectedd('');

      if (
        typeof ForgetPasswordFirstReducer.error !== 'undefined' &&
        typeof ForgetPasswordFirstReducer.error.response !== 'undefined' &&
        typeof ForgetPasswordFirstReducer.error.response.data !== 'undefined' &&
        typeof ForgetPasswordFirstReducer.error.response.data.message !==
          'undefined' &&
        ForgetPasswordFirstReducer.error.response.data.message ===
          'this email not found'
      ) {
        Toast.showWithGravity(
          'هذا البريد الإلكتروني غير موجود',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else {
        Toast.showWithGravity('حدث خطأ عام ', Toast.LONG, Toast.BOTTOM);
      }
    }
  } else if (selectedd === 'forgetpasswordsecond') {
    if (
      typeof ForgetPasswordSecondReducer !== 'undefined' &&
      typeof ForgetPasswordSecondReducer.error !== 'undefined' &&
      ForgetPasswordSecondReducer.error === 'success'
    ) {
      if (
        ForgetPasswordSecondReducer.payload.message ===
        'Password has been successfully changed'
      ) {
        setfullheighttt(0);
        setnewpassword('');
        setcode('');
        setforgetemail('');
        setcount(0);
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }

        setselectedd('');
        Toast.showWithGravity(
          'تم تغيير كلمة المرور بنجاح',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else if (ForgetPasswordSecondReducer.error !== 'success') {
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }
        setselectedd('');
        Toast.showWithGravity(
          'الكود الذي تم إدخاله خاطئ  ',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else {
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }
        setselectedd('');
        Toast.showWithGravity('حدث خطأ عام ', Toast.LONG, Toast.BOTTOM);
      }
    } else if (
      typeof ForgetPasswordSecondReducer !== 'undefined' &&
      typeof ForgetPasswordSecondReducer.error !== 'undefined' &&
      ForgetPasswordSecondReducer.error !== '' &&
      ForgetPasswordSecondReducer.error !== 'success'
    ) {
      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }
      setselectedd('');
      Toast.showWithGravity(
        'الكود الذي تم إدخاله خاطئ  ',
        Toast.LONG,
        Toast.BOTTOM
      );
    }
  }

  const backdrop = {
    transform: [
      {
        translateY: animation.interpolate({
          inputRange: [0, 0.01],
          outputRange: [DEVICE_hight, 0],
          extrapolate: 'clamp',
        }),
      },
    ],
    opacity: animation.interpolate({
      inputRange: [0.01, 0.5],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    }),
  };

  const slideUp = {
    transform: [
      {
        translateY: animation.interpolate({
          inputRange: [0.01, 1],
          outputRange: [0, -1 * DEVICE_hight],
          extrapolate: 'clamp',
        }),
      },
    ],
  };

  const loadApp = async () => {
    try {
      d = await AsyncStorage.getItem('devicetoken');
      let userloggedin = await AsyncStorage.getItem('loggedin');
      let email = await AsyncStorage.getItem('email');
      setdevicetoken(d);

      if (
        userloggedin !== null &&
        userloggedin === 'true' &&
        email !== null &&
        email !== 'null'
      ) {
        setspinner(true);

        setemail(email);

        let password = await AsyncStorage.getItem('password');
        setpassword(password);
        setclickable(true);

        setselectedd('login');
        dsebugger;
        dispatch(LoginUserAction(email.trim(), password, d));
      } else {
      }
    } catch (e) {}
  };
  useEffect(() => {
    try {
      if (loadd) {
        loadApp();
        loadd = false;
      }
    } catch (dd) {
      // alert(dd);
    }

    //  const keyboardDidShowListener = Keyboard.addListener(
    //   'keyboardDidShow',
    //   () => {

    //     if(opened){
    //       let d=(DEVICE_hight/3)+200
    //       setfullheighttt(d)
    //       sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0)
    //     }

    //   }
    // );

    // const keyboardDidHideListener = Keyboard.addListener(
    //   'keyboardDidHide',
    //   () => {
    //     if(opened){

    //     setfullheighttt(DEVICE_hight/3)
    //     sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0)

    //     }
    //   }
    // );

    //  getlabels(categoryid);
    return () => {
      //  keyboardDidHideListener.remove();
      //  keyboardDidShowListener.remove();
      //  BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };

    // getlabels(1);
  }, [GetCategoriesReducer]);

  const itemsSelectedcity = (selectedItem) => {
    // Alert.alert(selecteddd);
    if (selecteddd === '000') {
      setcityid(selectedItem.id);
      setsubcityid(null);
     
      setsubcityidd(null);

      // setcountryswipable(false)
      setcityswipable(false);
      setcityselected(selectedItem.name);
      setsubcityselected('المنطقة');
      let sss = getsubcitylist(country, selectedItem.idd);

      subcitylist=sss

      if (sss.length > 6) {
        setsubcitylarge(true);
      } else {
        setsubcitylarge(false);
      }
      setswipeablePanelActivecategory(false);
      setswipeablePanelstarthour(false);
      setswipeablePanelendhour(false);
      setsubcityswipable(false);
      setcityswipable(false);
    } else {
      if (selecteddd === '1') {
        setcityid1(selectedItem.idd);
        setsubcityid1('الكل');
        setdeliverycity1(selectedItem.name);
        setdeliverysubcity1('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
     

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '2') {
        setcityid2(selectedItem.idd);
        setsubcityid2('الكل');
        setdeliverycity2(selectedItem.name);
        setdeliverysubcity2('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '3') {
        setcityid3(selectedItem.idd);
        setsubcityid3('الكل');
        setdeliverycity3(selectedItem.name);
        setdeliverysubcity3('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '4') {
        setcityid4(selectedItem.idd);
        setsubcityid4('الكل');
        setdeliverycity4(selectedItem.name);
        setdeliverysubcity4('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '5') {
        setcityid5(selectedItem.idd);
        setsubcityid5('الكل');
        setdeliverycity5(selectedItem.name);
        setdeliverysubcity5('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
        
        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '6') {
        setcityid6(selectedItem.idd);
        setsubcityid6('الكل');
        setdeliverycity6(selectedItem.name);
        setdeliverysubcity6('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
       

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '7') {
        setcityid7(selectedItem.idd);
        setsubcityid7('الكل');
        setdeliverycity7(selectedItem.name);
        setdeliverysubcity7('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
     

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '8') {
        setcityid8(selectedItem.idd);
        setsubcityid8('الكل');
        setdeliverycity8(selectedItem.name);
        setdeliverysubcity8('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
      

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '9') {
        setcityid9(selectedItem.idd);
        setsubcityid9('الكل');
        setdeliverycity9(selectedItem.name);
        setdeliverysubcity9('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '10') {
        setcityid10(selectedItem.idd);
        setsubcityid10('الكل');
        setdeliverycity10(selectedItem.name);
        setdeliverysubcity10('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
        

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '11') {
        setcityid11(selectedItem.idd);
        setsubcityid11('الكل');
        setdeliverycity11(selectedItem.name);
        setdeliverysubcity11('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
       

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '12') {
        setcityid12(selectedItem.idd);
        setsubcityid12('الكل');
        setdeliverycity12(selectedItem.name);
        setdeliverysubcity12('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
       

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '13') {
        setcityid13(selectedItem.idd);
        setsubcityid13('الكل');
        setdeliverycity13(selectedItem.name);
        setdeliverysubcity13('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
       

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '14') {
        setcityid14(selectedItem.idd);
        setsubcityid14('الكل');
        setdeliverycity14(selectedItem.name);
        setdeliverysubcity14('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
         

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '15') {
        setcityid15(selectedItem.idd);
        setsubcityid15('الكل');
        setdeliverycity15(selectedItem.name);
        setdeliverysubcity15('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
         

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '16') {
        setcityid16(selectedItem.idd);
        setsubcityid16('الكل');
        setdeliverycity16(selectedItem.name);
        setdeliverysubcity16('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
         

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '17') {
        setcityid17(selectedItem.idd);
        setsubcityid17('الكل');
        setdeliverycity17(selectedItem.name);
        setdeliverysubcity17('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
         

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '18') {
        setcityid18(selectedItem.idd);
        setsubcityid18('الكل');
        setdeliverycity18(selectedItem.name);
        setdeliverysubcity18('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
         

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '19') {
        setcityid19(selectedItem.idd);
        setsubcityid19('الكل');
        setdeliverycity19(selectedItem.name);
        setdeliverysubcity19('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
         

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      } else if (selecteddd === '20') {
        setcityid20(selectedItem.idd);
        setsubcityid20('الكل');
        setdeliverycity20(selectedItem.name);
        setdeliverysubcity20('الكل');
        setcityswipable(false);
        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);
        let sss = getsubcitylist(country, selectedItem.idd);
         

        subcitylist=sss
        if (sss.length > 6) {
          setsubcitylarge(true);
        } else {
          setsubcitylarge(false);
        }
      }
    }
  };
  const itemsSelectedsubcity = (selectedItem) => {
    if (selecteddd === '000') {
    
      setsubcityid(selectedItem.id);
    
      setsubcityidd(selectedItem.idd);
    
      // setcountryswipable(false)
      setcityswipable(false);
      setsubcityswipable(false);
      setsubcityselected(selectedItem.name);
    } else {
      if (selecteddd === '1') {
        setsubcityid1(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity1(selectedItem.name);
      } else if (selecteddd === '2') {
        setsubcityid2(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity2(selectedItem.name);
      } else if (selecteddd === '3') {
        setsubcityid3(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity3(selectedItem.name);
      } else if (selecteddd === '4') {
        setsubcityid4(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity4(selectedItem.name);
      } else if (selecteddd === '5') {
        setsubcityid5(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity5(selectedItem.name);
      } else if (selecteddd === '6') {
        setsubcityid6(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity6(selectedItem.name);
      } else if (selecteddd === '7') {
        setsubcityid7(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity7(selectedItem.name);
      } else if (selecteddd === '8') {
        setsubcityid8(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity8(selectedItem.name);
      } else if (selecteddd === '9') {
        setsubcityid9(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity9(selectedItem.name);
      } else if (selecteddd === '10') {
        setsubcityid10(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity10(selectedItem.name);
      } else if (selecteddd === '11') {
        setsubcityid11(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity11(selectedItem.name);
      } else if (selecteddd === '12') {
        setsubcityid12(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity12(selectedItem.name);
      } else if (selecteddd === '13') {
        setsubcityid13(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity13(selectedItem.name);
      } else if (selecteddd === '14') {
        setsubcityid14(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity14(selectedItem.name);
      } else if (selecteddd === '15') {
        setsubcityid15(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity15(selectedItem.name);
      } else if (selecteddd === '16') {
        setsubcityid16(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity16(selectedItem.name);
      } else if (selecteddd === '17') {
        setsubcityid17(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity17(selectedItem.name);
      } else if (selecteddd === '18') {
        setsubcityid18(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity18(selectedItem.name);
      } else if (selecteddd === '19') {
        setsubcityid19(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity19(selectedItem.name);
      } else if (selecteddd === '20') {
        setsubcityid20(selectedItem.idd);
        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setdeliverysubcity20(selectedItem.name);
      }
    }
  };

  const itemsSelectedcategory = (selectedItem) => {
    setcategoryname(selectedItem.name);
    setcategoryid(selectedItem.id);
    setselectedlabels([]);

    getlabels(selectedItem.id);

    setlabelfirst(true);
    setswipeablePanelActivecategory(false);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setsubcityswipable(false);
    setcityswipable(false);
  };

  const itemsSelectedstart = (selectedItem) => {
    let date;

    let m;

    if (start === 'صباحا') {
      if (selectedItem.name === '1:00') {
        m = moment.utc([2010, 1, 13, 23, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '2:00') {
        m = moment.utc([2010, 1, 14, 0, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '3:00') {
        m = moment.utc([2010, 1, 14, 1, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '4:00') {
        m = moment.utc([2010, 1, 14, 2, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '5:00') {
        m = moment.utc([2010, 1, 14, 3, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '6:00') {
        m = moment.utc([2010, 1, 14, 4, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '7:00') {
        m = moment.utc([2010, 1, 14, 5, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '8:00') {
        m = moment.utc([2010, 1, 14, 6, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '9:00') {
        m = moment.utc([2010, 1, 14, 7, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '10:00') {
        m = moment.utc([2010, 1, 14, 8, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '11:00') {
        m = moment.utc([2010, 1, 14, 9, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '12:00') {
        m = moment.utc([2010, 1, 14, 10, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      }
    } else {
      if (selectedItem.name === '1:00') {
        m = moment.utc([2010, 1, 14, 11, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '2:00') {
        m = moment.utc([2010, 1, 14, 12, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '3:00') {
        m = moment.utc([2010, 1, 14, 13, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '4:00') {
        m = moment.utc([2010, 1, 14, 14, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '5:00') {
        m = moment.utc([2010, 1, 14, 15, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '6:00') {
        m = moment.utc([2010, 1, 14, 16, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '7:00') {
        m = moment.utc([2010, 1, 14, 17, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '8:00') {
        m = moment.utc([2010, 1, 14, 18, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '9:00') {
        m = moment.utc([2010, 1, 14, 19, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '10:00') {
        m = moment.utc([2010, 1, 14, , 20, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '11:00') {
        m = moment.utc([2010, 1, 14, 21, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '12:00') {
        m = moment.utc([2010, 1, 14, 22, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      }
    }

    setstartclock(m);

    setstartclocktext(selectedItem.name);
    setstartclockid(selectedItem.id);
    setswipeablePanelActivecategory(false);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setsubcityswipable(false);

    setcityswipable(false);
  };
  const itemsSelectedend = (selectedItem) => {
    let date;

    let m;
    if (end === 'صباحا') {
      if (selectedItem.name === '1:00') {
        m = moment.utc([2010, 1, 13, 23, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '2:00') {
        m = moment.utc([2010, 1, 14, 0, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '3:00') {
        m = moment.utc([2010, 1, 14, 1, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '4:00') {
        m = moment.utc([2010, 1, 14, 2, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '5:00') {
        m = moment.utc([2010, 1, 14, 3, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '6:00') {
        m = moment.utc([2010, 1, 14, 4, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '7:00') {
        m = moment.utc([2010, 1, 14, 5, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '8:00') {
        m = moment.utc([2010, 1, 14, 6, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '9:00') {
        m = moment.utc([2010, 1, 14, 7, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '10:00') {
        m = moment.utc([2010, 1, 14, 8, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '11:00') {
        m = moment.utc([2010, 1, 14, 9, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '12:00') {
        m = moment.utc([2010, 1, 14, 10, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      }
    } else {
      if (selectedItem.name === '1:00') {
        m = moment.utc([2010, 1, 14, 11, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '2:00') {
        m = moment.utc([2010, 1, 14, 12, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '3:00') {
        m = moment.utc([2010, 1, 14, 13, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '4:00') {
        m = moment.utc([2010, 1, 14, 14, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '5:00') {
        m = moment.utc([2010, 1, 14, 15, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '6:00') {
        m = moment.utc([2010, 1, 14, 16, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '7:00') {
        m = moment.utc([2010, 1, 14, 17, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '8:00') {
        m = moment.utc([2010, 1, 14, 18, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '9:00') {
        m = moment.utc([2010, 1, 14, 19, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '10:00') {
        m = moment.utc([2010, 1, 14, , 20, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '11:00') {
        m = moment.utc([2010, 1, 14, 21, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '12:00') {
        m = moment.utc([2010, 1, 14, 22, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      }
    }

    setendclock(m);
    setendclocktext(selectedItem.name);
    setendclockid(selectedItem.id);
    setswipeablePanelActivecategory(false);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setsubcityswipable(false);

    setcityswipable(false);
  };
  const rowItemstart = (item) => (
    <TouchableHighlight
      underlayColor='transparent'
      activeOpacity={0.2}
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 20,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        let date;

        let m;
        if (start === 'صباحا') {
          if (item.name === '1:00') {
            m = moment.utc([2010, 1, 13, 23, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '2:00') {
            m = moment.utc([2010, 1, 14, 0, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '3:00') {
            m = moment.utc([2010, 1, 14, 1, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '4:00') {
            m = moment.utc([2010, 1, 14, 2, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '5:00') {
            m = moment.utc([2010, 1, 14, 3, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '6:00') {
            m = moment.utc([2010, 1, 14, 4, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '7:00') {
            m = moment.utc([2010, 1, 14, 5, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '8:00') {
            m = moment.utc([2010, 1, 14, 6, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '9:00') {
            m = moment.utc([2010, 1, 14, 7, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '10:00') {
            m = moment.utc([2010, 1, 14, 8, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '11:00') {
            m = moment.utc([2010, 1, 14, 9, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '12:00') {
            m = moment.utc([2010, 1, 14, 10, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          }
        } else {
          if (item.name === '1:00') {
            m = moment.utc([2010, 1, 14, 11, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '2:00') {
            m = moment.utc([2010, 1, 14, 12, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '3:00') {
            m = moment.utc([2010, 1, 14, 13, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '4:00') {
            m = moment.utc([2010, 1, 14, 14, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '5:00') {
            m = moment.utc([2010, 1, 14, 15, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '6:00') {
            m = moment.utc([2010, 1, 14, 16, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '7:00') {
            m = moment.utc([2010, 1, 14, 17, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '8:00') {
            m = moment.utc([2010, 1, 14, 18, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '9:00') {
            m = moment.utc([2010, 1, 14, 19, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '10:00') {
            m = moment.utc([2010, 1, 14, , 20, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '11:00') {
            m = moment.utc([2010, 1, 14, 21, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '12:00') {
            m = moment.utc([2010, 1, 14, 22, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          }
        }

        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);

        setcityswipable(false);

        setstartclockid(item.id);
        setstartclock(m);
        setstartclocktext(item.name);
      }}>
      <View>
        {(Platform.OS === 'ios' ||
          (Platform.OS === 'android' && deviceLocale === 'en')) && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
            {item.name}
          </Text>
        )}
        {Platform.OS === 'android' && deviceLocale === 'ar' && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'left' }}>
            {item.name}
          </Text>
        )}
      </View>
    </TouchableHighlight>
  );

  const rowItemend = (item) => (
    <TouchableHighlight
      underlayColor='transparent'
      activeOpacity={0.2}
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 20,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        let date;

        let m;
        if (end === 'صباحا') {
          if (item.name === '1:00') {
            m = moment.utc([2010, 1, 13, 23, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '2:00') {
            m = moment.utc([2010, 1, 14, 0, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '3:00') {
            m = moment.utc([2010, 1, 14, 1, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '4:00') {
            m = moment.utc([2010, 1, 14, 2, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '5:00') {
            m = moment.utc([2010, 1, 14, 3, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '6:00') {
            m = moment.utc([2010, 1, 14, 4, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '7:00') {
            m = moment.utc([2010, 1, 14, 5, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '8:00') {
            m = moment.utc([2010, 1, 14, 6, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '9:00') {
            m = moment.utc([2010, 1, 14, 7, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '10:00') {
            m = moment.utc([2010, 1, 14, 8, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '11:00') {
            m = moment.utc([2010, 1, 14, 9, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '12:00') {
            m = moment.utc([2010, 1, 14, 10, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          }
        } else {
          if (item.name === '1:00') {
            m = moment.utc([2010, 1, 14, 11, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '2:00') {
            m = moment.utc([2010, 1, 14, 12, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '3:00') {
            m = moment.utc([2010, 1, 14, 13, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '4:00') {
            m = moment.utc([2010, 1, 14, 14, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '5:00') {
            m = moment.utc([2010, 1, 14, 15, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '6:00') {
            m = moment.utc([2010, 1, 14, 16, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '7:00') {
            m = moment.utc([2010, 1, 14, 17, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '8:00') {
            m = moment.utc([2010, 1, 14, 18, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '9:00') {
            m = moment.utc([2010, 1, 14, 19, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '10:00') {
            m = moment.utc([2010, 1, 14, , 20, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '11:00') {
            m = moment.utc([2010, 1, 14, 21, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '12:00') {
            m = moment.utc([2010, 1, 14, 22, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          }
        }

        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);

        setcityswipable(false);
        setendclocktext(item.name);
        setendclockid(item.id);
        setendclock(m);
      }}>
      <View>
        {(Platform.OS === 'ios' ||
          (Platform.OS === 'android' && deviceLocale === 'en')) && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
            {item.name}
          </Text>
        )}
        {Platform.OS === 'android' && deviceLocale === 'ar' && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'left' }}>
            {item.name}
          </Text>
        )}
      </View>
    </TouchableHighlight>
  );

  const rowcityItem = (item) => (
    <TouchableHighlight
      underlayColor='transparent'
      activeOpacity={0.2}
      style={{
        flex: 1,
        borderBottomWidth: 0.7,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 5,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        
        // Alert.alert(selecteddd);
        if (selecteddd === '000') {
          setcityid(item.id);
          setsubcityid(null);
        
          setsubcityidd(null);

          // setcountryswipable(false)
          setcityswipable(false);
          setcityselected(item.name);
          setsubcityselected('المنطقة');
          let sss = getsubcitylist(country, item.idd);
           

          subcitylist=sss

          if (sss.length > 6) {
            setsubcitylarge(true);
          } else {
            setsubcitylarge(false);
          }
          setswipeablePanelActivecategory(false);
          setswipeablePanelstarthour(false);
          setswipeablePanelendhour(false);
          setsubcityswipable(false);
          setcityswipable(false);
        } else {
         
          if (selecteddd === '1') {
            setcityid1(item.idd);
            setsubcityid1('الكل');
            setdeliverycity1(item.name);
            setdeliverysubcity1('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '2') {
            setcityid2(item.idd);
            setsubcityid2('الكل');
            setdeliverycity2(item.name);
            setdeliverysubcity2('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '3') {
            setcityid3(item.idd);
            setsubcityid3('الكل');
            setdeliverycity3(item.name);
            setdeliverysubcity3('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);

            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '4') {
            setcityid4(item.idd);
            setsubcityid4('الكل');
            setdeliverycity4(item.name);
            setdeliverysubcity4('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '5') {
            setcityid5(item.idd);
            setsubcityid5('الكل');
            setdeliverycity5(item.name);
            setdeliverysubcity5('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '6') {
            setcityid6(item.idd);
            setsubcityid6('الكل');
            setdeliverycity6(item.name);
            setdeliverysubcity6('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '7') {
            setcityid7(item.idd);
            setsubcityid7('الكل');
            setdeliverycity7(item.name);
            setdeliverysubcity7('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '8') {
            setcityid8(item.idd);
            setsubcityid8('الكل');
            setdeliverycity8(item.name);
            setdeliverysubcity8('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '9') {
            setcityid9(item.idd);
            setsubcityid9('الكل');
            setdeliverycity9(item.name);
            setdeliverysubcity9('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '10') {
            setcityid10(item.idd);
            setsubcityid10('الكل');
            setdeliverycity10(item.name);
            setdeliverysubcity10('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             

            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '11') {
            setcityid11(item.idd);
            setsubcityid11('الكل');
            setdeliverycity11(item.name);
            setdeliverysubcity11('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '12') {
            setcityid12(item.idd);
            setsubcityid12('الكل');
            setdeliverycity12(item.name);
            setdeliverysubcity12('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '13') {
            setcityid13(item.idd);
            setsubcityid13('الكل');
            setdeliverycity13(item.name);
            setdeliverysubcity13('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '14') {
            setcityid14(item.idd);
            setsubcityid14('الكل');
            setdeliverycity14(item.name);
            setdeliverysubcity14('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '15') {
            setcityid15(item.idd);
            setsubcityid15('الكل');
            setdeliverycity15(item.name);
            setdeliverysubcity15('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '16') {
            setcityid16(item.idd);
            setsubcityid16('الكل');
            setdeliverycity16(item.name);
            setdeliverysubcity16('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '17') {
            setcityid17(item.idd);
            setsubcityid17('الكل');
            setdeliverycity17(item.name);
            setdeliverysubcity17('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '18') {
            setcityid18(item.idd);
            setsubcityid18('الكل');
            setdeliverycity18(item.name);
            setdeliverysubcity18('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '19') {
            setcityid19(item.idd);
            setsubcityid19('الكل');
            setdeliverycity19(item.name);
            setdeliverysubcity19('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          } else if (selecteddd === '20') {
            setcityid20(item.idd);
            setsubcityid20('الكل');
            setdeliverycity20(item.name);
            setdeliverysubcity20('الكل');
            setcityswipable(false);
            setswipeablePanelActivecategory(false);
            setswipeablePanelstarthour(false);
            setswipeablePanelendhour(false);
            setsubcityswipable(false);
            let sss = getsubcitylist(country, item.idd);
             
            subcitylist=sss
            if (sss.length > 6) {
              setsubcitylarge(true);
            } else {
              setsubcitylarge(false);
            }
          }
        }
      }}>
      <View style={{ width: '120%' }}>
        {(Platform.OS === 'ios' ||
          (Platform.OS === 'android' && deviceLocale === 'en')) && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
            {item.name}
          </Text>
        )}
        {Platform.OS === 'android' && deviceLocale === 'ar' && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'left' }}>
            {item.name}
          </Text>
        )}
        <View
          style={{
            backgroundColor: '#CCCCCC',
            height: 1,
            width: '100%',
            marginTop: 8,
          }}></View>
      </View>
    </TouchableHighlight>
  );
  const rowsubcityItem = (item) => (
    <TouchableHighlight
      underlayColor='transparent'
      activeOpacity={0.2}
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 5,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        // Alert.alert(selecteddd);

        if (selecteddd === '000') {
          setsubcityid(item.id);
        
          setsubcityidd(item.idd);

          // setcountryswipable(false)
          setcityswipable(false);
          setsubcityswipable(false);
          setsubcityselected(item.name);
        } else {
          

          if (selecteddd === '1') {
            setsubcityid1(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity1(item.name);
          } else if (selecteddd === '2') {
            setsubcityid2(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity2(item.name);
          } else if (selecteddd === '3') {
            setsubcityid3(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity3(item.name);
          } else if (selecteddd === '4') {
            setsubcityid4(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity4(item.name);
          } else if (selecteddd === '5') {
            setsubcityid5(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity5(item.name);
          } else if (selecteddd === '6') {
            setsubcityid6(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity6(item.name);
          } else if (selecteddd === '7') {
            setsubcityid7(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity7(item.name);
          } else if (selecteddd === '8') {
            setsubcityid8(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity8(item.name);
          } else if (selecteddd === '9') {
            setsubcityid9(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity9(item.name);
          } else if (selecteddd === '10') {
            setsubcityid10(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity10(item.name);
          } else if (selecteddd === '11') {
            setsubcityid11(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity11(item.name);
          } else if (selecteddd === '12') {
            setsubcityid12(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity12(item.name);
          } else if (selecteddd === '13') {
            setsubcityid13(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity13(item.name);
          } else if (selecteddd === '14') {
            setsubcityid14(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity14(item.name);
          } else if (selecteddd === '15') {
            setsubcityid15(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity15(item.name);
          } else if (selecteddd === '16') {
            setsubcityid16(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity16(item.name);
          } else if (selecteddd === '17') {
            setsubcityid17(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity17(item.name);
          } else if (selecteddd === '18') {
            setsubcityid18(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity18(item.name);
          } else if (selecteddd === '19') {
            setsubcityid19(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity19(item.name);
          } else if (selecteddd === '20') {
            setsubcityid20(item.idd);
            // setcountryswipable(false)
            setcityswipable(false);
            setsubcityswipable(false);
            setdeliverysubcity20(item.name);
          }
        }
      }}>
      <View style={{ width: '120%' }}>
        {(Platform.OS === 'ios' ||
          (Platform.OS === 'android' && deviceLocale === 'en')) && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
            {item.name}
          </Text>
        )}
        {Platform.OS === 'android' && deviceLocale === 'ar' && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'left' }}>
            {item.name}
          </Text>
        )}
        <View
          style={{
            backgroundColor: '#CCCCCC',
            height: 1,
            width: '100%',
            marginTop: 8,
          }}></View>
      </View>
    </TouchableHighlight>
  );

  const rowItemcategory = (item) => (
    <TouchableHighlight
      underlayColor='transparent'
      activeOpacity={0.2}
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 5,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={() => {
        // Alert.alert('categorry');

        setswipeablePanelActivecategory(false);
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);
        setsubcityswipable(false);

        setcityswipable(false);
        setcategoryid(item.id);
        setcategoryname(item.name);
        setselectedlabels([]);

        setlabelfirst(true);

        getlabels(item.id);
      }}>
      <View style={{ width: '120%' }}>
        {(Platform.OS === 'ios' ||
          (Platform.OS === 'android' && deviceLocale === 'en')) && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
            {item.name}
          </Text>
        )}
        {Platform.OS === 'android' && deviceLocale === 'ar' && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'left' }}>
            {item.name}
          </Text>
        )}
        <View
          style={{
            backgroundColor: '#CCCCCC',
            height: 1,
            width: '100%',
            marginTop: 10,
            marginBottom: 10,
          }}></View>
      </View>
    </TouchableHighlight>
  );

  const handleOpen = () => {
    
    setfullheighttt(0);
    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);
    setdeliverycount(0);
    setforgetemail('');
    setcount(0);
    setcode('');
    setnewpassword('');
    opened = false;

    if (
      typeof GetCountriesReducer !== 'undefined' &&
      typeof GetCountriesReducer.error !== '' &&
      GetCountriesReducer.error == 'success'
    ) {
      if (
        typeof GetCountriesReducer.payload !== 'undefined' &&
        typeof GetCountriesReducer.payload.data !== 'undefined'
      ) {
        let citylist = [];

        for (let f = 0; f < GetCountriesReducer.payload.data.length; f++) {
          
          if (GetCountriesReducer.payload.data[f].country_name_ar === 'الإمارات العربية المتحدة') {
            
            if (GetCountriesReducer.payload.data[f].cities.length > 0) {
              for (
                let d = 0;
                d < GetCountriesReducer.payload.data[f].cities.length;
                d++
              ) {
                citylist.push({
                  id: d + 1,
                  name:
                    GetCountriesReducer.payload.data[f].cities[d].city_name_ar,
                  idd: GetCountriesReducer.payload.data[f].cities[d].id,
                });
              }
            }
          }
        }

        setcitylist(citylist);
      }
    }

    setnewpassword('');
    setcode('');
    setpassword('');
    setemail('');
    setforgetemail('');
    setcount(0);
    Animated.timing(animation, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
    // setclickable(true)
  };

  // const handleChoosePhoto = () => {

  //   const options = {
  //     noData: true,
  //   };
  //   ImagePicker.launchImageLibrary(options, response => {
  //     if (response.uri) {
  //       setphoto(response)
  //     }
  //   });
  //   ImagePicker.launchCamera(options, (response) => {
  //     if (response.uri) {
  //       setphoto(response)
  //     }
  //       });
  // };
  const selectPhotoTapped = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: 'itemfiles',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = { uri: response.uri };

        ImgToBase64.getBase64String(response.uri).then((base64String) => {
          setimagebaseprofile(base64String);
        });
        // .catch(err => {

        // });

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        setsrcimage(source);
      }
    });
  };

  const openCamera = (cropit) => {
    ImagePicker.openCamera({
      width: 500,
      height: 500,
      cropping: false,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        // Alert.alert(e.message ? e.message : e);
      });
  };

  const openGallary = (cropit) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: false,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        // Alert.alert(e.message ? e.message : e);
      });
  };

  const getinitial = (id) => {
    if (id === null) {
      return id - 1;
    } else return 1;
  };

  const onLoginPressed = (email, password) => {
    // const {username, password} = this.state;
    if (email.length > 0) {
      if (!validateEmail(email)) {
        Toast.showWithGravity('بريد إلكتروني خاطئ', Toast.LONG, Toast.BOTTOM);
      } else if (password.length < 8) {
        Toast.showWithGravity(
          'طول كلمة المرور غير صالحة',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else {
        // AsyncStorage.setItem('loggedin', "true").then(() => {

        setnewpassword('');
        setcode('');
        setforgetemail('');
        setcount(0);
        setclickable(true);
        setselectedd('login');

        setspinner(true);

        dispatch(LoginUserAction(email.trim(), password, d));

        // props.navigation.navigate('HomeStack', {
        // });
        // });
      }
    } else {
      Toast.showWithGravity(
        'تحتاج إلى إضافة الحقول المفقودة',
        Toast.LONG,
        Toast.BOTTOM
      );
    }
  };

  const onRegisterPressed = (
    email,
    password,
    name,
    phone,
    address,
    subcityselected,
    imagebaseprofile,
    subcityid,
    subcityidd,
    categoryid,
    selected,
    startclock,
    endclock,
    selectedholidays
  ) => {
    // const {username, password} = this.state;
    if (imagebaseprofile !== '') {
      if (
        name !== '' &&
        address !== '' &&
        phone !== '' &&
        email !== '' &&
        startclock !== '' &&
        endclock !== ''
      ) {
        if (email !== '' && !validateEmail(email)) {
          Toast.showWithGravity('بريد إلكتروني خاطئ', Toast.LONG, Toast.BOTTOM);
        } else if (password !== '' && password.length < 8) {
          Toast.showWithGravity(
            'طول كلمة المرور غير صالحة',
            Toast.LONG,
            Toast.BOTTOM
          );
        } else {
          // AsyncStorage.setItem('loggedin', "true").then(() => {
          if (email !== '' && password !== '') {
            if (selectedholidays.length > 4) {
              Toast.showWithGravity(
                'الحد الأقصى للعطلات أربعة أيام  ',
                Toast.LONG,
                Toast.BOTTOM
              );
            } else if (selected.length < 1) {
              Toast.showWithGravity(
                'تحتاج إلى اختيار فئة واحدة على الأقل  ',
                Toast.LONG,
                Toast.BOTTOM
              );
            } else {
              if (subcityid !== null) {
                setclickable(true);
                setselectedd('register');

                setspinner(true);

                let s =
                  country + ' - ' + cityselected + ' - ' + subcityselected;
                setswipeablePanelActivecategory(false);
                setswipeablePanelstarthour(false);
                setswipeablePanelendhour(false);
                setsubcityswipable(false);
                setnewpassword('');
                setcode('');
                setforgetemail('');
                setcount(0);
                setcityswipable(false);

                let deliverydestination = [];
                let costdeliverydestination = [];

                if (cityid1 !== null && subcityid1 !== 'الكل') {
                  deliverydestination.push(
                    cityid1 +
                      '==' +
                      subcityid1 +
                      '//' +
                      deliverycity1 +
                      '-' +
                      deliverysubcity1
                  );

                  costdeliverydestination.push('10');
                } else if (cityid1 !== null && subcityid1 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid1).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid1 +
                        '==' +
                        subcitylisttt1[s].idd +
                        '//' +
                        deliverycity1 +
                        '-' +
                        subcitylisttt1[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid2 !== null && subcityid2 !== 'الكل') {
                  deliverydestination.push(
                    cityid2 +
                      '==' +
                      subcityid2 +
                      '//' +
                      deliverycity2 +
                      '-' +
                      deliverysubcity2
                  );
                  costdeliverydestination.push('10');
                } else if (cityid2 !== null && subcityid2 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid2).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid2 +
                        '==' +
                        subcitylisttt2[s].idd +
                        '//' +
                        deliverycity2 +
                        '-' +
                        subcitylisttt2[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid3 !== null && subcityid3 !== 'الكل') {
                  deliverydestination.push(
                    cityid3 +
                      '==' +
                      subcityid3 +
                      '//' +
                      deliverycity3 +
                      '-' +
                      deliverysubcity3
                  );
                  costdeliverydestination.push('10');
                } else if (cityid3 !== null && subcityid3 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid3).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid3 +
                        '==' +
                        subcitylisttt3[s].idd +
                        '//' +
                        deliverycity3 +
                        '-' +
                        subcitylisttt3[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid4 !== null && subcityid4 !== 'الكل') {
                  deliverydestination.push(
                    cityid4 +
                      '==' +
                      subcityid4 +
                      '//' +
                      deliverycity4 +
                      '-' +
                      deliverysubcity4
                  );
                  costdeliverydestination.push('10');
                } else if (cityid4 !== null && subcityid4 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid4).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid4 +
                        '==' +
                        subcitylisttt4[s].idd +
                        '//' +
                        setdeliverycity4 +
                        '-' +
                        subcitylisttt4[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid5 !== null && subcityid5 !== 'الكل') {
                  deliverydestination.push(
                    cityid5 +
                      '==' +
                      subcityid5 +
                      '//' +
                      deliverycity5 +
                      '-' +
                      deliverysubcity5
                  );
                  costdeliverydestination.push('10');
                } else if (cityid5 !== null && subcityid5 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid5).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid5 +
                        '==' +
                        subcitylisttt5[s].idd +
                        '//' +
                        deliverycity5 +
                        '-' +
                        subcitylisttt5[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid6 !== null && subcityid6 !== 'الكل') {
                  deliverydestination.push(
                    cityid6 +
                      '==' +
                      subcityid6 +
                      '//' +
                      deliverycity6 +
                      '-' +
                      deliverysubcity6
                  );
                  costdeliverydestination.push('10');
                } else if (cityid6 !== null && subcityid6 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid6).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid6 +
                        '==' +
                        subcitylisttt6[s].idd +
                        '//' +
                        deliverycity6 +
                        '-' +
                        subcitylisttt6[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid7 !== null && subcityid7 !== 'الكل') {
                  deliverydestination.push(
                    cityid7 +
                      '==' +
                      subcityid7 +
                      '//' +
                      deliverycity7 +
                      '-' +
                      deliverysubcity7
                  );
                  costdeliverydestination.push('10');
                } else if (cityid7 !== null && subcityid7 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid7).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid7 +
                        '==' +
                        subcitylisttt7[s].idd +
                        '//' +
                        deliverycity7 +
                        '-' +
                        subcitylisttt7[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid8 !== null && subcityid8 !== 'الكل') {
                  deliverydestination.push(
                    cityid8 +
                      '==' +
                      subcityid8 +
                      '//' +
                      deliverycity8 +
                      '-' +
                      deliverysubcity8
                  );
                  costdeliverydestination.push('10');
                } else if (cityid8 !== null && subcityid8 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid8).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid8 +
                        '==' +
                        subcitylisttt8[s].idd +
                        '//' +
                        deliverycity8 +
                        '-' +
                        subcitylisttt8[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid9 !== null && subcityid9 !== 'الكل') {
                  deliverydestination.push(
                    cityid9 +
                      '==' +
                      subcityid9 +
                      '//' +
                      deliverycity9 +
                      '-' +
                      deliverysubcity9
                  );
                  costdeliverydestination.push('10');
                } else if (cityid9 !== null && subcityid9 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid9).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid9 +
                        '==' +
                        subcitylisttt9[s].idd +
                        '//' +
                        deliverycity9 +
                        '-' +
                        subcitylisttt9[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid10 !== null && subcityid10 !== 'الكل') {
                  deliverydestination.push(
                    cityid10 +
                      '==' +
                      subcityid10 +
                      '//' +
                      deliverycity10 +
                      '-' +
                      deliverysubcity10
                  );
                  costdeliverydestination.push('10');
                } else if (cityid10 !== null && subcityid10 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid10).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid10 +
                        '==' +
                        subcitylisttt10[s].idd +
                        '//' +
                        deliverycity10 +
                        '-' +
                        subcitylisttt10[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid11 !== null && subcityid11 !== 'الكل') {
                  deliverydestination.push(
                    cityid11 +
                      '==' +
                      subcityid11 +
                      '//' +
                      deliverycity11 +
                      '-' +
                      deliverysubcity11
                  );
                  costdeliverydestination.push('10');
                } else if (cityid11 !== null && subcityid11 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid11).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid11 +
                        '==' +
                        subcitylisttt11[s].idd +
                        '//' +
                        deliverycity11 +
                        '-' +
                        subcitylisttt11[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid12 !== null && subcityid12 !== 'الكل') {
                  deliverydestination.push(
                    cityid12 +
                      '==' +
                      subcityid12 +
                      '//' +
                      deliverycity12 +
                      '-' +
                      deliverysubcity12
                  );
                  costdeliverydestination.push('10');
                } else if (cityid12 !== null && subcityid12 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid12).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid12 +
                        '==' +
                        subcitylisttt12[s].idd +
                        '//' +
                        deliverycity12 +
                        '-' +
                        subcitylisttt12[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid13 !== null && subcityid13 !== 'الكل') {
                  deliverydestination.push(
                    cityid13 +
                      '==' +
                      subcityid13 +
                      '//' +
                      deliverycity13 +
                      '-' +
                      deliverysubcity13
                  );
                  costdeliverydestination.push('10');
                } else if (cityid13 !== null && subcityid13 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid13).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid13 +
                        '==' +
                        subcitylisttt13[s].idd +
                        '//' +
                        deliverycity13 +
                        '-' +
                        subcitylisttt13[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid14 !== null && subcityid14 !== 'الكل') {
                  deliverydestination.push(
                    cityid14 +
                      '==' +
                      subcityid14 +
                      '//' +
                      deliverycity14 +
                      '-' +
                      deliverysubcity14
                  );
                  costdeliverydestination.push('10');
                } else if (cityid14 !== null && subcityid14 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid14).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid14 +
                        '==' +
                        subcitylisttt14[s].idd +
                        '//' +
                        deliverycity14 +
                        '-' +
                        subcitylisttt14[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid15 !== null && subcityid15 !== 'الكل') {
                  deliverydestination.push(
                    cityid15 +
                      '==' +
                      subcityid15 +
                      '//' +
                      deliverycity15 +
                      '-' +
                      deliverysubcity15
                  );
                  costdeliverydestination.push('10');
                } else if (cityid15 !== null && subcityid15 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid15).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid15 +
                        '==' +
                        subcitylisttt15[s].idd +
                        '//' +
                        deliverycity15 +
                        '-' +
                        subcitylisttt15[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid16 !== null && subcityid16 !== 'الكل') {
                  deliverydestination.push(
                    cityid16 +
                      '==' +
                      subcityid16 +
                      '//' +
                      deliverycity16 +
                      '-' +
                      deliverysubcity16
                  );
                  costdeliverydestination.push('10');
                } else if (cityid16 !== null && subcityid16 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid16).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid16 +
                        '==' +
                        subcitylisttt16[s].idd +
                        '//' +
                        deliverycity16 +
                        '-' +
                        subcitylisttt16[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid17 !== null && subcityid17 !== 'الكل') {
                  deliverydestination.push(
                    cityid17 +
                      '==' +
                      subcityid17 +
                      '//' +
                      deliverycity17 +
                      '-' +
                      deliverysubcity17
                  );
                  costdeliverydestination.push('10');
                } else if (cityid17 !== null && subcityid17 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid17).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid17 +
                        '==' +
                        subcitylisttt17[s].idd +
                        '//' +
                        deliverycity17 +
                        '-' +
                        subcitylisttt17[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid18 !== null && subcityid18 !== 'الكل') {
                  deliverydestination.push(
                    cityid18 +
                      '==' +
                      subcityid18 +
                      '//' +
                      deliverycity18 +
                      '-' +
                      deliverysubcity18
                  );
                  costdeliverydestination.push('10');
                } else if (cityid18 !== null && subcityid18 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid18).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid18 +
                        '==' +
                        subcitylisttt18[s].idd +
                        '//' +
                        deliverycity18 +
                        '-' +
                        subcitylisttt18[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid19 !== null && subcityid19 !== 'الكل') {
                  deliverydestination.push(
                    cityid19 +
                      '==' +
                      subcityid19 +
                      '//' +
                      deliverycity19 +
                      '-' +
                      deliverysubcity19
                  );
                  costdeliverydestination.push('10');
                } else if (cityid19 !== null && subcityid19 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid19).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid19 +
                        '==' +
                        subcitylisttt19[s].idd +
                        '//' +
                        deliverycity19 +
                        '-' +
                        subcitylisttt19[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                if (cityid20 !== null && subcityid20 !== 'الكل') {
                  deliverydestination.push(
                    cityid20 +
                      '==' +
                      subcityid20 +
                      '//' +
                      deliverycity20 +
                      '-' +
                      deliverysubcity20
                  );
                  costdeliverydestination.push('10');
                } else if (cityid20 !== null && subcityid20 === 'الكل') {
                  for (
                    let s = 0;
                    s < getsubcitylist(country, cityid20).length;
                    s++
                  ) {
                    deliverydestination.push(
                      cityid20 +
                        '==' +
                        subcitylisttt20[s].idd +
                        '//' +
                        deliverycity20 +
                        '-' +
                        subcitylisttt20[s].name
                    );
                    costdeliverydestination.push('10');
                  }
                }

                dispatch(
                  RegisterAction(
                    costdeliverydestination,
                    deliverydestination,
                    email.trim(),
                    password,
                    name,
                    phone,
                    address,
                    s,
                    imagebaseprofile,
                    subcityid,
                    subcityidd,
                    categoryid,
                    selected,
                    startclock,
                    endclock,
                    selectedholidays,
                    currencyid
                  )
                );
              } else {
                Toast.showWithGravity(
                  'تحتاج إلى إضافة الحقول المفقودة',
                  Toast.LONG,
                  Toast.BOTTOM
                );
              }
            }
          } else {
            Toast.showWithGravity(
              'تحتاج إلى إضافة الحقول المفقودة',
              Toast.LONG,
              Toast.BOTTOM
            );
          }
          // props.navigation.navigate('HomeStack', {
          // });
          // });
        }
      } else {
        Toast.showWithGravity(
          'تحتاج إلى إضافة الحقول المفقودة',
          Toast.LONG,
          Toast.BOTTOM
        );
      }
    } else {
      Toast.showWithGravity(
        'تحتاج إلى إضافة صورة المحل ',
        Toast.LONG,
        Toast.BOTTOM
      );
    }
  };

  return (
    <View
      style={{
        flexDirection: 'column',
        height: '100%',
        backgroundColor: 'white',
        paddingBottom:20

      }}>
           <KeyboardAwareScrollView   style={{
          flexDirection: 'column',

          height: DEVICE_hight}}>
      <View
        style={{
          flexDirection: 'column',
          height: '100%',
          backgroundColor: 'white',
        }}>
        <ImageBackground
          resizeMode='stretch'
          source={require('../../imgs/loginbg.png')}
          style={{
            height: DEVICE_hight*.8,
            width: '100%',
            alignItems: 'center',
            flexDirection: 'column',
            justifyContent: 'center',
            alignContent: 'center',
          }}>
          <View style={{ height: 30 }}></View>
          <Image
            style={{
              width: 150,
              height: 150,
            }}
            resizeMode='contain'
            source={require('../../imgs/logoooo.png')}></Image>
      
          <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',alignItems:'flex-start',height:20, marginTop: -20}}
              resizeMode='stretch'
                />
          <View
            style={{
              width: '100%',

              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '100%',
                marginTop: 30,
                justifyContent: 'flex-start',
                flexDirection: 'row-reverse',
                flexDirection: 'column',
                alignItems: 'flex-start',
              }}>
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.input_login}
                  placeholder={'البريد الإلكتروني'}
                  autoCapitalize={'none'}
                  returnKeyType={'next'}
                  autoCorrect={false}
                  onFocus={() => {
                    // setswipeablePanelActivecategory(false);
                    // setswipeablePanelstarthour(false);
                    // setswipeablePanelendhour(false);
                    // setsubcityswipable(false);
                    // setcityswipable(false);
                  }}
                  onChangeText={(email) => {
                    opened = false;

                    setemail(email);
                  }}
                  value={email}
                  secureTextEntry={false}
                  placeholderTextColor='#3c9bc6'
                  underlineColorAndroid='transparent'
                />
              </View>
              <View style={styles.inputWrapper}>
                <TextInput
                  secureTextEntry={false}
                  placeholder={'كلمه السر'}
                  returnKeyType={'done'}
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  onFocus={() => {
                    // setswipeablePanelActivecategory(false);
                    // setswipeablePanelstarthour(false);
                    // setswipeablePanelendhour(false);
                    // setsubcityswipable(false);
                    // setcityswipable(false);
                  }}
                  secureTextEntry
                  onChangeText={(password) => {
                    setpassword(password);
                  }}
                  value={password}
                  style={styles.input_login}
                  placeholderTextColor='#3c9bc6'
                  underlineColorAndroid='transparent'
                />
              </View>
              <View
                style={{
                  width: DEVICE_width,
                  paddingLeft: 30,
                  paddingRight: 30,

                  paddingBottom: 20,

                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <TouchableHighlight
                  underlayColor='transparent'
                  activeOpacity={0.2}
                  onPress={() => {
                    setfullheighttt(DEVICE_hight / 3);

                    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);
                    // }
                    opened = true;
                    setnewpassword('');
                    setcode('');
                    setforgetemail('');
                    setcount(0);
                  }}>
                  <Text style={{ color: 'white', marginHorizontal: 10 }}>
                    {'نسيت كلمة المرور'}
                  </Text>
                </TouchableHighlight>
              </View>

              <View
                style={{
                  width: '100%',
                  flexDirection: 'row-reverse',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableHighlight
                  underlayColor='transparent'
                  activeOpacity={0.2}
                  onPress={() => {
                    setcityswipable(false);

                    setsubcityswipable(false);
                    setfullheighttt(0);
                    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);

                    setforgetemail('');
                    setcount(0);
                    setcode('');
                    setnewpassword('');

                    onLoginPressed(email, password);
                  }}>
                  <LinearGradient
                    colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                    style={{
                      justifyContent: 'center',
                      alignContent: 'center',
                      alignItems: 'center',
                      borderRadius: 40,
                    }}>
                    <Text style={styles.loginText}> {'تسجيل الدخول'}</Text>
                  </LinearGradient>
                </TouchableHighlight>
              </View>

              <View style={[styles.socialContainer]}></View>
            </View>
          </View>

        </ImageBackground>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            flexDirection: 'row-reverse',
          }}>
          <TouchableHighlight
            underlayColor='transparent'
            activeOpacity={0.2}
            onPress={() => {
              
              handleOpen();
            }}>
            <LinearGradient
              colors={['#04b5eb', '#06d4e5', '#06d4e5']}
              style={{
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center',
                borderRadius: 40,
              }}>
              <Text style={styles.loginText}> {'تسجيل مستخدم جديد'}</Text>
            </LinearGradient>
          </TouchableHighlight>
        </View>
      </View>
      </KeyboardAwareScrollView>
      <Animated.View style={[StyleSheet.absoluteFill, styles.cover, backdrop]}>
        <View style={[styles.sheet]}>
          <Animated.View style={[styles.popup, slideUp]}>
          <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ height: '100%', width: '100%' }}>
            <View
              style={{
                height: '100%',
                width: '100%',
                flexDirection: 'column',
              }}>
              <View
                style={{
                  height: 150,
                  flexDirection: 'column',
                  width: '100%',
                }}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    // profile=true

                    // if(this.state.clickable===true)
                    // {
                    // if(clickable === true){
                    // setimageIsProfile(true);
                    showModalVisible();
                    // selectPhotoTapped(this);

                    // setcityswipas

                    // }

                    // }else{

                    // }
                  }}>
                  <ImageBackground
                    style={{ width: '100%', height: 300 }}
                    resizeMode='cover'
                    source={srcimage}>
                    <ImageBackground
                      style={{ width: '100%', height: 100 }}
                      source={header}
                      resizeMode='cover'>
                      <View style={{ height: 30 }}></View>
                      <View
                        style={{
                          width: '100%',
                          flexDirection: 'row',
                          padding: 10,
                          justifyContent: 'flex-start',
                          alignItems: 'flex-start',
                        }}>
                        <Icon
                          name='arrow-back'
                          size={25}
                          color='white'
                          onPress={() => handleClose()}
                        />
                      </View>
                    </ImageBackground>
                  </ImageBackground>
                </TouchableWithoutFeedback>
              </View>
              <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 180 }}>
                <View
                  style={{
                    width: '100%',
                  }}>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.input}
                      placeholder={' اسم المحل'}
                      autoCapitalize={'none'}
                      returnKeyType={'next'}
                      autoCorrect={false}
                      onFocus={() => {
                        // setswipeablePanelActivecategory(false);
                        // setswipeablePanelstarthour(false);
                        // setswipeablePanelendhour(false);
                        // setsubcityswipable(false);
                        // setcityswipable(false);
                      }}
                      onChangeText={(name) => {
                        setname(name);
                      }}
                      value={name}
                      secureTextEntry={false}
                      placeholderTextColor='grey'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.input}
                      placeholder={'البريد الإلكتروني'}
                      autoCapitalize={'none'}
                      returnKeyType={'next'}
                      autoCorrect={false}
                      onFocus={() => {
                        // setswipeablePanelActivecategory(false);
                        // setswipeablePanelstarthour(false);
                        // setswipeablePanelendhour(false);
                        // setsubcityswipable(false);
                      }}
                      onChangeText={(email) => {
                        setemail(email);
                      }}
                      value={email}
                      secureTextEntry={false}
                      placeholderTextColor='grey'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      secureTextEntry={true}
                      placeholder={'كلمه السر'}
                      returnKeyType={'done'}
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      secureTextEntry
                      onFocus={() => {
                        // setswipeablePanelActivecategory(false);
                        // setswipeablePanelstarthour(false);
                        // setswipeablePanelendhour(false);
                        // setsubcityswipable(false);
                      }}
                      onChangeText={(password) => {
                        setpassword(password);
                      }}
                      value={password}
                      style={styles.input}
                      placeholderTextColor='grey'
                      underlineColorAndroid='transparent'
                    />
                  </View>

                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View style={styles.inputtt}>
                      <Image
                        style={{
                          width: 30,
                          height: 30,
                          marginLeft: 10,
                          marginRight: 10,
                        }}
                        resizeMode='cover'
                        source={require('../../imgs/flags/ae.png')}
                      />

                      <TextInput
                        placeholder={'رقم الهاتف '}
                        autoCorrect={false}
                        onFocus={() => {
                          // setswipeablePanelActivecategory(false);
                          // setswipeablePanelstarthour(false);
                          // setswipeablePanelendhour(false);
                          // setsubcityswipable(false);
                        }}
                        onChangeText={(phone) => {
                          setphone(phone);
                        }}
                        style={{height:80}}
                        value={phone}
                        keyboardType={'numeric'}
                        placeholderTextColor='grey'
                        underlineColorAndroid='transparent'
                      />
                    </View>
                  )}
                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View style={styles.inputttar}>
                      <Image
                        style={{ width: 30, height: 30, marginLeft: 10 }}
                        resizeMode='cover'
                        source={require('../../imgs/flags/ae.png')}
                      />

                      <TextInput
                        placeholder={'رقم الهاتف '}
                        autoCorrect={false}
                        onFocus={() => {
                          // setswipeablePanelActivecategory(false);
                          // setswipeablePanelstarthour(false);
                          // setswipeablePanelendhour(false);
                          // setsubcityswipable(false);
                        }}
                        onChangeText={(phone) => {
                          setphone(phone);
                        }}
                        value={phone}
                        keyboardType={'numeric'}
                        placeholderTextColor='grey'
                        underlineColorAndroid='transparent'
                      />
                    </View>
                  )}

                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View
                      style={{
                        flexDirection: 'row',
                        width: DEVICE_width,
                        marginBottom: 20,
                      }}>
                      {country !== 'الدولة' && (
                        <TouchableHighlight
                          underlayColor='transparent'
                          activeOpacity={0.2}
                          onPress={() => {
                            if (citylist.length > 0) {
                              setcityswipable(false);
                              setcityswipable(true);
                              setselecteddd('000');
                              setswipeablePanelActivecategory(false);
                              setswipeablePanelstarthour(false);
                              setswipeablePanelendhour(false);
                              setsubcityswipable(false);
                              setnewpassword('');
                              setcode('');
                              setforgetemail('');
                              setcount(0);
                            } else {
                              let citylist = [];
                              if (
                                typeof GetCountriesReducer !== 'undefined' &&
                                typeof GetCountriesReducer.data !== 'undefined'
                              ) {
                                for (
                                  let f = 0;
                                  f < GetCountriesReducer.payload.data.length;
                                  f++
                                ) {
                                  if (
                                    GetCountriesReducer.payload.data[f]
                                      .country_name_ar === 'الإمارات العربية المتحدة'
                                  ) {
                                    if (
                                      GetCountriesReducer.payload.data[f].cities
                                        .length > 0
                                    ) {
                                      for (
                                        let d = 0;
                                        d <
                                        GetCountriesReducer.payload.data[f]
                                          .cities.length;
                                        d++
                                      ) {
                                        citylist.push({
                                          id: d + 1,
                                          name:
                                            GetCountriesReducer.payload.data[f]
                                              .cities[d].city_name_ar,
                                          idd:
                                            GetCountriesReducer.payload.data[f]
                                              .cities[d].id,
                                        });
                                      }
                                    }
                                  }
                                }

                                setcitylist(citylist);
                                setcityswipable(false);
                                setcityswipable(true);
                              } else {
                                Toast.showWithGravity(
                                  '  لا توجد مدن في هذا البلد',
                                  Toast.LONG,
                                  Toast.BOTTOM
                                );
                              }
                            }
                          }}>
                          <View
                            style={{
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              marginTop: 5,
                              flexDirection: 'row',
                              width: (DEVICE_width - 70) / 2,
                              borderRadius: 20,
                              borderColor: '#3888C6',
                              height: 40,
                              backgroundColor: '#f1f2f2',

                              borderWidth: 1,
                              marginRight: 5,
                              marginLeft: 30,
                            }}>
                            <Text
                              style={{
                                fontSize: 13,
                                textAlign: 'center',
                                paddingBottom: 5,
                                paddingTop: 5,
                                color: 'grey',
                              }}>
                              {' '}
                              {cityselected}
                            </Text>

                            <Icond
                              style={{
                                alignSelf: 'center',
                                alignItems: 'center',
                                alignContent: 'center',
                                marginRight: 10,
                              }}
                              name='chevron-down'
                              size={17}
                              color='#0599ea'
                            />
                          </View>
                        </TouchableHighlight>
                      )}
                      {cityselected !== 'المدينة' && (
                        <TouchableHighlight
                          underlayColor='transparent'
                          activeOpacity={0.2}
                          onPress={() => {
                            if (subcitylist.length > 0) {
                              setselecteddd('000');
                              setsubcityswipable(false);
                              setsubcityswipable(true);
                              setswipeablePanelActivecategory(false);
                              setswipeablePanelstarthour(false);
                              setswipeablePanelendhour(false);
                              setnewpassword('');
                              setcode('');
                              setforgetemail('');
                              setcount(0);
                              setcityswipable(false);
                            } else {
                              Toast.showWithGravity(
                                'لا توجد مناطق في هذه المدينة',
                                Toast.LONG,
                                Toast.BOTTOM
                              );
                            }
                          }}>
                          <View
                            style={{
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              marginTop: 5,
                              flexDirection: 'row',
                              width: (DEVICE_width - 70) / 2,

                              borderWidth: 1,
                              marginLeft: 5,
                              marginRight: 30,
                              borderRadius: 20,
                              borderColor: '#3888C6',
                              height: 40,
                              backgroundColor: '#f1f2f2',
                            }}>
                            <Text
                              style={{
                                fontSize: 13,
                                textAlign: 'center',
                                paddingBottom: 5,
                                paddingTop: 5,
                                color: 'grey',
                              }}>
                              {' '}
                              {subcityselected}
                            </Text>

                            <Icond
                              style={{
                                alignSelf: 'center',
                                alignItems: 'center',
                                alignContent: 'center',
                                marginRight: 10,
                              }}
                              name='chevron-down'
                              size={17}
                              color='#0599ea'
                            />
                          </View>
                        </TouchableHighlight>
                      )}
                    </View>
                  )}
                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View
                      style={{
                        flexDirection: 'row-reverse',
                        width: DEVICE_width,
                        marginBottom: 20,
                      }}>
                      {country !== 'الدولة' && (
                        <TouchableHighlight
                          underlayColor='transparent'
                          activeOpacity={0.2}
                          onPress={() => {
                            if (citylist.length > 0) {
                              setcityswipable(false);
                              setcityswipable(true);
                              setselecteddd('000');
                              setswipeablePanelActivecategory(false);
                              setswipeablePanelstarthour(false);
                              setswipeablePanelendhour(false);
                              setsubcityswipable(false);
                              setnewpassword('');
                              setcode('');
                              setforgetemail('');
                              setcount(0);
                            } else {
                              let citylist = [];
                              if (
                                typeof GetCountriesReducer !== 'undefined' &&
                                typeof GetCountriesReducer.data !== 'undefined'
                              ) {
                                for (
                                  let f = 0;
                                  f < GetCountriesReducer.payload.data.length;
                                  f++
                                ) {
                                  if (
                                    GetCountriesReducer.payload.data[f]
                                      .country_name_ar === 'الإمارات العربية المتحدة'
                                  ) {
                                    if (
                                      GetCountriesReducer.payload.data[f].cities
                                        .length > 0
                                    ) {
                                      for (
                                        let d = 0;
                                        d <
                                        GetCountriesReducer.payload.data[f]
                                          .cities.length;
                                        d++
                                      ) {
                                        citylist.push({
                                          id: d + 1,
                                          name:
                                            GetCountriesReducer.payload.data[f]
                                              .cities[d].city_name_ar,
                                          idd:
                                            GetCountriesReducer.payload.data[f]
                                              .cities[d].id,
                                        });
                                      }
                                    }
                                  }
                                }

                                setcitylist(citylist);
                                setcityswipable(false);
                                setcityswipable(true);
                              } else {
                                Toast.showWithGravity(
                                  '  لا توجد مدن في هذا البلد',
                                  Toast.LONG,
                                  Toast.BOTTOM
                                );
                              }
                            }
                          }}>
                          <View
                            style={{
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              marginTop: 5,
                              flexDirection: 'row-reverse',
                              width: (DEVICE_width - 70) / 2,
                              borderRadius: 20,
                              borderWidth: 1,
                              marginRight: 5,
                              marginLeft: 30,
                              borderRadius: 20,
                              borderColor: '#3888C6',
                              height: 40,
                              backgroundColor: '#f1f2f2',
                            }}>
                            <Text
                              style={{
                                fontSize: 13,
                                textAlign: 'center',
                                paddingBottom: 5,
                                paddingTop: 5,
                                color: 'grey',
                              }}>
                              {' '}
                              {cityselected}
                            </Text>

                            <Icond
                              style={{
                                alignSelf: 'center',
                                alignItems: 'center',
                                alignContent: 'center',
                                marginLeft: 10,
                              }}
                              name='chevron-down'
                              size={17}
                              color='#0599ea'
                            />
                          </View>
                        </TouchableHighlight>
                      )}
                      {cityselected !== 'المدينة' && (
                        <TouchableHighlight
                          underlayColor='transparent'
                          activeOpacity={0.2}
                          onPress={() => {
                             

                            if (subcitylist.length > 0) {
                               

                              setselecteddd('000');
                              setsubcityswipable(false);
                              setsubcityswipable(true);
                              setswipeablePanelActivecategory(false);
                              setswipeablePanelstarthour(false);
                              setswipeablePanelendhour(false);
                              setnewpassword('');
                              setcode('');
                              setforgetemail('');
                              setcount(0);
                              setcityswipable(false);
                            } else {
                               

                              Toast.showWithGravity(
                                'لا توجد مناطق في هذه المدينة',
                                Toast.LONG,
                                Toast.BOTTOM
                              );
                            }
                          }}>
                          <View
                            style={{
                              justifyContent: 'space-between',
                              alignItems: 'center',
                              marginTop: 5,
                              flexDirection: 'row-reverse',
                              width: (DEVICE_width - 70) / 2,
                              borderWidth: 1,
                              marginLeft: 5,
                              marginRight: 30,
                              borderRadius: 20,
                              borderColor: '#3888C6',
                              height: 40,
                              backgroundColor: '#f1f2f2',
                            }}>
                            <Text
                              style={{
                                fontSize: 13,
                                textAlign: 'center',
                                paddingBottom: 5,
                                paddingTop: 5,
                                color: 'grey',
                              }}>
                              {' '}
                              {subcityselected}
                            </Text>

                            <Icond
                              style={{
                                alignSelf: 'center',
                                alignItems: 'center',
                                alignContent: 'center',
                                marginLeft: 10,
                              }}
                              name='chevron-down'
                              size={17}
                              color='#0599ea'
                            />
                          </View>
                        </TouchableHighlight>
                      )}
                    </View>
                  )}

                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.inputaddress}
                      placeholder={'العنوان'}
                      multiline={true}
                      autoCapitalize={'none'}
                      returnKeyType={'next'}
                      autoCorrect={false}
                      onFocus={() => {
                        // setswipeablePanelActivecategory(false);
                        // setswipeablePanelstarthour(false);
                        // setswipeablePanelendhour(false);
                        // setsubcityswipable(false);

                        // setcityswipable(false);
                      }}
                      onChangeText={(address) => {
                        setaddress(address);
                      }}
                      value={address}
                      secureTextEntry={false}
                      placeholderTextColor='grey'
                      underlineColorAndroid='transparent'
                    />
                  </View>

                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <TouchableWithoutFeedback
                      onPress={() => {
                        setswipeablePanelActivecategory(true);

                        setswipeablePanelstarthour(false);
                        setswipeablePanelendhour(false);
                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);
                        setcityswipable(false);
                      }}>
                      <View
                        style={{
                          backgroundColor: '#ffffff',
                          width: DEVICE_width - 60,
                          marginBottom: 10,
                          marginHorizontal: 30,
                          paddingRight: 20,
                          paddingLeft: 20,
                          paddingRight: 20,
                          textAlign: 'right',
                          borderWidth: 1,
                          justifyContent: 'space-between',
                          flexDirection: 'row-reverse',
                          borderRadius: 20,
                          borderColor: '#3888C6',
                          height: 40,
                          backgroundColor: '#f1f2f2',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            color: 'grey',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          {categoryname}
                        </Text>

                        <Icond
                          style={{
                            alignSelf: 'center',
                            alignItems: 'center',
                            alignContent: 'center',
                            marginLeft: 10,
                          }}
                          name='chevron-down'
                          size={17}
                          color='#0599ea'
                        />
                      </View>
                    </TouchableWithoutFeedback>
                  )}
                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <TouchableWithoutFeedback
                      onPress={() => {
                        setswipeablePanelActivecategory(true);

                        setswipeablePanelstarthour(false);
                        setswipeablePanelendhour(false);
                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);
                        setcityswipable(false);
                      }}>
                      <View
                        style={{
                          backgroundColor: '#ffffff',
                          width: DEVICE_width - 60,
                          marginBottom: 10,
                          marginHorizontal: 30,
                          paddingRight: 20,
                          paddingLeft: 20,
                          paddingRight: 20,
                          textAlign: 'right',
                          borderWidth: 1,
                          borderRadius: 5,
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          borderRadius: 20,
                          borderColor: '#3888C6',
                          height: 40,
                          backgroundColor: '#f1f2f2',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            color: 'grey',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          {categoryname}
                        </Text>

                        <Icond
                          style={{
                            alignSelf: 'center',
                            alignItems: 'center',
                            alignContent: 'center',
                            marginLeft: 10,
                          }}
                          name='chevron-down'
                          size={17}
                          color='#0599ea'
                        />
                      </View>
                    </TouchableWithoutFeedback>
                  )}

                  {typeof tags !== 'undefinned' && tags.length > 0 && (
                    <View style={{ width: '100%' }}>
                      <TagsViewSelectionClickablelogin
                        all={tags}
                        type={'categories'}
                        onc={this}
                        langugae={deviceLocale}
                        selected={selectedlabels}
                        first={labelfirst}
                        isExclusive={false}
                      />
                    </View>
                  )}
                  <View
                    style={{
                      width: DEVICE_width - 60,
                      flexDirection: 'row-reverse',
                      height: 40,
                      marginBottom: 10,
                      marginLeft: 30,
                      marginRight: 30,
                      marginTop: 10,
                      justifyContent: 'space-between',
                    }}>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        setswipeablePanelstarthour(true);
                        setswipeablePanelActivecategory(false);
                        setswipeablePanelendhour(false);
                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);
                        setcityswipable(false);
                      }}>
                      <View
                        style={{
                          backgroundColor: '#ffffff',
                          width: (DEVICE_width - 70) / 2,

                          paddingRight: 20,
                          paddingLeft: 20,
                          paddingRight: 20,

                          textAlign: 'right',
                          borderWidth: 1,
                          justifyContent: 'space-between',
                          flexDirection: 'row-reverse',
                          borderRadius: 20,
                          borderColor: '#3888C6',
                          height: 40,
                          backgroundColor: '#f1f2f2',
                          alignItems: 'center',
                        }}>
                        {startclocktext !== '' && (
                          <Text
                            style={{
                              color: 'grey',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            {startclocktext} {start}
                          </Text>
                        )}

                        {startclocktext === '' && (
                          <Text
                            style={{
                              color: 'grey',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            ساعة بدء العمل
                          </Text>
                        )}
                        <Icond
                          style={{
                            alignSelf: 'center',
                            alignItems: 'center',
                            alignContent: 'center',
                            marginLeft: 10,
                          }}
                          name='chevron-down'
                          size={17}
                          color='#0599ea'
                        />
                      </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                      onPress={() => {
                        setswipeablePanelendhour(true);
                        setswipeablePanelActivecategory(false);
                        setswipeablePanelstarthour(false);
                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);
                        setcityswipable(false);
                        // showMode('time')
                      }}>
                      <View
                        style={{
                          backgroundColor: '#ffffff',
                          width: (DEVICE_width - 70) / 2,

                          paddingRight: 20,
                          paddingLeft: 20,
                          paddingRight: 20,

                          textAlign: 'right',
                          borderWidth: 1,
                          justifyContent: 'space-between',
                          flexDirection: 'row-reverse',
                          borderRadius: 20,
                          borderColor: '#3888C6',
                          height: 40,
                          backgroundColor: '#f1f2f2',
                          alignItems: 'center',
                        }}>
                        {endclocktext !== '' && (
                          <Text
                            style={{
                              color: 'grey',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            {endclocktext} {end}
                          </Text>
                        )}

                        {endclocktext === '' && (
                          <Text
                            style={{
                              color: 'grey',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            {' '}
                            ساعة نهاية العمل
                          </Text>
                        )}
                        <Icond
                          style={{
                            alignSelf: 'center',
                            alignItems: 'center',
                            alignContent: 'center',
                            marginLeft: 10,
                          }}
                          name='chevron-down'
                          size={17}
                          color='#0599ea'
                        />
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View
                      style={{
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        flexDirection: 'row-reverse',
                      }}>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontSize: 20,
                          color: 'black',
                        }}>
                        حدد أيام العطل
                      </Text>
                    </View>
                  )}
                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View
                      style={{
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontSize: 20,
                          color: 'black',
                        }}>
                        حدد أيام العطل
                      </Text>
                    </View>
                  )}
                  <TagsViewSelectionClickablelogin
                    all={holidays}
                    type={'holiday'}
                    onc={this}
                    langugae={deviceLocale}
                    first={holidayfirst}
                    selected={selectedholidays}
                    isExclusive={false}
                  />
                  {subcityselected !== 'المنطقة' && cityselected !== 'المدينة' && (
                    <View style={{ flexDirection: 'column' }}>
                      {(Platform.OS === 'ios' ||
                        (Platform.OS === 'android' &&
                          deviceLocale === 'en')) && (
                        <View
                          style={{
                            width: DEVICE_width - 60,
                            marginLeft: 30,
                            marginRight: 30,
                            flexDirection: 'row-reverse',
                          }}>
                          <Text
                            style={{
                              textAlign: 'right',
                              fontSize: 20,
                              color: 'black',
                            }}>
                            {' '}
                            حدد أماكن التوصيل المتاحة
                          </Text>
                        </View>
                      )}

                      {Platform.OS === 'android' && deviceLocale === 'ar' && (
                        <View
                          style={{
                            width: DEVICE_width - 60,
                            marginLeft: 30,
                            marginRight: 30,
                            flexDirection: 'row',
                          }}>
                          <Text
                            style={{
                              textAlign: 'right',
                              fontSize: 20,
                              color: 'black',
                            }}>
                            {' '}
                            حدد أماكن التوصيل المتاحة
                          </Text>
                        </View>
                      )}
                      {(Platform.OS === 'ios' ||
                        (Platform.OS === 'android' &&
                          deviceLocale === 'en')) && (
                        <View
                          style={{
                            width: DEVICE_width - 60,
                            marginLeft: 30,
                            marginRight: 30,
                            flexDirection: 'row-reverse',
                            justifyContent: 'space-between',
                          }}>
                          <Text style={{ fontSize: 17 }}>
                            {cityselected}-{subcityselected}
                          </Text>

                          <TouchableHighlight
                            underlayColor='transparent'
                            activeOpacity={0.2}
                            onPress={() => {
                              setsubcityswipable(false);
                              if (deliverycount < 20) {
                                if (deliverycount === 0) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 1 &&
                                  cityid1 !== null &&
                                  subcityid1 !== null
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 2 &&
                                  cityid2 !== null &&
                                  subcityid2 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 3 &&
                                  cityid3 !== null &&
                                  subcityid3 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 4 &&
                                  cityid4 !== null &&
                                  subcityid4 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 5 &&
                                  cityid5 !== null &&
                                  subcityid5 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 6 &&
                                  cityid6 !== null &&
                                  subcityid6 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 7 &&
                                  cityid7 !== null &&
                                  subcityid7 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 8 &&
                                  cityid8 !== null &&
                                  subcityid8 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 9 &&
                                  cityid9 !== null &&
                                  subcityid9 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 10 &&
                                  cityid10 !== null &&
                                  subcityid10 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 11 &&
                                  cityid11 !== null &&
                                  subcityid11 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 12 &&
                                  cityid12 !== null &&
                                  subcityid12 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 13 &&
                                  cityid13 !== null &&
                                  subcityid13 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 14 &&
                                  cityid14 !== null &&
                                  subcityid14 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 15 &&
                                  cityid15 !== null &&
                                  subcityid15 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 16 &&
                                  cityid16 !== null &&
                                  subcityid16 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 17 &&
                                  cityid17 !== null &&
                                  subcityid17 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 18 &&
                                  cityid18 !== null &&
                                  subcityid18 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 19 &&
                                  cityid19 !== null &&
                                  subcityid19 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 20 &&
                                  cityid20 !== null &&
                                  subcityid20 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                }
                              }
                            }}>
                            <Text style={{ fontSize: 17 }}>+</Text>
                          </TouchableHighlight>
                        </View>
                      )}
                      {Platform.OS === 'android' && deviceLocale === 'ar' && (
                        <View
                          style={{
                            width: DEVICE_width - 60,
                            marginLeft: 30,
                            marginRight: 30,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <Text style={{ fontSize: 17 }}>
                            {cityselected}-{subcityselected}
                          </Text>

                          <TouchableHighlight
                            underlayColor='transparent'
                            activeOpacity={0.2}
                            onPress={() => {
                              setsubcityswipable(false);
                              if (deliverycount < 20) {
                                if (deliverycount === 0) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 1 &&
                                  cityid1 !== null &&
                                  subcityid1 !== null
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 2 &&
                                  cityid2 !== null &&
                                  subcityid2 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 3 &&
                                  cityid3 !== null &&
                                  subcityid3 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 4 &&
                                  cityid4 !== null &&
                                  subcityid4 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 5 &&
                                  cityid5 !== null &&
                                  subcityid5 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 6 &&
                                  cityid6 !== null &&
                                  subcityid6 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 7 &&
                                  cityid7 !== null &&
                                  subcityid7 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 8 &&
                                  cityid8 !== null &&
                                  subcityid8 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 9 &&
                                  cityid9 !== null &&
                                  subcityid9 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 10 &&
                                  cityid10 !== null &&
                                  subcityid10 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 11 &&
                                  cityid11 !== null &&
                                  subcityid11 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 12 &&
                                  cityid12 !== null &&
                                  subcityid12 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 13 &&
                                  cityid13 !== null &&
                                  subcityid13 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 14 &&
                                  cityid14 !== null &&
                                  subcityid14 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 15 &&
                                  cityid15 !== null &&
                                  subcityid15 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 16 &&
                                  cityid16 !== null &&
                                  subcityid16 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 17 &&
                                  cityid17 !== null &&
                                  subcityid17 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 18 &&
                                  cityid18 !== null &&
                                  subcityid18 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 19 &&
                                  cityid19 !== null &&
                                  subcityid19 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                } else if (
                                  deliverycount === 20 &&
                                  cityid20 !== null &&
                                  subcityid20 !== 'الكل'
                                ) {
                                  setdeliverycount(deliverycount + 1);
                                }
                              }
                            }}>
                            <Text style={{ fontSize: 17 }}>+</Text>
                          </TouchableHighlight>
                        </View>
                      )}
                      {(Platform.OS === 'ios' ||
                        (Platform.OS === 'android' &&
                          deviceLocale === 'en')) && (
                        <View style={{ flexDirection: 'column' }}>
                          {deliverycount > 0 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);

                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                    setselecteddd('' + 1);
                                     
                                  } else {
                                     
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                           
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                             
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                               
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity1}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity1 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                      setselecteddd('' + 1);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity1}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 1 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                    setselecteddd('' + 2);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity2}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity2 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                      setselecteddd('' + 2);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity2}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 2 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                    setselecteddd('' + 3);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity3}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity3 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                      setselecteddd('' + 3);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity3}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 3 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setselecteddd('' + 4);

                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity4}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity4 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setselecteddd('' + 4);

                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity4}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 4 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 5);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity5}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity5 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setselecteddd('' + 5);

                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity5}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 5 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 6);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity6}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity6 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setselecteddd('' + 6);

                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity6}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 6 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 7);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity7}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity7 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 7);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity7}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 7 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 8);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity8}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity8 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 8);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity8}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 8 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setselecteddd('' + 9);
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity9}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity9 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setsubcityswipable(false);
                                      setselecteddd('' + 9);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity9}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 9 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setselecteddd('' + 10);

                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity10}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity10 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 10);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity10}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 10 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 11);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity11}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity11 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 11);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity11}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 11 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 12);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity12}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity12 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 12);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity12}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 12 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 13);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity13}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity13 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 13);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity13}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 13 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 14);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity14}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity14 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 14);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity14}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 14 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setselecteddd('' + 15);
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity15}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity15 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 15);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity15}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 15 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 16);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity16}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity16 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 16);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity16}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 16 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setselecteddd('' + 17);

                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];

                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity17}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity17 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 17);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity17}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 17 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setselecteddd('' + 18);
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];

                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity18}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity18 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 18);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity18}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 18 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setselecteddd('' + 19);
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];

                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity19}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity19 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     
                                    if (subcitylist.length > 0) {
                                       
                                      setselecteddd('' + 19);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity19}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 19 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setselecteddd('' + 20);

                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];

                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row-reverse',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity20}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginLeft: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity20 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     

                                    if (subcitylist.length > 0) {
                                       

                                      setselecteddd('' + 20);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       

                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row-reverse',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity20}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginLeft: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                        </View>
                      )}

                      {Platform.OS === 'android' && deviceLocale === 'ar' && (
                        <View style={{ flexDirection: 'column' }}>
                          {deliverycount > 0 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);

                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                    setselecteddd('' + 1);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity1}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity1 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     

                                    if (subcitylist.length > 0) {
                                       

                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                      setselecteddd('' + 1);
                                    } else {
                                       

                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity1}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 1 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                    setselecteddd('' + 2);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity2}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity2 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     

                                    if (subcitylist.length > 0) {
                                       

                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                      setselecteddd('' + 2);
                                    } else {
                                       

                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity2}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 2 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                    setselecteddd('' + 3);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity3}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity3 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     

                                    if (subcitylist.length > 0) {
                                       

                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                      setselecteddd('' + 3);
                                    } else {
                                       

                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity3}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 3 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setselecteddd('' + 4);

                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity4}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity4 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                     

                                    if (subcitylist.length > 0) {
                                       

                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setselecteddd('' + 4);

                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                       

                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity4}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 4 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 5);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity5}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity5 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setselecteddd('' + 5);

                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity5}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 5 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 6);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity6}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity6 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setselecteddd('' + 6);

                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity6}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 6 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 7);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity7}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity7 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 7);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity7}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 7 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 8);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity8}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity8 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 8);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity8}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 8 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setselecteddd('' + 9);
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity9}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity9 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setsubcityswipable(false);
                                      setselecteddd('' + 9);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity9}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 9 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setselecteddd('' + 10);

                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity10}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity10 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 10);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity10}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 10 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 11);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity11}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity11 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 11);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity11}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 11 && (
                            <View
                              style={{
                                flexDirection: 'row-reverse',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 12);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity12}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity12 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 12);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity12}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 12 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 13);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity13}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity13 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 13);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity13}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 13 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 14);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity14}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity14 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 14);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity14}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 14 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setselecteddd('' + 15);
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity15}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity15 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 15);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity15}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 15 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setselecteddd('' + 16);

                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];
                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity16}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity16 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 16);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity16}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 16 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setselecteddd('' + 17);

                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];

                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity17}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity17 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 17);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity17}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 17 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setselecteddd('' + 18);
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];

                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity18}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity18 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 18);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity18}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 18 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setselecteddd('' + 19);
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];

                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity19}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity19 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 19);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity19}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                          {deliverycount > 19 && (
                            <View
                              style={{
                                flexDirection: 'row',
                                width: DEVICE_width,
                                marginBottom: 10,
                              }}>
                              <TouchableHighlight
                                underlayColor='transparent'
                                activeOpacity={0.2}
                                onPress={() => {
                                  if (citylist.length > 0) {
                                    setcityswipable(false);
                                    setcityswipable(true);
                                    setselecteddd('' + 20);

                                    setswipeablePanelActivecategory(false);
                                    setswipeablePanelstarthour(false);
                                    setswipeablePanelendhour(false);
                                    setsubcityswipable(false);
                                    setnewpassword('');
                                    setcode('');
                                    setforgetemail('');
                                    setcount(0);
                                  } else {
                                    let citylist = [];

                                    if (
                                      typeof GetCountriesReducer !==
                                        'undefined' &&
                                      typeof GetCountriesReducer.data !==
                                        'undefined'
                                    ) {
                                      for (
                                        let f = 0;
                                        f <
                                        GetCountriesReducer.payload.data.length;
                                        f++
                                      ) {
                                        if (
                                          GetCountriesReducer.payload.data[f]
                                            .country_name_ar === 'الإمارات العربية المتحدة'
                                        ) {
                                          if (
                                            GetCountriesReducer.payload.data[f]
                                              .cities.length > 0
                                          ) {
                                            for (
                                              let d = 0;
                                              d <
                                              GetCountriesReducer.payload.data[
                                                f
                                              ].cities.length;
                                              d++
                                            ) {
                                              citylist.push({
                                                id: d + 1,
                                                name:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d]
                                                    .city_name_ar,
                                                idd:
                                                  GetCountriesReducer.payload
                                                    .data[f].cities[d].id,
                                              });
                                            }
                                          }
                                        }
                                      }

                                      setcitylist(citylist);
                                      setcityswipable(false);
                                      setcityswipable(true);
                                    } else {
                                      Toast.showWithGravity(
                                        '  لا توجد مدن في هذا البلد',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }
                                }}>
                                <View
                                  style={{
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    width: (DEVICE_width - 70) / 2,
                                    borderRadius: 5,
                                    borderColor: '#3888C6',
                                    borderWidth: 1,
                                    marginRight: 5,
                                    marginLeft: 30,
                                    height: 30,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 10,
                                      textAlign: 'center',
                                      paddingBottom: 5,
                                      paddingTop: 5,
                                      color: 'grey',
                                    }}>
                                    {' '}
                                    {deliverycity20}
                                  </Text>

                                  <Icond
                                    style={{
                                      alignSelf: 'center',
                                      alignItems: 'center',
                                      alignContent: 'center',
                                      marginRight: 10,
                                    }}
                                    name='chevron-down'
                                    size={17}
                                    color='#0599ea'
                                  />
                                </View>
                              </TouchableHighlight>

                              {deliverycity20 !== 'المدينة' && (
                                <TouchableHighlight
                                  underlayColor='transparent'
                                  activeOpacity={0.2}
                                  onPress={() => {
                                    if (subcitylist.length > 0) {
                                      setselecteddd('' + 20);
                                      setsubcityswipable(false);
                                      setsubcityswipable(true);
                                      setswipeablePanelActivecategory(false);
                                      setswipeablePanelstarthour(false);
                                      setswipeablePanelendhour(false);
                                      setnewpassword('');
                                      setcode('');
                                      setforgetemail('');
                                      setcount(0);
                                      setcityswipable(false);
                                    } else {
                                      Toast.showWithGravity(
                                        'لا توجد مناطق في هذه المدينة',
                                        Toast.LONG,
                                        Toast.BOTTOM
                                      );
                                    }
                                  }}>
                                  <View
                                    style={{
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                      marginTop: 5,
                                      flexDirection: 'row',
                                      width: (DEVICE_width - 70) / 2,
                                      borderRadius: 5,
                                      borderColor: '#3888C6',
                                      borderWidth: 1,
                                      marginLeft: 5,
                                      marginRight: 30,
                                      height: 30,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        textAlign: 'center',
                                        paddingBottom: 5,
                                        paddingTop: 5,
                                        color: 'grey',
                                      }}>
                                      {' '}
                                      {deliverysubcity20}
                                    </Text>

                                    <Icond
                                      style={{
                                        alignSelf: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginRight: 10,
                                      }}
                                      name='chevron-down'
                                      size={17}
                                      color='#0599ea'
                                    />
                                  </View>
                                </TouchableHighlight>
                              )}
                            </View>
                          )}
                        </View>
                      )}
                    </View>
                  )}
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row-reverse',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableHighlight
                      underlayColor='transparent'
                      activeOpacity={0.2}
                      onPress={() => {
                        setswipeablePanelActivecategory(false);
                        setswipeablePanelstarthour(false);
                        setswipeablePanelendhour(false);
                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');

                        setforgetemail('');
                        setcount(0);
                        setcityswipable(false);
                        
                        onRegisterPressed(
                          email,
                          password,
                          name,
                          phone,
                          address,
                          subcityselected,
                          imagebaseprofile,
                          subcityid,
                          subcityidd,
                          categoryid,
                          selectedlabels,
                          startclock,
                          endclock,
                          selectedholidays
                        );
                      }}>
                      <LinearGradient
                        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                        style={{
                          justifyContent: 'center',
                          alignContent: 'center',
                          alignItems: 'center',
                          borderRadius: 40,
                        }}>
                        <Text style={styles.loginText}>{'تسجيل مستخدم'}</Text>
                      </LinearGradient>
                    </TouchableHighlight>
                  </View>
                </View>
              </ScrollView>
            </View>
       </KeyboardAwareScrollView>
          </Animated.View>
        </View>
      </Animated.View>

      <SwipeablePanel
        isActive={cityswipable}
        closeOnTouchOutside={true}
        onClose={this}
        openLarge={true}

        // onClose={this.closePanel}
        // onPressCloseButton={this.closePanel}
      >
        <View
          style={{
            marginTop: 15,
            marginBottom: 15,
            flexDirection: 'column',
          }}>
          <SelectableFlatlist
            data={citylist}
            state={STATE.EDIT}
            multiSelect={false}
            checkColor='white'
            uncheckColor='white'
            itemsSelected={(selectedItem) => {
              () => {
                itemsSelectedcity(selectedItem);
              };
            }}
            initialSelectedIndex={[getinitial(cityid)]}
            cellItemComponent={(item, otherProps) => rowcityItem(item)}
          />
          <View style={{ height: 30 }}></View>
        </View>
      </SwipeablePanel>

      <SwipeablePanel
        isActive={subcityswipable}
        closeOnTouchOutside={true}
        onClose={this}
        openLarge={true}
        // onClose={this.closePanel}
        // onPressCloseButton={this.closePanel}
      >
        <View style={{ marginTop: 15, flexDirection: 'column' }}>
          <SelectableFlatlist
            data={subcitylist}
            state={STATE.EDIT}
            multiSelect={false}
            checkColor='white'
            uncheckColor='white'
            itemsSelected={(selectedItem) => {
              () => {
                itemsSelectedsubcity(selectedItem);
              };
            }}
            initialSelectedIndex={getinitial(subcityid)}
            cellItemComponent={(item, otherProps) => rowsubcityItem(item)}
          />
          <View style={{ height: 20 }}></View>
        </View>
      </SwipeablePanel>

      <SwipeablePanels
        isActive={swipeablePanelActivecategory}
        closeOnTouchOutside={false}
        onClose={this}

        // onClose={this.closePanel}
        // onPressCloseButton={this.closePanel}
      >
        <View style={{ marginTop: 15, flexDirection: 'column' }}>
          <SelectableFlatlist
            data={[
              { id: 1, name: 'السلات  الغذائية' },
              { id: 2, name: 'المطاعم' },

              { id: 3, name: 'الحلويات' },
              { id: 4, name: 'الهدايا و الورود' },
            ]}
            state={STATE.EDIT}
            multiSelect={false}
            checkColor='white'
            uncheckColor='white'
            itemsSelected={(selectedItem) => {
              () => {
                itemsSelectedcategory(selectedItem);
              };
            }}
            initialSelectedIndex={[categoryid - 1]}
            cellItemComponent={(item, otherProps) => rowItemcategory(item)}
          />
          <View style={{ height: 20 }}></View>
        </View>
      </SwipeablePanels>

      <SwipeablePanelm
        isActive={swipeablePanelstarthour}
        closeOnTouchOutside={true}
        onClose={this}
        openLarge>
               <LinearGradient
                  colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                  style={{
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    borderRadius: 22,
                    width:AppMetrics.screenWidth,
                    height:AppMetrics.screenHeight
                  }}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <View>
          <View style={{  padding: 20 }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '400',
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  color: 'white',
                }}>
                {' '}
                حدد ساعة بدء العمل{' '}
              </Text>
            </View>
         
            <View style={{ width: '100%', flexDirection: 'row-reverse' }}>
              <TouchableHighlight
                underlayColor='transparent'
                activeOpacity={0.2}
                style={{ width: '50%', height: 50 }}
                onPress={(_) => {
                    setstart('صباحا');
                    setamcolors('white');
                    setamsizes(20);
                    setamweights('bold');
                    setambacks('rgba(52, 52, 52, 0.1)');
                    setpmcolors('grey');
                    setpmsizes(17);
                    setpmweights('normal');
                    setpmbacks('rgba(52, 52, 52, 0.1)');
                  }}>
                  <Text
                    style={{
                      textAlign: 'center',
                      color: amcolors,
                      fontSize: amsizes,
                      fontWeight: amweights,
                      padding: 10,
                      color: 'white',
                    }}>
                  صباحا
                </Text>
              </TouchableHighlight>
              <TouchableHighlight
                underlayColor='transparent'
                activeOpacity={0.2}
                style={{ width: '50%', height: 50 }}
                 onPress={(_) => {
                  setstart('مساء');
                  setpmcolors('white');
                  setpmsizes(20);
                  setpmweights('bold');
                  setamcolors('grey');
                  setamsizes(17);
                  setamweights('normal');
                  setpmbacks('#0599ea');
                  setambacks('white');
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: pmcolors,
                    fontSize: pmsizes,
                    fontWeight: pmweights,
                 
                    padding: 10,
                    color: 'white',
                  }}>
                  مساء
                </Text>
              </TouchableHighlight>
            </View>
            <View
              style={{
                width: '100%',
                height: 10,
                borderColor: '#dfdfdf',
                flexDirection: 'column',
              }}></View>
            <SelectableGrid
              data={[
                { id: 1, name: '2:00' },
                { id: 2, name: '1:00' },
                { id: 3, name: '4:00' },
                { id: 4, name: '3:00' },
                { id: 5, name: '6:00' },
                { id: 6, name: '5:00' },
                { id: 7, name: '8:00' },
                { id: 8, name: '7:00' },
                { id: 9, name: '10:00' },
                { id: 10, name: '9:00' },
                { id: 11, name: '12:00' },
                { id: 12, name: '11:00' },
              ]}
              state={STATE.EDIT}
              multiSelect={false}
              checkColor='#0599ea'
              uncheckColor='white'
              itemsSelected={(selectedItem) => {
                (_) => itemsSelectedstart(selectedItem);
              }}
              initialSelectedIndex={[startclockid - 1]}
              cellItemComponent={(item, otherProps) => rowItemstart(item)}
            />
            <View style={{ height: 20 }}></View>
          </View>
        </View>
        </LinearGradient>
      </SwipeablePanelm>

      <SwipeablePanelm
        isActive={swipeablePanelendhour}
        closeOnTouchOutside={true}
        onClose={this}
        openLarge>
             <LinearGradient
                  colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                  style={{
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    borderRadius: 22,
                    width:AppMetrics.screenWidth,
                    height:AppMetrics.screenHeight
                  }}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <View>
            <View style={{  padding: 20 }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '400',
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignItems: 'center',
                  color:'white',
                  alignSelf: 'center',
                }}>
                {' '}
                حدد ساعة نهاية العمل{' '}
              </Text>
            </View>
            <View style={{ width: '100%', flexDirection: 'row-reverse' }}>
              <TouchableHighlight
                underlayColor='transparent'
                activeOpacity={0.2}
                style={{ width: '50%', height: 50 }}
                onPress={(_) => {
                  setend('صباحا');
                  setamcolor('white');
                  setamsize(20);
                  setamweight('bold');
                  setamback('#0599ea');
                  setpmcolor('grey');
                  setpmsize(17);
                  setpmweight('normal');
                  setpmback('white');
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: amcolor,
                    fontSize: amsize,
                    fontWeight: amweight,
                    padding: 10,
                    color: 'white',
                  }}>
                  صباحا
                </Text>
              </TouchableHighlight>
              <TouchableHighlight
                underlayColor='transparent'
                activeOpacity={0.2}
                style={{ width: '50%', height: 50 }}
                onPress={(_) => {
                  setend('مساء');
                  setpmcolor('white');
                  setpmsize(20);
                  setpmweight('bold');
                  setamcolor('grey');
                  setamsize(17);
                  setamweight('normal');
                  setpmback('#0599ea');
                  setamback('white');
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: pmcolor,
                    fontSize: pmsize,
                    fontWeight: pmweight,
                    color: 'white',
                    padding: 10,
                  }}>
                  مساء
                </Text>
              </TouchableHighlight>
            </View>
            <View
              style={{
                width: '100%',
                height: 10,
                borderColor: '#dfdfdf',
                flexDirection: 'column',
              }}></View>
            <SelectableGrid
              data={[
                { id: 1, name: '2:00' },
                { id: 2, name: '1:00' },
                { id: 3, name: '4:00' },
                { id: 4, name: '3:00' },
                { id: 5, name: '6:00' },
                { id: 6, name: '5:00' },
                { id: 7, name: '8:00' },
                { id: 8, name: '7:00' },
                { id: 9, name: '10:00' },
                { id: 10, name: '9:00' },
                { id: 11, name: '12:00' },
                { id: 12, name: '11:00' },
              ]}
              state={STATE.EDIT}
              multiSelect={false}
              checkColor='#0599ea'
              uncheckColor='white'
              itemsSelected={(selectedItem) => {
                (_) => itemsSelectedend(selectedItem);
              }}
              initialSelectedIndex={[endclockid - 1]}
              cellItemComponent={(item, otherProps) => rowItemend(item)}
            />
            <View style={{ height: 20 }}></View>
          </View>
        </View>
        </LinearGradient>
      </SwipeablePanelm>
      <BottomSheet
        ref={sheetRef}
        snapPoints={[fullheighttt, 0]}
        initialSnap={0}
        borderRadius={20}
        renderContent={renderContent}
        renderHeader={renderHeader}
        enabledInnerScrolling={false}
        onOpenEnd={() => setSheetIsOpen(true)}
        onCloseEnd={() => setSheetIsOpen(false)}
      />

      <ActionSheet
        ref={actionSheetRef}
        containerStyle={{
          backgroundColor: 'transparent',
        }}
        elevation={0}>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            margin: 20,
            borderRadius: 10,
          }}>
          <TouchableHighlight
            underlayColor='transparent'
            activeOpacity={0.2}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openCamera(true);
              }, 1000);
            }}>
            <Text>الكاميرا</Text>
          </TouchableHighlight>
          <View
            style={{
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: '#707070',
            }}
          />
          <TouchableHighlight
            underlayColor='transparent'
            activeOpacity={0.2}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openGallary(true);
              }, 1000);
            }}>
            <Text>المعرض</Text>
          </TouchableHighlight>
        </View>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            marginHorizontal: 20,
            borderRadius: 10,
            marginBottom: 20,
          }}>
          <TouchableHighlight
            underlayColor='transparent'
            activeOpacity={0.2}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();
            }}>
            <Text style={{ color: 'red' }}>إلغاء</Text>
          </TouchableHighlight>
        </View>
      </ActionSheet>

      <Spinner
        visible={spinner}
        textContent={''}
        textStyle={styles.spinnerTextStyle}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 80,
    marginRight: 10,
    marginLeft: 10,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0599ea',
    height: MARGIN,
    borderRadius: 5,
    zIndex: 100,
  },
  socialContainer: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    width: DEVICE_width - 60,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#0599ea',
    borderRadius: 100,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#0599ea',
  },

  image: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  text: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 5,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 30,
    marginBottom: 20,
  },
  btnEye: {
    top: 10,
    left: 40,
  },
  iconEye: {
    width: 25,
    height: 25,
  },
  btnEyeRight: {
    top: 10,
    right: 40,
  },
  iconEyeRight: {
    width: 25,
    height: 25,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 5,
  },

  restoreButtonContainer: {
    width: 250,
    marginBottom: 15,
    alignItems: 'flex-end',
  },
  socialButtonContent: {
    marginRight: 20,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    color: '#FFFFFF',
    marginRight: 5,
  },
  socialLoginButton: {
    backgroundColor: '#929292',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 10,
    borderRadius: 5,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginText: {
    color: '#FFFFFF',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
  buttonText: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    paddingHorizontal: 20,
  },
  buttonStyle: {
    borderRadius: 5,
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  input: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#3888C6',
    color: 'grey',
  },
  inputaddress: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 70,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#3888C6',
    color: 'grey',
  },

  inputtt: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 50,
    flexDirection: 'row-reverse',
    marginBottom: 20,

    alignItems: 'center',
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#3888C6',
    color: 'grey',
  },
  inputttar: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 50,
    flexDirection: 'row',
    marginBottom: 20,

    alignItems: 'center',
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'left',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#3888C6',
    color: 'grey',
  },
  inputDialog: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 100,
    height: 40,
    marginTop: 15,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#3888C6',
    color: 'grey',
  },
  inputRight: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    textAlign: 'left',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#3888C6',
    color: 'grey',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',
    marginBottom: 20,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperr: {
    flexDirection: 'row-reverse',
    width: DEVICE_width / 2,
    marginBottom: 20,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  cover: {
    height: '90%',
  },
  sheet: {
    top: Dimensions.get('window').height,
    left: 0,
    right: 0,
    height: '100%',
    justifyContent: 'flex-end',
  },
  popup: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 80,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  header: {
    height: 25,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

    backgroundColor: '#cccccc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  panelHandle: {
    width: 35,
    height: 6,
    borderRadius: 4,
    backgroundColor: 'rgba(255,255,255,0.7)',
    marginBottom: 0,
  },
  container: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 80,
    marginRight: 10,
    marginLeft: 10,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3888C6',
    height: MARGIN,
    borderRadius: 5,
    zIndex: 100,
  },
  socialContainer: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    width: DEVICE_width - 60,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#3888C6',
    borderRadius: 100,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#3888C6',
  },

  image: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  text: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 5,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 30,
    marginBottom: 20,
  },
  btnEye: {
    top: 10,
    left: 40,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: '#3888C6',
  },
  btnEyeRight: {
    top: 10,
    right: 40,
  },
  iconEyeRight: {
    width: 25,
    height: 25,
    tintColor: '#3888C6',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 5,
  },
  loginButton: {
    backgroundColor: '#3888C6',
  },
  restoreButtonContainer: {
    width: 250,
    marginBottom: 15,
    alignItems: 'flex-end',
  },
  socialButtonContent: {
    marginRight: 20,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    color: '#FFFFFF',
    marginRight: 5,
  },
  socialLoginButton: {
    backgroundColor: '#929292',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 10,
    borderRadius: 5,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginText: {
    color: '#FFFFFF',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    margin: 14,
    textAlign: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
  buttonText: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    paddingHorizontal: 20,
  },
  buttonStyle: {
    borderRadius: 5,
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  input: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 20,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  input_login: {
    backgroundColor: 'white',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 20,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  inputaddress: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 60,
    height: 70,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 40,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
    borderRadius: 20,
  },

  inputtt: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 60,
    height: 40,
    flexDirection: 'row-reverse',
    marginBottom: 10,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    alignItems: 'center',
    marginHorizontal: 30,
    paddingRight: 20,
    paddingLeft: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  inputDialog: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 100,
    height: 40,
    marginTop: 15,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 20,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  inputRight: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    textAlign: 'left',
    borderWidth: 1,
    borderRadius: 10,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',
    marginBottom: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperr: {
    flexDirection: 'row-reverse',

    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperrr: {
    flexDirection: 'row-reverse',
    marginTop: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  cover: {
    height: '100%',
  },
  sheet: {
    top: Dimensions.get('window').height,
    left: 0,
    right: 0,
    height: '100%',
    justifyContent: 'flex-end',
  },
  popup: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 80,
  },
  header: {
    height: 25,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  panelHandle: {
    width: 35,
    height: 6,
    borderRadius: 4,
    backgroundColor: 'rgba(255,255,255,0.7)',
    marginBottom: 0,
  },
});
