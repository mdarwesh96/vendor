import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  View,
  StyleSheet,
  TouchableHighlight,
  Dimensions,
  ImageBackground,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  Platform,
  NativeModules,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { drawer } from '../../AppNavigation';
import { drawerD } from '../../AppNavigationDefault';

const DEVICE_hight = Dimensions.get('window').height;
const DEVICE_width = Dimensions.get('window').width;

import TagsVieww from '../../common/Tags/TagsVieww';
import Stars from 'react-native-stars';
import { ButtonGroup } from 'react-native-elements';
import DynamicTabView from '../../common/DynamicTabs/DynamicTabView';
import { UpdateVendorStateAction } from '../../actions/Actions';
import { UpdateAvailabiltyAction } from '../../actions/Actions';
import { ClearAction } from '../../actions/Actions';
import { LogoutAction } from '../../actions/Actions';
import moment from 'moment';
import LinearGradient from 'react-native-linear-gradient';

let templabels = [];

let deviceLocale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;
if (
  typeof deviceLocale !== 'undefined' &&
  deviceLocale !== null &&
  deviceLocale.includes('ar')
) {
  deviceLocale = 'ar';
} else {
  deviceLocale = 'en';
}
export default function HomeSeller(props) {
  const dispatch = useDispatch();

  const [vendorname, setvendorname] = useState('');
  const [vendordescription, setvendordescription] = useState('');
  const [vendoroffdays, setvendoroffdays] = useState('');
  const [vendoropeningtime, setvendoropeningtime] = useState('');
  const [imageLoadingvendor, setimageLoadingvendor] = useState(true);
  const [imageLoading, setimageLoading] = useState(true);

  const [currency, setcurrency] = useState('درهم اماراتي');
  const [currencyid, setcurrencyid] = useState('1');
  const [id, setiddd] = useState(1);

  const [selected, setselected] = useState('');
  const [prev, setprev] = useState(2);

  const [vendorratingcount, setvendorratingcount] = useState(0);
  const [vendorrating, setvendorrating] = useState(0);
  const [defaultIndex, setdefaultIndex] = useState(0);
  const [menuelist, setmenuelist] = useState([]);
  const [vendorlabels, setvendorlabels] = useState(templabels);
  const [token, settoken] = useState('');

  const [colorss, setcolorss] = useState('#0599ea');
  const [selectedIndexOptions, setselectedIndexOptions] = useState(2);
  const component2 = () => (
    <Text style={{ color: selectedIndexOptions === 2 ? 'white' : 'grey' }}>
      مفتوح
    </Text>
  );
  const component1 = () => (
    <Text style={{ color: selectedIndexOptions === 1 ? 'white' : 'grey' }}>
      مشغول
    </Text>
  );
  const component0 = () => (
    <Text style={{ color: selectedIndexOptions === 0 ? 'white' : 'grey' }}>
      مغلق
    </Text>
  );
  const VendorDetailsReducer = useSelector(
    (state) => state.VendorDetailsReducer
  );
  const GetCategoriesReducer = useSelector(
    (state) => state.GetCategoriesReducer
  );
  const GetLabelsReducer = useSelector((state) => state.GetLabelsReducer);
  const UpdateVendorReducer = useSelector((state) => state.UpdateVendorReducer);
  const UpdateAvailabilityReducer = useSelector(
    (state) => state.UpdateAvailabilityReducer
  );

  const getprice = (id) => {
    if (
      typeof VendorDetailsReducer !== 'undefined' &&
      typeof VendorDetailsReducer.prices !== 'undefined'
    ) {
      for (let s = 0; s < VendorDetailsReducer.prices.length; s++) {
        if ('' + id === '' + VendorDetailsReducer.prices[s].item_id) {
          return parseFloat(VendorDetailsReducer.prices[s].price).toFixed(2);
        }
      }
    }
  };

  const getmylbals = (labelsarray, myregisteredlabels) => {
    let labels = [];
    for (let s = 0; s < myregisteredlabels.length; s++) {
      for (let a = 0; a < labelsarray.length; a++) {
        if ('' + labelsarray[a].id === '' + myregisteredlabels[s].label_id) {
          labels.push(labelsarray[a]);
        }
      }
    }
    return labels;
  };

  if (selected !== '') {
    if (
      typeof UpdateVendorReducer !== 'undefined' &&
      typeof UpdateVendorReducer.error !== 'undefined' &&
      UpdateVendorReducer.error !== '' &&
      UpdateVendorReducer.error !== 'success'
    ) {
      if (prev === 0) {
        setcolorss('red');
      }
      setselectedIndexOptions(prev);
      setselected('');
    } else if (
      typeof UpdateVendorReducer !== 'undefined' &&
      typeof UpdateVendorReducer.error !== 'undefined' &&
      UpdateVendorReducer.error !== '' &&
      UpdateVendorReducer.error === 'success'
    ) {
      setselected('');

      setprev(selectedIndexOptions);
    }
  }
  // setVendorDetailsReducer(VendorDetailsReducerl);
  const loadApp = async () => {
    try {
      settoken(props.navigation.state.params.token);

      setiddd(props.navigation.state.params.id);
      let d = props.navigation.state.params.stateavailable;

      if (d === true) {
        setselectedIndexOptions(2);
        setcolorss('#0599ea');
        setprev(2);
      } else {
        setselectedIndexOptions(0);
        setcolorss('red');

        setprev(0);
      }
    } catch (e) {}
  };

  useEffect(() => {
    setselected('');

    loadApp();

    try {
      if (
        typeof UpdateAvailabilityReducer !== 'undefined' &&
        typeof UpdateAvailabilityReducer.availableindex !== 'undefined'
      ) {
        if ('' + UpdateAvailabilityReducer.availableindex === '1') {
          setselectedIndexOptions(2);
          setcolorss('#0599ea');
          setprev(2);
        } else if ('' + UpdateAvailabilityReducer.availableindex === '2') {
          setselectedIndexOptions(1);
          setcolorss('orange');
          setprev(1);
        } else if ('' + UpdateAvailabilityReducer.availableindex === '3') {
          setselectedIndexOptions(0);
          setcolorss('red');

          setprev(0);
        }
      }
    } catch (e) {}

    try {
      if (VendorDetailsReducer.ratevendor !== null) {
        setvendorrating(VendorDetailsReducer.ratevendor);
      } else {
        setvendorrating(0);
      }

      if (VendorDetailsReducer.payload.item !== null) {
        setvendorname(VendorDetailsReducer.payload.vendor.name);

        try {
          for (let i = 0; i < GetCategoriesReducer.payload.length; i++) {
            if (
              GetCategoriesReducer.payload[i].id ===
              parseInt(
                VendorDetailsReducer.payload.vendor.category_id[0].category_id
              )
            ) {
              setvendordescription(
                GetCategoriesReducer.payload[i].category_name
              );
            }
          }

          let offdays = ' ';
          if (VendorDetailsReducer.payload.vendor.work_days !== null) {
            for (
              let s = 0;
              s < VendorDetailsReducer.payload.vendor.work_days.length;
              s++
            ) {
              if (
                VendorDetailsReducer.payload.vendor.work_days[0].offdays !==
                null
              ) {
                if (s === 0) {
                  offdays =
                    offdays +
                    VendorDetailsReducer.payload.vendor.work_days[s].offdays;
                } else {
                  offdays =
                    offdays +
                    ' , ' +
                    VendorDetailsReducer.payload.vendor.work_days[s].offdays;
                }
              } else {
                offdays = 'مفتوح طوال الأسبوع';
              }
            }
          } else {
            offdays = 'مفتوح طوال الأسبوع';
          }

          setvendoroffdays(offdays);
          let r,y;

try{
          let from = VendorDetailsReducer.payload.vendor.work_hours[0].start;
          if(from.includes('صباحا'))
          {
            from= from.replace('صباحا','') 
           }
        
          if(from.includes('مساء'))
          {
             from=from.replace('مساء','') 
          }
         
          //  let duration =moment.utc.duration(from.asMilliseconds())
          // let duration = moment.duration(from.diff(offset))
          let duration = moment.duration(from, 'milliseconds');
      
           y = parseInt(moment.utc(duration.asMilliseconds()).format('HH'));
          

          y = y + 2;
       

          if (y === 25) {
            y = 12 + ' صباحا ';
          } else {
            if (y > 12) {
              y = y - 12;
              y = y + ' مساء ';
            } else {
              y = y + ' صباحا ';
            }
          }
         

        }catch(r)
        {
        
        }
          
try{
          let to = VendorDetailsReducer.payload.vendor.work_hours[0].end;
       
          if(to.includes('صباحا'))
          {
             to=to.replace('صباحا','') 
          }
    
          if(to.includes('مساء'))
          {
           to= to.replace('مساء','') 
          }
          let durationv = moment.duration(to, 'milliseconds');
           r = parseInt(moment.utc(durationv.asMilliseconds()).format('HH'));
          r = r + 2;
        

          if (r === 25) {
            r = 12 + ' صباحا ';
          } else {
            if (r > 12) {
              r = r - 12;
              r = r + ' مساء ';
            } else {
              r = r + ' صباحا ';
            }
          }
        }catch(e)
        {
         
        }
          setvendoropeningtime(y + ' - ' + r);

          if (
            typeof VendorDetailsReducer.ratecount !== 'undefined' &&
            VendorDetailsReducer.ratecount !== null
          ) {
            setvendorratingcount(VendorDetailsReducer.ratecount);
          } else {
            setvendorratingcount(0);
          }

          if (VendorDetailsReducer.payload.item.length > 0) {
            let setmenuelistt = [];

            for (let s = 0; s < VendorDetailsReducer.payload.item.length; s++) {
              if (
                setmenuelistt.some(
                  (setmenuelistt) =>
                    setmenuelistt.subcategoryid ===
                    VendorDetailsReducer.payload.item[s].subcategory_id
                )
              ) {
                let index = setmenuelistt.findIndex(
                  (x) =>
                    x.subcategoryid ===
                    VendorDetailsReducer.payload.item[s].subcategory_id
                );

                let previtems = setmenuelistt[index].items;
                previtems.push(VendorDetailsReducer.payload.item[s]);

                setmenuelistt.splice(index, 1, {
                  subcategoryid:
                    VendorDetailsReducer.payload.item[s].subcategory_id,
                  subcategoryname:
                    VendorDetailsReducer.payload.item[s].subcategories
                      .category_name_ar,
                  items: previtems,
                });
              } else {
                if (
                  VendorDetailsReducer.payload.item[s].subcategory_id !== null
                ) {
                  let items = [];

                  items.push(VendorDetailsReducer.payload.item[s]);

                  setmenuelistt.push({
                    subcategoryid:
                      VendorDetailsReducer.payload.item[s].subcategory_id,
                    subcategoryname:
                      VendorDetailsReducer.payload.item[s].subcategories
                        .category_name_ar,
                    items: items,
                  });
                }
              }
            }

            setmenuelist(setmenuelistt);
          }
        } catch (ee) {}
      }
    } catch (e) {}

    try {
      if (
        GetLabelsReducer.payload !== null &&
        VendorDetailsReducer.payload.vendor.label_id !== null &&
        VendorDetailsReducer.payload.vendor.label_id.length > 0
      ) {
        setvendorlabels(
          getmylbals(
            GetLabelsReducer.payload,
            VendorDetailsReducer.payload.vendor.label_id
          )
        );
      }
    } catch (ej) {}
  }, [VendorDetailsReducer]);

  const updateIndexOptions = (selectedIndex) => {
    if (token != '') {
      if (selectedIndex === 0) {
        setcolorss('red');

        dispatch(UpdateVendorStateAction(token, 3));
        dispatch(UpdateAvailabiltyAction(3));
        setselected('updateavailability');
      } else if (selectedIndex === 1) {
        setcolorss('orange');
        dispatch(UpdateAvailabiltyAction(2));

        dispatch(UpdateVendorStateAction(token, 2));
        setselected('updateavailability');
      } else {
        dispatch(UpdateVendorStateAction(token, 1));
        dispatch(UpdateAvailabiltyAction(1));

        setselected('updateavailability');
        setcolorss('#0599ea');
      }
      setselectedIndexOptions(selectedIndex);
    }
  };

  const onChangeTab = (index) => {};

  return (
    <View
      style={{
        flexDirection: 'column',
        height: DEVICE_hight,
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          flexDirection: 'column',
        }}>
        <ImageBackground
          style={{
            width: '100%',
          }}
          resizeMode='stretch'
          source={require('../../imgs/profilebg.png')}>
          <ImageBackground
            style={{
              width: '100%',
              height: 150,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
            }}
            resizeMode='stretch'
            source={require('../../imgs/transheader.png')}>
            <View
              style={{
                width: '100%',
                marginTop: 70,
                paddingHorizontal: 30,
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
              }}>
              <TouchableHighlight   underlayColor='transparent'
                     onPress={() => {  try {
                      drawer.current.open();
                    } catch (ff) {
                      drawerD.current.open();
                    }}}
                activeOpacity={0.2}>
                <Image
                 source={require('../../imgs/menueic.png')}
              style={{width:25,height:20,tintColor:'white'}}
              resizeMode='contain'
                />
                </TouchableHighlight>

     <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',alignItems:'flex-start',height:20}}
              resizeMode='stretch'
                />    
                       <View style={{width:25}}/>
            </View>
          </ImageBackground>

          <View
            style={{
              backgroundColor: 'white',

              width: '95%',
              marginLeft: '2.5%',
              marginRight: '2.5%',
              backgroundColor: 'white',
              marginBottom: 15,
              borderColor: 'grey',
              borderRadius: 20,
              flexDirection: 'row-reverse',
              borderWidth: 0.5,
            }}>
            {(Platform.OS === 'ios' ||
              (Platform.OS === 'android' && deviceLocale === 'en')) && (
              <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row-reverse' }}>
                  {typeof VendorDetailsReducer !== 'undefined' &&
                    typeof VendorDetailsReducer.payload !== 'undefined' &&
                    typeof VendorDetailsReducer.payload.vendor !==
                      'undefined' &&
                    typeof VendorDetailsReducer.payload.vendor.photo !==
                      'undefined' &&
                    VendorDetailsReducer.payload.vendor.photo !== '' &&
                    VendorDetailsReducer.payload.vendor.photo !== null && (
                      <Image
                        source={
                          imageLoadingvendor
                            ? {
                                uri:
                                  'http://ecommerce.waddeely.com' +
                                  VendorDetailsReducer.payload.vendor.photo,
                              }
                            : require('../../imgs/defaultt.png')
                        }
                        onError={(_) => setimageLoadingvendor(false)}
                        style={{
                          width: (0.2 * DEVICE_hight) / 2,
                          height: (0.2 * DEVICE_hight) / 2,
                          margin: 15,
                          borderRadius: 20,
                        }}
                      />
                    )}
                  {(typeof VendorDetailsReducer === 'undefined' ||
                    typeof VendorDetailsReducer.payload === 'undefined' ||
                    typeof VendorDetailsReducer.payload.vendor ===
                      'undefined' ||
                    typeof VendorDetailsReducer.payload.vendor.photo ===
                      'undefined' ||
                    VendorDetailsReducer.payload.vendor.photo === '' ||
                    VendorDetailsReducer.payload.vendor.photo === null ||
                    imageLoadingvendor === false) && (
                    <Image
                      source={require('../../imgs/defaultt.png')}
                      style={{
                        width: 100,
                        height: 100,
                        margin: 15,
                        borderRadius: 20,
                      }}
                    />
                  )}

                  <View
                    style={{
                      flexDirection: 'column',
                      marginTop: (0.15 * DEVICE_hight) / 7,
                      marginRight: 5,
                      marginLeft: 5,
                    }}>
                    <View
                      style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        flexDirection: 'row-reverse',
                      }}>
                      <Text
                        style={{
                          fontSize: 17,
                          fontWeight: '400',
                          color: '#1789bc',
                        }}>
                        {vendorname}
                      </Text>
                    </View>

                    <View
                      style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        flexDirection: 'row-reverse',
                        marginTop: 5,
                        marginBottom: 5,
                      }}>
                      <Stars
                        disabled={true}
                        count={5}
                        half={true}
                        starSize={100}
                        display={parseInt(vendorrating)}
                        spacing={8}
                        fullStar={
                          <Icon name={'star'} style={[styles.myStarStyle]} />
                        }
                        emptyStar={
                          <Icon
                            name={'star-outline'}
                            style={[
                              styles.myStarStyle,
                              styles.myEmptyStarStyle,
                            ]}
                          />
                        }
                        halfStar={
                          <Icon
                            name={'star-half'}
                            style={[styles.myStarStyle]}
                          />
                        }
                      />
                      <Text
                        style={{
                          marginLeft: 5,
                          marginRight: 5,
                          color: '#1789bc',
                        }}>
                        ({vendorratingcount})
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row-reverse',
                        width: DEVICE_width / 2,
                      }}>
                      <TagsVieww
                        language={deviceLocale}
                        all={vendorlabels}
                        selected={selected}
                        isExclusive={true}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row-reverse',
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  <Text
                    style={{
                      fontSize: 17,
                      fontWeight: '400',
                      color: '#1789bc',
                    }}>
                    {vendordescription}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row-reverse',
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '400',
                      color: '#1789bc',
                    }}>
                    أيام العطلة
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '300',
                      color: '#1789bc',
                    }}>
                    :
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '300',
                      color: '#1789bc',
                    }}>
                    {vendoroffdays}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row-reverse',
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '400',
                      color: '#1789bc',
                    }}>
                    {' '}
                    ساعات العمل
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '300',
                      color: '#1789bc',
                    }}>
                    :
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '300',
                      color: '#1789bc',
                    }}>
                    {vendoropeningtime}
                  </Text>
                </View>

                <View
                  style={{
                    width: DEVICE_width,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ButtonGroup
                    onPress={(index) => {
                      updateIndexOptions(index);
                    }}
                    selectedIndex={selectedIndexOptions}
                    innerBorderStyle={{ color: '#f2f2f2' }}
                    buttons={[
                      { element: component0 },
                      { element: component1 },
                      { element: component2 },
                    ]}
                    textStyle={{ color: 'white' }}
                    selectedButtonStyle={{
                      backgroundColor: colorss,
                      borderRadius: 30,
                    }}
                    underlayColor='#1789bc'
                    containerStyle={{
                      marginTop: 8,
                      marginBottom: 10,
                      height: (0.3 * DEVICE_hight) / 7,
                      borderRadius: 30,
                      borderBottomColor: '#f2f2f2',
                      borderColor: '#f2f2f2',
                      backgroundColor: '#f2f2f2',
                      width: DEVICE_width - 160,
                      margin: 30,
                    }}
                  />
                </View>
              </View>
            )}

            {Platform.OS === 'android' && deviceLocale === 'ar' && (
              <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row' }}>
                  {typeof VendorDetailsReducer !== 'undefined' &&
                    typeof VendorDetailsReducer.payload !== 'undefined' &&
                    typeof VendorDetailsReducer.payload.vendor !==
                      'undefined' &&
                    typeof VendorDetailsReducer.payload.vendor.photo !==
                      'undefined' &&
                    VendorDetailsReducer.payload.vendor.photo !== '' &&
                    VendorDetailsReducer.payload.vendor.photo !== null && (
                      <Image
                        source={
                          imageLoadingvendor
                            ? {
                                uri:
                                  'http://ecommerce.waddeely.com' +
                                  VendorDetailsReducer.payload.vendor.photo,
                              }
                            : require('../../imgs/defaultt.png')
                        }
                        onError={(_) => setimageLoadingvendor(false)}
                        style={{
                          width: (0.2 * DEVICE_hight) / 2,
                          height: (0.2 * DEVICE_hight) / 2,
                          margin: 15,
                          borderRadius: 20,
                        }}
                      />
                    )}
                  {(typeof VendorDetailsReducer === 'undefined' ||
                    typeof VendorDetailsReducer.payload === 'undefined' ||
                    typeof VendorDetailsReducer.payload.vendor ===
                      'undefined' ||
                    typeof VendorDetailsReducer.payload.vendor.photo ===
                      'undefined' ||
                    VendorDetailsReducer.payload.vendor.photo === '' ||
                    VendorDetailsReducer.payload.vendor.photo === null ||
                    imageLoadingvendor === false) && (
                    <Image
                      source={require('../../imgs/defaultt.png')}
                      style={{
                        width: 100,
                        height: 100,
                        margin: 15,
                        borderRadius: 20,
                      }}
                    />
                  )}

                  <View
                    style={{
                      flexDirection: 'column',
                      marginTop: (0.15 * DEVICE_hight) / 7,
                      marginRight: 5,
                      marginLeft: 5,
                    }}>
                    <View
                      style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          fontSize: 17,
                          fontWeight: '400',
                          color: '#1789bc',
                        }}>
                        {vendorname}
                      </Text>
                    </View>

                    <View
                      style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        flexDirection: 'row',
                        marginTop: 5,
                        marginBottom: 5,
                      }}>
                      <Stars
                        disabled={true}
                        count={5}
                        half={true}
                        starSize={100}
                        display={parseInt(vendorrating)}
                        spacing={8}
                        fullStar={
                          <Icon name={'star'} style={[styles.myStarStyle]} />
                        }
                        emptyStar={
                          <Icon
                            name={'star-outline'}
                            style={[
                              styles.myStarStyle,
                              styles.myEmptyStarStyle,
                            ]}
                          />
                        }
                        halfStar={
                          <Icon
                            name={'star-half'}
                            style={[styles.myStarStyle]}
                          />
                        }
                      />
                      <Text
                        style={{
                          marginLeft: 5,
                          marginRight: 5,
                          color: '#1789bc',
                        }}>
                        ({vendorratingcount})
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        width: DEVICE_width / 2,
                      }}>
                      <TagsVieww
                        language={deviceLocale}
                        all={vendorlabels}
                        selected={selected}
                        isExclusive={true}
                      />
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  <Text
                    style={{
                      fontSize: 17,
                      fontWeight: '400',
                      color: '#1789bc',
                    }}>
                    {vendordescription}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '400',
                      color: '#1789bc',
                    }}>
                    أيام العطلة
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '300',
                      color: '#1789bc',
                    }}>
                    :
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '300',
                      color: '#1789bc',
                    }}>
                    {vendoroffdays}
                  </Text>
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '400',
                      color: '#1789bc',
                    }}>
                    {' '}
                    ساعات العمل
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '300',
                      color: '#1789bc',
                    }}>
                    :
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '300',
                      color: '#1789bc',
                    }}>
                    {vendoropeningtime}
                  </Text>
                </View>

                <View
                  style={{
                    width: DEVICE_width,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ButtonGroup
                    onPress={(index) => {
                      updateIndexOptions(index);
                    }}
                    selectedIndex={selectedIndexOptions}
                    innerBorderStyle={{ color: '#f2f2f2' }}
                    buttons={[
                      { element: component0 },
                      { element: component1 },
                      { element: component2 },
                    ]}
                    textStyle={{ color: 'white' }}
                    selectedButtonStyle={{
                      backgroundColor: colorss,
                      borderRadius: 30,
                    }}
                    underlayColor='#1789bc'
                    containerStyle={{
                      marginTop: 8,
                      marginBottom: 10,
                      height: (0.3 * DEVICE_hight) / 7,
                      borderRadius: 30,
                      borderBottomColor: '#f2f2f2',
                      borderColor: '#f2f2f2',
                      backgroundColor: '#f2f2f2',
                      width: DEVICE_width - 160,
                      margin: 30,
                      flexDirection: 'row-reverse',
                    }}
                  />
                </View>
              </View>
            )}
          </View>
        </ImageBackground>
      </View>
      {typeof VendorDetailsReducer.payload !== 'undefined' &&
        VendorDetailsReducer.payload !== null &&
        typeof VendorDetailsReducer.payload.item !== 'undefined' &&
        VendorDetailsReducer.payload.item !== null &&
        VendorDetailsReducer.payload.item.length > 0 && (
          <DynamicTabView
            data={menuelist}
            renderTab={(item) => (
              <ScrollView showsVerticalScrollIndicator={false}>
                {item['items'].map((item, index) => (
                  <View>
                    <TouchableOpacity
                      activeOpacity={0.9}
                      onPress={() => {
                        props.navigation.navigate('ItemDetailsScreen', {
                          itemm: item,

                          currencyid: currencyid,
                          currency: currency,
                          token: token,
                          id: id,
                          price: getprice(item.id),
                          userid: VendorDetailsReducer.payload.vendor.id,
                        });
                      }}>
                      {(Platform.OS === 'ios' ||
                        (Platform.OS === 'android' &&
                          deviceLocale === 'en')) && (
                        <View
                          style={{
                            backgroundColor: 'white',
                            marginBottom: 1,
                            borderColor: 'grey',
                            flexDirection: 'row-reverse',
                          }}>
                          <View style={{ flexDirection: 'row-reverse' }}>
                            {item.photos.length > 0 &&
                              item.photos[0].item_photo !== '' && (
                                <Image
                                  source={
                                    imageLoading
                                      ? {
                                          uri:
                                            'http://ecommerce.waddeely.com' +
                                            item.photos[0].item_photo,
                                        }
                                      : require('../../imgs/defaultt.png')
                                  }
                                  onError={(_) => setimageLoading(false)}
                                  style={{
                                    width: (0.2 * DEVICE_hight) / 2,
                                    height: (0.2 * DEVICE_hight) / 2,
                                    margin: 10,
                                    borderRadius: 10,
                                  }}
                                />
                              )}
                            {!(
                              item.photos.length > 0 &&
                              item.photos[0].item_photo !== ''
                            ) && (
                              <Image
                                source={require('../../imgs/defaultt.png')}
                                style={{
                                  width: (0.2 * DEVICE_hight) / 2,
                                  height: (0.2 * DEVICE_hight) / 2,
                                  margin: 10,
                                  borderRadius: 10,
                                }}
                              />
                            )}
                            <View
                              style={{
                                flexDirection: 'column',
                                marginTop: 15,
                                marginBottom: 15,
                                marginRight: 5,
                                marginLeft: 5,
                              }}>
                              <View
                                style={{
                                  justifyContent: 'flex-start',
                                  alignItems: 'flex-start',
                                  flexDirection: 'row-reverse',
                                }}>
                                <View style={{ flexDirection: 'column' }}>
                                  <View
                                    style={{
                                      flexDirection: 'row-reverse',
                                      justifyContent: 'flex-start',
                                      alignItems: 'flex-start',
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 13,
                                        fontWeight: 'bold',
                                        color: '#1789bc',
                                      }}>
                                      {item.name_ar}
                                    </Text>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: 'row-reverse',
                                      justifyContent: 'flex-start',
                                      alignItems: 'flex-start',
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 13,
                                        color: '#1789bc',
                                      }}>
                                      {item.description_ar}
                                    </Text>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: 'row-reverse',
                                      justifyContent: 'flex-start',
                                      alignItems: 'flex-start',
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        color: '#1789bc',
                                      }}>
                                      السعر :{getprice(item.id)} {currency}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      )}

                      {Platform.OS === 'android' && deviceLocale === 'ar' && (
                        <View
                          style={{
                            backgroundColor: 'white',
                            marginBottom: 1,
                            borderColor: 'grey',
                            flexDirection: 'row',
                          }}>
                          <View style={{ flexDirection: 'row' }}>
                            {item.photos.length > 0 &&
                              item.photos[0].item_photo !== '' && (
                                <Image
                                  source={
                                    imageLoading
                                      ? {
                                          uri:
                                            'http://ecommerce.waddeely.com' +
                                            item.photos[0].item_photo,
                                        }
                                      : require('../../imgs/defaultt.png')
                                  }
                                  onError={(_) => setimageLoading(false)}
                                  style={{
                                    width: (0.2 * DEVICE_hight) / 2,
                                    height: (0.2 * DEVICE_hight) / 2,
                                    margin: 10,
                                    borderRadius: 10,
                                  }}
                                />
                              )}
                            {!(
                              item.photos.length > 0 &&
                              item.photos[0].item_photo !== ''
                            ) && (
                              <Image
                                source={require('../../imgs/defaultt.png')}
                                style={{
                                  width: (0.2 * DEVICE_hight) / 2,
                                  height: (0.2 * DEVICE_hight) / 2,
                                  margin: 10,
                                  borderRadius: 10,
                                }}
                              />
                            )}
                            <View
                              style={{
                                flexDirection: 'column',
                                marginTop: 15,
                                marginBottom: 15,
                                marginRight: 5,
                                marginLeft: 5,
                              }}>
                              <View
                                style={{
                                  justifyContent: 'flex-start',
                                  alignItems: 'flex-start',
                                  flexDirection: 'row-reverse',
                                }}>
                                <View style={{ flexDirection: 'column' }}>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      justifyContent: 'flex-start',
                                      alignItems: 'flex-start',
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 13,
                                        color: '#1789bc',
                                        fontWeight: 'bold',
                                      }}>
                                      {item.name_ar}
                                    </Text>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      justifyContent: 'flex-start',
                                      alignItems: 'flex-start',
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 13,
                                        color: '#1789bc',
                                      }}>
                                      {item.description_ar}
                                    </Text>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      justifyContent: 'flex-start',
                                      alignItems: 'flex-start',
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 10,
                                        color: '#1789bc',
                                      }}>
                                      السعر :{item.price} {currency}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      )}
                    </TouchableOpacity>
                  </View>
                ))}
              </ScrollView>
            )}
            defaultIndex={defaultIndex}
            containerStyle={styles.container}
            headerBackgroundColor={'white'}
            headerTextStyle={styles.headerText}
            onChangeTab={() => onChangeTab()}
            headerUnderlayColor={'#0599ea'}
            parentScreen={this}
          />
        )}
      {typeof VendorDetailsReducer.payload !== 'undefined' &&
        VendorDetailsReducer.payload !== null &&
        typeof VendorDetailsReducer.payload.item !== 'undefined' &&
        VendorDetailsReducer.payload.item !== null &&
        VendorDetailsReducer.payload.item.length === 0 && (
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{ color: '#1789bc' }}>لم تتم إضافة عناصر من قبل</Text>
          </View>
        )}

      <TouchableOpacity
        style={{
          alignItems: 'center',
          marginBottom: 30,
        }}
        onPress={() =>
          props.navigation.navigate('NewItemDetailsScreen', {
            currencyid: currencyid,
            currency: currency,
            token: token,
            id: id,
          })
        }>
        <LinearGradient
          colors={['#04b5eb', '#06d4e5', '#06d4e5']}
          style={{ borderRadius: 20 }}>
          <Text
            style={{
              color: 'white',
              marginHorizontal: 25,
              marginVertical: 8,
              fontSize: 20,
            }}>
            أضف منتج جديد
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginBottom: 20,
  },

  headerContainer: {
    marginTop: 16,
  },
  headerText: {
    color: '#1789bc',
  },
  tabItemContainer: {
    backgroundColor: '#cf6bab',
  },
  myStarStyle: {
    color: 'yellow',
    backgroundColor: 'transparent',
    textShadowColor: 'black',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 2,
  },
  myEmptyStarStyle: {
    color: 'white',
  },
});
