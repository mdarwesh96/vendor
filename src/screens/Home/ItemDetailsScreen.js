import React, { useEffect, useState, useRef } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  ImageBackground,
  Image,
  Text,
  TextInput,
  NativeModules,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
const DEVICE_hight = Dimensions.get('window').height;
const DEVICE_width = Dimensions.get('window').width;
import TagsViewSelection from '../../common/Tags/TagsViewSelection';
import TagsViewSelectionClickableSingle from '../../common/Tags/TagsViewSelectionClickableSingle';
import { CheckBox } from 'react-native-elements';
let itemid;
import LinearGradient from 'react-native-linear-gradient';

let imagechanged = false;
import Spinner from 'react-native-loading-spinner-overlay';

import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import ActionSheet from 'react-native-actions-sheet';
import Iconn from 'react-native-vector-icons/MaterialCommunityIcons';
import Icond from 'react-native-vector-icons/AntDesign';
import { DeleteItemAction, EditItemAction } from '../../actions/Actions';
import { GetUserDetailsAction } from '../../actions/Actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
let deviceLocale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;
if (
  typeof deviceLocale !== 'undefined' &&
  deviceLocale !== null &&
  deviceLocale.includes('ar')
) {
  deviceLocale = 'ar';
} else {
  deviceLocale = 'en';
}
export default function ItemDetailsScreen(props) {
  const dispatch = useDispatch();
  const [subcategory, setsubcategory] = useState([]);
  const [imagebaseprofile, setimagebaseprofile] = useState('');
  const [srcimage, setsrcimage] = useState(require('../../imgs/defaultt.png'));
  const [countt, setcount] = useState(0);
  const [price, setprice] = useState(0);
  const [editproduct, seteditproduct] = useState('تعديل المنتج');
  const [saveproduct, setsaveproduct] = useState('حفظ التعديل');
  const [selectedtype, setselectedtype] = useState('');
  const [description, setdescription] = useState('');
  const [tags, settags] = useState([]);
  const [name, setname] = useState('');
  const [token, settoken] = useState('');
  const [id, setid] = useState('');
  const [selected, setselected] = useState([]);
  const [spespeciali, setspespeciali] = useState(false);
  const [currencyid, setcurrencyid] = useState('1');
  const [iddd, setiddd] = useState('');
  const [imageLoadingvendor, setimageLoadingvendor] = useState(true);
  const [clickable, setclickable] = useState(false);
  const [currency, setcurrency] = useState('درهم اماراتي');
  const [spinner, setspinner] = useState(false);
  const actionSheetRef = useRef();
  const showModalVisible = () => {
    actionSheetRef.current?.setModalVisible(true);
  };
  const hideModalVisible = () => {
    actionSheetRef.current?.setModalVisible(false);
  };

  actionSheetRef.current?.snapToOffset(300);

  const [isLoading, setisLoading] = useState(false);
  const VendorDetailsReducer = useSelector(
    (state) => state.VendorDetailsReducer
  );
  const GetCategoriesReducer = useSelector(
    (state) => state.GetCategoriesReducer
  );
  const DeleteItemReducer = useSelector((state) => state.DeleteItemReducer);
  const UpdateItemReducer = useSelector((state) => state.UpdateItemReducer);
  const [maincategoryid, setmaincategoryid] = useState(null);
  const [userid, setuserid] = useState(1);

  if (isLoading) {
    if (selectedtype === 'delete') {
      if (
        typeof DeleteItemReducer !== 'undefined' &&
        DeleteItemReducer != null
      ) {
        if (
          typeof DeleteItemReducer.payload !== 'undefined' &&
          DeleteItemReducer.payload != null
        ) {
          if (DeleteItemReducer.payload.status === 'success') {
            Toast.showWithGravity(
              'تم حذف المنتج بنجاح ',
              Toast.LONG,
              Toast.BOTTOM
            );

            dispatch(GetUserDetailsAction(iddd, currencyid));
            setisLoading(false);
            // setspinner(false)
            props.navigation.goBack(null);
          } else {
            if (DeleteItemReducer.error != '') {
              if (
                DeleteItemReducer.error.response.data.message ===
                'item not found'
              ) {
                Toast.showWithGravity(
                  'العنصر غير موجود ',
                  Toast.LONG,
                  Toast.BOTTOM
                );
              } else
                try {
                  if (
                    DeleteItemReducer.error.response.data.message.includes(
                      'Unauthenticated'
                    )
                  ) {
                    AsyncStorage.setItem('loggedin', 'false').then(() => {
                      AsyncStorage.setItem('email', 'null').then(() => {
                        AsyncStorage.setItem('password', 'null').then(() => {
                          AsyncStorage.setItem('token', 'null').then(() => {
                            AsyncStorage.setItem('id', 'null').then(() => {
                              Toast.showWithGravity(
                                'تحتاج إلى تسجيل الدخول مرة أخرى ',
                                Toast.LONG,
                                Toast.BOTTOM
                              );

                              props.navigation.navigate('LoginScreen', {});
                            });
                          });
                        });
                      });
                    });
                  } else {
                    Toast.showWithGravity(
                      DeleteItemReducer.error.response.data.message,
                      Toast.LONG,
                      Toast.BOTTOM
                    );
                  }
                } catch (e) {
                  // setspinner(false)

                  Toast.showWithGravity(
                    'حدث خطأ عام',
                    Toast.LONG,
                    Toast.BOTTOM
                  );
                }
            }
          }
        }
      }
    } else if (selectedtype === 'edit') {
      if (
        typeof UpdateItemReducer !== 'undefined' &&
        UpdateItemReducer != null
      ) {
        if (UpdateItemReducer.payload) {
          if (UpdateItemReducer.payload.status === 'success') {
            Toast.showWithGravity(
              'تم تحديث المنتج بنجاح ',
              Toast.LONG,
              Toast.BOTTOM
            );
            dispatch(GetUserDetailsAction(iddd, currencyid));
            setisLoading(false);
            // setspinner(false)
            props.navigation.goBack(null);
          }
        } else {
          if (UpdateItemReducer.error != '') {
            if (
              UpdateItemReducer.error.response.data.message === 'item not found'
            ) {
              Toast.showWithGravity(
                'العنصر غير موجود ',
                Toast.LONG,
                Toast.BOTTOM
              );
            } else
              try {
                if (
                  UpdateItemReducer.error.response.data.message.includes(
                    'Unauthenticated'
                  )
                ) {
                  AsyncStorage.setItem('loggedin', 'false').then(() => {
                    AsyncStorage.setItem('email', 'null').then(() => {
                      AsyncStorage.setItem('password', 'null').then(() => {
                        AsyncStorage.setItem('token', 'null').then(() => {
                          AsyncStorage.setItem('id', 'null').then(() => {
                            Toast.showWithGravity(
                              'تحتاج إلى تسجيل الدخول مرة أخرى ',
                              Toast.LONG,
                              Toast.BOTTOM
                            );

                            props.navigation.navigate('LoginScreen', {});
                          });
                        });
                      });
                    });
                  });
                } else {
                  Toast.showWithGravity(
                    DeleteItemReducer.error.response.data.message,
                    Toast.LONG,
                    Toast.BOTTOM
                  );
                }
              } catch (e) {
                // setspinner(false)
                Toast.showWithGravity('حدث خطأ عام', Toast.LONG, Toast.BOTTOM);
              }
          }
        }
      }
    }
  }

  const loadApp = async () => {
    try {
      // setcurrencyid(props.navigation.state.params.currencyid);

      // setcurrency(props.navigation.state.params.currency);
      settoken(props.navigation.state.params.token);
      setiddd(props.navigation.state.params.id);
    } catch (e) {}
  };
  useEffect(() => {
    setuserid(props.navigation.state.params.userid);
    setname(props.navigation.state.params.itemm.name_ar);
    setdescription(props.navigation.state.params.itemm.description_ar);
    setid(props.navigation.state.params.itemm.id);
    // setprice(getconvertedprice(props.navigation.state.params.itemm.price)
    //  setitemid(props.navigation.state.params.itemm.id)
    itemid = props.navigation.state.params.itemm.id;

    setprice(props.navigation.state.params.price);
    setcount('' + props.navigation.state.params.itemm.stock_count);

    if (props.navigation.state.params.itemm.special_item === '1') {
      setspespeciali(true);
    } else {
      setspespeciali(false);
    }

    let sell = props.navigation.state.params.itemm.subcategories;
    let d = [];
    d.push(sell);
    setselected(d);

    setsubcategory(d);
    // setmaincategoryid(d[0].parent_id)

    // setspespecial(props.navigation.state.params.itemm.special_item)

    // setisLoading(false);
    try {
      for (let i = 0; i < GetCategoriesReducer.payload.length; i++) {
        if (
          GetCategoriesReducer.payload[i].id ===
          parseInt(
            VendorDetailsReducer.payload.vendor.category_id[0].category_id
          )
        ) {
          settags(GetCategoriesReducer.payload[i].subcategory);
        }
      }
    } catch (e) {}
    loadApp();
  }, []);

  handleChoosePhoto = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.uri) {
        // this.setState({ photo: response ,avatarSource:response});
      }
    });
  };

  selectPhotoTapped = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: 'itemfiles',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = { uri: response.uri };

        ImgToBase64.getBase64String(response.uri)
          .then((base64String) => {
            setimagebaseprofile(base64String);
            // this.setState({"imagebase":base64String});
          })
          .catch((err) => {});

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        // this.setState({
        //     srcimage: source,
        // });

        imagechanged = true;
        setsrcimage(source);
      }
    });
  };

  const openCamera = (cropit) => {
    ImagePicker.openCamera({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  const openGallary = (cropit) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  return (
    <View
      style={{
        flexDirection: 'column',
        backgroundColor: 'white',
        height: DEVICE_hight,
      }}>
      <View style={{ flexDirection: 'column' }}>
        {typeof props.navigation.state.params.itemm.photos[0] !== 'undefined' &&
          props.navigation.state.params.itemm.photos[0] !== null &&
          props.navigation.state.params.itemm.photos[0] !== '' &&
          props.navigation.state.params.itemm.photos[0].item_photo !== '' &&
          imagechanged === false && (
            <ImageBackground
              resizeMode='cover'
              source={
                imageLoadingvendor
                  ? {
                      uri:
                        'http://ecommerce.waddeely.com' +
                        props.navigation.state.params.itemm.photos[0]
                          .item_photo,
                    }
                  : require('../../imgs/defaultt.png')
              }
              onError={(_) => setimageLoadingvendor(false)}
              style={{
                width: '100%',
                height: clickable
                  ? (DEVICE_hight / 3) * 1.2
                  : (DEVICE_hight / 3) * 1.7,
              }}>
              <ImageBackground
                style={{
                  width: '100%',
                  height: 150,
                  flexDirection: 'row-reverse',
                  justifyContent: 'space-between',
                }}
                resizeMode='stretch'
                source={require('../../imgs/transheader.png')}>
                <View
                  style={{
                    width: '100%',
                    marginTop: 70,
                    paddingHorizontal: 30,
                    flexDirection: 'row-reverse',
                    justifyContent: 'space-between',
                  }}>
                  <Icon
                    name='arrow-back'
                    size={25}
                    color='white'
                    style={{ transform: [{ rotateY: '180deg' }] }}
                    onPress={() => props.navigation.goBack(null)}
                  />
                  <Text style={{ fontSize: 20, color: 'white' }}>Waddelly</Text>
                  <View>
                    {clickable === false && (
                      <Icond
                        name='delete'
                        size={25}
                        color='white'
                        onPress={() => {
                          setisLoading(true);

                          dispatch(DeleteItemAction(id, token));
                          setselectedtype('delete');
                          // props.navigation.goBack(null)}
                        }}
                      />
                    )}
                    {clickable === true && (
                      <Iconn
                        name='image-edit-outline'
                        size={25}
                        color='white'
                        onPress={() => {
                          showModalVisible();
                        }}
                      />
                    )}
                  </View>
                </View>
              </ImageBackground>
            </ImageBackground>
          )}
        {imagechanged === true && (
          <ImageBackground
            resizeMode='cover'
            source={srcimage}
            onError={(_) => setimageLoadingvendor(false)}
            style={{
              width: '100%',
              height: (DEVICE_hight / 3) * 1.2,

              flex: 1,
            }}>
            <ImageBackground
              style={{
                width: '100%',
                height: 150,
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
              }}
              resizeMode='stretch'
              source={require('../../imgs/transheader.png')}>
              <View
                style={{
                  width: '100%',
                  marginTop: 70,
                  paddingHorizontal: 30,
                  flexDirection: 'row-reverse',
                  justifyContent: 'space-between',
                }}>
                <Icon
                  name='arrow-back'
                  size={25}
                  color='white'
                  style={{ transform: [{ rotateY: '180deg' }] }}
                  onPress={() => props.navigation.goBack(null)}
                />
                <Text style={{ fontSize: 20, color: 'white' }}>Waddelly</Text>
                <View>
                  {clickable === false && (
                    <Icond
                      name='delete'
                      size={25}
                      color='white'
                      onPress={() => {
                        setisLoading(true);

                        dispatch(DeleteItemAction(id, token));
                        setselectedtype('delete');
                        // props.navigation.goBack(null)}
                      }}
                    />
                  )}
                  {clickable === true && (
                    <Iconn
                      name='image-edit-outline'
                      size={25}
                      color='#0599ea'
                      onPress={() => {
                        showModalVisible();
                      }}
                    />
                  )}
                </View>
              </View>
            </ImageBackground>
          </ImageBackground>
        )}

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ height: '65%' }}>
          <View style={{ width: '100%', flexDirection: 'column' }}>
            {(Platform.OS === 'ios' ||
              (Platform.OS === 'android' && deviceLocale === 'en')) && (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row-reverse',
                  marginTop: 5,
                  marginBottom: 8,
                }}>
                {clickable === false && (
                  <View
                    style={{
                      flexDirection: 'row-reverse',
                      marginLeft: '3%',
                      marginRight: '3%',
                      width: '94%',
                    }}>
                    <Text
                      style={{
                        fontSize: 20,

                        color: '#44a0c9',
                        textAlign: 'right',
                        alignSelf: 'stretch',
                      }}>
                      اسم المنتج :
                    </Text>
                    <Text
                      style={{
                        fontSize: 20,
                        color: '#44a0c9',
                        textAlign: 'right',
                        alignSelf: 'stretch',
                      }}>
                      {name}
                    </Text>
                  </View>
                )}
                {clickable === true && (
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.inputname}
                      placeholder={' اسم المنتج '}
                      multiline={false}
                      autoCapitalize={'none'}
                      returnKeyType={'next'}
                      autoCorrect={false}
                      onChangeText={(name) => setname(name)}
                      value={name}
                      secureTextEntry={false}
                      placeholderTextColor='grey'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                )}
              </View>
            )}
            {Platform.OS === 'android' && deviceLocale === 'ar' && (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  marginTop: 5,
                  marginBottom: 8,
                }}>
                {clickable === false && (
                  <View
                    style={{
                      flexDirection: 'row-reverse',
                      marginLeft: '3%',
                      marginRight: '3%',
                      width: '94%',
                    }}>
                    <Text
                      style={{
                        fontSize: 20,

                        color: '#44a0c9',
                        textAlign: 'left',
                        alignSelf: 'stretch',
                      }}>
                      اسم المنتج :
                    </Text>
                    <Text
                      style={{
                        fontSize: 20,
                        color: '#44a0c9',
                        textAlign: 'left',
                        alignSelf: 'stretch',
                      }}>
                      {name}
                    </Text>
                  </View>
                )}
                {clickable === true && (
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.inputname}
                      placeholder={' اسم المنتج '}
                      multiline={false}
                      autoCapitalize={'none'}
                      returnKeyType={'next'}
                      autoCorrect={false}
                      onChangeText={(name) => setname(name)}
                      value={name}
                      secureTextEntry={false}
                      placeholderTextColor='grey'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                )}
              </View>
            )}

            {(Platform.OS === 'ios' ||
              (Platform.OS === 'android' && deviceLocale === 'en')) && (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row-reverse',
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                }}>
                {clickable === false && (
                  <View
                    style={{
                      flexDirection: 'row-reverse',
                      marginLeft: '3%',
                      marginRight: '3%',
                      width: '94%',
                    }}>
                    <Text
                      style={{
                        fontSize: 20,

                        color: '#44a0c9',
                        textAlign: 'right',
                        alignSelf: 'stretch',
                      }}>
                      وصف المنتج :
                    </Text>

                    <Text
                      style={{
                        marginLeft: '3%',
                        marginRight: '3%',
                        fontSize: 20,
                        width: '94%',
                        color: '#44a0c9',
                        textAlign: 'right',
                        alignSelf: 'stretch',
                      }}>
                      {description}
                    </Text>
                  </View>
                )}
                {clickable === true && (
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.inputaddress}
                      placeholder={'وصف المنتج'}
                      multiline={true}
                      autoCapitalize={'none'}
                      returnKeyType={'next'}
                      autoCorrect={false}
                      onChangeText={(address) => setdescription(address)}
                      value={description}
                      secureTextEntry={false}
                      placeholderTextColor='grey'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                )}
              </View>
            )}

            {Platform.OS === 'android' && deviceLocale === 'ar' && (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                }}>
                {clickable === false && (
                  <View
                    style={{
                      flexDirection: 'row-reverse',
                      marginLeft: '3%',
                      marginRight: '3%',
                      width: '94%',
                    }}>
                    <Text
                      style={{
                        fontSize: 20,

                        color: '#44a0c9',
                        textAlign: 'left',
                        alignSelf: 'stretch',
                      }}>
                      وصف المنتج :
                    </Text>
                    <Text
                      style={{
                        marginLeft: '3%',
                        marginRight: '3%',
                        fontSize: 20,
                        width: '94%',
                        color: '#44a0c9',
                        textAlign: 'left',
                        alignSelf: 'stretch',
                      }}>
                      {description}
                    </Text>
                  </View>
                )}
                {clickable === true && (
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.inputaddress}
                      placeholder={'وصف المنتج'}
                      multiline={true}
                      autoCapitalize={'none'}
                      returnKeyType={'next'}
                      autoCorrect={false}
                      onChangeText={(address) => setdescription(address)}
                      value={description}
                      secureTextEntry={false}
                      placeholderTextColor='grey'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                )}
              </View>
            )}

            {(Platform.OS === 'ios' ||
              (Platform.OS === 'android' && deviceLocale === 'en')) && (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row-reverse',
                  alignItems: 'center',
                  marginTop: 5,
                  paddingRight: 5,
                  height: 50,
                }}>
                <View
                  style={{
                    flexDirection: 'row-reverse',
                    width: '100%',
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      color: '#44a0c9',
                      marginHorizontal: 5,
                    }}>
                    {' '}
                    السعر
                  </Text>
                  <View>
                    {clickable === false && (
                      <LinearGradient
                        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                        style={{
                          borderRadius: 30,
                          width: 100,
                          textAlign: 'center',
                          justifyContent: 'center',
                          alignItems: 'center',
                          height: 40,
                        }}>
                        <Text
                          style={{
                            fontSize: 18,
                            color: 'white',
                            textAlign: 'center',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          {' '}
                          {price}{' '}
                        </Text>
                      </LinearGradient>
                    )}
                    {clickable === true && (
                      <LinearGradient
                        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                        style={{ borderRadius: 30 }}>
                        <View style={styles.inputWrapperprice}>
                          <TextInput
                            style={styles.inputprice}
                            placeholder={'السعر'}
                            autoCapitalize={'none'}
                            returnKeyType={'next'}
                            autoCorrect={false}
                            keyboardType={'numeric'}
                            onChangeText={(price) => setprice(price)}
                            value={price}
                            secureTextEntry={false}
                            placeholderTextColor='grey'
                            underlineColorAndroid='transparent'
                          />
                        </View>
                      </LinearGradient>
                    )}
                  </View>
                  <Text
                    style={{
                      fontSize: 15,
                      color: '#44a0c9',
                      marginHorizontal: 5,
                    }}>
                    {' '}
                    {currency}
                  </Text>
                </View>
              </View>
            )}

            {Platform.OS === 'android' && deviceLocale === 'ar' && (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 5,
                  paddingRight: 5,
                  height: 50,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignContent: 'center',
                    alignItems: 'center',
                    width: '100%',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      color: '#44a0c9',
                      marginHorizontal: 5,
                    }}>
                    {' '}
                    السعر
                  </Text>
                  <View>
                    {clickable === false && (
                      <LinearGradient
                        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                        style={{
                          borderRadius: 30,
                          width: 100,
                          textAlign: 'center',
                          justifyContent: 'center',
                          alignItems: 'center',
                          height: 40,
                        }}>
                        <Text
                          style={{
                            fontSize: 18,
                            color: 'white',
                            textAlign: 'center',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          {' '}
                          {price}{' '}
                        </Text>
                      </LinearGradient>
                    )}
                    {clickable === true && (
                      <LinearGradient
                        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                        style={{ borderRadius: 30 }}>
                        <View style={styles.inputWrapperprice}>
                          <TextInput
                            style={styles.inputprice}
                            placeholder={'السعر'}
                            autoCapitalize={'none'}
                            returnKeyType={'next'}
                            autoCorrect={false}
                            keyboardType={'numeric'}
                            onChangeText={(price) => setprice(price)}
                            value={price}
                            secureTextEntry={false}
                            placeholderTextColor='grey'
                            underlineColorAndroid='transparent'
                          />
                        </View>
                      </LinearGradient>
                    )}
                  </View>
                  <Text
                    style={{
                      fontSize: 15,
                      color: '#44a0c9',
                      marginHorizontal: 5,
                    }}>
                    {' '}
                    {currency}
                  </Text>
                </View>
              </View>
            )}

            {(Platform.OS === 'ios' ||
              (Platform.OS === 'android' && deviceLocale === 'en')) && (
              <View
                style={{
                  width: DEVICE_width,
                  flexDirection: 'row-reverse',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                }}>
                {clickable === true && (
                  <CheckBox
                    onPress={() => setspespeciali(!spespeciali)}
                    size={25}
                    containerStyle={{ marginRight: -10, marginLeft: -10 }}
                    checked={spespeciali}
                  />
                )}
                {clickable === false && spespeciali === true && (
                  <Image
                    resizeMode='cover'
                    source={require('../../imgs/ads.png')}
                    style={{
                      width: 30,
                      height: 30,
                      marginRight: 10,
                    }}></Image>
                )}
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => {
                    setspespeciali(!spespeciali);
                    // if(spespeciali=== true)
                    // {
                    //   setspespeciali(1)
                    // }else{
                    //   setspespeciali(0)

                    // }
                  }}>
                  {clickable && (
                    <Text
                      style={{
                        fontSize: 15,
                        marginRight: 10,
                        color: '#44a0c9',
                      }}>
                      عرض خاص{' '}
                    </Text>
                  )}
                  {clickable === false && spespeciali === true && (
                    <Text
                      style={{
                        fontSize: 18,
                        marginRight: 10,
                        color: '#44a0c9',
                      }}>
                      عرض خاص{' '}
                    </Text>
                  )}
                </TouchableOpacity>
              </View>
            )}
            {Platform.OS === 'android' && deviceLocale === 'ar' && (
              <View
                style={{
                  width: DEVICE_width,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                }}>
                {clickable === true && (
                  <CheckBox
                    onPress={() => setspespeciali(!spespeciali)}
                    size={25}
                    containerStyle={{ marginRight: -10, marginLeft: -10 }}
                    checked={spespeciali}
                  />
                )}
                {clickable === false && spespeciali === true && (
                  <Image
                    resizeMode='cover'
                    source={require('../../imgs/ads.png')}
                    style={{
                      width: 30,
                      height: 30,
                      marginRight: 10,
                    }}></Image>
                )}
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => {
                    setspespeciali(!spespeciali);
                    // if(spespecial=== true)
                    // {
                    //   setspespeciali(1)
                    // }else{
                    //   setspespeciali(0)

                    // }
                  }}>
                  {clickable && (
                    <Text
                      style={{
                        fontSize: 15,
                        marginRight: 10,
                        color: '#44a0c9',
                      }}>
                      عرض خاص{' '}
                    </Text>
                  )}
                  {clickable === false && spespeciali === '2' && (
                    <Text
                      style={{
                        fontSize: 18,
                        marginRight: 10,
                        color: '#44a0c9',
                      }}>
                      عرض خاص{' '}
                    </Text>
                  )}
                </TouchableOpacity>
              </View>
            )}

            {(Platform.OS === 'ios' ||
              (Platform.OS === 'android' && deviceLocale === 'en')) && (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row-reverse',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexDirection: 'row-reverse',
                    marginTop: 10,
                    paddingRight: 5,
                  }}>
                  <Text style={{ fontSize: 15, color: '#44a0c9' }}>
                    {' '}
                    نوع المنتج :{' '}
                  </Text>
                </View>
              </View>
            )}

            {Platform.OS === 'android' && deviceLocale === 'ar' && (
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    paddingRight: 5,
                  }}>
                  <Text style={{ fontSize: 15, color: '#44a0c9' }}>
                    {' '}
                    نوع المنتج :{' '}
                  </Text>
                </View>
              </View>
            )}

            {clickable === true && (
              <TagsViewSelectionClickableSingle
                all={tags}
                language={deviceLocale}
                selected={subcategory}
                isExclusive={false}
                selectSubCategory={(item) => {
                  try {
                    setsubcategory(item.id);
                    setmaincategoryid(item.parent_id);
                  } catch (e) {
                    setsubcategory([]);
                    setmaincategoryid(null);
                  }
                }}
              />
            )}
            {clickable === false && (
              <TagsViewSelection
                all={selected}
                langugae={deviceLocale}
                selected={selected}
                isExclusive={false}
              />
            )}
            <TouchableOpacity
              style={{
                alignItems: 'center',
                marginBottom: 30,
              }}
              onPress={() => {
                if (clickable === true) {
                  // setclickable(false)
                  setselectedtype('edit');

                  if (
                    name.length > 0 &&
                    description.length > 0 &&
                    ('' + subcategory).length > 0 &&
                    price.length > 0 &&
                    countt.length > 0 &&
                    maincategoryid !== null &&
                    itemid != null
                  ) {
                    setisLoading(true);
                    // setspinner(true)
                    dispatch(
                      EditItemAction(
                        name,
                        description,
                        maincategoryid,
                        subcategory,
                        price,
                        '100',
                        countt,
                        currencyid,
                        spespeciali,
                        imagebaseprofile,
                        token,
                        itemid,
                        userid
                      )
                    );
                  } else {
                    Toast.showWithGravity(
                      'تحتاج إلى إضافة التفاصيل المفقودة ',
                      Toast.LONG,
                      Toast.BOTTOM
                    );
                  }
                } else {
                  setclickable(true);
                }
              }}>
              <LinearGradient
                colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                style={{ borderRadius: 20 }}>
                {clickable === false && (
                  <Text
                    style={{
                      color: 'white',
                      marginHorizontal: 25,
                      marginVertical: 8,
                      fontSize: 20,
                    }}>
                    {editproduct}
                  </Text>
                )}
                {clickable === true && (
                  <Text
                    style={{
                      color: 'white',
                      marginHorizontal: 25,
                      marginVertical: 8,
                      fontSize: 20,
                    }}>
                    {saveproduct}
                  </Text>
                )}
              </LinearGradient>
            </TouchableOpacity>
            <View style={{ height: 20 }}></View>
          </View>
        </ScrollView>
      </View>

      <ActionSheet
        ref={actionSheetRef}
        containerStyle={{
          backgroundColor: 'transparent',
        }}
        elevation={0}>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            margin: 20,
            borderRadius: 10,
          }}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openCamera(true);
              }, 1000);
            }}>
            <Text>الكاميرا</Text>
          </TouchableOpacity>
          <View
            style={{
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: '#707070',
            }}
          />
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openGallary(true);
              }, 1000);
            }}>
            <Text>المعرض</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            marginHorizontal: 20,
            borderRadius: 10,
            marginBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();
            }}>
            <Text style={{ color: 'red' }}>إلغاء</Text>
          </TouchableOpacity>
        </View>
      </ActionSheet>

      <Spinner
        visible={spinner}
        textContent={''}
        textStyle={styles.spinnerTextStyle}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  inputaddress: {
    backgroundColor: '#ffffff',
    width: '90%',
    marginHorizontal: '5%',
    height: 100,
    fontSize: 14,
    marginBottom: 10,
    paddingVertical: 40,
    paddingHorizontal: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 40,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },
  inputname: {
    marginHorizontal: '5%',
    backgroundColor: '#ffffff',
    width: '90%',
    marginTop: 10,
    height: 50,
    fontSize: 14,

    padding: 10,

    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 30,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },

  input: {
    backgroundColor: '#ffffff',
    width: '94%',
    marginLeft: '3%',
    marginRight: '3%',
    height: 30,
    marginHorizontal: 30,
    paddingRight: 20,
    fontSize: 14,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },
  inputprice: {
    width: 100,
    fontSize: 14,
    height: 30,
    paddingRight: 5,
    textAlign: 'center',
    borderWidth: 0,
    borderRadius: 30,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: 'white',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',

    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperprice: {
    flexDirection: 'row-reverse',

    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  myStarStyle: {
    color: 'yellow',
    backgroundColor: 'transparent',
    textShadowColor: 'black',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 2,
  },
  myEmptyStarStyle: {
    color: 'white',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});
