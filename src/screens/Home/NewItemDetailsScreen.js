import React, { useEffect, useState, useRef } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  ImageBackground,
  Platform,
  Text,
  TextInput,
  NativeModules,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {  ScrollView } from 'react-native-gesture-handler';
const DEVICE_hight = Dimensions.get('window').height;
const DEVICE_width = Dimensions.get('window').width;
import TagsViewSelectionClickableSingle from '../../common/Tags/TagsViewSelectionClickableSingle';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import Iconn from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CheckBox } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import ActionSheet from 'react-native-actions-sheet';
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
import { AddProductAction } from '../../actions/Actions';
import { EmptyAddItemAction } from '../../actions/Actions';
import { GetUserDetailsAction } from '../../actions/Actions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

let deviceLocale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;
if (
  typeof deviceLocale !== 'undefined' &&
  deviceLocale !== null &&
  deviceLocale.includes('ar')
) {
  deviceLocale = 'ar';
} else {
  deviceLocale = 'en';
}
import LinearGradient from 'react-native-linear-gradient';

export default function NewItemDetailsScreen(props) {
  const dispatch = useDispatch();

  const [srcimage, setsrcimage] = useState(require('../../imgs/defaultt.png'));
  const [subcategory, setsubcategory] = useState([]);
  const [price, setprice] = useState(0);
  const [addproduct, setaddproduct] = useState('أضف المنتج الجديد');
  const [description, setdescription] = useState('');
  const [maincategoryid, setmaincategoryid] = useState(null);
  const [imagebaseprofile, setimagebaseprofile] = useState('');
  const [tags, settags] = useState([]);
  const [name, setname] = useState('');
  const [id, setid] = useState('');
  const [spespecial, setspespecial] = useState(false);
  const [currency, setcurrency] = useState('درهم اماراتي');
  const [currencyid, setcurrencyid] = useState(1);
  const [token, settoken] = useState('');
  const actionSheetRef = useRef();
  const showModalVisible = () => {
    actionSheetRef.current?.setModalVisible(true);
  };
  const hideModalVisible = () => {
    actionSheetRef.current?.setModalVisible(false);
  };

  actionSheetRef.current?.snapToOffset(300);

  const [isLoading, setisLoading] = useState(false);
  const VendorDetailsReducer = useSelector(
    (state) => state.VendorDetailsReducer
  );
  const GetCategoriesReducer = useSelector(
    (state) => state.GetCategoriesReducer
  );
  const AddItemReducer = useSelector((state) => state.AddItemReducer);
  const [spinner, setspinner] = useState(false);

  if (isLoading) {
    if (typeof AddItemReducer !== 'undefined' && AddItemReducer != null) {
      if (AddItemReducer.error === 'success') {
        Toast.showWithGravity(
          'تمت إضافة المنتج بنجاح',
          Toast.LONG,
          Toast.BOTTOM
        );
        dispatch(GetUserDetailsAction(id, currencyid));
        setisLoading(false);

        props.navigation.goBack(null);
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }
      } else if (
        (typeof AddItemReducer.error !== 'undefined' ||
          typeof AddItemReducer.error.response !== 'undefined' ||
          typeof AddItemReducer.error.response.data !== 'undefined') &&
        AddItemReducer.error !== ''
      ) {
        if (
          typeof AddItemReducer.error.response.data !== 'undefined' &&
          AddItemReducer.error.response.data.errors !== 'undefined' &&
          AddItemReducer.error.response.data.errors.length > 0 &&
          ('' + AddItemReducer.error.response.data.errors[0]).includes(
            'The item_photo'
          )
        ) {
          Toast.showWithGravity(
            ' تحتاج إلى إضافة صورة المنتج  ',
            Toast.LONG,
            Toast.BOTTOM
          );
          dispatch(EmptyAddItemAction());
          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }
        } else {
          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('password', 'null').then(() => {
                AsyncStorage.setItem('token', 'null').then(() => {
                  AsyncStorage.setItem('id', 'null').then(() => {
                    Toast.showWithGravity(
                      'تحتاج إلى تسجيل الدخول مرة أخرى ',
                      Toast.LONG,
                      Toast.BOTTOM
                    );

                    props.navigation.navigate('LoginScreen', {});
                  });
                });
              });
            });
          });
        }
      }
    }
  }

  const loadApp = async () => {
    try {
      // setcurrencyid(props.navigation.state.params.currencyid);

      //       setcurrency(props.navigation.state.params.currency);
      settoken(props.navigation.state.params.token);
      setid(props.navigation.state.params.id);
    } catch (e) {}
  };
  useEffect(() => {
    setisLoading(false);
    try {
      for (let i = 0; i < GetCategoriesReducer.payload.length; i++) {
        if (
          GetCategoriesReducer.payload[i].id ===
          parseInt(
            VendorDetailsReducer.payload.vendor.category_id[0].category_id
          )
        ) {
          settags(GetCategoriesReducer.payload[i].subcategory);
        }
      }
    } catch (e) {}
    loadApp();
  }, []);

  handleChoosePhoto = () => {
    const options = {
      noData: true,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.uri) {
        // this.setState({ photo: response ,avatarSource:response});
      }
    });
  };

  // const selectPhotoTapped=() =>  {
  //   const options = {
  //     quality: 1.0,
  //     maxWidth: 500,
  //     maxHeight: 500,
  //     storageOptions: {
  //       skipBackup: true,
  //       path: "itemfiles"
  //     },
  //   };
  //   ImagePicker.showImagePicker(options, (response) => {

  //     if (response.didCancel) {
  //     } else if (response.error) {
  //     } else if (response.customButton) {
  //     } else {
  //       let source = { uri: response.uri };

  //       ImgToBase64.getBase64String(response.uri)
  //       .then(base64String => {
  //         setimagebaseprofile(base64String);
  //       }

  //         )
  //       .catch(err => {

  //       });

  //       // You can also display the image using data:
  //       // let source = { uri: 'data:image/jpeg;base64,' + response.data };

  //       // this.setState({
  //       //     srcimage: source,
  //       // });
  //       setsrcimage(source);
  //     }
  //   });
  // }

  const openCamera = (cropit) => {
    ImagePicker.openCamera({
      width: 500,
      height: 500,
      cropping: true,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  const openGallary = (cropit) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  return (
    <View
      style={{
        flexDirection: 'column',
        backgroundColor: 'white',
        height: DEVICE_hight,
      }}>
      <ImageBackground
        resizeMode='cover'
        source={srcimage}
        style={{
          width: '100%',
          height: 400,
        }}>
        <ImageBackground
          style={{
            width: '100%',
            height: 150,
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
          }}
          resizeMode='stretch'
          source={require('../../imgs/transheader.png')}>
          <View
            style={{
              width: '100%',
              marginTop: 70,
              paddingHorizontal: 30,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
            }}>
            <Icon
              name='arrow-back'
              size={25}
              color='white'
              style={{ transform: [{ rotateY: '180deg' }] }}
              onPress={() => props.navigation.goBack(null)}
            />
            <Text style={{ fontSize: 20, color: 'white' }}>Waddelly</Text>
            <Iconn
              name='image-edit-outline'
              size={25}
              color='white'
              onPress={() => {
                showModalVisible();
              }}
            />
          </View>
        </ImageBackground>
      </ImageBackground>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ height: '65%' }}>
 <KeyboardAwareScrollView   style={{
          flexDirection: 'column',
          height: '65%' }}>
        <View style={{ width: '100%', flexDirection: 'column' }}>
          {(Platform.OS === 'ios' ||
            (Platform.OS === 'android' && deviceLocale === 'en')) && (
            <View
              style={{
                width: '100%',
                flexDirection: 'row-reverse',
                marginTop: 5,
                marginBottom: 8,
              }}>
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.inputname}
                  placeholder={' اسم المنتج '}
                  multiline={false}
                  autoCapitalize={'none'}
                  returnKeyType={'next'}
                  autoCorrect={false}
                  onChangeText={(name) => setname(name)}
                  value={name}
                  secureTextEntry={false}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
            </View>
          )}
          {Platform.OS === 'android' && deviceLocale === 'ar' && (
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                marginTop: 5,
                marginBottom: 8,
              }}>
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.inputname}
                  placeholder={' اسم المنتج '}
                  multiline={false}
                  autoCapitalize={'none'}
                  returnKeyType={'next'}
                  autoCorrect={false}
                  onChangeText={(name) => setname(name)}
                  value={name}
                  secureTextEntry={false}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
            </View>
          )}

          {(Platform.OS === 'ios' ||
            (Platform.OS === 'android' && deviceLocale === 'en')) && (
            <View
              style={{
                width: '100%',
                flexDirection: 'row-reverse',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
              }}>
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.inputaddress}
                  placeholder={'وصف المنتج'}
                  multiline={true}
                  autoCapitalize={'none'}
                  returnKeyType={'next'}
                  autoCorrect={false}
                  onChangeText={(address) => setdescription(address)}
                  value={description}
                  secureTextEntry={false}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
            </View>
          )}

          {Platform.OS === 'android' && deviceLocale === 'ar' && (
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
              }}>
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.inputaddress}
                  placeholder={'وصف المنتج'}
                  multiline={true}
                  autoCapitalize={'none'}
                  returnKeyType={'next'}
                  autoCorrect={false}
                  onChangeText={(address) => setdescription(address)}
                  value={description}
                  secureTextEntry={false}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
            </View>
          )}

          {(Platform.OS === 'ios' ||
            (Platform.OS === 'android' && deviceLocale === 'en')) && (
            <View
              style={{
                width: '100%',
                flexDirection: 'row-reverse',
                alignItems: 'center',
                marginTop: 5,
                paddingRight: 5,
                height: 50,
              }}>
              <View
                style={{
                  flexDirection: 'row-reverse',
                  width: '100%',
                  alignContent: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    color: '#44a0c9',
                    marginHorizontal: 5,
                  }}>
                  {' '}
                  السعر
                </Text>
                <View>
                  <LinearGradient
                    colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                    style={{ borderRadius: 30 }}>
                    <View style={styles.inputWrapperprice}>
                      <TextInput
                        style={styles.inputprice}
                        placeholder={'السعر'}
                        autoCapitalize={'none'}
                        returnKeyType={'next'}
                        autoCorrect={false}
                        keyboardType={'numeric'}
                        onChangeText={(price) => setprice(price)}
                        value={price}
                        secureTextEntry={false}
                        placeholderTextColor='grey'
                        underlineColorAndroid='transparent'
                      />
                    </View>
                  </LinearGradient>
                </View>
                <Text
                  style={{
                    fontSize: 15,
                    color: '#44a0c9',
                    marginHorizontal: 5,
                  }}>
                  {' '}
                  {currency}
                </Text>
              </View>
            </View>
          )}

          {Platform.OS === 'android' && deviceLocale === 'ar' && (
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 5,
                paddingRight: 5,
                height: 50,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignContent: 'center',
                  alignItems: 'center',
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    color: '#44a0c9',
                    marginHorizontal: 5,
                  }}>
                  {' '}
                  السعر
                </Text>
                <View>
                  <LinearGradient
                    colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                    style={{ borderRadius: 30 }}>
                    <View style={styles.inputWrapperprice}>
                      <TextInput
                        style={styles.inputprice}
                        placeholder={'السعر'}
                        autoCapitalize={'none'}
                        returnKeyType={'next'}
                        autoCorrect={false}
                        keyboardType={'numeric'}
                        onChangeText={(price) => setprice(price)}
                        value={price}
                        secureTextEntry={false}
                        placeholderTextColor='grey'
                        underlineColorAndroid='transparent'
                      />
                    </View>
                  </LinearGradient>
                </View>
                <Text
                  style={{
                    fontSize: 15,
                    color: '#44a0c9',
                    marginHorizontal: 5,
                  }}>
                  {' '}
                  {currency}
                </Text>
              </View>
            </View>
          )}

          {(Platform.OS === 'ios' ||
            (Platform.OS === 'android' && deviceLocale === 'en')) && (
            <View
              style={{
                width: DEVICE_width,
                flexDirection: 'row-reverse',
                alignItems: 'center',
                justifyContent: 'flex-start',
              }}>
                  <CheckBox
                onPress={() => setspespecial(!spespecial)}
                size={25}
                containerStyle={{  marginLeft: -10 }}
                checked={spespecial}
              />
              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => {
                  setspespecial(!spespecial);
                  // if(spespeciali=== true)
                  // {
                  //   setspespeciali(1)
                  // }else{
                  //   setspespeciali(0)

                  // }
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    marginRight: 10,
                    color: '#44a0c9',
                  }}>
                  عرض خاص{' '}
                </Text>
              </TouchableOpacity>
            </View>
          )}
          {Platform.OS === 'android' && deviceLocale === 'ar' && (
            <View
              style={{
                width: DEVICE_width,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                onPress={() => setspespecial(!spespecial)}
                size={25}
                containerStyle={{ marginRight: -10, marginLeft: -10 }}
                checked={spespecial}
              />

              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => {
                  setspespecial(!spespecial);
                  // if(spespecial=== true)
                  // {
                  //   setspespeciali(1)
                  // }else{
                  //   setspespeciali(0)

                  // }
                }}>
                <Text
                  style={{
                    fontSize: 15,
                    marginRight: 10,
                    color: '#44a0c9',
                  }}>
                  عرض خاص{' '}
                </Text>
              </TouchableOpacity>
            </View>
          )}

          {(Platform.OS === 'ios' ||
            (Platform.OS === 'android' && deviceLocale === 'en')) && (
            <View
              style={{
                width: '100%',
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row-reverse',
                  marginTop: 10,
                  paddingRight: 5,
                }}>
                <Text style={{ fontSize: 15, color: '#44a0c9' }}>
                  {' '}
                  نوع المنتج :{' '}
                </Text>
              </View>
            </View>
          )}

          {Platform.OS === 'android' && deviceLocale === 'ar' && (
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  paddingRight: 5,
                }}>
                <Text style={{ fontSize: 15, color: '#44a0c9' }}>
                  {' '}
                  نوع المنتج :{' '}
                </Text>
              </View>
            </View>
          )}

          <TagsViewSelectionClickableSingle
            all={tags}
            language={deviceLocale}
            selected={subcategory}
            isExclusive={false}
            selectSubCategory={(item) => {
              try {
                setsubcategory(item.id);
                setmaincategoryid(item.parent_id);
              } catch (e) {
                setsubcategory([]);
                setmaincategoryid(null);
              }
            }}
          />

          <TouchableOpacity
            style={{
              alignItems: 'center',
              marginBottom: 30,
            }}
            onPress={() => {
              if (
                name.length > 0 &&
                description.length > 0 &&
                ('' + subcategory).length > 0 &&
                price.length > 0 &&
                maincategoryid !== null
              ) {
                if (imagebaseprofile !== '') {
                  setisLoading(true);

                  dispatch(
                    AddProductAction(
                      name,
                      description,
                      maincategoryid,
                      subcategory,
                      price,
                      '100',
                      currencyid,
                      spespecial,
                      imagebaseprofile,
                      token
                    )
                  );

                  setspinner(true);
                } else {
                  Toast.showWithGravity(
                    'تحتاج إلى إضافة صورة المنتج ',
                    Toast.LONG,
                    Toast.BOTTOM
                  );
                }
              } else {
                Toast.showWithGravity(
                  'تحتاج إلى إضافة التفاصيل المفقودة ',
                  Toast.LONG,
                  Toast.BOTTOM
                );
              }
              // props.navigation.goBack(null)
            }}>
            <LinearGradient
              colors={['#04b5eb', '#06d4e5', '#06d4e5']}
              style={{ borderRadius: 20 }}>
              <Text
                style={{
                  color: 'white',
                  marginHorizontal: 25,
                  marginVertical: 8,
                  fontSize: 20,
                }}>
                {addproduct}
              </Text>
            </LinearGradient>
          </TouchableOpacity>

          <View style={{ height: 20 }}></View>
        </View>
    
        </KeyboardAwareScrollView>
      </ScrollView>

      <ActionSheet
        ref={actionSheetRef}
        containerStyle={{
          backgroundColor: 'transparent',
        }}
        elevation={0}>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            margin: 20,
            borderRadius: 10,
          }}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              //
              console.log("TEST");
              hideModalVisible();

              setTimeout(() => {
                openCamera(true);
              }, 1000);
            }}>
            <Text>الكاميرا</Text>
          </TouchableOpacity>
          <View
            style={{
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: '#707070',
            }}
          />
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openGallary(true);
              }, 1000);
            }}>
            <Text>المعرض</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            marginHorizontal: 20,
            borderRadius: 10,
            marginBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();
            }}>
            <Text style={{ color: 'red' }}>إلغاء</Text>
          </TouchableOpacity>
        </View>
      </ActionSheet>

      <Spinner
        visible={spinner}
        textContent={''}
        textStyle={styles.spinnerTextStyle}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  inputaddress: {
    backgroundColor: '#ffffff',
    width: '90%',
    marginHorizontal: '5%',
    height: 100,
    fontSize: 14,
    marginBottom: 10,
    paddingVertical: 40,
    paddingHorizontal: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 40,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },
  inputname: {
    marginHorizontal: '5%',
    backgroundColor: '#ffffff',
    width: '90%',
    marginTop: 10,
    height: 50,
    fontSize: 14,

    padding: 10,

    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 30,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },

  input: {
    backgroundColor: '#ffffff',
    width: '94%',
    marginLeft: '3%',
    marginRight: '3%',
    height: 30,
    marginHorizontal: 30,
    paddingRight: 20,
    fontSize: 14,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },
  inputprice: {
    width: 100,
    fontSize: 14,
    height: 40,
    paddingRight: 5,
    textAlign: 'center',
    borderWidth: 0,
    borderRadius: 30,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: 'white',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',

    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperprice: {
    flexDirection: 'row-reverse',

    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  myStarStyle: {
    color: 'yellow',
    backgroundColor: 'transparent',
    textShadowColor: 'black',
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 2,
  },
  myEmptyStarStyle: {
    color: 'white',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});
