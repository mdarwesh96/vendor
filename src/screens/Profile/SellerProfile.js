import React, { useEffect, useState, useRef } from 'react';
import moment from 'moment';

import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  TouchableOpacity,
  TextInput,
  NativeModules,
  Dimensions,
  ImageBackground,
  Image,
  TouchableWithoutFeedback,
  Platform,
  TouchableHighlight
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { drawer } from '../../AppNavigation';
import { drawerD } from '../../AppNavigationDefault';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ActionSheet from 'react-native-actions-sheet';
import Icond from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-async-storage/async-storage';
let bgg = require('../../imgs/profilebg.png');
const DEVICE_width = Dimensions.get('window').width;
const DEVICE_hight = Dimensions.get('window').height;
const MARGIN = 60;
import SwipeablePanelm from '../../common/actualComponents/Panelm';
import SelectableFlatlist, { STATE } from 'react-native-selectable-flatlist';

import SelectableGrid from '../../common/selectablegrid/SelectableGrid';

let holidays = [
  { label_name_ar: 'السبت' },
  { label_name_ar: 'الأحد' },
  { label_name_ar: 'الإثنين' },
  { label_name_ar: 'الثلاثاء' },
  { label_name_ar: 'الأربعاء' },
  { label_name_ar: 'الخميس' },
  { label_name_ar: 'الجمعة' },
];

import TagsViewSelectionClickablelogin from '../../common/Tags/TagsViewSelectionClickablelogin';
import { ScrollView } from 'react-native-gesture-handler';
import Spinner from 'react-native-loading-spinner-overlay';
import { UpdateVendorStateAction } from '../../actions/Actions';
import { UpdateAvailabiltyAction } from '../../actions/Actions';
import IconA from 'react-native-vector-icons/AntDesign';
import AppMetrics from '../../common/metrics'
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
// import {GetCategoriesSubCategoriesLabelsAction} from '..././actions/Actions';
import { LoginUserAction } from '../../actions/Actions';
import { EditProfileAction } from '../../actions/Actions';
import { GetUserDetailsAction } from '../../actions/Actions';
import TagsVieww from '../../common/Tags/TagsVieww';

let deviceLocale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;
if (
  typeof deviceLocale !== 'undefined' &&
  deviceLocale !== null &&
  deviceLocale.includes('ar')
) {
  deviceLocale = 'ar';
} else {
  deviceLocale = 'en';
}
let enn = true;
let to,from;
export default function SellerProfile(props) {
  const dispatch = useDispatch();

  const [citylist, setcitylist] = useState([]);
  //1 - DECLARE VARIABLES
  const actionSheetRef = useRef();
  const showModalVisible = () => {
    actionSheetRef.current?.setModalVisible(true);
  };
  const hideModalVisible = () => {
    actionSheetRef.current?.setModalVisible(false);
  };

  actionSheetRef.current?.snapToOffset(300);

  const [selectedd, setselectedd] = useState('');
  const [clickable, setclickable] = useState(false);
  const [spinner, setspinner] = useState(false);
  const [tags, settags] = useState([]);
  const [srcimage, setsrcimage] = useState(require('../../imgs/defaultt.png'));
  const [name, setname] = useState('');
  const [phone, setphone] = useState('');
  const [email, setemail] = useState('');
  const [vendordescription, setvendordescription] = useState('');
  const [password, setpassword] = useState('');
  const [address, setaddress] = useState('');
  const [imagebaseprofile, setimagebaseprofile] = useState('');

  const [devicetoken, setdevicetoken] = useState('kkkkkkkkkkkk');
  const [citiesss, setcitiesss] = useState([]);
  const [currency, setcurrency] = useState('دولار');
  const [currencyid, setcurrencyid] = useState('5');

  const [startclocktext, setstartclocktext] = useState('');
  const [endclocktext, setendclocktext] = useState('');

  const [categoryname, setcategoryname] = useState('اختر فئة المحل');
  const [city, setcity] = useState('اختر المدينة ');
  const [textcolor, settextcolor] = useState('grey');
  const [cityid, setcityid] = useState(1);
  const [subcityid, setsubcityid] = useState(1);

  
  const [categoryid, setcategoryid] = useState(1);
  const [vendorlabels, setvendorlabels] = useState([]);

  const [offdays, setoffdays] = useState('');

  const [start, setstart] = useState('صباحا');
  const [startclock, setstartclock] = useState('');
  const [startclockid, setstartclockid] = useState(2);
  const [iddd, setiddd] = useState(1);

  const [end, setend] = useState('مساء');
  const [endclock, setendclock] = useState('');
  const [openingtime, setopeningtime] = useState('');
  const [endclockid, setendclockid] = useState(2);

  const [swipeablePanelendhour, setswipeablePanelendhour] = useState(false);
  const [swipeablePanelstarthour, setswipeablePanelstarthour] = useState(false);
  const [currencyswipable, setcurrencyswipable] = useState(false);

  const [amcolors, setamcolors] = useState('white');
  const [amsizes, setamsizes] = useState(20);
  const [amweights, setamweights] = useState('bold');
  const [ambacks, setambacks] = useState('#0599ea');

  const [pmcolors, setpmcolors] = useState('grey');
  const [pmsizes, setpmsizes] = useState(17);
  const [pmweights, setpmweights] = useState('normal');
  const [pmbacks, setpmbacks] = useState('white');
  const [amcolor, setamcolor] = useState('grey');
  const [amsize, setamsize] = useState(17);
  const [amweight, setamweight] = useState('normal');
  const [amback, setamback] = useState('white');
  const [pmcolor, setpmcolor] = useState('white');
  const [pmsize, setpmsize] = useState(20);
  const [pmweight, setpmweight] = useState('bold');
  const [pmback, setpmback] = useState('#0599ea');
  const [token, settoken] = useState('');
  const [labelfirst, setlabelfirst] = useState(true);
  const [holidayfirst, setholidayfirst] = useState(true);
  const [selectedlabels, setselectedlabels] = useState([]);
  const [selectedholidays, setselectedholidays] = useState([]);
  const [delivery_address, setdelivery_address] = useState([]);
  const GetCategoriesReducer = useSelector(
    (state) => state.GetCategoriesReducer
  );
  const GetLabelsReducer = useSelector((state) => state.GetLabelsReducer);
  const VendorDetailsReducer = useSelector(
    (state) => state.VendorDetailsReducer
  );
  const EditProfileReducer = useSelector((state) => state.EditProfileReducer);
  const LoginReducer = useSelector((state) => state.LoginReducer);
  try{
   to = VendorDetailsReducer.payload.vendor.work_hours[0].end;
   from = VendorDetailsReducer.payload.vendor.work_hours[0].start;
  }catch(r)
  {
   
  }
  const getdeliverytext = (deliveryid) => {
    let d = '';

    if (deliveryid !== null) {
      if (deliveryid.indexOf('/') > -1) {
        let iinn = deliveryid.indexOf('/') + 2;
        d = deliveryid.substring(iinn, deliveryid.length);
      }
    }

    return d;
  };

  if (enn) {
    let citylisttf = [];
    for (let f = 0; f < citiesss.length; f++) {
      if (citiesss[f].cities.length > 0) {
        for (let d = 0; d < citiesss[f].cities.length; d++) {
          citylisttf.push({
            id: d + 1,
            name: citiesss[f].cities[d].city_name_ar,
            idd: citiesss[f].cities[d].id,
          });
        }
      }
    }
    enn = false;

    setcitylist(citylisttf);
  }
  // const removeitemaddress = (arr, innn) => {
  //   let d = [];
  //   for (let s = 0; s < arr.length; s++) {
  //     if (s !== parseInt(innn)) {
  //       d.push(arr[s]);
  //     }
  //   }
  //   return d;
  // };
  // const getdeliverycityname = (deliveryid) => {
  //   let d = '';

  //   if (deliveryid.indexOf('-') > -1) {
  //     let iinn = deliveryid.indexOf('-') + 1;
  //     d = deliveryid.substring(iinn, deliveryid.length);
  //   }

  //   return d;
  // };

  // const getdeliverysubcityname = (deliveryid) => {
  //   let d = '';

  //   if (deliveryid.indexOf('/') > -1) {
  //     let iinn = deliveryid.indexOf('/') + 2;
  //     d = deliveryid.substring(iinn, deliveryid.indexOf('-'));
  //   }

  //   return d;
  // };
  // const getsubcitylistwithtwo = (city, showswipableflag) => {
  //   let subcitylisttt = [];
  //   let cityid = city.substring(0, city.indexOf('='));

  //   for (let f = 0; f < citiesss.length; f++) {
  //     if (citiesss[f].subcities.length > 0) {
  //       for (let d = 0; d < citiesss[f].subcities.length; d++) {
  //         if ('' + citiesss[f].subcities[d].city_id === '' + cityid) {
  //           subcitylisttt.push({
  //             id: d + 1,
  //             name: citiesss[f].subcities[d].subcity_name_ar,
  //             idd: citiesss[f].subcities[d].id,
  //           });
  //         }
  //       }
  //     }
  //   }

  //   if (showswipableflag) {
  //     if (subcitylisttt.length > 0) {
  //       setspinner(false);

  //       setsubcityswipable(false);
  //       setsubcityswipable(true);

  //       setsubcitylist(subcitylisttt);

  //       setcityswipable(false);
  //     } else {
  //       setcityswipable(false);
  //       setspinner(false);
  //       setsubcitylist(subcitylisttt);

  //       Toast.showWithGravity(
  //         'لا توجد مناطق في هذه المدينة',
  //         Toast.LONG,
  //         Toast.BOTTOM
  //       );
  //     }
  //   } else {
  //     setsubcitylist(subcitylisttt);
  //   }

  //   return subcitylist;
  // };

  // const getsubcitylist = (cityid) => {
  //   let subcitylist = [];

  //   for (let f = 0; f < citiesss.length; f++) {
  //     if (citiesss[f].subcities.length > 0) {
  //       for (let d = 0; d < citiesss[f].subcities.length; d++) {
  //         if ('' + citiesss[f].subcities[d].city_id === '' + cityid) {
  //           subcitylist.push({
  //             id: d + 1,
  //             name: citiesss[f].subcities[d].subcity_name_ar,
  //             idd: citiesss[f].subcities[d].id,
  //           });
  //         }
  //       }
  //     }
  //   }
  //   if (selecteddd === '1') {
  //     subcitylisttt1 = subcitylist;
  //   }

  //   return subcitylist;
  // };

  // const itemsSelectedcity = (selectedItem) => {
  //   if (selecteddd === '000') {
  //     setcityid(selectedItem.idd);
  //     setsubcityid(null);
  //     setsubcityidd(null);

  //     setcityswipable(false);
  //     setcityselected(selectedItem.name);
  //     setsubcityselected('المنطقة');

  //     let sss = getsubcitylist(selectedItem.idd);

  //     setsubcitylist(sss);

  //     if (sss.length > 6) {
  //       setsubcitylarge(true);
  //     } else {
  //       setsubcitylarge(false);
  //     }

  //     setsubcityswipable(false);
  //     setcityswipable(false);
  //   }
  // };
  // const itemsSelectedsubcity = (selectedItem) => {
  //   if (selecteddd === '1') {
  //     setsubcityid1(selectedItem.idd);

  //     setcityswipable(false);
  //     setsubcityswipable(false);
  //     setdeliverysubcity1(selectedItem.name);
  //   }
  // };

  // const rowcityItem = (item) => (
  //   <TouchableOpacity
  //     activeOpacity={0.9}
  //     style={{
  //       flex: 1,
  //       borderBottomWidth: 0.7,
  //       alignItems: 'stretch',
  //       paddingLeft: 30,
  //       paddingRight: 30,
  //       justifyContent: 'space-between',
  //       paddingVertical: 5,
  //       flexDirection: 'row-reverse',
  //       width: '100%',
  //       borderBottomColor: 'white',
  //     }}
  //     onPress={(_) => {}}>
  //     <View style={{ width: '120%' }}>
  //       <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
  //         {item.name}
  //       </Text>
  //       <View
  //         style={{
  //           backgroundColor: '#CCCCCC',
  //           height: 1,
  //           width: '100%',
  //           marginTop: 8,
  //         }}></View>
  //     </View>
  //   </TouchableOpacity>
  // );
  // const rowsubcityItem = (item) => (
  //   <TouchableOpacity
  //     activeOpacity={0.9}
  //     style={{
  //       flex: 1,
  //       borderBottomWidth: 0.0,
  //       alignItems: 'stretch',
  //       paddingLeft: 30,
  //       paddingRight: 30,
  //       justifyContent: 'space-between',
  //       paddingVertical: 5,
  //       flexDirection: 'row-reverse',
  //       width: '100%',
  //       borderBottomColor: 'white',
  //     }}
  //     onPress={(_) => {
  //       if (selecteddd === '1') {
  //         setsubcityid1(item.idd);

  //         setcityswipable(false);
  //         setsubcityswipable(false);
  //         setdeliverysubcity1(item.name);
  //       }
  //     }}>
  //     <View style={{ width: '120%' }}>
  //       <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
  //         {item.name}
  //       </Text>
  //       <View
  //         style={{
  //           backgroundColor: '#CCCCCC',
  //           height: 1,
  //           width: '100%',
  //           marginTop: 8,
  //         }}></View>
  //     </View>
  //   </TouchableOpacity>
  // );

  const checkdayisopenday = (day) => {
    let weeksid = [];
    if (
      VendorDetailsReducer.payload.vendor.work_days === null ||
      VendorDetailsReducer.payload.vendor.work_days.length === 0
    ) {
      let vendorisopen = true;

      dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 1));
      dispatch(UpdateAvailabiltyAction(1));

      props.navigation.navigate('HomeSeller', {
        token: LoginReducer.payload.token.token,
        stateavailable: vendorisopen,
        id: LoginReducer.payload.data.id,
        currencyid: currencyid,
        currency: currency,
      });
    } else {
      for (
        let s = 0;
        s < VendorDetailsReducer.payload.vendor.work_days.length;
        s++
      ) {
        if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الأحد'
        ) {
          weeksid.push(0);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الإثنين'
        ) {
          weeksid.push(1);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
          'الثلاثاء'
        ) {
          weeksid.push(2);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
          'الأربعاء'
        ) {
          weeksid.push(3);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الخميس'
        ) {
          weeksid.push(4);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الجمعة'
        ) {
          weeksid.push(5);
        } else if (
          VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'السبت'
        ) {
          weeksid.push(6);
        }
      }

      if (weeksid.includes(day)) {
        let vendorisopen = false;
        dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 3));
        dispatch(UpdateAvailabiltyAction(3));

        props.navigation.navigate('HomeSeller', {
          token: LoginReducer.payload.token.token,
          stateavailable: vendorisopen,
          id: LoginReducer.payload.data.id,
          currencyid: currencyid,
          currency: currency,
        });
      } else {
        let vendorisopen = true;
        dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 1));
        dispatch(UpdateAvailabiltyAction(1));

        props.navigation.navigate('HomeSeller', {
          token: LoginReducer.payload.token.token,
          stateavailable: vendorisopen,
          id: LoginReducer.payload.data.id,
          currencyid: currencyid,
          currency: currency,
        });
      }
    }
  };

  updtaefirstste = (s, kkkk) => {
    if (kkkk === 'categories') {
      setlabelfirst(false);
    } else {
      setholidayfirst(false);
    }
  };
  const getisvendoropen = () => {
    var now = moment.utc();
    var hour = now.hour();
    var min = now.minutes();
    var current = new Date();

    var day = current.getDay();

    let curremntmillic = moment.utc([2010, 1, 14, hour, min, 0, 0]).valueOf();

    if(from.includes('صباحا'))
    {
      from= from.replace('صباحا','') 
     }
  
    if(from.includes('مساء'))
    {
       from=from.replace('مساء','') 
    }
    if(to.includes('صباحا'))
    {
       to=to.replace('صباحا','') 
    }

    if(to.includes('مساء'))
    {
     to= to.replace('مساء','') 
    }
    if (curremntmillic > from && curremntmillic < to) {
      checkdayisopenday(day);
    } else {
      let vendorisopen = false;

      dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 3));
      dispatch(UpdateAvailabiltyAction(3));

      props.navigation.navigate('HomeSeller', {
        token: LoginReducer.payload.token.token,
        stateavailable: vendorisopen,
        id: LoginReducer.payload.data.id,
        currencyid: currencyid,
        currency: currency,
      });
    }
  };

  updtaefirstste = (s, kkkk) => {
    if (kkkk === 'categories') {
      setlabelfirst(false);
    } else {
      setholidayfirst(false);
    }
  };

  selectlabelsss = (selectedd) => {
    setselectedlabels(selectedd);
  };

  selectholidaysss = (selecteddd) => {
    setselectedholidays(selecteddd);
  };

  const getmylbals = (labelsarray, myregisteredlabels) => {
    let labels = [];
    for (let s = 0; s < myregisteredlabels.length; s++) {
      for (let a = 0; a < labelsarray.length; a++) {
        if ('' + labelsarray[a].id === '' + myregisteredlabels[s].label_id) {
          labels.push(labelsarray[a]);
        }
      }
    }
    if (labels.length > 0) {
      setlabelfirst(false);
    }
    return labels;
  };

  if (selectedd === 'updateprofile') {
    if (
      typeof EditProfileReducer !== 'undefined' &&
      typeof EditProfileReducer.payload !== 'undefined' &&
      EditProfileReducer.error === 'success'
    ) {
      setselectedd('login');
      dispatch(LoginUserAction(email, password, devicetoken));
    } else if (
      typeof EditProfileReducer !== 'undefined' &&
      typeof EditProfileReducer.payload !== 'undefined' &&
      EditProfileReducer.error !== 'success' &&
      EditProfileReducer.payload.error !== ''
    ) {
      setselectedd('');
      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }
      Toast.showWithGravity(
        'حدث خطأ أثناء تحديث الحساب',
        Toast.LONG,
        Toast.BOTTOM
      );
    }
  } else if (selectedd === 'login') {
    if (
      typeof LoginReducer.payload !== 'undefined' &&
      typeof LoginReducer.payload.status !== 'undefined' &&
      LoginReducer.error !== ''
    ) {
      if (LoginReducer.payload.status === '2') {
        Toast.showWithGravity(
          'حسابك غير نشط يرجى الاتصال بالإدارة  ',
          Toast.LONG,
          Toast.BOTTOM
        );
        setselectedd('');
        AsyncStorage.setItem('loggedin', 'false').then(() => {
          AsyncStorage.setItem('email', 'null').then(() => {
            AsyncStorage.setItem('password', 'null').then(() => {
              AsyncStorage.setItem('token', 'null').then(() => {
                AsyncStorage.setItem('id', 'null').then(() => {
                  if (Platform.OS === 'ios') {
                    setInterval(() => {
                      setspinner(false);
                    }, 1000);
                  } else {
                    // setInterval(() => {

                    setspinner(false);

                    // }, 1000);
                  }
                  props.navigation.navigate('LoginScreen', {});
                });
              });
            });
          });
        });
      } else if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error === 'success'
      ) {
        let id = LoginReducer.payload.data.id;
        dispatch(GetUserDetailsAction(id, currencyid));
        setselectedd('getvendordetails');

        AsyncStorage.setItem('loggedin', 'true').then(() => {
          AsyncStorage.setItem('email', email).then(() => {
            AsyncStorage.setItem('password', password).then(() => {
              AsyncStorage.setItem(
                'token',
                LoginReducer.payload.token.token
              ).then(() => {
                AsyncStorage.setItem(
                  'id',
                  '' + LoginReducer.payload.data.id
                ).then(() => {});
              });
            });
          });
        });
      } else {
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }
      }
    }
  } else if (selectedd === 'getvendordetails') {
    if (
      typeof VendorDetailsReducer !== 'undefined' &&
      typeof VendorDetailsReducer.error !== 'undefined' &&
      VendorDetailsReducer.error === 'success'
    ) {
      AsyncStorage.setItem('name', LoginReducer.payload.data.name).then(() => {
        AsyncStorage.setItem('loggedin', 'true').then(() => {
          AsyncStorage.setItem('email', email).then(() => {
            AsyncStorage.setItem('password', password).then(() => {
              AsyncStorage.setItem(
                'id',
                '' + LoginReducer.payload.data.id
              ).then(() => {
                AsyncStorage.setItem(
                  'phone',
                  '' + LoginReducer.payload.data.phone[0].phone
                ).then(() => {
                  AsyncStorage.setItem(
                    'address1',
                    '' + LoginReducer.payload.data.address[0].address_name
                  ).then(() => {
                    AsyncStorage.setItem(
                      'address2',
                      '' + LoginReducer.payload.data.address[1].address_name
                    ).then(() => {
                      AsyncStorage.setItem(
                        'address2id',
                        '' + LoginReducer.payload.data.address[1].lat
                      ).then(() => {
                        AsyncStorage.setItem(
                          'image',
                          '' + LoginReducer.payload.data.photo
                        ).then(() => {
                          AsyncStorage.setItem(
                            'token',
                            LoginReducer.payload.token.token
                          ).then(() => {
                            if (Platform.OS === 'ios') {
                              setInterval(() => {
                                setspinner(false);
                              }, 1000);
                            } else {
                              // setInterval(() => {

                              setspinner(false);

                              // }, 1000);
                            }

                            Toast.showWithGravity(
                              'تم تحديث معلوماتك',
                              Toast.LONG,
                              Toast.BOTTOM
                            );

                            getisvendoropen();
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    }
  }

  const validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };
  const setDateTime = (date, str) => {
    var sp = str.split(':');
    date.setHours(parseInt(sp[0], 10));
    date.setMinutes(parseInt(sp[1], 10));
    return date;
  };

  const getholidays = () => {
    try {
      let r = [];

      if (
        typeof VendorDetailsReducer.payload.vendor.work_days !== 'undefined' &&
        VendorDetailsReducer.payload.vendor.work_days !== null
      ) {
      

        for (
          let g = 0;
          g < VendorDetailsReducer.payload.vendor.work_days.length;
          g++
        ) {
       

          for (let s = 0; s < holidays.length; s++) {
          

            if (
              holidays[s].label_name_ar ===
              VendorDetailsReducer.payload.vendor.work_days[g].offdays
            ) {
             

              r.push({'label_name_ar':holidays[s].label_name_ar});
            }
          }
        }
      } else {
   

        // r.push['مفتوح طوال الأسبوع']
      }

      setselectedholidays(r);
    } catch (e) {}
  };
  const getlabels = (catid) => {
    try {
      settags([]);
      try {
        if (
          typeof GetCategoriesReducer !== 'undefined' &&
          typeof GetCategoriesReducer.payload !== 'undefined'
        ) {
          let s = [];
          for (let i = 0; i < GetCategoriesReducer.payload.length; i++) {
            let t = [];

            if (GetCategoriesReducer.payload[i].id === catid) {
              for (
                let f = 0;
                f < GetCategoriesReducer.payload[i].label.length;
                f++
              ) {
                s.push(GetCategoriesReducer.payload[i].label[f]);
                t.push(GetCategoriesReducer.payload[i].label[f]);
              }
            }

            if (t.length > 0) {
              settags(t);
            }
          }
          let sss = [];
          for (
            let g = 0;
            g < VendorDetailsReducer.payload.vendor.label_id.length;
            g++
          ) {
            for (let i = 0; i < s.length; i++) {
              if (
                '' +
                  VendorDetailsReducer.payload.vendor.label_id[g].label_id ===
                '' + s[i].id
              ) {
                sss.push(s[i]);
              }
            }
          }

          setselectedlabels(sss);
        }
      } catch (e) {}
    } catch (ee) {}
  };

  closepanell = (index) => {
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
    setcurrencyswipable(false);
    // this.closePanel();
  };

  const loadApp = async () => {
    try {
      settoken(props.navigation.state.params.token);
      setcurrencyid(1);
      setcurrency('درهم اماراتي');
     let  d = await AsyncStorage.getItem('devicetoken');
     setdevicetoken(d);
      setiddd(props.navigation.state.params.id);
      setemail(props.navigation.state.params.email);
      setpassword(props.navigation.state.params.password);
      setcitiesss(props.navigation.state.params.cityreducer);
    } catch (e) {}
  };
  useEffect(() => {
    loadApp();

    if (VendorDetailsReducer.payload.vendor.delivery_address !== null) {
      setdelivery_address(VendorDetailsReducer.payload.vendor.delivery_address);
    } else {
      setdelivery_address([]);
    }

    try {
      let off = '';
      for (let i = 0; i < GetCategoriesReducer.payload.length; i++) {
        if (
          GetCategoriesReducer.payload[i].id ===
          parseInt(
            VendorDetailsReducer.payload.vendor.category_id[0].category_id
          )
        ) {
          setvendordescription(
            GetCategoriesReducer.payload[i].category_name_ar
          );
        }
      }
    } catch (e) {}
    try {
      let off = '';
      for (
        let s = 0;
        s < VendorDetailsReducer.payload.vendor.work_days.length;
        s++
      ) {
        if (VendorDetailsReducer.payload.vendor.work_days[0].offdays !== null) {
          if (s === 0) {
            off =
              off + VendorDetailsReducer.payload.vendor.work_days[s].offdays;
          } else {
            off =
              off +
              ' , ' +
              VendorDetailsReducer.payload.vendor.work_days[s].offdays;
          }
          setoffdays(off);
          setholidayfirst(false);
        } else {
          setoffdays('مفتوح طوال الأسبوع');
          setholidayfirst(true);
        }
      }
    } catch (e) {
      setholidayfirst(true);
      setoffdays('مفتوح طوال الأسبوع');
    }
    if(from.includes('صباحا'))
    {
      from= from.replace('صباحا','') 
     }
  
    if(from.includes('مساء'))
    {
       from=from.replace('مساء','') 
    }
    let duration = moment.duration(from, 'milliseconds');


    let y = parseInt(moment.utc(duration.asMilliseconds()).format('HH'));

    y = y + 2;

    if (y === 25) {
      y = 12;

      setstartclocktext(y);

      y = y + ' صباحا ';
 

      setstartclock(y);
    } else {
      if (y > 12) {
        y = y - 12;

        setstartclocktext(y);

        y = y + ' مساء ';
      } else {
        setstartclocktext(y);

        y = y + ' صباحا ';
      }

      setstartclock(y);
    }
    if(to.includes('صباحا'))
    {
       to=to.replace('صباحا','') 
    }

    if(to.includes('مساء'))
    {
     to= to.replace('مساء','') 
    }
    let durationv = moment.duration(to, 'milliseconds');
    let r = parseInt(moment.utc(durationv.asMilliseconds()).format('HH'));
    r = r + 2;

    if (r === 25) {
      r = 12;

      setendclocktext(r);
      setendclock(r);

      r = r + ' صباحا ';
    } else {
      if (r > 12) {
        r = r - 12;

        setendclocktext(r);
        r = r + ' مساء ';
      } else {
        setendclocktext(r);

        r = r + ' صباحا ';
      }

      setendclock(r);
    }
    setopeningtime(y + ' - ' + r);

    try {
      if (
        GetLabelsReducer.payload !== null &&
        VendorDetailsReducer.payload.vendor.label_id !== null &&
        VendorDetailsReducer.payload.vendor.label_id.length > 0
      ) {
        setvendorlabels(
          getmylbals(
            GetLabelsReducer.payload,
            VendorDetailsReducer.payload.vendor.label_id
          )
        );
      }
    } catch (e) {}
    try {
      setname(VendorDetailsReducer.payload.vendor.name);
    } catch (e) {}
    try {
      setphone('' + VendorDetailsReducer.payload.vendor.phone[0].phone);
    } catch (e) {}
    try {
      setcity(VendorDetailsReducer.payload.vendor.address[1].address_name);
    } catch (e) {}
    try {
      
      setcityid(parseInt(VendorDetailsReducer.payload.vendor.address[1].lat));
      setsubcityid(parseInt(VendorDetailsReducer.payload.vendor.address[1].lang));   
      setaddress(VendorDetailsReducer.payload.vendor.address[0].address_name);
      settextcolor('black');
    } catch (e) {}
    try {
      getlabels(
        parseInt(VendorDetailsReducer.payload.vendor.category_id[0].category_id)
      );
    } catch (e) {}
    try {
      getholidays();
    } catch (e) {}

    try {
      setcategoryid(LoginReducer.payload.data.category_id[0].category_id);
    } catch (e) {}
    try {
      let kkkkkkkk = VendorDetailsReducer.payload.vendor.photo;

      if (
        typeof kkkkkkkk !== 'undefined' &&
        kkkkkkkk !== null &&
        kkkkkkkk !== 'null' &&
        kkkkkkkk !== 'null'
      ) {
        let jj = { uri: 'http://ecommerce.waddeely.com' + kkkkkkkk };

        setsrcimage(jj);
      }
    } catch (ee) {}
  }, []);

  const itemsSelectedstart = (selectedItem) => {
    let date;

    let m;

    if (start === 'صباحا') {
      if (selectedItem.name === '1:00') {
        m = moment.utc([2010, 1, 13, 23, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '2:00') {
        m = moment.utc([2010, 1, 14, 0, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '3:00') {
        m = moment.utc([2010, 1, 14, 1, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '4:00') {
        m = moment.utc([2010, 1, 14, 2, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '5:00') {
        m = moment.utc([2010, 1, 14, 3, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '6:00') {
        m = moment.utc([2010, 1, 14, 4, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '7:00') {
        m = moment.utc([2010, 1, 14, 5, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '8:00') {
        m = moment.utc([2010, 1, 14, 6, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '9:00') {
        m = moment.utc([2010, 1, 14, 7, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '10:00') {
        m = moment.utc([2010, 1, 14, 8, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '11:00') {
        m = moment.utc([2010, 1, 14, 9, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '12:00') {
        m = moment.utc([2010, 1, 14, 10, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      }
    } else {
      if (selectedItem.name === '1:00') {
        m = moment.utc([2010, 1, 14, 11, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '2:00') {
        m = moment.utc([2010, 1, 14, 12, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '3:00') {
        m = moment.utc([2010, 1, 14, 13, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '4:00') {
        m = moment.utc([2010, 1, 14, 14, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '5:00') {
        m = moment.utc([2010, 1, 14, 15, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '6:00') {
        m = moment.utc([2010, 1, 14, 16, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '7:00') {
        m = moment.utc([2010, 1, 14, 17, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '8:00') {
        m = moment.utc([2010, 1, 14, 18, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '9:00') {
        m = moment.utc([2010, 1, 14, 19, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '10:00') {
        m = moment.utc([2010, 1, 14, , 20, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '11:00') {
        m = moment.utc([2010, 1, 14, 21, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '12:00') {
        m = moment.utc([2010, 1, 14, 22, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      }
    }

    setstartclock(m);

    setstartclocktext(selectedItem.name);
    setstartclockid(selectedItem.id);
    setswipeablePanelstarthour(false);

    setswipeablePanelendhour(false);
  };
  const itemsSelectedend = (selectedItem) => {
    let date;

    let m;
    if (end === 'صباحا') {
      if (selectedItem.name === '1:00') {
        m = moment.utc([2010, 1, 13, 23, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '2:00') {
        m = moment.utc([2010, 1, 14, 0, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '3:00') {
        m = moment.utc([2010, 1, 14, 1, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '4:00') {
        m = moment.utc([2010, 1, 14, 2, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '5:00') {
        m = moment.utc([2010, 1, 14, 3, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '6:00') {
        m = moment.utc([2010, 1, 14, 4, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '7:00') {
        m = moment.utc([2010, 1, 14, 5, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '8:00') {
        m = moment.utc([2010, 1, 14, 6, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '9:00') {
        m = moment.utc([2010, 1, 14, 7, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '10:00') {
        m = moment.utc([2010, 1, 14, 8, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '11:00') {
        m = moment.utc([2010, 1, 14, 9, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '12:00') {
        m = moment.utc([2010, 1, 14, 10, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      }
    } else {
      if (selectedItem.name === '1:00') {
        m = moment.utc([2010, 1, 14, 11, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '2:00') {
        m = moment.utc([2010, 1, 14, 12, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '3:00') {
        m = moment.utc([2010, 1, 14, 13, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '4:00') {
        m = moment.utc([2010, 1, 14, 14, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '5:00') {
        m = moment.utc([2010, 1, 14, 15, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '6:00') {
        m = moment.utc([2010, 1, 14, 16, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '7:00') {
        m = moment.utc([2010, 1, 14, 17, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '8:00') {
        m = moment.utc([2010, 1, 14, 18, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '9:00') {
        m = moment.utc([2010, 1, 14, 19, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '10:00') {
        m = moment.utc([2010, 1, 14, , 20, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '11:00') {
        m = moment.utc([2010, 1, 14, 21, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      } else if (selectedItem.name === '12:00') {
        m = moment.utc([2010, 1, 14, 22, 0, 0, 0]).valueOf();
        // m=  date.getTime()
      }
    }

    setendclock(m);

    setendclocktext(selectedItem.name);

    setendclockid(selectedItem.id);
    setswipeablePanelstarthour(false);
    setswipeablePanelendhour(false);
  };
  const rowItemstart = (item) => (
    <TouchableOpacity
      activeOpacity={0.9}
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 20,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        let date;
        let m;
        if (start === 'صباحا') {
          if (item.name === '1:00') {
            m = moment.utc([2010, 1, 13, 23, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '2:00') {
            m = moment.utc([2010, 1, 14, 0, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '3:00') {
            m = moment.utc([2010, 1, 14, 1, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '4:00') {
            m = moment.utc([2010, 1, 14, 2, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '5:00') {
            m = moment.utc([2010, 1, 14, 3, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '6:00') {
            m = moment.utc([2010, 1, 14, 4, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '7:00') {
            m = moment.utc([2010, 1, 14, 5, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '8:00') {
            m = moment.utc([2010, 1, 14, 6, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '9:00') {
            m = moment.utc([2010, 1, 14, 7, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '10:00') {
            m = moment.utc([2010, 1, 14, 8, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '11:00') {
            m = moment.utc([2010, 1, 14, 9, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '12:00') {
            m = moment.utc([2010, 1, 14, 10, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          }
        } else {
          if (item.name === '1:00') {
            m = moment.utc([2010, 1, 14, 11, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '2:00') {
            m = moment.utc([2010, 1, 14, 12, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '3:00') {
            m = moment.utc([2010, 1, 14, 13, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '4:00') {
            m = moment.utc([2010, 1, 14, 14, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '5:00') {
            m = moment.utc([2010, 1, 14, 15, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '6:00') {
            m = moment.utc([2010, 1, 14, 16, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '7:00') {
            m = moment.utc([2010, 1, 14, 17, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '8:00') {
            m = moment.utc([2010, 1, 14, 18, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '9:00') {
            m = moment.utc([2010, 1, 14, 19, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '10:00') {
            m = moment.utc([2010, 1, 14, , 20, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '11:00') {
            m = moment.utc([2010, 1, 14, 21, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '12:00') {
            m = moment.utc([2010, 1, 14, 22, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          }
        }
        setswipeablePanelstarthour(false);
        setswipeablePanelendhour(false);

        setstartclockid(item.id);

        setstartclock(m);
        setstartclocktext(item.name);
      }}>
      <View>
        {(Platform.OS === 'ios' ||
          (Platform.OS === 'android' && deviceLocale === 'en')) && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
            {item.name}
          </Text>
        )}
        {Platform.OS === 'android' && deviceLocale === 'ar' && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'left' }}>
            {item.name}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );

  const rowItemend = (item) => (
    <TouchableOpacity
      activeOpacity={0.9}
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 20,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        let date;

        let m;

        if (end === 'صباحا') {
          if (item.name === '1:00') {
            m = moment.utc([2010, 1, 13, 23, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '2:00') {
            m = moment.utc([2010, 1, 14, 0, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '3:00') {
            m = moment.utc([2010, 1, 14, 1, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '4:00') {
            m = moment.utc([2010, 1, 14, 2, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '5:00') {
            m = moment.utc([2010, 1, 14, 3, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '6:00') {
            m = moment.utc([2010, 1, 14, 4, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '7:00') {
            m = moment.utc([2010, 1, 14, 5, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '8:00') {
            m = moment.utc([2010, 1, 14, 6, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '9:00') {
            m = moment.utc([2010, 1, 14, 7, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '10:00') {
            m = moment.utc([2010, 1, 14, 8, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '11:00') {
            m = moment.utc([2010, 1, 14, 9, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '12:00') {
            m = moment.utc([2010, 1, 14, 10, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          }
        } else {
          if (item.name === '1:00') {
            m = moment.utc([2010, 1, 14, 11, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '2:00') {
            m = moment.utc([2010, 1, 14, 12, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '3:00') {
            m = moment.utc([2010, 1, 14, 13, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '4:00') {
            m = moment.utc([2010, 1, 14, 14, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '5:00') {
            m = moment.utc([2010, 1, 14, 15, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '6:00') {
            m = moment.utc([2010, 1, 14, 16, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '7:00') {
            m = moment.utc([2010, 1, 14, 17, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '8:00') {
            m = moment.utc([2010, 1, 14, 18, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '9:00') {
            m = moment.utc([2010, 1, 14, 19, 0, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '10:00') {
            m = moment.utc([2010, 1, 14, , 20, 0, 0]).valueOf();
            // m=  date.getTime()
          } else if (item.name === '11:00') {
            m = moment.utc([2010, 1, 14, 21, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          } else if (item.name === '12:00') {
            m = moment.utc([2010, 1, 14, 22, 0, 0, 0]).valueOf();

            // m=  date.getTime()
          }
        }

        setswipeablePanelstarthour(false);

        setswipeablePanelendhour(false);

        setendclocktext(item.name);

        setendclockid(item.id);

        setendclock(m);
      }}>
      <View>
        {(Platform.OS === 'ios' ||
          (Platform.OS === 'android' && deviceLocale === 'en')) && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'right' }}>
            {item.name}
          </Text>
        )}
        {Platform.OS === 'android' && deviceLocale === 'ar' && (
          <Text style={{ fontSize: 18, color: 'black', textAlign: 'left' }}>
            {item.name}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );

  // const handleChoosePhoto = () => {
  //   const options = {
  //     noData: true,
  //   };
  //   ImagePicker.launchImageLibrary(options, response => {
  //     if (response.uri) {
  //       setphoto(response)
  //     }
  //   });
  //   ImagePicker.launchCamera(options, (response) => {
  //     if (response.uri) {
  //       setphoto(response)
  //     }
  //       });
  // };
  //   const selectPhotoTapped=() =>  {
  //     const options = {
  //       quality: 1.0,
  //       maxWidth: 500,
  //       maxHeight: 500,
  //       storageOptions: {
  //         skipBackup: true,
  //         path: "itemfiles"
  //       },
  //     };
  //     ImagePicker.showImagePicker(options, (response) => {

  //       if (response.didCancel) {
  //       } else if (response.error) {
  //       } else if (response.customButton) {
  //       } else {
  //         let source = { uri: response.uri };

  //         ImgToBase64.getBase64String(response.uri)
  //         .then(base64String => {
  //           setimagebaseprofile(base64String);
  //         }
  //           )
  //         // .catch(err => {

  //         // });

  // setsrcimage(source);

  //       }
  //     });
  //   }

  const openCamera = (cropit) => {
    ImagePicker.openCamera({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  const openGallary = (cropit) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setimagebaseprofile(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  return (
    <View style={{ height: '100%', width: '100%' }}>
      <View
        style={{
          flexDirection: 'column',
          height: DEVICE_hight,

          justifyContent: 'space-between',
        }}>
             <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ height: '90%', width: '100%' }}>
        <ImageBackground
          style={{
            height: '100%',
            width: '100%',
            flexDirection: 'column',
          }}
          resizeMode='cover'
          source={bgg}>
          <ImageBackground
            style={{ width: '100%', height: clickable ? 200 : 450 }}
            resizeMode='cover'
            source={srcimage}>
            <ImageBackground
              style={{
                width: '100%',
                height: 150,
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
              }}
              resizeMode='stretch'
              source={require('../../imgs/transheader.png')}>
              <View
                style={{
                  width: '100%',
                  marginTop: 70,
                  paddingHorizontal: 30,
                  flexDirection: 'row-reverse',
                  justifyContent: 'space-between',
                }}>
                  <TouchableHighlight   underlayColor='transparent'
                     onPress={() => {  try {
                      drawer.current.open();
                    } catch (ff) {
                      drawerD.current.open();
                    }}}
                activeOpacity={0.2}>
                <Image
                 source={require('../../imgs/menueic.png')}
              style={{width:25,height:20,tintColor:'white'}}
              resizeMode='contain'
                />
                </TouchableHighlight>
                <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',alignItems:'flex-start',height:20}}
              resizeMode='stretch'
                />
                {clickable === false && (
                  <TouchableOpacity
                    onPress={(_) => {
                      setstartclock(
                        from
                      );
                      setendclock(
                      to
                      );

                      if (
                        VendorDetailsReducer.payload.vendor.work_days === null
                      ) {
                        setselectedholidays([]);
                      } else {
                       
                        let d=[];
                        for(let r=0;r<VendorDetailsReducer.payload.vendor.work_days.length;r++)
                        {
                          d.push({'label_name_ar':VendorDetailsReducer.payload.vendor.work_days[r].offdays})
                        }
                        setselectedholidays(d);
                      }

                      setclickable(true);
                    }}>
                    <IconA name='edit' size={25} color='white' />
                  </TouchableOpacity>
                )}

                {clickable === true && (
                  <TouchableOpacity
                    onPress={(_) => {
                      if (
                        name.length > 0 &&
                        phone.length > 0 &&
                        address.length > 0 &&
                        address !== 'NAC' &&
                        phone !== '0'
                      ) {
                        if (selectedholidays.length > 4) {
                          Toast.showWithGravity(
                            'الحد الأقصى للعطلات أربعة أيام  ',
                            Toast.LONG,
                            Toast.BOTTOM
                          );
                        } else {
                          if (selectedlabels.length > 0) {
                            setclickable(false);
                            setselectedd('updateprofile');

                            setspinner(true);
                            let deliverydestination = [];
                            let costdeliverydestination = [];

                            for (let d = 0; d < delivery_address.length; d++) {
                              deliverydestination.push(delivery_address[d].id);
                              costdeliverydestination.push(10);
                            }
                          

                            let s=startclock;
                            let e=endclock;
                            try{
                              
                            if(s.includes('صباحا'))
                             {
                               s=s.replace('صباحا','')
                              }
                             if(s.includes('مساء'))
                             {
                               s=s.replace('مساء','') 
                             }
                            }catch(d)
                            {
                              
                            }
                            try{
                             if(e.includes('صباحا'))
                             {
                               e=e.replace('صباحا','')
                             }
                             if(e.includes('مساء'))
                             {
                               e=e.replace('مساء','')
                             }
                            }catch(d)
                            {
                            }
                           
                            dispatch(
                              EditProfileAction(
                                costdeliverydestination,
                                deliverydestination,
                                name,
                                phone,
                                city,
                                cityid,
                                subcityid,
                                address,
                                imagebaseprofile,
                                selectedholidays,
                                selectedlabels,
                                token,
                                s + start,
                                e + end,
                                categoryid
                              )
                            );
                          } else {
                            Toast.showWithGravity(
                              'تحتاج إلى تسجيل فئة واحدة على الأقل ',
                              Toast.LONG,
                              Toast.BOTTOM
                            );
                          }
                        }
                      } else {
                        Toast.showWithGravity(
                          'تحتاج إلى إضافة الحقول المفقودة',
                          Toast.LONG,
                          Toast.BOTTOM
                        );
                      }
                    }}>
                    <Icond name='save' size={25} color='white' />
                  </TouchableOpacity>
                )}
              </View>
            </ImageBackground>
          </ImageBackground>

          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{ marginTop: 10 }}>
            {clickable === true &&
              typeof VendorDetailsReducer !== 'undefined' &&
              typeof VendorDetailsReducer.payload !== 'undefined' && (
                <View style={{ height: '100%', flexDirection: 'column' }}>
                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View
                      style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row-reverse',
                      }}>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: 18,
                          fontWeight: 'bold',
                          marginBottom: 10,
                          marginTop: 5,
                          marginRight: 10,
                          color: 'white',
                        }}>
                        المعلومات الشخصية
                      </Text>
                    </View>
                  )}

                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View
                      style={{
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: 18,
                          textAlign: 'center',
                          fontWeight: 'bold',
                          marginBottom: 10,
                          marginTop: 5,
                          marginRight: 10,

                          color: 'white',
                        }}>
                        المعلومات الشخصية
                      </Text>
                    </View>
                  )}
                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View
                      style={{
                        backgroundColor: '',
                        width: '100%',
                        justifyContent: 'flex-start',
                        flexDirection: 'row-reverse',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                      }}>
                      <View style={styles.inputWrapper}>
                        <TextInput
                          style={styles.input}
                          placeholder={' اسم المحل'}
                          autoCapitalize={'none'}
                          returnKeyType={'next'}
                          autoCorrect={false}
                          onChangeText={(name) => setname(name)}
                          value={name}
                          secureTextEntry={false}
                          placeholderTextcolor='#039df0'
                          color='#039df0'
                          underlineColorAndroid='transparent'
                        />
                      </View>

                      <View style={styles.inputtt}>
                        <TextInput
                          placeholder={'رقم الهاتف '}
                          autoCorrect={false}
                          onChangeText={(phone) => setphone(phone)}
                          value={phone}
                          keyboardType={'numeric'}
                          placeholderTextcolor='#039df0'
                          color='#039df0'
                          style={{
                            color: '#039df0',
                            width: '100%',
                            textAlign: 'right',
                          }}
                          underlineColorAndroid='transparent'
                        />
                      </View>
                    </View>
                  )}
                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View
                      style={{
                        backgroundColor: '',
                        width: '100%',
                        justifyContent: 'flex-start',
                        flexDirection: 'row',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                      }}>
                      <View style={styles.inputWrapper}>
                        <TextInput
                          style={styles.input}
                          placeholder={' اسم المحل'}
                          autoCapitalize={'none'}
                          returnKeyType={'next'}
                          autoCorrect={false}
                          onChangeText={(name) => setname(name)}
                          value={name}
                          secureTextEntry={false}
                          placeholderTextcolor='#039df0'
                          color='#039df0'
                          underlineColorAndroid='transparent'
                        />
                      </View>

                      <View style={styles.inputttar}>
                        <TextInput
                          placeholder={'رقم الهاتف '}
                          autoCorrect={false}
                          onChangeText={(phone) => setphone(phone)}
                          value={phone}
                          keyboardType={'numeric'}
                          placeholderTextcolor='#039df0'
                          color='#039df0'
                          style={{
                            color: '#039df0',
                            width: '100%',
                            textAlign: 'right',
                          }}
                          underlineColorAndroid='transparent'
                        />
                      </View>
                    </View>
                  )}
                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View style={styles.inputWrapper}>
                      <TextInput
                        style={styles.inputaddress}
                        placeholder={'العنوان'}
                        multiline={true}
                        autoCapitalize={'none'}
                        returnKeyType={'next'}
                        autoCorrect={false}
                        onChangeText={(address) => setaddress(address)}
                        value={address}
                        secureTextEntry={false}
                        placeholderTextcolor='#039df0'
                        color='#039df0'
                        underlineColorAndroid='transparent'
                      />
                    </View>
                  )}
                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View style={styles.inputWrapper}>
                      <TextInput
                        style={styles.inputaddress}
                        placeholder={'العنوان'}
                        multiline={true}
                        autoCapitalize={'none'}
                        returnKeyType={'next'}
                        autoCorrect={false}
                        onChangeText={(address) => setaddress(address)}
                        value={address}
                        secureTextEntry={false}
                        placeholderTextcolor='#039df0'
                        color='#039df0'
                        underlineColorAndroid='transparent'
                      />
                    </View>
                  )}
                  {tags.length > 0 && (
                    <TagsViewSelectionClickablelogin
                      all={tags}
                      langugae={deviceLocale}
                      type={'categories'}
                      first={labelfirst}
                      onc={this}
                      selected={selectedlabels}
                      isExclusive={false}
                    />
                  )}

                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View
                      style={{
                        width: DEVICE_width - 60,
                        flexDirection: 'row-reverse',
                        height: 40,
                        marginBottom: 10,
                        marginLeft: 30,
                        marginRight: 30,
                        marginTop: 10,
                        justifyContent: 'space-between',
                      }}>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          setswipeablePanelstarthour(true);
                        }}>
                        <View
                          style={{
                            backgroundColor: '#ffffff',
                            width: (DEVICE_width - 70) / 2,

                            paddingRight: 20,
                            paddingLeft: 20,
                            paddingRight: 20,
                            height: 40,

                            textAlign: 'right',
                            borderWidth: 1,
                            borderRadius: 30,
                            justifyContent: 'space-between',
                            flexDirection: 'row-reverse',
                            borderColor: '#039df0',
                            color: '#039df0',
                            alignItems: 'center',
                          }}>
                          {startclocktext !== '' && (
                            <Text
                              style={{
                                color: '#039df0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: 15,
                              }}>
                              {startclocktext} {start}
                            </Text>
                          )}

                          {startclocktext === '' && (
                            <Text
                              style={{
                                color: '#039df0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: 17,
                              }}>
                              ساعة بدء العمل
                            </Text>
                          )}
                          <Icond
                            style={{
                              alignSelf: 'center',
                              alignItems: 'center',
                              alignContent: 'center',
                              marginLeft: 10,
                            }}
                            name='chevron-down'
                            size={17}
                            color='#0599ea'
                          />
                        </View>
                      </TouchableWithoutFeedback>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          setswipeablePanelendhour(true);
                          // showMode('time')
                        }}>
                        <View
                          style={{
                            backgroundColor: '#ffffff',
                            width: (DEVICE_width - 70) / 2,

                            paddingRight: 20,
                            paddingLeft: 20,
                            paddingRight: 20,
                            height: 40,

                            textAlign: 'right',
                            borderWidth: 1,
                            borderRadius: 30,
                            justifyContent: 'space-between',
                            flexDirection: 'row-reverse',
                            borderColor: '#039df0',
                            color: '#039df0',
                            alignItems: 'center',
                          }}>
                          {endclocktext !== '' && (
                            <Text
                              style={{
                                color: '#039df0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: 15,
                              }}>
                              {endclocktext} {end}
                            </Text>
                          )}

                          {endclocktext === '' && (
                            <Text
                              style={{
                                color: '#039df0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: 17,
                              }}>
                              {' '}
                              ساعة نهاية العمل
                            </Text>
                          )}
                          <Icond
                            style={{
                              alignSelf: 'center',
                              alignItems: 'center',
                              alignContent: 'center',
                              marginLeft: 10,
                            }}
                            name='chevron-down'
                            size={17}
                            color='#0599ea'
                          />
                        </View>
                      </TouchableWithoutFeedback>
                    </View>
                  )}
                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View
                      style={{
                        width: DEVICE_width - 60,
                        flexDirection: 'row',
                        height: 40,
                        marginBottom: 10,
                        marginLeft: 30,
                        marginRight: 30,
                        marginTop: 10,
                        justifyContent: 'space-between',
                      }}>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          setswipeablePanelstarthour(true);
                        }}>
                        <View
                          style={{
                            backgroundColor: '#ffffff',
                            width: (DEVICE_width - 70) / 2,

                            paddingRight: 20,
                            paddingLeft: 20,
                            paddingRight: 20,
                            height: 40,

                            textAlign: 'left',
                            borderWidth: 1,
                            borderRadius: 30,
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            borderColor: '#039df0',
                            color: '#039df0',
                            alignItems: 'center',
                          }}>
                          {startclock !== '' && (
                            <Text
                              style={{
                                color: '#039df0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: 15,
                              }}>
                              {startclocktext} {start}
                            </Text>
                          )}

                          {startclock === '' && (
                            <Text
                              style={{
                                color: '#039df0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: 17,
                              }}>
                              ساعة بدء العمل
                            </Text>
                          )}
                          <Icond
                            style={{
                              alignSelf: 'center',
                              alignItems: 'center',
                              alignContent: 'center',
                              marginLeft: 10,
                            }}
                            name='chevron-down'
                            size={17}
                            color='#0599ea'
                          />
                        </View>
                      </TouchableWithoutFeedback>
                      <TouchableWithoutFeedback
                        onPress={() => {
                          setswipeablePanelendhour(true);
                          // showMode('time')
                        }}>
                        <View
                          style={{
                            backgroundColor: '#ffffff',
                            width: (DEVICE_width - 70) / 2,

                            paddingRight: 20,
                            paddingLeft: 20,
                            paddingRight: 20,
                            height: 40,

                            textAlign: 'right',
                            borderWidth: 1,
                            borderRadius: 30,
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            borderColor: '#039df0',
                            color: '#039df0',
                            alignItems: 'center',
                          }}>
                          {endclock !== '' && (
                            <Text
                              style={{
                                color: '#039df0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: 15,
                              }}>
                              {endclocktext} {end}
                            </Text>
                          )}

                          {endclock === '' && (
                            <Text
                              style={{
                                color: '#039df0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: 17,
                              }}>
                              {' '}
                              ساعة نهاية العمل
                            </Text>
                          )}
                          <Icond
                            style={{
                              alignSelf: 'center',
                              alignItems: 'center',
                              alignContent: 'center',
                              marginLeft: 10,
                            }}
                            name='chevron-down'
                            size={17}
                            color='#0599ea'
                          />
                        </View>
                      </TouchableWithoutFeedback>
                    </View>
                  )}
       
                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View
                      style={{
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        flexDirection: 'row-reverse',
                      }}>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontSize: 17,
                          color: 'white',
                        }}>
                        حدد أيام العطل
                      </Text>
                    </View>
                  )}

                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View
                      style={{
                        marginLeft: 30,
                        marginRight: 30,
                        flexDirection: 'row',
                      }}>
                      <Text
                        style={{
                          textAlign: 'right',
                          fontSize: 17,
                          color: 'white',
                        }}>
                        حدد أيام العطل
                      </Text>
                    </View>
                  )}
                  <TagsViewSelectionClickablelogin
                    all={holidays}
                    type={'holiday'}
                    langugae={deviceLocale}
                    onc={this}
                    first={holidayfirst}
                    selected={selectedholidays}
                    isExclusive={false}
                  />

                  <View style={{ height: 200 }} />
                </View>
              )}
            {clickable === false &&
              typeof VendorDetailsReducer !== 'undefined' &&
              typeof VendorDetailsReducer.payload !== 'undefined' && (
                <View style={{ flexDirection: 'column' }}>
                  {(Platform.OS === 'ios' ||
                    (Platform.OS === 'android' && deviceLocale === 'en')) && (
                    <View
                      style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row-reverse',
                      }}>
                      <Text
                        style={{
                          color: 'white',
                          fontSize: 18,
                          fontWeight: 'bold',

                          marginBottom: 10,
                          marginTop: 5,
                          marginRight: 10,
                        }}>
                        المعلومات الشخصية
                      </Text>
                    </View>
                  )}

                  {Platform.OS === 'android' && deviceLocale === 'ar' && (
                    <View
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          color: 'white',
                          fontSize: 18,
                          fontWeight: 'bold',
                          marginBottom: 10,
                          textAlign: 'center',
                          marginTop: 5,
                          marginRight: 10,
                        }}>
                        المعلومات الشخصية
                      </Text>
                    </View>
                  )}

                  <View
                    style={{
                      margin: 5,
                      borderRadius: 10,
                      flexDirection: 'column',
                    }}>
                    {(Platform.OS === 'ios' ||
                      (Platform.OS === 'android' && deviceLocale === 'en')) && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          justifyContent: 'space-between',
                          marginBottom: 5,

                          flexDirection: 'row-reverse',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 17, color: 'white' }}>
                          اسم المحل
                        </Text>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {VendorDetailsReducer.payload.vendor.name}
                        </Text>
                      </View>
                    )}
                    {Platform.OS === 'android' && deviceLocale === 'ar' && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          justifyContent: 'space-between',
                          marginBottom: 5,
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 17, color: 'white' }}>
                          اسم المحل
                        </Text>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {VendorDetailsReducer.payload.vendor.name}
                        </Text>
                      </View>
                    )}
                    <View
                      style={{
                        height: 0.5,
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        backgroundColor: '#095c92',
                      }}
                    />
                    {(Platform.OS === 'ios' ||
                      (Platform.OS === 'android' && deviceLocale === 'en')) && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          marginBottom: 5,
                          justifyContent: 'space-between',
                          flexDirection: 'row-reverse',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 17, color: 'white' }}>
                          البريد الإلكتروني{' '}
                        </Text>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {VendorDetailsReducer.payload.vendor.email}
                        </Text>
                      </View>
                    )}

                    {Platform.OS === 'android' && deviceLocale === 'ar' && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          marginBottom: 5,
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 17, color: 'white' }}>
                          البريد الإلكتروني{' '}
                        </Text>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {VendorDetailsReducer.payload.vendor.email}
                        </Text>
                      </View>
                    )}
                    <View
                      style={{
                        height: 0.5,
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        backgroundColor: '#095c92',
                      }}
                    />
                    {(Platform.OS === 'ios' ||
                      (Platform.OS === 'android' && deviceLocale === 'en')) && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          marginBottom: 5,
                          flexDirection: 'row-reverse',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{ fontSize: 20, color: 'white' }}>
                          رقم الهاتف
                        </Text>

                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {VendorDetailsReducer.payload.vendor.phone[0].phone}{' '}
                        </Text>
                      </View>
                    )}

                    {Platform.OS === 'android' && deviceLocale === 'ar' && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          marginBottom: 5,
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          رقم الهاتف
                        </Text>

                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {
                            VendorDetailsReducer.payload.vendor.phone[0].phone
                          }{' '}
                        </Text>
                      </View>
                    )}
                    <View
                      style={{
                        height: 0.5,
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        backgroundColor: '#095c92',
                      }}
                    />
                    {(Platform.OS === 'ios' ||
                      (Platform.OS === 'android' && deviceLocale === 'en')) && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          flexDirection: 'row-reverse',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {
                            VendorDetailsReducer.payload.vendor.address[1]
                              .address_name
                          }
                        </Text>
                      </View>
                    )}

                    {Platform.OS === 'android' && deviceLocale === 'ar' && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {
                            VendorDetailsReducer.payload.vendor.address[1]
                              .address_name
                          }
                        </Text>
                      </View>
                    )}
                    <View
                      style={{
                        height: 0.5,
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        backgroundColor: '#095c92',
                      }}
                    />
                    {(Platform.OS === 'ios' ||
                      (Platform.OS === 'android' && deviceLocale === 'en')) && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginBottom: 5,
                          flexDirection: 'row-reverse',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {
                            VendorDetailsReducer.payload.vendor.address[0]
                              .address_name
                          }
                        </Text>
                      </View>
                    )}

                    {Platform.OS === 'android' && deviceLocale === 'ar' && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginBottom: 5,
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {
                            VendorDetailsReducer.payload.vendor.address[0]
                              .address_name
                          }
                        </Text>
                      </View>
                    )}
                    <View
                      style={{
                        height: 0.5,
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        backgroundColor: '#095c92',
                      }}
                    />
                    {(Platform.OS === 'ios' ||
                      (Platform.OS === 'android' && deviceLocale === 'en')) && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          marginBottom: 5,
                          justifyContent: 'space-between',
                          flexDirection: 'row-reverse',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 17, color: 'white' }}>
                          ساعات العمل
                        </Text>

                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {openingtime}
                        </Text>
                      </View>
                    )}

                    {Platform.OS === 'android' && deviceLocale === 'ar' && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          marginBottom: 5,
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={{ fontSize: 17, color: 'white' }}>
                          ساعات العمل
                        </Text>

                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {openingtime}
                        </Text>
                      </View>
                    )}
                    <View
                      style={{
                        height: 0.5,
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        backgroundColor: '#095c92',
                      }}
                    />
                    {(Platform.OS === 'ios' ||
                      (Platform.OS === 'android' && deviceLocale === 'en')) && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          marginBottom: 5,
                          justifyContent: 'space-between',
                          flexDirection: 'row-reverse',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          أيام العطل
                        </Text>

                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {offdays}
                        </Text>
                      </View>
                    )}

                    {Platform.OS === 'android' && deviceLocale === 'ar' && (
                      <View
                        style={{
                          width: DEVICE_width - 60,
                          marginRight: 30,
                          marginLeft: 30,
                          marginTop: 5,
                          marginBottom: 5,
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          أيام العطل
                        </Text>

                        <Text style={{ fontSize: 15, color: 'white' }}>
                          {' '}
                          {offdays}
                        </Text>
                      </View>
                    )}
                    <View
                      style={{
                        height: 0.5,
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                        backgroundColor: '#095c92',
                      }}
                    />

                    <View
                      style={{
                        flexDirection: 'row-reverse',
                        width: DEVICE_width - 60,
                        marginLeft: 30,
                        marginRight: 30,
                      }}>
                      <TagsVieww
                        language={deviceLocale}
                        all={vendorlabels}
                        selected={[]}
                        isExclusive={true}
                      />
                    </View>
                  </View>
                </View>
              )}

            <View style={{ height: 100 }}></View>
          </ScrollView>
        </ImageBackground>
   </KeyboardAwareScrollView>
      </View>

      <SwipeablePanelm
        isActive={swipeablePanelstarthour}
        closeOnTouchOutside={true}
        onClose={this}
        openLarge>
            <LinearGradient
                  colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                  style={{
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    borderRadius: 22,
                    width:AppMetrics.screenWidth,
                    height:AppMetrics.screenHeight
                  }}>
      
          <View>
            <View style={{  padding: 20 }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '400',
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  color: 'white',
                }}>
                {' '}
                حدد ساعة بدء العمل{' '}
              </Text>
            </View>
            <View style={{ width: '100%', flexDirection: 'row-reverse' ,backgroundColor:'rgba(52, 52, 52, 0.0)'}}>
              <TouchableOpacity
                activeOpacity={0.9}
                style={{ width: '50%', height: 50 }}
                onPress={(_) => {
                  setstart('صباحا');
                  setamcolors('white');
                  setamsizes(20);
                  setamweights('bold');
                  setambacks('rgba(52, 52, 52, 0.1)');
                  setpmcolors('grey');
                  setpmsizes(17);
                  setpmweights('normal');
                  setpmbacks('rgba(52, 52, 52, 0.1)');
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: amcolors,
                    fontSize: amsizes,
                    fontWeight: amweights,
                
                    padding: 20,
                    color: 'white',
                  }}>
                  صباحا
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ width: '50%', height: 50 }}
                onPress={(_) => {
                  setstart('مساء');
                  setpmcolors('white');
                  setpmsizes(20);
                  setpmweights('bold');
                  setamcolors('grey');
                  setamsizes(17);
                  setamweights('normal');
                  setpmbacks('#0599ea');
                  setambacks('white');
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: pmcolors,
                    fontSize: pmsizes,
                    fontWeight: pmweights,
                 
                    padding: 20,
                    color: 'white',
                  }}>
                  مساء
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: '100%',
                height: 10,
                borderColor: '#dfdfdf',
                flexDirection: 'column',
              }}></View>
            <SelectableGrid
              data={[
                { id: 1, name: '2:00' },
                { id: 2, name: '1:00' },
                { id: 3, name: '4:00' },
                { id: 4, name: '3:00' },
                { id: 5, name: '6:00' },
                { id: 6, name: '5:00' },
                { id: 7, name: '8:00' },
                { id: 8, name: '7:00' },
                { id: 9, name: '10:00' },
                { id: 10, name: '9:00' },
                { id: 11, name: '12:00' },
                { id: 12, name: '11:00' },
              ]}
              state={STATE.EDIT}
              multiSelect={false}
              checkcolor='blue'
              uncheckcolor='white'
              itemsSelected={(selectedItem) => {
                (_) => itemsSelectedstart(selectedItem);
              }}
              initialSelectedIndex={[startclockid - 1]}
              cellItemComponent={(item, otherProps) => rowItemstart(item)}
            />
            <View style={{ height: 20 ,backgroundColor:'rgba(52, 52, 52, 0.0)'}}></View>
          </View>
     
        </LinearGradient>
      </SwipeablePanelm>

      <SwipeablePanelm
        isActive={swipeablePanelendhour}
        closeOnTouchOutside={true}
        onClose={this}
        openLarge>
          <LinearGradient
                  colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                  style={{
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center',
                    borderRadius: 22,
                    width:AppMetrics.screenWidth,
                    height:AppMetrics.screenHeight
                  }}>
       
          <View >
            <View style={{  padding: 20,backgroundColor:'rgba(52, 52, 52, 0.0)' }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '400',
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  color: 'white',
                  backgroundColor:'rgba(52, 52, 52, 0.0)'
                }}>
                {' '}
                حدد ساعة نهاية العمل{' '}
              </Text>
            </View>
            <View style={{ width: '100%', flexDirection: 'row-reverse' }}>
              <TouchableOpacity
                style={{ width: '50%', height: 50 }}
                onPress={(_) => {
                  setend('صباحا');
                  setamcolor('white');
                  setamsize(20);
                  setamweight('bold');
                  setamback('#0599ea');
                  setpmcolor('grey');
                  setpmsize(17);
                  setpmweight('normal');
                  setpmback('white');
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: amcolor,
                    fontSize: amsize,
                    fontWeight: amweight,
                    padding: 20,
                    color: 'white',
                  }}>
                  صباحا
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ width: '50%', height: 50 }}
                onPress={(_) => {
                  setend('مساء');
                  setpmcolor('white');
                  setpmsize(20);
                  setpmweight('bold');
                  setamcolor('grey');
                  setamsize(17);
                  setamweight('normal');
                  setpmback('#0599ea');
                  setamback('white');
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: pmcolor,
                    fontSize: pmsize,
                    fontWeight: pmweight,
                    color: 'white',
                    padding: 20,
                  }}>
                  مساء
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: '100%',
                height: 10,
                flexDirection: 'column',
              }}></View>
            <SelectableGrid
              data={[
                { id: 1, name: '2:00' },
                { id: 2, name: '1:00' },
                { id: 3, name: '4:00' },
                { id: 4, name: '3:00' },
                { id: 5, name: '6:00' },
                { id: 6, name: '5:00' },
                { id: 7, name: '8:00' },
                { id: 8, name: '7:00' },
                { id: 9, name: '10:00' },
                { id: 10, name: '9:00' },
                { id: 11, name: '12:00' },
                { id: 12, name: '11:00' },
              ]}
              state={STATE.EDIT}
              multiSelect={false}
              checkcolor='white'
              uncheckcolor='white'
              itemsSelected={(selectedItem) => {
                (_) => itemsSelectedend(selectedItem);
              }}
              initialSelectedIndex={[endclockid - 1]}
              cellItemComponent={(item, otherProps) => rowItemend(item)}
            />
            <View style={{ height: 20,backgroundColor:'rgba(52, 52, 52, 0.0)' }}></View>
          </View>
      
        </LinearGradient>
      </SwipeablePanelm>

      <Spinner
        visible={spinner}
        textContent={''}
        textStyle={styles.spinnerTextStyle}
      />


      <ActionSheet
        ref={actionSheetRef}
        containerStyle={{
          backgroundColor: 'transparent',
        }}
        elevation={0}>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            margin: 20,
            borderRadius: 10,
          }}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openCamera(true);
              }, 1000);
            }}>
            <Text>الكاميرا</Text>
          </TouchableOpacity>
          <View
            style={{
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: '#707070',
            }}
          />
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openGallary(true);
              }, 1000);
            }}>
            <Text>المعرض</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            marginHorizontal: 20,
            borderRadius: 10,
            marginBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();
            }}>
            <Text style={[{ color: 'red' }]}>إلغاء</Text>
          </TouchableOpacity>
        </View>
      </ActionSheet>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  image: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  text: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 5,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 30,
    marginBottom: 20,
  },
  btnEye: {
    position: 'absolute',
    top: 10,
    left: 40,
  },
  iconEye: {
    width: 25,
    height: 25,
  },
  btnEyeRight: {
    position: 'absolute',
    top: 10,
    right: 40,
  },
  iconEyeRight: {
    width: 25,
    height: 25,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 5,
  },

  restoreButtonContainer: {
    width: 250,
    marginBottom: 15,
    alignItems: 'flex-end',
  },
  socialButtonContent: {
    marginRight: 20,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    color: '#FFFFFF',
    marginRight: 5,
  },
  socialLoginButton: {
    backgroundColor: '#929292',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 10,
    borderRadius: 5,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginText: {
    color: '#FFFFFF',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
  buttonText: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    paddingHorizontal: 20,
  },
  buttonStyle: {
    borderRadius: 5,
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  input: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 30,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },
  inputaddress: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 70,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 30,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },

  inputtt: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 40,
    flexDirection: 'row-reverse',
    marginBottom: 20,
    borderRadius: 30,
    alignItems: 'center',
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    marginTop: 10,

    borderColor: '#039df0',
    color: '#039df0',
  },
  inputttar: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 40,
    flexDirection: 'row',
    marginBottom: 20,
    borderRadius: 30,
    alignItems: 'center',
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'left',
    borderWidth: 1,

    borderColor: '#039df0',
    color: '#039df0',
  },
  inputDialog: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 100,
    height: 40,
    marginTop: 15,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 30,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },
  inputRight: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    textAlign: 'left',
    borderWidth: 1,
    borderRadius: 30,
    flexDirection: 'row',
    borderColor: '#039df0',
    color: '#039df0',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',
    marginBottom: 5,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperr: {
    flexDirection: 'row-reverse',
    width: DEVICE_width / 2,
    marginBottom: 5,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  cover: {
    height: '90%',
  },
  sheet: {
    position: 'absolute',
    top: Dimensions.get('window').height,
    left: 0,
    right: 0,
    height: '100%',
    justifyContent: 'flex-end',
  },
  popup: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 80,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});
