import { StyleSheet, Dimensions } from 'react-native';

const screenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  defaultTouch: {
    width: (screenWidth/2)-4,
    flexDirection: 'row',
    marginHorizontal:2,
    backgroundColor: 'white'
    ,marginBottom:3
  },
  defaultContainer: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
});


export default styles;
