import React from 'react';
import {Icon} from 'react-native-elements';

const ChevronAr = () => (
  <Icon
    name="chevron-left"
    type="entypo"
    color="#3888C6"
    containerStyle={{marginRight: -15, width: 20}}
  />
);

export default ChevronAr;
