/*
 * platform/application wide metrics for proper styling
 */
import { Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get( 'window' );

const AppMetrics = {
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  navBarHeight: Platform.OS === 'ios' ? 54 : 66,
  innerNavBarHeihgt: Platform.OS === 'ios' ? 74 : 86,
};

export default AppMetrics;
