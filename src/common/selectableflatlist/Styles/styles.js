import { StyleSheet, Dimensions } from 'react-native';

const screenWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  defaultTouch: {
    width: screenWidth,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF'
  },
  defaultContainer: {
    height: 30,
    width: 30,
    marginLeft:30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
});


export default styles;
