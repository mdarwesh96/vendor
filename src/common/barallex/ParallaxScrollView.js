import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Text,
  View,
  Image,
  Animated,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import { Icon, List, ListItem } from 'react-native-elements';
import Iconn from 'react-native-vector-icons/Ionicons'

import { USER, FACEBOOK_LIST, SLACK_LIST, GENERIC_LIST, SCREEN_WIDTH, SCREEN_HEIGHT, DEFAULT_WINDOW_MULTIPLIER, DEFAULT_NAVBAR_HEIGHT } from './constants';

import styles from './styles';

const ScrollViewPropTypes = ScrollView.propTypes;

export default class ParallaxScrollView extends Component {
  constructor() {
    super();

    this.state = {
      scrollY: new Animated.Value(0)
    };
  }

  scrollTo(where) {
    if (!this._scrollView) return;
    this._scrollView.scrollTo(where);
  }
  
  renderBackground() {
    var { windowHeight, backgroundSource, onBackgroundLoadEnd, onBackgroundLoadError } = this.props;
    var { scrollY } = this.state;
    if (!windowHeight || !backgroundSource) {
      return null;
    }

    return (
      <Animated.Image
        style={[
          styles.background,
          {
            height: windowHeight,
            transform: [
              {
                translateY: scrollY.interpolate({
                  inputRange: [-windowHeight, 0, windowHeight],
                  outputRange: [windowHeight / 2, 0, -windowHeight / 3]
                })
              },
              {
                scale: scrollY.interpolate({
                  inputRange: [-windowHeight, 0, windowHeight],
                  outputRange: [2, 1, 1]
                })
              }
            ]
          }
        ]}
        source={backgroundSource}
resizeMode='contain'
        onLoadEnd={onBackgroundLoadEnd}
        onError={onBackgroundLoadError}
      >
      </Animated.Image>
    );
  }

  renderHeaderView() {
    const { windowHeight, backgroundSource, userImage, userName, userTitle, navBarHeight } = this.props;
    const { scrollY } = this.state;
    if (!windowHeight || !backgroundSource) {
      return null;
    }

    const newNavBarHeight = navBarHeight || DEFAULT_NAVBAR_HEIGHT;    
    const newWindowHeight = windowHeight - newNavBarHeight;

    return (
      <Animated.View
        style={{
          opacity: scrollY.interpolate({
            inputRange: [-windowHeight, 0, windowHeight * DEFAULT_WINDOW_MULTIPLIER + newNavBarHeight],
            outputRange: [1, 1, 0]
          })
        }}
      >
        <View style={{height: newWindowHeight, justifyContent: 'center', alignItems: 'center'}}>
          {this.props.headerView ||
            (
              <View>
                <View
                  style={styles.avatarView}
                >
                  <Image source={{uri: userImage || USER.image}} style={{height: 120, width: 120, borderRadius: 60}} />
                </View>
                <View style={{paddingVertical: 10}}>
                  <Text style={{textAlign: 'center', fontSize: 22, color: 'white', paddingBottom: 5}}>{userName || USER.name}</Text>
                  <Text style={{textAlign: 'center', fontSize: 17, color: '#3888C6', paddingBottom: 5}}>{userTitle || USER.title}</Text>
                </View>
              </View>
            )
          }
        </View>
      </Animated.View>
    );
  }

  renderNavBarTitle() {
    const { windowHeight, backgroundSource, navBarTitleColor, navBarTitleComponent } = this.props;
    const { scrollY } = this.state;
    if (!windowHeight || !backgroundSource) {
      return null;
    }

    return (
      <Animated.View
        style={{
          opacity: scrollY.interpolate({
            inputRange: [-windowHeight, windowHeight * DEFAULT_WINDOW_MULTIPLIER, windowHeight * 0.8],
            outputRange: [0, 0, 1]
          })
        }}
      >
        {navBarTitleComponent ||
        <Text style={{ fontSize: 18, fontWeight: '600', color: navBarTitleColor || 'white' }}>
         sdsdsdsd,,lsfl;msdlfmldsml;
        </Text>}
      </Animated.View>
    );
  }

  rendernavBar() {
    const {
      windowHeight, backgroundSource, leftIcon,
      rightIcon, leftIconOnPress, rightIconOnPress, navBarColor, navBarHeight, leftIconUnderlayColor, rightIconUnderlayColor
    } = this.props;

    const { scrollY } = this.state;
    if (!windowHeight || !backgroundSource) {
      return null;
    }

    const newNavBarHeight = navBarHeight || DEFAULT_NAVBAR_HEIGHT;

    if(this.props.navBarView)
    {
        return (
          <Animated.View
            style={{
              height: newNavBarHeight,
              width: SCREEN_WIDTH,
              flexDirection: 'row',
              backgroundColor: scrollY.interpolate({
                inputRange: [-windowHeight, windowHeight * DEFAULT_WINDOW_MULTIPLIER, windowHeight * 0.8],
                outputRange: ['transparent', 'transparent', navBarColor || 'rgba(0, 0, 0, 1.0)'],
                extrapolate: 'clamp'
              })
            }}
          >
          {this.props.navBarView}
          </Animated.View>
        );                
    }
    else
    {
        return (
          <Animated.View
            style={{
              height: newNavBarHeight, 
              width: SCREEN_WIDTH,
              flexDirection: 'row',
              justifyContent:'space-between',
              backgroundColor: scrollY.interpolate({
                inputRange: [-windowHeight, windowHeight * DEFAULT_WINDOW_MULTIPLIER, windowHeight * 0.8],
                outputRange: ['transparent', 'transparent', navBarColor || 'rgba(0, 0, 0, 1.0)']
              })
            }}
          >
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
                        <TouchableOpacity
                         activeOpacity={.9}  onPress={leftIconOnPress} activeOpacity={1} style={{padding:15}}>

              <Iconn

name="arrow-back"
size={25}
color='#3888C6'

onPress={() => this.props.navigation.goBack(null)}/>
</TouchableOpacity>
            </View>
       <View
              style={{
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
                                    <TouchableOpacity  activeOpacity={.9}  onPress={rightIconOnPress} activeOpacity={1}>

               {/* <Image
                name={rightIcon && rightIcon.name || 'menu'}
                resizeMode='contain'
style={{height:30,width:30,marginLeft:10,marginRight:10,tintColor:"#ffffff"}}
                source={require('../../res/share.png')}  
              /> */}
              </TouchableOpacity>
            </View>
          
          </Animated.View>
        );        
    }
  }

  // renderTodoListContent() {
  //   return (
  //     <View style={styles.listView}>
  //       <List>
  //       {
  //         FACEBOOK_LIST.map((item, index) => (
  //           <ListItem
  //             key={index}
  //             onPress={() => console.log('List item pressed')}
  //             title={item.title}
  //             leftIcon={{name: item.icon}} />
  //         ))
  //       }
  //       </List>
  //       <List>
  //       {
  //         SLACK_LIST.map((item, index) => (
  //           <ListItem
  //             key={index}
  //             onPress={() => console.log('List item pressed')}
  //             title={item.title}
  //             leftIcon={{name: item.icon}} />
  //         ))
  //       }
  //       </List>
  //       <List>
  //       {
  //         GENERIC_LIST.map((item, index) => (
  //           <ListItem
  //             key={index}
  //             onPress={() => console.log('List item pressed')}
  //             title={item.title}
  //             leftIcon={{name: item.icon}} />
  //         ))
  //       }
  //       </List>
  //       <List containerStyle={{marginBottom: 15}}>
  //         <ListItem
  //           key={1}
  //           hideChevron={true}
  //           onPress={() => console.log('Logout Pressed')}
  //           title='LOGOUT'
  //           titleStyle={styles.logoutText}
  //           icon={{name: ''}} />
  //       </List>
  //     </View>
  //   );
  // }

  render() {
    const { style, ...props } = this.props;

    return (
      <View style={[styles.container, style]}>
        {this.renderBackground()}
        {this.rendernavBar()}
        <ScrollView
        showsVerticalScrollIndicator={false}
          ref={component => {
            this._scrollView = component;
          }}
          {...props}
          style={styles.scrollView}
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { y: this.state.scrollY } } }
          ])}
          scrollEventThrottle={16}
        >
          {this.renderHeaderView()}
          <View style={[styles.content, props.scrollableViewStyle]}>
            {this.props.children || this.renderTodoListContent()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

ParallaxScrollView.defaultProps = {

  backgroundSource: {uri: 'https://firebasestorage.googleapis.com/v0/b/aboaser.appspot.com/o/photos%2Fdefaultt.png?alt=media&token=9d9fd5f8-e786-42a4-9190-054027582b02'},
  windowHeight: SCREEN_HEIGHT * DEFAULT_WINDOW_MULTIPLIER,
//   leftIconOnPress: () => console.log('Left icon pressed'),
//   rightIconOnPress: () => 
//   {

//     console.log('Right icon pressed')
// }
};

ParallaxScrollView.propTypes = {
  ...ScrollViewPropTypes,
  backgroundSource: PropTypes.object,
  windowHeight: PropTypes.number,
  navBarTitle: PropTypes.string,
  navBarTitleColor: PropTypes.string,
  navBarTitleComponent: PropTypes.node,
  navBarColor: PropTypes.string,
  userImage: PropTypes.string,
  userName: PropTypes.string,
  userTitle: PropTypes.string,
  headerView: PropTypes.node,
  leftIcon: PropTypes.object,
  rightIcon: PropTypes.object
};