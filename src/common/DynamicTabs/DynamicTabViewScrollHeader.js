import React from 'react';
import {
  Button,
  TouchableHighlight,
  Text,
  FlatList,
  View,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';

class DynamicTabViewScrollHeader extends React.Component {
  constructor(props) {
    super(props);
    this.defaultStyle = DynamicdefaultStyle;
    this.state = {
      selected: this.props.selectedTab,
    };
  }

  _onPressHeader = (index) => {
    this.props.goToPage(index);
  };
  rowItem = (item) => (
    <View
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 20,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}>
      <Text style={{ fontSize: 12 }}>{item.test}</Text>
    </View>
  );
  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedTab !== this.props.selectedTab) {
      this.setState({ selected: nextProps.selectedTab });
    }
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: '100%',
          width: 0.5,
          marginTop: 8,
          marginBottom: 8,
          backgroundColor: 'grey',
        }}
      />
    );
  };
  itemsSelected = (selectedItem) => {};
  _renderTitle = ({ item, index }) => {
    let isTabActive = index === this.state.selected;
    let fontWeight = isTabActive ? 'bold' : 'normal';

    return (
      <TouchableHighlight
        underlayColor='transparent'
        activeOpacity={0.2}
        onPress={this._onPressHeader.bind(this, index)}
        style={[
          this.defaultStyle.tabItemContainer,
          { backgroundColor: this.props.headerBackgroundColor },
        ]}
        underlayColor={'#00000033'}>
        <View>
          <View
            style={{
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
            }}>
            <Text
              style={[
                {
                  fontWeight: fontWeight,
                  fontSize: 12,
                  marginLeft: 20,
                  marginRight: 20,
                },
                this.defaultStyle.tabItemText,
                this.props.headerTextStyle,
              ]}>
              {item['subcategoryname']}
            </Text>
          </View>
          {this._renderHighlight(isTabActive)}
        </View>
      </TouchableHighlight>
    );
  };

  _renderHighlight = (showHighlight) => {
    if (showHighlight) {
      return (
        <View
          style={[
            this.defaultStyle.highlight,
            this.props.highlightStyle,
            { backgroundColor: this.props.headerUnderlayColor },
          ]}
        />
      );
    } else {
      return (
        <View
          style={[this.defaultStyle.noHighlight, this.props.noHighlightStyle]}
        />
      );
    }
  };

  scrollHeader = (index) => {
    this.headerView.scrollToIndex({ index, animated: true });
  };

  render() {
    return (
      <View style={{ width: '100%' }}>
        <FlatList
          inverted={true}
          horizontal
          alwaysBounceHorizontal={false}
          ref={(headerView) => {
            this.headerView = headerView;
          }}
          bounces={false}
          showsHorizontalScrollIndicator={false}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          data={this.props.data}
          extraData={this.state}
          renderItem={this._renderTitle}
          style={[
            this.defaultStyle.headerStyle,
            { backgroundColor: this.props.headerBackgroundColor },
          ]}
        />
        {/* <View style={{width:'100%',height:.5,backgroundColor:'grey'}}/> */}
      </View>
    );
  }
}

DynamicdefaultStyle = {
  headerStyle: {},
  tabItemText: {
    color: 'white',
  },
  tabItemContainer: {
    overflow: 'hidden',
    backgroundColor: '#555555',
    paddingTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  highlight: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 2,
    marginTop: 5,
  },
  noHighlight: {
    paddingHorizontal: 10,
    paddingVertical: 2,
    marginTop: 5,
  },
};

DynamicTabViewScrollHeader.defaultProps = {
  selectedTab: 0,
  headerBackgroundColor: '#555555',
  headerUnderlayColor: '#00000033',
};

DynamicTabViewScrollHeader.propTypes = {
  goToPage: PropTypes.func.isRequired,
  selectedTab: PropTypes.number.isRequired,
  headerBackgroundColor: PropTypes.any,
  headerTextStyle: PropTypes.any,
  highlightStyle: PropTypes.any,
  noHighlightStyle: PropTypes.any,
  headerUnderlayColor: PropTypes.any,
};

export default DynamicTabViewScrollHeader;
