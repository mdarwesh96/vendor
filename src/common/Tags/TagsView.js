import React from 'react';
import { View, StyleSheet, Button } from 'react-native';
import BackgroundButton from './BackgroundButton';
const addOrRemove = (array, item) => {
  const exists = array.includes(item);
  if (exists) {
    return array.filter((c) => {
      return c !== item;
    });
  } else {
    const result = array;
    result.push(item);
    return result;
  }
};
export default class TagsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
    };
  }

  render() {
    return <View style={styles.container}>{this.makeButtons()}</View>;
  }

  onPress = (tag) => {
    let selected;
    if (this.props.isExclusive) {
      selected = [tag];
    } else {
      selected = addOrRemove(this.state.selected, tag);
    }

    this.setState({
      selected,
    });
    this.props.checkeditemsss.setselected(selected);
  };

  makeButtons() {
    return this.props.all.map((tag, i) => {
      const on = this.state.selected.includes(tag);
      const backgroundColor = on ? 'grey' : 'grey';
      const textColor = on ? 'white' : 'white';
      const borderColor = on ? 'grey' : 'grey';

      return (
        <View>
          <BackgroundButton
            backgroundColor={backgroundColor}
            textColor={textColor}
            borderColor={borderColor}
            key={i}
            title={tag}
          />
        </View>
      );
    });
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row-reverse',
    paddingBottom: 5,
    paddingTop: 5,
    paddingRight: 20,
  },
});
