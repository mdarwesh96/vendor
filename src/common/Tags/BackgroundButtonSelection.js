import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Image } from 'react-native';

export default class BackgroundButtonSelection extends React.Component {
  render() {
    const styles = this.makeStyles();
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={styles.touchable}
        onPress={this.props.onPress}>
        <View style={styles.view}>
          <Text style={styles.text}>{this.props.title}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  makeImageIfAny(styles) {
    if (this.props.showImage) {
      return <Image style={styles.image} />;
    }
  }

  makeStyles() {
    return StyleSheet.create({
      view: {
        flexDirection: 'row',
        borderRadius: 8,
        padding: 5,

        borderColor: this.props.borderColor,
        borderWidth: 0.5,
        backgroundColor: this.props.backgroundColor,

        alignItems: 'center',
        justifyContent: 'center',
      },
      touchable: {
        marginLeft: 4,
        marginRight: 4,
        marginBottom: 5,
      },
      image: {
        marginRight: 8,
      },
      text: {
        fontSize: 14,
        textAlign: 'center',

        padding: 5,
        color: this.props.textColor,
      },
    });
  }
}
