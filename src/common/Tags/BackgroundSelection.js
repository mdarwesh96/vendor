import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Image } from 'react-native';

export default class BackgroundSelection extends React.Component {
  render() {
    const styles = this.makeStyles();
    return (
      <View style={styles.touchable}>
        <View style={styles.view}>
          <Text style={styles.text}>{this.props.title}</Text>
        </View>
      </View>
    );
  }

  makeImageIfAny(styles) {
    if (this.props.showImage) {
      return <Image style={styles.image} />;
    }
  }

  makeStyles() {
    return StyleSheet.create({
      view: {
        flexDirection: 'row',
        borderRadius: 8,

        borderColor: this.props.borderColor,
        borderWidth: 0.5,
        backgroundColor: this.props.backgroundColor,

        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
      },
      touchable: {
        marginLeft: 4,
        marginRight: 4,
        marginBottom: 5,
      },
      image: {
        marginRight: 8,
      },
      text: {
        fontSize: 14,
        textAlign: 'center',

        padding: 5,
        color: this.props.textColor,
      },
    });
  }
}
