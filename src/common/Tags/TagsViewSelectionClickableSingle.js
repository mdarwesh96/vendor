import React from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import BackgroundButtonSelection from './BackgroundButtonSelection';
let language = 'en';
const addOrRemove = (array, item) => {
  const exists = array.includes(item);
  if (exists) {
    return array.filter((c) => {
      return c !== item;
    });
  } else {
    const result = [];
    result.push(item);
    return result;
  }
};
export default class TagsViewSelectionClickableSingle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
      language: props.language,
    };
    language = props.language;
  }
  render() {
    return <View style={styles.container}>{this.makeButtons()}</View>;
  }
  onPress = (tag) => {
    let selected;
    if (this.props.isExclusive) {
      selected = [tag];
    } else {
      selected = addOrRemove(this.state.selected, tag);
    }
    this.setState({
      selected,
    });

    this.props.selectSubCategory(selected[0]);
  };
  makeButtons() {
    return this.props.all.map((tag, i) => {
      const on = this.state.selected.includes(tag);
      const backgroundColor = on ? '#1789bc' : 'white';
      const textColor = on ? 'white' : '#1789bc';
      const borderColor = on ? '#1789bc' : '#1789bc';

      return (
        <BackgroundButtonSelection
          style={{ padding: 10 }}
          backgroundColor={backgroundColor}
          textColor={textColor}
          borderColor={borderColor}
          onPress={() => {
            this.onPress(tag);
          }}
          key={i}
          showImage={on}
          title={tag.category_name_ar}
        />
      );
    });
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:
      Platform.OS === 'ios' || (Platform.OS === 'android' && language === 'ar')
        ? 'row-reverse'
        : 'row',
    flexWrap: 'wrap',
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop: 10,
  },
});
