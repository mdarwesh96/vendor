import React from 'react'
import { View, StyleSheet, Button } from 'react-native'
import BackgroundButtonSelection from './BackgroundButtonSelection'
const addOrRemove = (array, item) => {
  const exists = array.includes(item)
if (exists) {
    return array.filter((c) => { return c !== item })
  } else {
    const result = array
    result.push(item)
    return result
  }
}
export default class TagsViewSelectionClickable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selected: props.selected,
      clickable:props.clickable
    }
  }
render() {
    return (
      <View style={styles.container}>
        {this.makeButtons()}
      </View>
    )
  }
onPress = (tag) => {
    let selected
    if (this.props.isExclusive) {
      selected = [tag]
    } else {
      selected = addOrRemove(this.state.selected, tag)
    }
this.setState({
      selected
    })
  }
makeButtons() {
    return this.props.all.map((tag, i) => {
      const on = this.state.selected.includes(tag)
      const backgroundColor = on ? '#3888C6' : 'grey'
      const textColor = on ?'white' : 'white'
      const borderColor = on ? '#3888C6' : 'grey'
return (
        <BackgroundButtonSelection
        style={{padding:10}}
          backgroundColor={backgroundColor}
          textColor={textColor}
          borderColor={borderColor}
          onPress={() => {
            this.onPress(tag)
          }}
          key={i}
          showImage={on}
          title={tag.category_name} />
      )
      
    })
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row-reverse',
    flexWrap: 'wrap',
    paddingLeft: 20,
    paddingRight: 10,
    paddingTop:10,
  }
})