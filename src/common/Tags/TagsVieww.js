import React from 'react';
import { View, StyleSheet, Dimensions, Platform } from 'react-native';
import BackgroundButton from './BackgroundButton';
const DEVICE_width = Dimensions.get('window').width;
let language = 'en';
const addOrRemove = (array, item) => {
  const exists = array.includes(item);
  if (exists) {
    return array.filter((c) => {
      return c !== item;
    });
  } else {
    const result = array;
    result.push(item);
    return result;
  }
};
export default class TagsVieww extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
      language: props.language,
    };
    language = props.language;
  }

  render() {
    return <View style={styles.container}>{this.makeButtons()}</View>;
  }

  onPress = (tag) => {
    let selected;
    if (this.props.isExclusive) {
      selected = [tag];
    } else {
      selected = addOrRemove(this.state.selected, tag);
    }

    this.setState({
      selected,
    });
    this.props.checkeditemsss.setselected(selected);
  };

  makeButtons() {
    return this.props.all.map((tag, i) => {
      const on = this.state.selected.includes(tag);
      const backgroundColor = on ? '#1789bc' : 'white';
      const textColor = on ? 'white' : '#1789bc';
      const borderColor = on ? 'grey' : '#1789bc';

      return (
        <View>
          <BackgroundButton
            backgroundColor={backgroundColor}
            textColor={textColor}
            borderColor={borderColor}
            key={i}
            title={tag.label_name_ar}
          />
        </View>
      );
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection:
      Platform.OS === 'ios' || (Platform.OS === 'android' && language === 'en')
        ? 'row'
        : 'row-reverse',
    paddingBottom: 5,
    paddingTop: 5,
    flexWrap: 'wrap',
  },
});
