import React from 'react';
import { View, StyleSheet, Button, Platform } from 'react-native';
import BackgroundButtonSelection from './BackgroundButtonSelection';
const addOrRemove = (array, item) => {
  const exists = array.includes(item);

  if (exists) {
    return array.filter((c) => {
      return c !== item;
    });
  } else {
    const result = array;
    result.push(item);
    return result;
  }
};
export default class TagsViewSelectionClickablelogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedd: props.selected,
      clickable: props.clickable,
      langugae: props.langugae,
    };
  }
  render() {
  
    return (
      <View style={{ flexDirection: 'row', width: '100%' }}>
        {(Platform.OS === 'ios' ||
          (Platform.OS === 'android' && this.state.langugae === 'en')) && (
          <View style={styles.container}>{this.makeButtons()}</View>
        )}
        {Platform.OS === 'android' && this.state.langugae === 'ar' && (
          <View style={styles.container_ar}>{this.makeButtons()}</View>
        )}
      </View>
    );
  }
  onPresss = (tag) => {
    let selectedd = [];

    if (this.props.isExclusive) {
      selectedd = [tag];
    } else {
      if (this.props.first === false) {
        selectedd = addOrRemove(this.state.selectedd, tag);
      } else {
        this.setState({ selectedd: [] });
        selectedd = [tag];
        this.props.onc.updtaefirstste(this.props.first, this.props.type);
      }
      try {
        if (this.props.type === 'categories') {
          this.props.onc.selectlabelsss(selectedd);
        } else {
          this.props.onc.selectholidaysss(selectedd);
        }
      } catch (e) {}
    }
    this.setState({
      selectedd,
    });
  };
  makeButtons() {
    return this.props.all.map((tag, i) => {
      const on = this.props.selected.includes(tag)||this?.props?.selected[i]?.label_name_ar===tag.label_name_ar;

      const backgroundColor = on ? '#1789bc' : 'white';
      const textColor = on ? 'white' : '#1789bc';
      const borderColor = on ? 'grey' : '#1789bc';
      return (
        <BackgroundButtonSelection
          style={{ padding: 10 }}
          backgroundColor={backgroundColor}
          textColor={textColor}
          borderColor={borderColor}
          onPress={() => {
            this.onPresss(tag);
          }}
          key={i}
          showImage={on}
          title={tag.label_name_ar}
        />
      );
    });
  }
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row-reverse',
    flexWrap: 'wrap',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 10,
  },
  container_ar: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 10,
  },
});
