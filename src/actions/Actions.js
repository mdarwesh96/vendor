import {
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAIL,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  GET_PAYMENT_FAIL,
  GET_PAYMENT_SUCCESS,
  FORGET_SECOND_FAIL,
  FORGET_SECOND_SUCCESS,
  FORGET_SECOND_ATTEMPT,
  FORGET_FIRST_ATTEMPT,
  FORGET_FIRST_SUCCESS,
  FORGET_FIRST_FAIL,
  GET_VENDOR_DETAILS_FAIL,
  GET_VENDOR_DETAILS_SUCCESS,
  GET_LABELS_SUCCESS,
  GET_LABELS_FAIL,
  GET_CURRENCIES_SUCCESS,
  GET_CURRENCIES_FAUL,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAUL,
  ADD_PRODUCT_SUCCESS,
  ADD_PRODUCT_FAIL,
  EDIT_PRODUCT_FAIL,
  EDIT_PRODUCT_SUCCESS,
  GET_VENDOR_ORDERS_SUCCESS,
  GET_VENDOR_ORDERS_FAIL,
  UPDATE_ORDER_FAIL,
  UPDATE_ORDER_SUCCESS,
  GET_VENDOR_DETAILS_ATTEMPT,
  GET_VENDOR_ORDERS_Attempt,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  REGISTER_ATTEMPT,
  LOGIN_USER_ATTEMPT,
  EDIT_PROFILE_AATEMPT,
  UPDATE_VENDOR_STATE_SUCCESS,
  UPDATE_VENDOR_STATE_FAIL,
  UPDATE_VENDOR_STATE_ATTEMPT,
  EDIT_PROFILE_FAIL,
  EDIT_PROFILE_SUCCESS,
  UPDATE_VENDOR_ORDERS_SUCCESS,
  UPDATE_VENDOR_ORDERS_FAIL,
  UPDATE_VENDOR_ORDERS_ATTEMPT,
  UPADTE_AVAILABILITY,
  ADD_PRODUCT_ATTEMPT,
  LOGOUT_USER_ATTEMPT,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAILL,
  ACCEPT_ORDER_FAIL,
  ACCEPT_ORDER_SUCCESS,
  ACCEPT_ORDER_ATEMPT,
  UPADTE_CURRENCY,
  COUNTRIES_FAIL,
  COUNTRIES_SUCCESS,
  GET_VERSION_FAIL,
  GET_VERSION_Attempt,
  GET_VERSION_SUCCESS,
} from './types';

import axios from 'axios';
import { debug } from 'react-native-reanimated';

export const EditProfileAction = (
  costdeliverydestination,
  deliverydestination,
  name,
  phone,
  city,
  cityid,
  subcityid,
  address,
  imagebaseprofile,
  selectedholidays,
  selected,
  token,
  startclock,
  endclock,
  categoryid
) => {
  let URL = 'http://ecommerce.waddeely.com/api/update-user';
  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    var formData = new FormData();
    formData.append('name', name);
    formData.append('address[0][address_name]', '' + address);
    formData.append('address[0][lat]', 222222);
    formData.append('language', 1);
    formData.append('address[0][lang]', 333333);
    formData.append('address[1][address_name]', '' + city);
    formData.append('address[1][lat]', cityid);
    formData.append('address[1][lang]', subcityid);
    formData.append('phone[0][phone]', parseInt(phone));
    formData.append('photo', imagebaseprofile);
    formData.append('work_hours[0][start]]', '' + startclock);
    formData.append('work_hours[0][end]', '' + endclock);
    formData.append('category_id[0][category_id]', '' + categoryid);
    formData.append('currency_id', '1');
  
    if (selected.length > 0) {
      for (let i = 0; i < selected.length; i++) {
        let d = 'label_id[' + i + '][label_id]';

        formData.append(d, '' + selected[i].id);
      }
    }

    if (costdeliverydestination.length > 0) {
      for (let f = 0; f < costdeliverydestination.length; f++) {
        formData.append(
          'delivery_address[' + f + '][id]',
          deliverydestination[f]
        );
        formData.append(
          'delivery_address[' + f + '][cost]',
          parseInt(costdeliverydestination[f])
        );
      }
    } else {
      formData.append('delivery_address[' + 0 + '][id]', '');
      formData.append('delivery_address[' + 0 + '][cost]', 10);
    }

    if (selectedholidays.length > 0) {
      for (let k = 0; k < selectedholidays.length; k++) {
        let hh = 'work_days[' + k + '][offdays]';

        if (typeof selectedholidays[k].label_name_ar !== 'undefined') {
          formData.append(hh, '' + selectedholidays[k].label_name_ar);
        } else if (typeof selectedholidays[k].offdays !== 'undefined') {
          formData.append(hh, '' + selectedholidays[k].offdays);
        }
      }
    }

    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        
        try {
          dispatch({ type: EDIT_PROFILE_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: EDIT_PROFILE_FAIL, error: e });
        }
      })
      .catch((error) => {
      
        dispatch({ type: EDIT_PROFILE_FAIL, error: error });
      });
  };
};

export const RegisterAction = (
  costdeliverydestination,
  deliverydestination,
  email,
  password,
  name,
  phone,
  address,
  city,
  imagebaseprofile,
  cityid,
  subcityidd,
  categoryid,
  selected,
  startclock,
  endclock,
  selectedholidays,
  currencyidd
) => {
  let URL = 'http://ecommerce.waddeely.com/api/register';

  return (dispatch) => {
    var formData = new FormData();
    formData.append('name', name);
    formData.append('email', email);
    formData.append('language', 1);
    formData.append('password_confirmation', password);
    formData.append('password', password);
    formData.append('device_id', 'llllllll');
    formData.append('address[0][address_name]', '' + address);
    formData.append('address[0][lat]', 222222);
    formData.append('address[0][lang]', 333333);
    formData.append('address[1][address_name]', '' + city);
    formData.append('address[1][lat]', cityid);
    formData.append('address[1][lang]', subcityidd);
    formData.append('photo', imagebaseprofile);
    formData.append('phone[0][phone]', phone);
    formData.append('type', 2);
    formData.append('category_id[0][category_id]', categoryid);
    formData.append('work_hours[0][start]]', startclock);
    formData.append('work_hours[0][end]', endclock);
    formData.append('currency_id', '1');

    if (selected.length > 0) {
      for (let i = 0; i < selected.length; i++) {
        formData.append('label_id[' + i + '][label_id]', selected[i].id);
      }
    }

    if (costdeliverydestination.length > 0) {
      for (let f = 0; f < costdeliverydestination.length; f++) {
        formData.append(
          'delivery_address[' + f + '][id]',
          deliverydestination[f]
        );
        formData.append(
          'delivery_address[' + f + '][cost]',
          parseInt(costdeliverydestination[f])
        );
      }
    }

    if (selectedholidays.length > 0) {
      for (let k = 0; k < selectedholidays.length; k++) {
        formData.append(
          'work_days[' + k + '][offdays]',
          selectedholidays[k].label_name_ar
        );
      }
    }

    dispatch({ type: REGISTER_ATTEMPT });
    axios
      .post(URL, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      .then((response) => {
        try {
          dispatch({ type: REGISTER_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: REGISTER_FAILED, error: e });
        }
      })
      .catch((error) => {
        try {
          dispatch({
            type: REGISTER_FAILED,
            error: error.response.data.errors.email[0],
          });
        } catch (ee) {
          dispatch({
            type: REGISTER_FAILED,
            error: error.response.data.errors,
          });
        }
      });
  };
};
export const ForgetPasswordFirstAction = (email) => {
  let URL = 'http://ecommerce.waddeely.com/api/password/email';

  return (dispatch) => {
    var formData = new FormData();
    formData.append('email', email);
    dispatch({ type: FORGET_FIRST_ATTEMPT });

    axios
      .post(URL, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      .then((response) => {
        try {
          dispatch({ type: FORGET_FIRST_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: FORGET_FIRST_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: FORGET_FIRST_FAIL, error: error });
      });
  };
};

export const ForgetPasswordSecondAction = (email, code, newpassword) => {
  let URL = 'http://ecommerce.waddeely.com/api/password/reset';

  return (dispatch) => {
    var formData = new FormData();
    formData.append('email', email);
    formData.append('password', newpassword);
    formData.append('token', code);

    formData.append('password_confirmation', newpassword);

    dispatch({ type: FORGET_SECOND_ATTEMPT });

    axios
      .post(URL, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      .then((response) => {
        try {
          dispatch({ type: FORGET_SECOND_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: FORGET_SECOND_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: FORGET_SECOND_FAIL, error: error });
      });
  };
};

export const LoginUserAction = (email, password, devicetoken) => {
  let URL = 'http://ecommerce.waddeely.com/api/login';

  return (dispatch) => {
    var formData = new FormData();
    formData.append('password', password);
    formData.append('email', email.trim());
    formData.append('language', 1);

    formData.append('device_id', devicetoken);

    dispatch({ type: LOGIN_USER_ATTEMPT });
    dispatch({ type: EDIT_PROFILE_AATEMPT });

    axios
      .post(URL, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      .then((response) => {
        
        try {
          dispatch({ type: LOGIN_USER_SUCCESS, payload: response.data });
        } catch (e) {
          
          dispatch({ type: LOGIN_USER_FAIL, error: e });
        }
      })
      .catch((error) => {
       
        dispatch({ type: LOGIN_USER_FAIL, error: error });
      });
  };
};
export const GetCountriesCitiesCounranciesAction = () => {
  return (dispatch) => {
    axios
      .get('http://ecommerce.waddeely.com/api/user/get-countries', {
        headers: {
          'Content-Type': 'application/json',
          //other header fields
        },
      })

      .then((resp) => {
        dispatch({ type: COUNTRIES_SUCCESS, payload: resp.data });
      })

      .catch((error) => {
        try {
          dispatch({ type: COUNTRIES_FAIL, error });

          // handlefail(dispatch,error);
        } catch (er) {
          dispatch({ type: COUNTRIES_FAIL, error: er });
        }
      });
  };
};

export const GetCategoriesSubCategoriesLabelsAction = () => {
  return (dispatch) => {
    axios
      .get(
        'http://ecommerce.waddeely.com/api/user/get-maincategories',
        {},
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )

      .then((resp) => {
        dispatch({ type: GET_CATEGORIES_SUCCESS, payload: resp.data.data });
      })

      .catch((error) => {
        try {
          dispatch({ type: GET_CATEGORIES_FAIL, error });

          // handlefail(dispatch,error);
        } catch (er) {
          dispatch({ type: GET_CATEGORIES_FAIL, error: er });
        }
      });
  };
};

export const GetLabelsAction = () => {
  return (dispatch) => {
    axios
      .get(
        'http://ecommerce.waddeely.com/api/user/get-labels',
        {},
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )

      .then((resp) => {
        dispatch({ type: GET_LABELS_SUCCESS, payload: resp.data.data });
      })

      .catch((error) => {
        try {
          dispatch({ type: GET_LABELS_FAIL, error });

          // handlefail(dispatch,error);
        } catch (er) {
          dispatch({ type: GET_LABELS_FAIL, error: er });
        }
      });
  };
};

export const GetCurrenciesAction = () => {
  return (dispatch) => {
    axios
      .get(
        'http://ecommerce.waddeely.com/api/user/get-currencies',
        {},
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )

      .then((resp) => {
        dispatch({ type: GET_CURRENCIES_SUCCESS, payload: resp.data.data });
      })

      .catch((error) => {
        try {
          dispatch({ type: GET_CURRENCIES_FAUL, error });

          // handlefail(dispatch,error);
        } catch (er) {
          dispatch({ type: GET_CURRENCIES_FAUL, error: er });
        }
      });
  };
};

// export const EmptyDetailsUserAction = (id,currencyid) => {
//   return (dispatch) => {

//   }

// }

export const GetUserDetailsAction = (id, currencyid) => {
  return (dispatch) => {
    dispatch({ type: GET_VENDOR_DETAILS_ATTEMPT });

    axios
      .get(
        'http://ecommerce.waddeely.com/api/user/vendor-details/' + id + '/' + 5,
        {},
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )

      .then((resp) => {
     
        dispatch({ type: GET_VENDOR_DETAILS_SUCCESS, payload: resp.data });
      })

      .catch((error) => {
      

        try {
          dispatch({ type: GET_VENDOR_DETAILS_FAIL, error });

          // handlefail(dispatch,error);
        } catch (er) {
          dispatch({ type: GET_VENDOR_DETAILS_FAIL, error: er });
        }
      });
  };
};
export const AddProductAction = (
  name,
  description,
  maincategoryid,
  subcategory,
  price,
  time,
  currencyid,
  spespecial,
  imagebase,
  token
) => {
  let URL = 'http://ecommerce.waddeely.com/api/vendor/save-item';
  const AuthStr = 'Bearer '.concat(token);

  let isads = 1;
  if (spespecial) {
    isads = 1;
  } else {
    isads = 0;
  }

  return (dispatch) => {
    dispatch({ type: ADD_PRODUCT_ATTEMPT });
    var formData = new FormData();
    formData.append('name_ar', name);
    formData.append('name_en', name);

    formData.append('description_ar', description);
    formData.append('description_en', description);

    formData.append('maincategory_id', maincategoryid);
    formData.append('subcategory_id', subcategory);
    formData.append('price', '' + price);
    formData.append('prepare_time', '' + time);
    formData.append('stock_count', '22222');
    formData.append('currency_id', '1');
    formData.append('special_item', isads);
    formData.append('item_photo[0]', imagebase);
    formData.append('label_id[0]', '');
    formData.append('related_item[0]', '');

    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({ type: ADD_PRODUCT_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: ADD_PRODUCT_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: ADD_PRODUCT_FAIL, error: error });
      });
  };
};
export const EditItemAction = (
  name,
  description,
  maincategoryid,
  subcategory,
  price,
  time,
  count,
  currencyid,
  spespecial,
  imagebase,
  token,
  itemid,
  userid
) => {
  let URL = 'http://ecommerce.waddeely.com/api/vendor/update-item/' + itemid;
  const AuthStr = 'Bearer '.concat(token);
  let isads = 1;

  if (spespecial) {
    isads = 1;
  } else {
    isads = 0;
  }

  return (dispatch) => {
    var formData = new FormData();
    formData.append('name_ar', name);
    formData.append('name_en', name);

    formData.append('description_ar', description);
    formData.append('description_en', description);
    formData.append('user_id', userid);
    formData.append('maincategory_id', maincategoryid);
    formData.append('subcategory_id', subcategory);
    formData.append('price', '' + price);
    formData.append('prepare_time', '' + time);
    formData.append('stock_count', '' + count);
    formData.append('currency_id', '1');
    formData.append('special_item', isads);
    formData.append('item_photo[0]', imagebase);
    formData.append('label_id[0]', 22);
    formData.append('related_item[0]', 11);

    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({ type: EDIT_PRODUCT_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: EDIT_PRODUCT_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: EDIT_PRODUCT_FAIL, error: error });
      });
  };
};

export const UpdateCurrencyAction = (CURRENCY, CURRENCYID) => {
  return (dispatch) => {
    dispatch({
      type: UPADTE_CURRENCY,
      currency: CURRENCY,
      currencyid: CURRENCYID,
    });
  };
};

export const EmptyAddItemAction = () => {
  return (dispatch) => {
    dispatch({ type: ADD_PRODUCT_ATTEMPT });
  };
};

export const UpdateAvailabiltyAction = (availableindex) => {
  return (dispatch) => {
    dispatch({ type: UPADTE_AVAILABILITY, availableindex: availableindex });
  };
};

export const UpdateVendorStateAction = (token, d) => {
  let URL = 'http://ecommerce.waddeely.com/api/user/update-available';
  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    var formData = new FormData();
    formData.append('available', d);
    dispatch({ type: UPDATE_VENDOR_STATE_ATTEMPT });

    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({ type: UPDATE_VENDOR_STATE_SUCCESS, payload: response });
        } catch (e) {
          dispatch({ type: UPDATE_VENDOR_STATE_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: UPDATE_VENDOR_STATE_FAIL, error: error });
      });
  };
};

export const DeleteItemAction = (id, token) => {
  let URL = 'http://ecommerce.waddeely.com/api/vendor/delete-item/' + id;
  const AuthStr = 'Bearer '.concat(token);
  return (dispatch) => {
    axios
      .get(URL, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })
      .then((response) => {
        try {
          // If request is good...
          dispatch({ type: DELETE_PRODUCT_SUCCESS, payload: response.data });
        } catch (err) {
          dispatch({ type: DELETE_PRODUCT_FAUL, error: err });
        }
      })
      .catch((e) => {
        dispatch({ type: DELETE_PRODUCT_FAUL, error: e });

        //  console.log('error ' + error);
      });
  };
};

export const GetAppVersion = () => {
  let URL = 'http://ecommerce.waddeely.com/api/last-version';

  return (dispatch) => {
    dispatch({ type: GET_VERSION_Attempt });

    axios
      .get(URL, { headers: { 'Content-Type': 'application/json' } })
      .then((response) => {
        try {
          dispatch({ type: GET_VERSION_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: GET_VERSION_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: GET_VERSION_FAIL, error: error });
      });
  };
};

export const GetOrdersAction = (currencyid, token, userid) => {
  let URL =
    'http://ecommerce.waddeely.com/api/vendor/get-order/' + userid + '/' + '5';

  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    dispatch({ type: GET_VENDOR_ORDERS_Attempt });

    axios
      .get(URL, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })
      .then((response) => {
        try {
          
          dispatch({ type: GET_VENDOR_ORDERS_SUCCESS, payload: response.data });
        } catch (e) {
      
          dispatch({ type: GET_VENDOR_ORDERS_FAIL, error: e });
        }
      })
      .catch((error) => {
   
        dispatch({ type: GET_VENDOR_ORDERS_FAIL, error: error });
      });
  };
};

export const ClearAction = () => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER_ATTEMPT });
    dispatch({ type: REGISTER_ATTEMPT });
    dispatch({ type: GET_VENDOR_ORDERS_Attempt });
    dispatch({ type: GET_VENDOR_DETAILS_ATTEMPT });
  };
};
export const LogoutAction = (token) => {
  let URL = 'https://ecommerce.waddeely.com/api/logout';

  return (dispatch) => {
    var formData = new FormData();
    const AuthStr = 'Bearer '.concat(token);
    dispatch({ type: LOGOUT_USER_ATTEMPT });
    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({ type: LOGOUT_USER_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: LOGOUT_USER_FAILL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: LOGOUT_USER_FAILL, error: error });
      });
  };
};

export const AcceptOrdersAction = (orderid, token) => {
  let URL =
    'http://ecommerce.waddeely.com/api/delivary/request-order/' + orderid;

  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    dispatch({ type: ACCEPT_ORDER_ATEMPT });

    axios
      .get(URL, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })
      .then((response) => {
        try {
          dispatch({ type: ACCEPT_ORDER_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: ACCEPT_ORDER_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: ACCEPT_ORDER_FAIL, error: error });
      });
  };
};

export const UpdateOrdersAction = (
  timetodeliver,
  orderid,
  token,
  orderstate,
  reason,
  success
) => {
  let URL = 'http://ecommerce.waddeely.com/api/vendor/update-order/' + orderid;

  const AuthStr = 'Bearer '.concat(token);
  var formData = new FormData();

  formData.append('status', orderstate);

  if (orderstate === 2) {
    formData.append('expected_delivary_time', timetodeliver);
  }


  if (reason != '' && (orderstate === 6 || orderstate === 10)) {
    formData.append('failed', reason);
  }
 

  if (success != '') {
    formData.append('success', success);
  }


  return (dispatch) => {
    dispatch({ type: UPDATE_VENDOR_ORDERS_ATTEMPT });


    axios
      .post(URL, formData, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })
      .then((response) => {
        try {
          dispatch({
            type: UPDATE_VENDOR_ORDERS_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
        

          dispatch({ type: UPDATE_VENDOR_ORDERS_FAIL, error: e });
        }
      })
      .catch((error) => {


        dispatch({ type: UPDATE_VENDOR_ORDERS_FAIL, error: error });
      });
  };
};

// export const UpdateOrdersAction = (currencyid,token,userid,orderid,state) => {

//   let URL='http://8fca8330e33070290.temporary.link/api/vendor/get-order/'+userid+'/'+currencyid;

//   const AuthStr = 'Bearer '.concat(token);

//   return (dispatch) => {

//     axios.get(URL, { headers: { 'Authorization': AuthStr ,'Content-Type':'application/json'
//     ,
// } })
//    .then(response => {
//      try{

//     dispatch({ type: UPDATE_ORDER_SUCCESS,payload:response.data})
//      }catch(e)
//      {

//       dispatch({ type: UPDATE_ORDER_FAIL,error:e})

//      }

//     })
//    .catch((error) => {

//     dispatch({ type:  UPDATE_ORDER_FAIL,error:error})

//   });

//   }

// }

export const GetPaymrentAction = (token) => {
  let URL = 'http://ecommerce.waddeely.com/api/vendor/payments/' + '5';
  const AuthStr = 'Bearer '.concat(token);
  return (dispatch) => {
    axios
      .get(URL, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })

      .then((resp) => {
        dispatch({ type: GET_PAYMENT_SUCCESS, payload: resp.data });
      })

      .catch((error) => {
        try {
          dispatch({ type: GET_PAYMENT_FAIL, error });

          // handlefail(dispatch,error);
        } catch (er) {
          dispatch({ type: GET_PAYMENT_FAIL, error: er });
        }
      });
  };
};
