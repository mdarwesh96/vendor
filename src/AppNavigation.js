import React, { Component, createRef } from 'react';
import ScalingDrawer from './SwipeAbleDrawer';
import { Platform, NativeModules } from 'react-native';
import NavigationService from './screens/NavigationService';


import LeftMenu from './LeftMenu';
import OrdersScreen from './screens/Orders/OrdersScreen';
import HomeSeller from './screens/Home/HomeSeller';
import ItemDetailsScreen from './screens/Home/ItemDetailsScreen';
import SellerProfile from './screens/Profile/SellerProfile';
import AboutScreen from './screens/About/AboutScreen';
import LoginScreen from './screens/Login/LoginScreen';
// import PaymentScreen from './screens/payment/PaymentScreen'
import LoadingScreen from './screens/LoadingScreen';
import NewItemDetailsScreen from './screens/Home/NewItemDetailsScreen';
let userloggedin = 'false';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
let deviceLocale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale
    : NativeModules.I18nManager.localeIdentifier;
if (
  typeof deviceLocale !== 'undefined' &&
  deviceLocale !== null &&
  deviceLocale.includes('ar')
) {
  deviceLocale = 'ar';
} else {
  deviceLocale = 'en';
}
export const HomeStack = createAppContainer(
  createStackNavigator(
    {
      HomeSeller: {
        screen: HomeSeller,
        navigationOptions: {
          headerShown: null,
        },
      },

      ItemDetailsScreen: {
        screen: ItemDetailsScreen,
        navigationOptions: {
          headerShown: null,
        },
      },
      NewItemDetailsScreen: {
        screen: NewItemDetailsScreen,
        navigationOptions: {
          headerShown: null,
        },
      },
    },
    {
      initialRouteName: 'HomeSeller',
    }
  )
);

export const AboutStack = createAppContainer(
  createStackNavigator({
    AboutScreen: {
      screen: AboutScreen,
      navigationOptions: {
        headerShown: null,
      },
    },
  })
);

export const ProfileStack = createAppContainer(
  createStackNavigator({
    SellerProfile: {
      screen: SellerProfile,
      navigationOptions: {
        headerShown: null,
      },
    },
  })
);
export const OrdersStack = createAppContainer(
  createStackNavigator(
    {
      OrdersScreen: {
        screen: OrdersScreen,
        navigationOptions: {
          headerShown: null,
        },
      },
    },
    {
      initialRouteName: 'OrdersScreen',
    }
  )
);

// export const PaymentStack = createAppContainer(createStackNavigator({

//   PaymentScreen: {
//     screen: PaymentScreen,
//     navigationOptions: {
//       headerShown: null,
//     }
//   }
//   },
//   {
//     initialRouteName: 'PaymentScreen'

//   }
//   ));

export const LoginStack = createAppContainer(
  createStackNavigator({
    LoginScreen: {
      screen: LoginScreen,
      navigationOptions: {
        headerShown: null,
      },
    },
  })
);
const AppSwitch = createAppContainer(
  createSwitchNavigator(
    {
      HomeStack: HomeStack,
      OrdersStack: OrdersStack,
      ProfileStack: ProfileStack,
      AboutStack: AboutStack,
      LoginScreen: LoginScreen,

      // PaymentStack:PaymentStack,
    },
    {
      initialRouteName: 'LoginScreen',
    }
  )
);

export const drawer = createRef();

const defaultScalingDrawerConfig = {
  scalingFactor: 0.8,
  minimizeFactor: 0.9,
  swipeOffset: 50,
};

export default class AppNavigation extends Component {
  constructor() {
    super();
    this.state = {
      userLoggedInState: false,
    };
  }

  componentDidMount() {
    this.mounted = true;
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  render() {
    return (
      <ScalingDrawer
        ref={drawer}
        content={
          <LeftMenu
            language={deviceLocale}
            userLoggedInStatee={true}
            drawer={drawer}
            navigation={this.props.navigation}
          />
        }
        {...defaultScalingDrawerConfig}
        onClose={() => {}}
        onOpen={() => {}}>
        <AppSwitch
          ref={(navigatorRef) => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </ScalingDrawer>
    );
  }
}
