import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import { NavigationActions } from 'react-navigation';
import { useDispatch, useSelector } from 'react-redux';
import { UpdateVendorStateAction ,LogoutAction,ClearAction} from './actions/Actions';
import NavigationService from './screens/NavigationService';
import moment from 'moment';

const { width, height } = Dimensions.get('window');
let vendorisopen = false;

let laaang = 'en';

const resetAction = (routeName) =>
  NavigationActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: routeName, drawer: 'close' }),
    ],
  });

export default function LeftMenu(props) {
  const dispatch = useDispatch();
  const [name, setname] = useState('');
  const [currency, setcurrency] = useState('');
  const [currencyid, setcurrencyid] = useState('1');
  const LogoutReducer = useSelector((state) => state.LogoutReducer);
  const [token, settoken] = useState('');
  const [userId, setuserId] = useState('');
  const [password, setpassword] = useState(null);
  const [email, setemail] = useState(null);
  const GetCountriesReducer = useSelector((state) => state.GetCountriesReducer);

  const VendorDetailsReducer = useSelector(
    (state) => state.VendorDetailsReducer
  );
  const LoginReducer = useSelector((state) => state.LoginReducer);

  const setDateTime = (date, str) => {
    var sp = str.split(':');
    date.setHours(parseInt(sp[0], 10));
    date.setMinutes(parseInt(sp[1], 10));
    return date;
  };

  const loadApp = async () => {
    try {
      setcurrencyid(await AsyncStorage.getItem('currencyid'));

      setcurrency(await AsyncStorage.getItem('currency'));

      setpassword(await AsyncStorage.getItem('password'));

      setemail(await AsyncStorage.getItem('email'));

      setname(await AsyncStorage.getItem('name'));

      settoken(await AsyncStorage.getItem('token'));

      setuserId(await AsyncStorage.getItem('id'));
    } catch (e) {}
  };

  useEffect(() => {
    laaang = props.language;

    loadApp();
  }, []);
  // onNavigate(route) {
  //   this.props.navigation.dispatch(resetAction(route))
  // }
  //3
  const checkdayisopenday = (day) => {
    let weeksid = [];
    try {
      if (
        VendorDetailsReducer.payload.vendor.work_days === null ||
        VendorDetailsReducer.payload.vendor.work_days.length === 0 ||
        (VendorDetailsReducer.payload.vendor.work_days > 0 &&
          VendorDetailsReducer.payload.vendor.work_days[0] === 'undefined')
      ) {
        vendorisopen = true;
        dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 1));
      } else {
        for (
          let s = 0;
          s < VendorDetailsReducer.payload.vendor.work_days.length;
          s++
        ) {
          if (
            VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'الأحد'
          ) {
            weeksid.push(0);
          } else if (
            VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
            'الإثنين'
          ) {
            weeksid.push(1);
          } else if (
            VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
            'الثلاثاء'
          ) {
            weeksid.push(2);
          } else if (
            VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
            'الأربعاء'
          ) {
            weeksid.push(3);
          } else if (
            VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
            'الخميس'
          ) {
            weeksid.push(4);
          } else if (
            VendorDetailsReducer.payload.vendor.work_days[s].offdays ===
            'الجمعة'
          ) {
            weeksid.push(5);
          } else if (
            VendorDetailsReducer.payload.vendor.work_days[s].offdays === 'السبت'
          ) {
            weeksid.push(6);
          }
        }

        if (weeksid.includes(day)) {
          vendorisopen = false;
          dispatch(
            UpdateVendorStateAction(LoginReducer.payload.token.token, 3)
          );
        } else {
          vendorisopen = true;
          dispatch(
            UpdateVendorStateAction(LoginReducer.payload.token.token, 1)
          );
        }
      }
    } catch (e) {}
  };
  const getavilabilitystate = () => {
    var current = new Date();
    var c = current.getTime();
    var day = current.getDay();

    let from = VendorDetailsReducer.payload.vendor.work_hours[0].start;
    if(from.includes('صباحا'))
    {
      from= from.replace('صباحا','') 
     }
  
    if(from.includes('مساء'))
    {
       from=from.replace('مساء','') 
    }
    let to = VendorDetailsReducer.payload.vendor.work_hours[0].end;
    if(to.includes('صباحا'))
    {
       to=to.replace('صباحا','') 
    }

    if(to.includes('مساء'))
    {
     to= to.replace('مساء','') 
    }
    // let durationfrom = moment.duration(from, 'milliseconds');
    // let durationto = moment.duration(to, 'milliseconds');

    var now = moment.utc();
    var hour = now.hour();
    var min = now.minutes();

    let curremntmillic = moment.utc([2010, 1, 14, hour, min, 0, 0]).valueOf();

    if (curremntmillic > from && curremntmillic < to) {
      checkdayisopenday(day);
    } else {
      vendorisopen = false;

      dispatch(UpdateVendorStateAction(LoginReducer.payload.token.token, 0));
    }
  };

  return (
    <View style={styles.container}>
      <Image source={require('./imgs/profilebg.png')} style={styles.imgBg} />
      {((Platform.OS === 'android' && laaang === 'en') ||
        Platform.OS === 'ios') && (
        <View>
          <View style={styles.profile}>
            <View style={styles.colheader}>
              <Image
                source={require('./imgs/waddeely_op.png')}
                style={styles.profilePic}
                resizeMode={'contain'}
              />
            </View>

            <View style={styles.profileInfo}>
              <Text style={styles.name}> مرحبا {name}</Text>
              <Text style={styles.name}> </Text>
            </View>
          </View>
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.col}
              onPress={() => {
                getavilabilitystate();
                loadApp();

                NavigationService.navigate('HomeSeller', {
                  stateavailable: vendorisopen,
                  token: token,
                  id: userId,
                  currencyid: currencyid,
                  currency: currency,
                });
                props.drawer.current.close();
              }}>
              <Image source={require('./imgs/homeicon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>الرئيسية</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.col}
              onPress={() => {
                loadApp();
                NavigationService.navigate('OrdersScreen', {
                  currency: currency,
                  currencyid: currencyid,
                  userid: userId,
                  usertoken: token,
                });
                props.drawer.current.close();
              }}>
              <Image source={require('./imgs/ordersicon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>الطلبات</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.col}
              onPress={() => {
                loadApp();
                NavigationService.navigate('SellerProfile', {
                  token: token,
                  id: userId,
                  currencyid: currencyid,
                  currency: currency,
                  email: email,
                  password: password,
                  cityreducer: GetCountriesReducer.payload.data,
                });
                props.drawer.current.close();
              }}>
              <Image source={require('./imgs/profileicon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>الملف الشخصي</Text>
            </TouchableOpacity>
          </View>
          {/* <View style={{width:'100%'}}>

         <TouchableOpacity  style={styles.col}

            onPress={() => {
              // loadApp();
              NavigationService.navigate('PaymentScreen',{
              usertoken:token});
              props.drawer.current.close();
            }}>
            <Icon
              name={"user"}
              style={styles.icon}
            />
            <Text style={styles.menuTxt}> الحسابات</Text>
          </TouchableOpacity>
       
        </View> */}
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.col}
              onPress={() => {
                NavigationService.navigate('AboutStack');
                props.drawer.current.close();
              }}>
              <Image source={require('./imgs/abouticon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>عن التطبيق</Text>
            </TouchableOpacity>
          </View>

          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.col}
              onPress={() => {
                AsyncStorage.setItem('loggedin', 'false').then(() => {
                  AsyncStorage.setItem('email', 'null').then(() => {
                    AsyncStorage.setItem('password', 'null').then(() => {
                      AsyncStorage.setItem('token', 'null').then(() => {
                        AsyncStorage.setItem('id', 'null').then(() => {
                          AsyncStorage.setItem('currencyid', '1').then(() => {
                            AsyncStorage.setItem(
                              'currency',
                              'درهم اماراتي'
                            ).then(() => {
                              AsyncStorage.setItem('name', 'null').then(
                                () => {
                                 
                                  try{
                                  dispatch(ClearAction());
                                  dispatch(
                                    LogoutAction(
                                      token
                                    )
                                  );
                              
                                  props.navigation.navigate(
                                    'LoginScreen',
                                    {}
                                  );
                                  }
                                  catch(ee)
                                  {
                                    
                                  }
                                }
                              );
                            });
                          });
                        });
                      });
                    });
                  });
                });
                                // props.drawer.current.close();
              }}>
              <Image source={require('./imgs/logouticon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>تسجيل الخروج</Text>
            </TouchableOpacity>
          </View>
        
        </View>

        
      )}
      {Platform.OS === 'android' && laaang === 'ar' && (
        <View>
          <View style={styles.profile}>
            <View style={styles.colheaderar}>
              <Image
                source={require('./imgs/logoooo.png')}
                style={styles.profilePic}
                resizeMode={'stretch'}
              />
            </View>

            <View style={styles.profileInfo}>
              <Text style={styles.namear}> مرحبا {name}</Text>
              <Text style={styles.namear}> </Text>
            </View>
          </View>
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.colar}
              onPress={() => {
                getavilabilitystate();
                loadApp();

                NavigationService.navigate('HomeSeller', {
                  stateavailable: vendorisopen,
                  token: token,
                  id: userId,
                  currencyid: currencyid,
                  currency: currency,
                });
                props.drawer.current.close();
              }}>
              <Image source={require('./imgs/homeicon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>الرئيسية</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.colar}
              onPress={() => {
                loadApp();
                NavigationService.navigate('OrdersScreen', {
                  currency: currency,
                  currencyid: currencyid,
                  userid: userId,
                  usertoken: token,
                });
                props.drawer.current.close();
              }}>
              <Image source={require('./imgs/ordersicon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>الطلبات</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.colar}
              onPress={() => {
                loadApp();
                NavigationService.navigate('SellerProfile', {
                  token: token,
                  id: userId,
                  currencyid: currencyid,
                  currency: currency,
                  email: email,
                  password: password,
                  cityreducer: GetCountriesReducer.payload.data,
                });
                props.drawer.current.close();
              }}>
              <Image source={require('./imgs/profileicon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>الملف الشخصي</Text>
            </TouchableOpacity>
          </View>
          {/* <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.colar}
              onPress={() => {
                // loadApp();
                NavigationService.navigate('PaymentScreen', {
                  usertoken: token,
                });
                props.drawer.current.close();
              }}>
              <Icon name={'user'} style={styles.icon} />
              <Text style={styles.menuTxt}> الحسابات</Text>
            </TouchableOpacity>
          </View>
          */}
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.colar}
              onPress={() => {
                NavigationService.navigate('AboutStack');
                props.drawer.current.close();
              }}>
              <Image source={require('./imgs/abouticon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>عن التطبيق</Text>
            </TouchableOpacity>
          </View>
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              style={styles.col}
              onPress={() => {
                AsyncStorage.setItem('loggedin', 'false').then(() => {
                  AsyncStorage.setItem('email', 'null').then(() => {
                    AsyncStorage.setItem('password', 'null').then(() => {
                      AsyncStorage.setItem('token', 'null').then(() => {
                        AsyncStorage.setItem('id', 'null').then(() => {
                          AsyncStorage.setItem('currencyid', '5').then(() => {
                            AsyncStorage.setItem(
                              'currency',
                              'درهم اماراتي'
                            ).then(() => {
                              AsyncStorage.setItem('name', 'null').then(
                                () => {
                            
                                  try{
                                  dispatch(
                                    LogoutAction(
                                      token
                                    )
                                  );
                                  dispatch(ClearAction());
                                  props.navigation.navigate(
                                    'LoginScreen',
                                    {}
                                  
                                  );
                                  }
                                  catch(ee)
                                  {
                                    
                                  }
                                }
                              );
                            });
                          });
                        });
                      });
                    });
                  });
                });
                // props.drawer.current.close();
              }}>
              <Image source={require('./imgs/logouticon.png')}style={{tintColor:'white',width:30,height:30,marginHorizontal:5}} resizeMode='contain' />
              <Text style={styles.menuTxt}>تسجيل الخروج</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    width: width,
  },
  profile: {
    marginTop: 80,
    marginRight:
      Platform.OS === 'ios' || (Platform.OS === 'android' && laaang === 'en')
        ? 50
        : 50,
    marginBottom: 20,
  },

  imgWrapper: {
    width: height / 10,
    height: height / 10,
    borderWidth: 0,
    borderRadius: 120,
  },
  settings: {
    fontSize: 30,
    color: '#fff',
    marginLeft: 15,
  },
  profilePic: {
    width: height / 10,
    height: height / 10,
  },
  imgBg: {
    position: 'absolute',
    top: 0,
    left: 0,
    width,
    height,
  },
  icon: {
    fontSize: 45,
    color: '#fff',
  },
  col: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    padding: 10,
    width: '100%',
    borderTopWidth: 1,
    borderColor: '#fff',
  },
  colar: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    width: '100%',
    borderTopWidth: 1,
    borderColor: '#fff',
  },
  colheader: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    padding: 10,
    width: '100%',
    borderTopWidth: 0,
  },
  colheaderar: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    width: '100%',
    borderTopWidth: 0,
  },
  menuTxt: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'right',
  },
  profileInfo: {
    width: '100%',
    flexDirection: 'column',
    marginTop: 10,
  },
  name: {
    fontSize: 18,
    color: '#fff',
    marginRight: -40,

    textAlign:
      Platform.OS === 'ios' || (Platform.OS === 'android' && laaang === 'en')
        ? 'right'
        : 'left',
  },
  namear: {
    fontSize: 18,
    color: '#fff',
    marginRight: -40,

    textAlign: 'left',
  },
});
